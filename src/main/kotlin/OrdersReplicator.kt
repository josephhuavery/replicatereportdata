package com.avery.dify

import com.avery.dify.*
import com.github.davidmoten.rx.jdbc.Database
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.runBlocking
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit

/**
 * Created by jhu on 7/11/2017.
 */

class OrdersReplicator {
    companion object {

        @JvmStatic
        fun replicateNightly(
                srcDb : Database,
                destDb : Database) {

            val jobs = arrayListOf<Deferred<Unit>>()
            runBlocking {
                jobs += AveryRollConfiguration.replicateAllAsync(srcDb, destDb)
                jobs += AveryRollDie.replicateAllAsync(srcDb, destDb)
                jobs += AveryRollShape.replicateAllAsync(srcDb, destDb)
                jobs += AveryRollUnwind.replicateAllAsync(srcDb, destDb)

                // Note - might be redundant
                // AveryRollItem, AveryRollVersion, CatalogProductEntity are being replicated together with the orders
                jobs += AveryRollItem.replicateAllAsync(srcDb, destDb)
                jobs += AveryRollVersion.replicateAllAsync(srcDb, destDb)
                jobs += CatalogProductEntity.replicateAllAsync(srcDb, destDb)
                jobs += CatalogProductEntityInt.replicateAllAsync(srcDb, destDb)

                jobs += EavAttributeSet.replicateAllAsync(srcDb, destDb)
                jobs += SalesFlatShipment.replicateAllAsync(srcDb, destDb)

                jobs.forEach { it.await() }
            }
        }

        @JvmStatic
        fun replicateParallel(
                srcDb : Database,
                destDb : Database,
                offset : Int, timeunit: TimeUnit, batchSize : Int = 500) : String {

            assert(timeunit == TimeUnit.DAYS ||
                    timeunit == TimeUnit.HOURS ||
                    timeunit == TimeUnit.MINUTES ||
                    timeunit == TimeUnit.SECONDS ||
                    timeunit == TimeUnit.MICROSECONDS)

            val utc = ZonedDateTime.now(ZoneOffset.UTC)
            val now = utc.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
            val unit = timeunit.name.substring(0, timeunit.name.lastIndexOf('S'))

            //val additionalWhere = if (offset == 0) "" else  " where updated_at > '$now' - interval $offset $unit"
            //val additionalWhere = if (offset == 0) "" else  " where updated_at between '$now' - interval $offset $unit - interval 35 MINUTE and '$now' - interval 30 MINUTE"
            val additionalWhere = if (offset == 0) "" else
                """ where (sales_flat_order.updated_at between '$now' - interval $offset $unit - interval 35 MINUTE and '$now' - interval 30 MINUTE)
                       or (sales_flat_order.created_at between '$now' - interval $offset $unit - interval 35 MINUTE and '$now' - interval 30 MINUTE)
                       or (exists (select * from sales_flat_shipping_group sg where sg.order_id = sales_flat_order.entity_id
                            and (sg.updated_at > sg.created_at + interval 30 MINUTE)
                            and sg.updated_at between '$now' - interval $offset $unit - interval 5 MINUTE and '$now'))
                """.trimIndent()

            val additionalWhereNoOrder = if (offset == 0) "" else  " where updated_at between '$now' - interval $offset $unit - interval 35 MINUTE and '$now' - interval 30 MINUTE"

            val jobs = arrayListOf<Deferred<Unit>>()
            runBlocking {
                jobs += SalesFlatOrder.replicateAsync(srcDb, destDb, additionalWhere, batchSize)
                jobs += SalesFlatOrderItem.replicateAsync(srcDb, destDb, additionalWhere, batchSize)
                jobs += SalesFlatOrderAddress.replicateAsync(srcDb, destDb, additionalWhere, batchSize)
                jobs += SalesFlatOrderPayment.replicateAsync(srcDb, destDb, additionalWhere, batchSize)
                jobs += SalesFlatShippingGroup.replicateAsync(srcDb, destDb, additionalWhere, batchSize)
                // sales_flat_shipping_group_item is used by DW report
                jobs += SalesFlatShippingGroupItem.replicateAsync(srcDb, destDb, additionalWhere, batchSize)

                jobs += SalesFlatInvoice.replicateAsync(srcDb, destDb, additionalWhereNoOrder, batchSize)
                jobs += CustomerEntity.replicateAsync(srcDb, destDb, additionalWhereNoOrder, batchSize)

                jobs += SalesFlatCreditmemo.replicateAsync(srcDb, destDb, additionalWhereNoOrder, batchSize)
                jobs += SalesFlatCreditmemoItem.replicateAsync(srcDb, destDb, additionalWhereNoOrder, batchSize)
                jobs += SalesFlatCreditmemoComment.replicateAsync(srcDb, destDb, additionalWhereNoOrder, batchSize)

                jobs += AveryRollItem.replicateAsync(srcDb, destDb, additionalWhereNoOrder, batchSize)
                jobs += AveryRollVersion.replicateAsync(srcDb, destDb, additionalWhereNoOrder, batchSize)
                jobs += CatalogProductEntity.replicateAsync(srcDb, destDb, additionalWhereNoOrder, batchSize)

                jobs.forEach { it.await() }
            }
            // SalesFlatShippingGroup will not longer generate AveryOrderItemFreight at the same time
            SalesFlatShippingGroup.genAveryItemFreight(destDb, batchSize)
            return additionalWhere
        }

        @JvmStatic
        fun replicateParallel(
                srcDb : Database,
                destDb : Database,
                additionalWhere : String, batchSize : Int = 500) {

            val jobs = arrayListOf<Deferred<Unit>>()
            runBlocking {
                jobs += SalesFlatOrder.replicateAsync(srcDb, destDb, additionalWhere, batchSize)
                jobs += SalesFlatOrderItem.replicateAsync(srcDb, destDb, additionalWhere, batchSize)
                jobs += SalesFlatOrderAddress.replicateAsync(srcDb, destDb, additionalWhere, batchSize)
                jobs += SalesFlatOrderPayment.replicateAsync(srcDb, destDb, additionalWhere, batchSize)
                jobs += SalesFlatShippingGroup.replicateAsync(srcDb, destDb, additionalWhere, batchSize)
                // sales_flat_shipping_group_item is used by DW report
                jobs += SalesFlatShippingGroupItem.replicateAsync(srcDb, destDb, additionalWhere, batchSize)

                jobs += SalesFlatInvoice.replicateForOrderAsync(srcDb, destDb, additionalWhere, batchSize)
                jobs += CustomerEntity.replicateForOrderAsync(srcDb, destDb, additionalWhere, batchSize)

                jobs += SalesFlatCreditmemo.replicateForOrderAsync(srcDb, destDb, additionalWhere, batchSize)
                jobs += SalesFlatCreditmemoItem.replicateForOrderAsync(srcDb, destDb, additionalWhere, batchSize)
                jobs += SalesFlatCreditmemoComment.replicateForOrderAsync(srcDb, destDb, additionalWhere, batchSize)

                jobs.forEach { it.await() }
            }
            // SalesFlatShippingGroup will not longer generate AveryOrderItemFreight at the same time
            SalesFlatShippingGroup.genAveryItemFreight(destDb, batchSize)
        }
    }
}