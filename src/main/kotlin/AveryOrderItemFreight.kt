package com.avery.dify;

import java.sql.PreparedStatement

data class AveryOrderItemFreight  (
        val orderItemId: Long,
        val shippingAmount : Double,
        val shippingDiscountAmount : Double,
        val orderId : Long
) {
    companion object {
        val upsertJdbcSQL = """{call upsert_avery_order_item_freigh (
        ?,?,?,?
        )}
        """
    }
    fun upsertPstmt(pstmt : PreparedStatement) {
        pstmt.setLong(1, orderItemId)
        if (shippingAmount == null) {
            pstmt.setNull(2, java.sql.Types.DOUBLE)
        } else {
            pstmt.setDouble(2, shippingAmount.toDouble())
        }
        if (shippingDiscountAmount == null) {
            pstmt.setNull(3, java.sql.Types.DOUBLE)
        } else {
            pstmt.setDouble(3, shippingDiscountAmount.toDouble())
        }
        pstmt.setLong(4, orderId)
    }
}

/*********************************************************************

create or replace procedure upsert_avery_order_item_freigh (
    xorder_item_id IN avery_order_item_freight.order_item_id%TYPE,
    xshipping_amount IN avery_order_item_freight.shipping_amount%TYPE,
    xshipping_discount_amount IN avery_order_item_freight.shipping_discount_amount%TYPE,
    xorder_id IN avery_order_item_freight.order_id%TYPE
) as
x_record_count NUMBER;
begin
select count(*) into x_record_count from avery_order_item_freight where order_item_id = xorder_item_id;
if (x_record_count <= 0) then
begin
    insert into avery_order_item_freight (
        order_item_id,
        shipping_amount,
        shipping_discount_amount,
        order_id
    ) values (
        xorder_item_id,
        xshipping_amount,
        xshipping_discount_amount,
        xorder_id
    );
end;
else
begin
    update avery_order_item_freight set
        shipping_amount = xshipping_amount,
        shipping_discount_amount = xshipping_discount_amount,
        order_id = xorder_id,
        ins_timestamp = current_timestamp
    where order_item_id = xorder_item_id;
end;
end if;
end;

 *********************************************************************/
