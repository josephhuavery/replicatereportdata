package com.avery.dify;
// Generated Sep 14, 2017 3:57:17 PM by Hibernate Tools 5.2.3.Final

import com.github.davidmoten.rx.jdbc.Database
import kotlinx.coroutines.*
import org.slf4j.LoggerFactory
import rx.Observable
import java.util.Date
import java.math.BigDecimal
import java.math.BigInteger
import java.sql.PreparedStatement
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit



/**
 * CustomerEntity generated by hbm2kotlin version 2017-07-05
 */
data class CustomerEntity  (
        val entityId: Long,
        val entityTypeId: Int,
        val attributeSetId: Int,
        val websiteId: Int?,
        val email: String?,
        val groupId: Int,
        val incrementId: String?,
        val storeId: Int?,
        val createdAt: String,
        val updatedAt: String,
        val isActive: Int,
        val disableAutoGroupChange: Int
) {
    companion object {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss z")
        val selectSQL = """select
                entity_id,
                entity_type_id,
                attribute_set_id,
                website_id,
                email,
                group_id,
                increment_id,
                store_id,
                DATE_FORMAT(created_at,'%Y-%m-%d %T GMT') as created_at,
                DATE_FORMAT(updated_at,'%Y-%m-%d %T GMT') as updated_at,
                is_active,
                disable_auto_group_change
        from customer_entity
        """
        // **** Oracle column name limit to 30 chars. Use only 29 because of Hibernate
        val selectOracleSQL = """select
                entity_id,
                entity_type_id,
                attribute_set_id,
                website_id,
                email,
                group_id,
                increment_id,
                store_id,
                created_at,
                updated_at,
                is_active,
                disable_auto_group_change
        from customer_entity
        """
        val insertSQL = """insert into customer_entity (
                entity_id,
                entity_type_id,
                attribute_set_id,
                website_id,
                email,
                group_id,
                increment_id,
                store_id,
                created_at,
                updated_at,
                is_active,
                disable_auto_group_change
        ) values (
                :entityId,
                :entityTypeId,
                :attributeSetId,
                :websiteId,
                :email,
                :groupId,
                :incrementId,
                :storeId,
                :createdAt,
                :updatedAt,
                :isActive,
                :disableAutoGroupChange
        )
        """
        // **** Oracle column name limit to 30 chars. Use only 29 because of Hibernate
        val insertOracleSQL = """insert into customer_entity (
                entity_id,
                entity_type_id,
                attribute_set_id,
                website_id,
                email,
                group_id,
                increment_id,
                store_id,
                created_at,
                updated_at,
                is_active,
                disable_auto_group_change
        ) values (
                :entityId,
                :entityTypeId,
                :attributeSetId,
                :websiteId,
                :email,
                :groupId,
                :incrementId,
                :storeId,
                :createdAt,
                :updatedAt,
                :isActive,
                :disableAutoGroupChange
        )
        """
        val updateSQL = """update customer_entity set

                entity_type_id = :entityTypeId,
                attribute_set_id = :attributeSetId,
                website_id = :websiteId,
                email = :email,
                group_id = :groupId,
                increment_id = :incrementId,
                store_id = :storeId,
                created_at = :createdAt,
                updated_at = :updatedAt,
                is_active = :isActive,
                disable_auto_group_change = :disableAutoGroupChange
        where entity_id = :entityId
        """
        // **** Oracle column name limit to 30 chars. Use only 29 because of Hibernate
        val updateOracleSQL = """update customer_entity set

                entity_type_id = :entityTypeId,
                attribute_set_id = :attributeSetId,
                website_id = :websiteId,
                email = :email,
                group_id = :groupId,
                increment_id = :incrementId,
                store_id = :storeId,
                created_at = :createdAt,
                updated_at = :updatedAt,
                is_active = :isActive,
                disable_auto_group_change = :disableAutoGroupChange
        where entity_id = :entityId
        """
        val deleteSQL = """delete from customer_entity
        where entity_id = :entityId
        """
        val deleteAllSQL = """delete from customer_entity"""
        val upsertSQL = """{call upsert_customer_entity (
                :entityId,
                :entityTypeId,
                :attributeSetId,
                :websiteId,
                :email,
                :groupId,
                :incrementId,
                :storeId,
                :createdAt,
                :updatedAt,
                :isActive,
                :disableAutoGroupChange
        )}
        """
        val upsertJdbcSQL = """{call upsert_customer_entity (
        ?,?,?,?,?,?,?,?,?,?,?,?
        )}
        """

        fun select(db: Database, additionalWhere : String = "") : Observable<CustomerEntity> =
                db.select(selectSQL + additionalWhere)
                        // If SQL has additional where clause, e.g.
                        // where entity_id = :entityId
                        //.parameter("entityId", entityId)
                        .autoMap(CustomerEntity::class.java)

        fun deleteAll(db : Database) : Int =
                db.update(deleteAllSQL).execute()

        fun replicateAsync(srcDatabase : Database, destDatabase : Database, additionalWhere : String, batchSize : Int = 500)
                = GlobalScope.async { replicate(srcDatabase, destDatabase, additionalWhere, batchSize) }

        fun replicate(srcDatabase : Database, destDatabase : Database, additionalWhere : String, batchSize : Int = 500) {
            val logger = LoggerFactory.getLogger(CustomerEntity::class.java)

            logger.info("{} Start replicate CustomerEntity.", Date())

            val items = srcDatabase.select(selectSQL + additionalWhere)
                    .fetchSize(batchSize)
                    .autoMap(CustomerEntity::class.java)

            val destConn = destDatabase.connectionProvider.get()

            val pstmt = destConn.prepareStatement(upsertJdbcSQL)
            var count = 0

            items.subscribe {
                it.upsertPstmt(pstmt)
                pstmt.addBatch()
                count++
                if (count >= batchSize) {
                    pstmt.executeBatch()
                    pstmt.clearBatch()
                    count = 0
                }
            }
            if (count>0) {
                pstmt.executeBatch()
                pstmt.clearBatch()
            }

            logger.info("{} End replicate CustomerEntity.", Date())
        }

        fun replicateForOrderAsync(srcDatabase : Database, destDatabase : Database, additionalWhere : String, batchSize : Int = 500)
                = GlobalScope.async { replicateForOrder(srcDatabase, destDatabase, additionalWhere, batchSize) }

        fun replicateForOrder(srcDatabase : Database, destDatabase : Database, additionalWhere : String, batchSize : Int = 500) {
            val logger = LoggerFactory.getLogger(CustomerEntity::class.java)

            logger.info("{} Start replicate (for Orders) CustomerEntity.", Date())

            val items = srcDatabase.select(selectSQL +
                    " where entity_id in (select customer_id from sales_flat_order ${additionalWhere})")
                    .fetchSize(batchSize)
                    .autoMap(CustomerEntity::class.java)

            val destConn = destDatabase.connectionProvider.get()

            val pstmt = destConn.prepareStatement(upsertJdbcSQL)
            var count = 0

            items.subscribe {
                it.upsertPstmt(pstmt)
                pstmt.addBatch()
                count++
                if (count >= batchSize) {
                    pstmt.executeBatch()
                    pstmt.clearBatch()
                    count = 0
                }
            }
            if (count>0) {
                pstmt.executeBatch()
                pstmt.clearBatch()
            }

            logger.info("{} End replicate (for Orders) CustomerEntity.", Date())
        }

    }

    fun insert(db: Database) : Int =
            db.update(insertOracleSQL)
                    .parameter("entityId",entityId)
                    .parameter("entityTypeId",entityTypeId)
                    .parameter("attributeSetId",attributeSetId)
                    .parameter("websiteId",websiteId)
                    .parameter("email",email)
                    .parameter("groupId",groupId)
                    .parameter("incrementId",incrementId)
                    .parameter("storeId",storeId)
                    .parameter("createdAt",createdAt)
                    .parameter("updatedAt",updatedAt)
                    .parameter("isActive",isActive)
                    .parameter("disableAutoGroupChange",disableAutoGroupChange)
                    .execute()

    fun update(db: Database) : Int =
            db.update(updateOracleSQL)

                    .parameter("entityTypeId",entityTypeId)
                    .parameter("attributeSetId",attributeSetId)
                    .parameter("websiteId",websiteId)
                    .parameter("email",email)
                    .parameter("groupId",groupId)
                    .parameter("incrementId",incrementId)
                    .parameter("storeId",storeId)
                    .parameter("createdAt",createdAt)
                    .parameter("updatedAt",updatedAt)
                    .parameter("isActive",isActive)
                    .parameter("disableAutoGroupChange",disableAutoGroupChange)
                    .parameter("entityId",entityId)
                    .execute()

    fun delete(db: Database) : Int =
            db.update(deleteSQL)
                    .parameter("entityId", entityId)
                    .execute()

    fun upsert(db: Database) =
            db.update(upsertSQL)
                    .parameter("entityId",entityId)
                    .parameter("entityTypeId",entityTypeId)
                    .parameter("attributeSetId",attributeSetId)
                    .parameter("websiteId",websiteId)
                    .parameter("email",email)
                    .parameter("groupId",groupId)
                    .parameter("incrementId",incrementId)
                    .parameter("storeId",storeId)
                    .parameter("createdAt",createdAt)
                    .parameter("updatedAt",updatedAt)
                    .parameter("isActive",isActive)
                    .parameter("disableAutoGroupChange",disableAutoGroupChange)
                    .execute()

    fun upsertJdbc(db: Database) =
            db.update(upsertJdbcSQL)
                    .parameters(
                            entityId,
                            entityTypeId,
                            attributeSetId,
                            websiteId,
                            email,
                            groupId,
                            incrementId,
                            storeId,
                            createdAt,
                            updatedAt,
                            isActive,
                            disableAutoGroupChange
                    )
                    .execute()

    fun upsertPstmt(pstmt : PreparedStatement) {
        pstmt.setLong(1, entityId)
        pstmt.setInt(2, entityTypeId)
        pstmt.setInt(3, attributeSetId)
        if (websiteId == null) {
            pstmt.setNull(4, java.sql.Types.INTEGER)
        } else {
            pstmt.setInt(4, websiteId)
        }
        if (email == null) {
            pstmt.setNull(5, java.sql.Types.VARCHAR)
        } else {
            pstmt.setString(5, email)
        }
        pstmt.setInt(6, groupId)
        if (incrementId == null) {
            pstmt.setNull(7, java.sql.Types.VARCHAR)
        } else {
            pstmt.setString(7, incrementId)
        }
        if (storeId == null) {
            pstmt.setNull(8, java.sql.Types.INTEGER)
        } else {
            pstmt.setInt(8, storeId)
        }
        pstmt.setTimestamp(9, java.sql.Timestamp(dateFormat.parse(createdAt).getTime()))
        pstmt.setTimestamp(10, java.sql.Timestamp(dateFormat.parse(updatedAt).getTime()))
        pstmt.setInt(11, isActive)
        pstmt.setInt(12, disableAutoGroupChange)
    }
}

/*********************************************************************

create or replace view customer_entity_view as select
    entity_id,
    entity_type_id,
    attribute_set_id,
    website_id,
    email,
    group_id,
    increment_id,
    store_id,
    created_at,
    updated_at,
    is_active,
    disable_auto_group_change
from customer_entity

create or replace procedure upsert_customer_entity (
    xentity_id IN stg_customer_entity.entity_id%TYPE,
    xentity_type_id IN stg_customer_entity.entity_type_id%TYPE,
    xattribute_set_id IN stg_customer_entity.attribute_set_id%TYPE,
    xwebsite_id IN stg_customer_entity.website_id%TYPE,
    xemail IN stg_customer_entity.email%TYPE,
    xgroup_id IN stg_customer_entity.group_id%TYPE,
    xincrement_id IN stg_customer_entity.increment_id%TYPE,
    xstore_id IN stg_customer_entity.store_id%TYPE,
    xcreated_at IN stg_customer_entity.created_at%TYPE,
    xupdated_at IN stg_customer_entity.updated_at%TYPE,
    xis_active IN stg_customer_entity.is_active%TYPE,
    xdisable_auto_group_change IN stg_customer_entity.disable_auto_group_change%TYPE
) as
x_record_count NUMBER;
begin
    select count(*) into x_record_count from stg_customer_entity where entity_id = xentity_id;
    if (x_record_count <= 0) then
    begin
        insert into stg_customer_entity (
            entity_id,
            entity_type_id,
            attribute_set_id,
            website_id,
            email,
            group_id,
            increment_id,
            store_id,
            created_at,
            updated_at,
            is_active,
            disable_auto_group_change
        ) values (
            xentity_id,
            xentity_type_id,
            xattribute_set_id,
            xwebsite_id,
            xemail,
            xgroup_id,
            xincrement_id,
            xstore_id,
            xcreated_at,
            xupdated_at,
            xis_active,
            xdisable_auto_group_change
        );
    end;
    else
    begin
        -- ************ delete below if no updated_at ************
        select count(*) into x_record_count from stg_customer_entity where entity_id = xentity_id and updated_at = xupdated_at;
        if (x_record_count <= 0) then
        begin
        -- ************ delete above if no updated_at ************
            update stg_customer_entity set
                entity_type_id = xentity_type_id,
                attribute_set_id = xattribute_set_id,
                website_id = xwebsite_id,
                email = xemail,
                group_id = xgroup_id,
                increment_id = xincrement_id,
                store_id = xstore_id,
                created_at = xcreated_at,
                updated_at = xupdated_at,
                is_active = xis_active,
                disable_auto_group_change = xdisable_auto_group_change
            where entity_id = xentity_id;
        -- ************ delete below if no updated_at ************
        end;
        end if;
        -- ************ delete above if no updated_at ************
    end;
    end if;
end;

 *********************************************************************/

