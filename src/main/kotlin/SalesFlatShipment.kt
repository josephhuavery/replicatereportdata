package com.avery.dify;

import com.github.davidmoten.rx.jdbc.Database
import kotlinx.coroutines.*
import org.slf4j.LoggerFactory
import rx.Observable
import java.util.Date
import java.sql.PreparedStatement
import java.util.concurrent.TimeUnit



data class SalesFlatShipment  (
        val	entityId: Long,
        val	storeId: Long?,
        val	totalWeight: Long?,
        val	totalQty: Long?,
        val	emailSent: Long?,
        val	orderId: Long,
        val	customerId: Long?,
        val	shippingAddressId: Long?,
        val	billingAddressId: Long?,
        val	shipmentStatus: Long?,
        val	incrementId: String?,
        val	createdAt: Date?,
        val	updatedAt: Date?,
        val	packages: String?,
        val	shippingLabel: String?,
        val	contentType: String?
) {
    companion object {
        // JH
        val selectSQL = """select
                entity_id,
                store_id,
                total_weight,
                total_qty,
                email_sent,
                order_id,
                customer_id,
                shipping_address_id,
                billing_address_id,
                shipment_status,
                increment_id,
                created_at,
                updated_at,
                substring(packages,1,4000) as packages,
                null as shipping_label,
                content_type
        from sales_flat_shipment
        """

        val upsertJdbcSQL = """{call upsert_sales_flat_shipment (
        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?
        )}
        """

        val deleteAllSQL = """delete from sales_flat_shipment"""

        fun replicateAsync(srcDatabase : Database, destDatabase : Database, additionalWhere : String, batchSize : Int = 500)
                = GlobalScope.async { replicate(srcDatabase, destDatabase, additionalWhere, batchSize) }

        fun replicate(srcDatabase : Database, destDatabase : Database, additionalWhere : String, batchSize : Int = 500) {
            val logger = LoggerFactory.getLogger(SalesFlatShipment::class.java)

            logger.info("{} Start replicate SalesFlatShipment.", Date())

            val items = srcDatabase.select(selectSQL + additionalWhere)
                    .autoMap(SalesFlatShipment::class.java)

            val destConn = destDatabase.connectionProvider.get()

            val pstmt = destConn.prepareStatement(upsertJdbcSQL)
            var count = 0

            items.subscribe {
                it.upsertPstmt(pstmt)
                pstmt.addBatch()
                count++
                if (count >= batchSize) {
                    pstmt.executeBatch()
                    pstmt.clearBatch()
                    count = 0
                }
            }
            if (count>0) {
                pstmt.executeBatch()
                pstmt.clearBatch()
            }

            logger.info("{} End replicate SalesFlatShipment.", Date())
        }

        fun replicateAllAsync(srcDatabase : Database, destDatabase : Database)
                = GlobalScope.async { replicate(srcDatabase, destDatabase, 7, TimeUnit.DAYS) }

        fun replicate(srcDatabase : Database, destDatabase : Database, offset : Int, timeunit : TimeUnit, batchSize : Int = 500) {
            val logger = LoggerFactory.getLogger(SalesFlatShipment::class.java)

            logger.info("{} Start replicate (All) SalesFlatShipment.", Date())

            assert(timeunit == TimeUnit.DAYS ||
                    timeunit == TimeUnit.HOURS ||
                    timeunit == TimeUnit.MINUTES ||
                    timeunit == TimeUnit.SECONDS ||
                    timeunit == TimeUnit.MICROSECONDS)
            val unit = timeunit.name.substring(0, timeunit.name.lastIndexOf('S'))

            val additionalWhere = if (offset == 0) "" else  "where updated_at > now() - interval $offset $unit"

            val items = srcDatabase.select(selectSQL + additionalWhere)
                    .autoMap(SalesFlatShipment::class.java)

            val destConn = destDatabase.connectionProvider.get()

            if (offset == 0) {
                val pstmtDeleteAll = destConn.prepareStatement(deleteAllSQL)
                pstmtDeleteAll.execute()
            }

            val pstmt = destConn.prepareStatement(upsertJdbcSQL)
            var count = 0

            items.subscribe {
                it.upsertPstmt(pstmt)
                pstmt.addBatch()
                count++
                if (count >= batchSize) {
                    pstmt.executeBatch()
                    pstmt.clearBatch()
                    count = 0
                }
            }
            if (count>0) {
                pstmt.executeBatch()
                pstmt.clearBatch()
            }

            logger.info("{} End replicate (All) SalesFlatShipment.", Date())
        }
    }

    fun upsertPstmt(pstmt : PreparedStatement) {
        pstmt.setLong(1, entityId)
        if (storeId == null) {
            pstmt.setNull(2, java.sql.Types.BIGINT)
        } else {
            pstmt.setLong(2, storeId)
        }
        if (totalWeight == null) {
            pstmt.setNull(3, java.sql.Types.BIGINT)
        } else {
            pstmt.setLong(3, totalWeight)
        }
        if (totalQty == null) {
            pstmt.setNull(4, java.sql.Types.BIGINT)
        } else {
            pstmt.setLong(4, totalQty)
        }
        if (emailSent == null) {
            pstmt.setNull(5, java.sql.Types.BIGINT)
        } else {
            pstmt.setLong(5, emailSent)
        }
        pstmt.setLong(6, orderId)
        if (customerId == null) {
            pstmt.setNull(7, java.sql.Types.BIGINT)
        } else {
            pstmt.setLong(7, customerId)
        }
        if (shippingAddressId == null) {
            pstmt.setNull(8, java.sql.Types.BIGINT)
        } else {
            pstmt.setLong(8, shippingAddressId)
        }
        if (billingAddressId == null) {
            pstmt.setNull(9, java.sql.Types.BIGINT)
        } else {
            pstmt.setLong(9, billingAddressId)
        }
        if (shipmentStatus == null) {
            pstmt.setNull(10, java.sql.Types.BIGINT)
        } else {
            pstmt.setLong(10, shipmentStatus)
        }
        if (incrementId == null) {
            pstmt.setNull(11, java.sql.Types.VARCHAR)
        } else {
            pstmt.setString(11, incrementId)
        }
        if (createdAt == null) {
            pstmt.setNull(12, java.sql.Types.DATE)
        } else {
            pstmt.setTimestamp(12, java.sql.Timestamp(createdAt.getTime()))
        }
        if (updatedAt == null) {
            pstmt.setNull(13, java.sql.Types.DATE)
        } else {
            pstmt.setTimestamp(13, java.sql.Timestamp(updatedAt.getTime()))
        }
        if (packages == null) {
            pstmt.setNull(14, java.sql.Types.VARCHAR)
        } else {
            pstmt.setString(14, packages)
        }
        pstmt.setNull(15, java.sql.Types.VARCHAR)
        if (contentType == null) {
            pstmt.setNull(16, java.sql.Types.VARCHAR)
        } else {
            pstmt.setString(16, contentType)
        }
    }
}

/*********************************************************************

create or replace procedure upsert_sales_flat_shipment (
xentity_id IN sales_flat_shipment.entity_id%TYPE,
xstore_id IN sales_flat_shipment.store_id%TYPE,
xtotal_weight IN sales_flat_shipment.total_weight%TYPE,
xtotal_qty IN sales_flat_shipment.total_qty%TYPE,
xemail_sent IN sales_flat_shipment.email_sent%TYPE,
xorder_id IN sales_flat_shipment.order_id%TYPE,
xcustomer_id IN sales_flat_shipment.customer_id%TYPE,
xshipping_address_id IN sales_flat_shipment.shipping_address_id%TYPE,
xbilling_address_id IN sales_flat_shipment.billing_address_id%TYPE,
xshipment_status IN sales_flat_shipment.shipment_status%TYPE,
xincrement_id IN sales_flat_shipment.increment_id%TYPE,
xcreated_at IN sales_flat_shipment.created_at%TYPE,
xupdated_at IN sales_flat_shipment.updated_at%TYPE,
xpackages IN sales_flat_shipment.packages%TYPE,
xshipping_label IN sales_flat_shipment.shipping_label%TYPE,
xcontent_type IN sales_flat_shipment.content_type%TYPE
) as
x_record_count NUMBER;
begin
select count(*) into x_record_count from sales_flat_shipment where entity_id = xentity_id;
if (x_record_count <= 0) then
begin
insert into sales_flat_shipment (
entity_id,
store_id,
total_weight,
total_qty,
email_sent,
order_id,
customer_id,
shipping_address_id,
billing_address_id,
shipment_status,
increment_id,
created_at,
updated_at,
packages,
shipping_label,
content_type
) values (
xentity_id,
xstore_id,
xtotal_weight,
xtotal_qty,
xemail_sent,
xorder_id,
xcustomer_id,
xshipping_address_id,
xbilling_address_id,
xshipment_status,
xincrement_id,
xcreated_at,
xupdated_at,
xpackages,
xshipping_label,
xcontent_type
);
end;
else
begin
-- ************ delete below if no updated_at ************
select count(*) into x_record_count from sales_flat_shipment where entity_id = xentity_id and updated_at = xupdated_at;
if (x_record_count <= 0) then
begin
-- ************ delete above if no updated_at ************
update sales_flat_shipment set

store_id = xstore_id,
total_weight = xtotal_weight,
total_qty = xtotal_qty,
email_sent = xemail_sent,
order_id = xorder_id,
customer_id = xcustomer_id,
shipping_address_id = xshipping_address_id,
billing_address_id = xbilling_address_id,
shipment_status = xshipment_status,
increment_id = xincrement_id,
created_at = xcreated_at,
updated_at = xupdated_at,
packages = xpackages,
shipping_label = xshipping_label,
content_type = xcontent_type
where entity_id = xentity_id;
-- ************ delete below if no updated_at ************
end;
end if;
-- ************ delete above if no updated_at ************
end;
end if;
end;

 *********************************************************************/

