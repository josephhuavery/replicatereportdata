package export


import com.avery.dify.export.GloviaOrder
import com.github.davidmoten.rx.jdbc.Database
import rx.Observable
import java.util.Date
import java.math.BigDecimal
import java.math.BigInteger
import java.sql.PreparedStatement
import java.util.concurrent.TimeUnit

data class SalesFlatShippingGroup  (
        val entityId: BigInteger?,
        val orderId: BigInteger?,
        val type: String?,
        val name: String?,
        val shippingMethod: String?,
        val shippingDescription: String?,
        val shippedDethod: String?,
        val shippingAmount: BigDecimal?,
        val subsidizedShippingAmount: BigDecimal?,
        var baseShippingAmount: BigDecimal?,
        val baseSubsidizedShippingAmt: BigDecimal?,
        val upsYellowbagTrackNumber: String?,
        val productionDueDate: Date?,
        val actualDueDate: Date?,
        val orderEstimated_delivery_date: Date?,
        val orderEstDeliveryDateTo: Date?,
        val shippingEstDeliveryDate: Date?,
        val shippingEstDeliveryDateTo: Date?,
        val parentId: BigInteger?,
        val shippingTaxAmount: BigDecimal?,
        var baseShippingTaxAmount: BigDecimal?,
        val shippingDiscountAmount: BigDecimal?,
        var baseShippingDiscountAmount: BigDecimal?,
        val shippingHiddenTaxAmount: BigDecimal?,
        val baseShippingHiddenTaxAmt: BigDecimal?,
        val shippingInclTax: BigDecimal?,
        val baseShippingInclTax: BigDecimal?
) {
    companion object {
        val selectSQL = """select
                            entity_id,
                            order_id,
                            type,
                            name,
                            shipping_method,
                            shipping_description,
                            shipped_method,
                            shipping_amount,
                            subsidized_shipping_amount,
                            base_shipping_amount,
                            base_subsidized_shipping_amt,
                            ups_yellowbag_track_number,
                            production_due_date,
                            actual_due_date,
                            order_estimated_delivery_date,
                            order_est_delivery_date_to,
                            shipping_est_delivery_date,
                            shipping_est_delivery_date_to,
                            parent_id,
                            shipping_tax_amount,
                            base_shipping_tax_amount,
                            shipping_discount_amount,
                            base_shipping_discount_amount,
                            shipping_hidden_tax_amount,
                            base_shipping_hidden_tax_amt,
                            shipping_incl_tax,
                            base_shipping_incl_tax
                from sales_flat_shipping_group
        """


        fun select(db: Database, additionalWhere : String = "") : List<SalesFlatShippingGroup> =
                db.select( selectSQL + additionalWhere)
                        .autoMap(SalesFlatShippingGroup::class.java)
                        .toList().toBlocking().single()


    }


}