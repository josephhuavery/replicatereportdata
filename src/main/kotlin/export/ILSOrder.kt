/**
 * Created by kkim on 7/14/2017.
 */
package com.avery.dify.export;


import com.github.davidmoten.rx.jdbc.Database
import org.apache.xpath.operations.Bool
import rx.Observable
import java.util.Date
import java.math.BigDecimal
import java.math.BigInteger
import java.sql.PreparedStatement
import java.util.concurrent.TimeUnit
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ibm.disthubmq.impl.formats.Envelop.payload.error
import java.awt.SystemColor.info
import com.avery.dify.util.DIFYProperty
import com.avery.dify.util.StringUtils
import java.sql.ResultSet
import java.sql.Statement
import java.util.HashMap
import java.util.ArrayList
import com.avery.dify.util.DIFYProperty.AVYPRO_STORE_ID
import com.avery.dify.util.DIFYProperty.DIFY_STORE_ID
import com.avery.dify.xmlmodel.ILSOrderAckHeader
import com.avery.dify.xmlmodel.ILSOrderAck
import java.sql.Timestamp


data class ILSOrder  (
        var isRetailOrder: Boolean,
        var isServiceOrder: Boolean,
        var isOnlineAisleOrder: Boolean,
        var isEtsyOrder: Boolean,
        var isEStoreOrder: Boolean,
        var orderId: BigInteger?,
        var internalPartnerID: String?,
        var companyCode: String?,
        var customerPONumber: String?,
        var aSBOrderNumber: String?,
        var creditMemoNumber: String?,
        var customerAccountNumber: String?,
        var dateOrderReceived: Date?,
        var asbPartnerID: String?,
        var numofLinesonPO: BigInteger?,
        var orderSourceCode: String?,
        var poDate: Date?,
        var poOrderType: String?,
        var storeNum: String?,
        var carrierCode: String?,
        var carrierName: String?,
        var carrierTrackingNum: String?,
        var shipDate: Date?,
        var subtotal: BigDecimal?,
        var shipping: BigDecimal?,
        var shippingDiscount: BigDecimal?,
        var discount: BigDecimal?,
        var tax: BigDecimal?,
        var total: BigDecimal?,
        var subtotalRefunded: BigDecimal?,
        var shippingRefunded: BigDecimal?,
        var shippingDiscountRefunded: BigDecimal?,
        var discountRefunded: BigDecimal?,
        var taxRefunded: BigDecimal?,
        var totalRefunded: BigDecimal?,
        var adjustment: BigDecimal?,
        var isCredit: Boolean?,
        var isDuplicate: Boolean? = false,
        var ilsCredited: Boolean? = false,
        var billToName: String?,
        var billToAddress1: String?,
        var billToAddress2: String?,
        var billToAddress3: String?,
        var billToCity: String?,
        var billToState: String?,
        var billToZip: String?,
        var billToCountry: String?,
        var billToCompanyName: String?,
        var customerName: String?,
        var customerAddress1: String?,
        var customerAddress2: String?,
        var customerAddress3: String?,
        var customerCity: String?,
        var customerState: String?,
        var customerZip: String?,
        var customerCountry: String?,
        var shipCompanyName: String?,
        var adjustmentNegative: BigDecimal?,
        val orderDetails: MutableList<ILSOrderDetail>?,
        val specialCharges: MutableList<ILSOrderSpecialCharge>?,
        var isTerm: Boolean,
        val originStoreID: BigInteger,
        val shippingMethod: String?,
        val baseShippingTaxAmount: BigDecimal?,
        val pbaseTaxAmount: BigDecimal?,
        val customerID: BigInteger?,
        val customerEmail: String?,
        val etsyId: String?,
        val eCostNumber: String?,
        val channelAdvisor_Order_Id: BigInteger?,
        val marketPlace_Site_Id: BigInteger?,
        val marketPlace_Order_Id: String?,
        val storeCreditAmount: BigDecimal?
) {
    companion object {

        val selectAvyPro = """select sfo.increment_id, sfo.entity_id, sfo.shipping_amount freight, sfo.shipping_discount_amount freight_discount,
		  sfo.discount_amount promotion, sfo.tax_amount tax, sfo.CREATED_AT_PST , sfo.total_item_count, sfo.subtotal, sfo.grand_total,
		  sfo.usps_track_number, sfoi.sku, sfoi.name, sfoi.item_id, sfoi.qty_ordered, sfoi.price, sfoi.row_total,
		  sfoa.firstname || ' ' || sfoa.lastname custname, sfoa.city, sfoa.region, sfoa.postcode, c.iso3_code country,
		  regexp_substr(sfoa.street, '[[:print:]]+') as address_line1,
          regexp_substr(sfoa.street, '[[:cntrl:]]+([[:print:]]+)',1,1,null,1) as address_line2,
          sfoa.company as address_line3,
          sfoab.firstname || ' ' || sfoab.lastname billto_custname, sfoab.city billto_city,
          regexp_substr(sfoab.street, '[[:print:]]+') as billto_address_line1,
          regexp_substr(sfoab.street, '[[:cntrl:]]+([[:print:]]+)',1,1,null,1) as billto_address_line2,
          sfoab.company as billto_address_line3,
		  sfoab.region billto_state, sfoab.postcode billto_postcode, cb.iso3_code billto_country,
		  row_number() over (partition by sfo.increment_id order by sfo.increment_id, sfoi.item_id) order_line,
		  sfo.total_refunded, sfo.tax_refunded total_tax_refunded, sfo.discount_refunded total_discount_refunded, sfo.shipping_refunded,
		  sfoi.amount_refunded, sfoi.tax_refunded, sfoi.tax_amount item_tax_amount, sfoi.discount_refunded, sfo.store_id, sfp.po_number,
		  nvl(sm.ilscarnam, 'UPS') ILS_Carrier_Name, nvl(sm.ilscar,'UPSG') ILS_Carrier_Code, sfo.product_group, sfo.shipping_method,
		  sfoi.base_tax_amount, sfoi.base_discount_amount,  sfo.base_shipping_tax_amount, sfo.base_tax_amount p_base_tax_amount
		from stg_sales_flat_order sfo
		  join stg_sales_flat_order_item sfoi on sfo.entity_id = sfoi.order_id
		  join stg_sales_flat_order_address sfoa on sfo.entity_id = sfoa.parent_id and sfoa.address_type = 'shipping'
		  join directory_country c on c.country_id = sfoa.country_id
		  join stg_sales_flat_order_address sfoab on sfo.entity_id = sfoab.parent_id and sfoab.address_type = 'billing'
		  left join directory_country cb on cb.country_id = sfoab.country_id
		  left join sales_flat_order_payment sfp on sfo.entity_id = sfp.parent_id and sfp.method='purchaseorder'
          left join avery_magento_ils_ship_mapping sm on sfo.shipping_method = sm.ship_code and sm.use_for_ils = 'Y'
		where (lower(sfo.coupon_code) not in ('gorilla007') or sfo.coupon_code is null)
		 and sfo.state not in ( 'canceled' , 'closed' , 'complete' , 'holded' , 'payment_review' )
        """

        val selectRetail = """SELECT main.*, nvl(sfsg.sum_base_shipping_amount,0) sum_base_shipping_amount,
			   nvl(sfsg.sum_base_ship_dicnt_amt,0) sum_base_ship_dicnt_amt,
			   nvl(sfsg.sum_base_shipping_tax_amount,0) sum_base_shipping_tax_amount,
			   CASE WHEN order_line = 1 THEN
			   	CASE WHEN main.shipping_method = 'split_group' THEN nvl(main.item_tax_amount, 0) + nvl(sfsg.sum_base_shipping_tax_amount, 0) ELSE nvl(main.item_tax_amount, 0) + nvl(main.base_shipping_tax_amount, 0) END
			   ELSE 0 END as cal_item_tax_amount,
			   sfsg.production_due_date, sfsg.warehouse,
			   nvl(sm.ilscarnam, 'UPS') ILS_Carrier_Name, nvl(sm.ilscar,'UPSG') ILS_Carrier_Code
		FROM
				(select sfo.increment_id, sfo.entity_id, sfo.shipping_amount freight, sfo.shipping_discount_amount freight_discount,
				  sfo.discount_amount promotion, sfo.tax_amount tax, sfo.CREATED_AT_PST , sfo.total_item_count, sfo.subtotal, sfo.grand_total,
				  sfo.usps_track_number, sfoi.sku, sfoi.name, sfoi.item_id, sfoi.qty_ordered, sfoi.price, sfoi.row_total,
				  sfoa.firstname || ' ' || sfoa.lastname custname, sfoa.city, sfoa.region, sfoa.postcode, c.iso3_code country,
				  regexp_substr(sfoa.street, '[[:print:]]+') as address_line1,
                  regexp_substr(sfoa.street, '[[:cntrl:]]+([[:print:]]+)',1,1,null,1) as address_line2,
                  sfoa.company as address_line3,
                  sfoab.firstname || ' ' || sfoab.lastname billto_custname, sfoab.city billto_city,
                  regexp_substr(sfoab.street, '[[:print:]]+') as billto_address_line1,
                  regexp_substr(sfoab.street, '[[:cntrl:]]+([[:print:]]+)',1,1,null,1) as billto_address_line2,
                  sfoab.company as billto_address_line3,
				  sfoab.region billto_state, sfoab.postcode billto_postcode, cb.iso3_code billto_country,
				  row_number() over (partition by sfo.increment_id order by sfo.increment_id, sfoi.item_id) order_line,
				  sfo.total_refunded, sfo.tax_refunded total_tax_refunded, sfo.discount_refunded total_discount_refunded, sfo.shipping_refunded,
				  sfoi.amount_refunded, sfoi.tax_refunded, sfoi.tax_amount item_tax_amount, sfoi.discount_refunded, sfo.store_id, sfp.po_number,
				  sfo.product_group, sfo.shipping_method,
				  sfoi.base_tax_amount, sfoi.base_discount_amount, sfo.base_shipping_tax_amount, sfo.base_tax_amount p_base_tax_amount,
                  sfo.base_customer_balance_amount
				from stg_sales_flat_order sfo
				  join stg_sales_flat_order_item sfoi on sfo.entity_id = sfoi.order_id
				  join stg_sales_flat_order_address sfoa on sfo.entity_id = sfoa.parent_id and sfoa.address_type = 'shipping'
				  join sku_product_type spt on spt.product_type = 'Retail' and sfoi.sku = spt.sku
				  join directory_country c on c.country_id = sfoa.country_id
				  join stg_sales_flat_order_address sfoab on sfo.entity_id = sfoab.parent_id and sfoab.address_type = 'billing'
				  left join directory_country cb on cb.country_id = sfoab.country_id
				  left join sales_flat_order_payment sfp on sfo.entity_id = sfp.parent_id and sfp.method='purchaseorder'
		        where (lower(sfo.coupon_code) not in ('gorilla007') or sfo.coupon_code is null)
                      and sfo.state not in ( 'canceled' , 'closed' , 'complete' , 'holded' , 'payment_review' )
                      and sfo.entity_id in (<entity_ids>)  ) main
		join (select sum(nvl(base_shipping_amount, 0)) sum_base_shipping_amount, sum(nvl(base_shipping_discount_amount, 0)) sum_base_ship_dicnt_amt, sum(nvl(base_shipping_tax_amount, 0)) + sum(nvl(base_shipping_hidden_tax_amt, 0)) sum_base_shipping_tax_amount, order_id, production_due_date, warehouse, shipping_method 
				from sales_flat_shipping_group
				where type = 'retail' AND ORDER_ID IN (<entity_ids>) group by order_id, production_due_date, warehouse, shipping_method) sfsg ON main.entity_id = sfsg.order_id
		left join avery_magento_ils_ship_mapping sm on sfsg.shipping_method = sm.ship_code and sm.use_for_ils = 'Y'
		order by increment_id, item_id
        """

        val selectEStore = """SELECT main.*, nvl(sfsg.sum_base_shipping_amount,0) sum_base_shipping_amount,
			   nvl(sfsg.sum_base_ship_dicnt_amt,0) sum_base_ship_dicnt_amt,
			   nvl(sfsg.sum_base_shipping_tax_amount,0) sum_base_shipping_tax_amount,
			   CASE WHEN order_line = 1 THEN
			   	CASE WHEN main.shipping_method = 'split_group' THEN nvl(main.item_tax_amount, 0) + nvl(sfsg.sum_base_shipping_tax_amount, 0) ELSE nvl(main.item_tax_amount, 0) + nvl(main.base_shipping_tax_amount, 0) END
			   ELSE 0 END as cal_item_tax_amount,
			   sfsg.production_due_date, sfsg.warehouse,
			   nvl(sm.ilscarnam, 'UPS') ILS_Carrier_Name, nvl(sm.ilscar,'UPSG') ILS_Carrier_Code
		FROM
				(select sfo.increment_id, sfo.entity_id, sfo.shipping_amount freight, sfo.shipping_discount_amount freight_discount,
				  sfo.discount_amount promotion, sfo.tax_amount tax, sfo.CREATED_AT_PST , sfo.total_item_count, sfo.subtotal, sfo.grand_total,
				  sfo.usps_track_number, sfoi.sku, sfoi.name, sfoi.item_id, sfoi.qty_ordered, sfoi.price, sfoi.row_total,
				  sfoa.firstname || ' ' || sfoa.lastname custname, sfoa.city, sfoa.region, sfoa.postcode, c.iso3_code country,
				  regexp_substr(sfoa.street, '[[:print:]]+') as address_line1,
                  regexp_substr(sfoa.street, '[[:cntrl:]]+([[:print:]]+)',1,1,null,1) as address_line2,
                  sfoa.company as address_line3,
                  sfoab.firstname || ' ' || sfoab.lastname billto_custname, sfoab.city billto_city,
                  regexp_substr(sfoab.street, '[[:print:]]+') as billto_address_line1,
                  regexp_substr(sfoab.street, '[[:cntrl:]]+([[:print:]]+)',1,1,null,1) as billto_address_line2,
                  sfoab.company as billto_address_line3,
				  sfoab.region billto_state, sfoab.postcode billto_postcode, cb.iso3_code billto_country,
				  row_number() over (partition by sfo.increment_id order by sfo.increment_id, sfoi.item_id) order_line,
				  sfo.total_refunded, sfo.tax_refunded total_tax_refunded, sfo.discount_refunded total_discount_refunded, sfo.shipping_refunded,
				  sfoi.amount_refunded, sfoi.tax_refunded, sfoi.tax_amount item_tax_amount, sfoi.discount_refunded, sfo.store_id, sfp.po_number,
				  sfo.product_group, sfo.shipping_method,
				  sfoi.base_tax_amount, sfoi.base_discount_amount, sfo.base_shipping_tax_amount, sfo.base_tax_amount p_base_tax_amount,
				  sfp.ecost_number, sfo.base_customer_balance_amount
				from stg_sales_flat_order sfo
				  join stg_sales_flat_order_item sfoi on sfo.entity_id = sfoi.order_id
				  join stg_sales_flat_order_address sfoa on sfo.entity_id = sfoa.parent_id and sfoa.address_type = 'shipping'
				  join sku_product_type spt on spt.product_type = 'Estore' and sfoi.sku = spt.sku
                  join sales_flat_order_payment sfp on sfo.entity_id = sfp.parent_id and sfp.method='ecost'
				  join directory_country c on c.country_id = sfoa.country_id
				  join stg_sales_flat_order_address sfoab on sfo.entity_id = sfoab.parent_id and sfoab.address_type = 'billing'
				  left join directory_country cb on cb.country_id = sfoab.country_id
				  left join sales_flat_order_payment sfp on sfo.entity_id = sfp.parent_id and sfp.method='purchaseorder'
		          left join avery_magento_ils_ship_mapping sm on sfo.shipping_method = sm.ship_code and sm.use_for_ils = 'Y'
				where (lower(sfo.coupon_code) not in ('gorilla007') or sfo.coupon_code is null)
                      and sfo.state not in ( 'canceled' , 'closed' , 'complete' , 'holded' , 'payment_review' )
                      and sfo.entity_id in (<entity_ids>)  ) main
		join (select sum(nvl(base_shipping_amount, 0)) sum_base_shipping_amount, sum(nvl(base_shipping_discount_amount, 0)) sum_base_ship_dicnt_amt, sum(nvl(base_shipping_tax_amount, 0)) + sum(nvl(base_shipping_hidden_tax_amt, 0)) sum_base_shipping_tax_amount, order_id, production_due_date, warehouse, shipping_method 
				from sales_flat_shipping_group
				where type = 'estore' AND ORDER_ID IN (<entity_ids>) group by order_id, production_due_date, warehouse, shipping_method) sfsg ON main.entity_id = sfsg.order_id
		left join avery_magento_ils_ship_mapping sm on sfsg.shipping_method = sm.ship_code and sm.use_for_ils = 'Y'
		order by increment_id, item_id
        """

        val selectService = """SELECT main.*, 0 sum_base_shipping_amount, 0 sum_base_ship_dicnt_amt, 0 sum_base_shipping_tax_amount,
			   CASE WHEN order_line = 1 THEN
			   	CASE WHEN main.shipping_method = 'split_group' THEN nvl(main.item_tax_amount, 0) ELSE nvl(main.item_tax_amount, 0) + nvl(main.base_shipping_tax_amount, 0) END
			   ELSE 0 END as cal_item_tax_amount
		FROM
				(select sfo.increment_id, sfo.entity_id, sfo.shipping_amount freight, sfo.shipping_discount_amount freight_discount,
				  sfo.discount_amount promotion, sfo.tax_amount tax, sfo.CREATED_AT_PST , sfo.total_item_count, sfo.subtotal, sfo.grand_total,
				  sfo.usps_track_number, sfoi.sku, sfoi.name, sfoi.item_id, sfoi.qty_ordered, sfoi.price, sfoi.row_total,
				  sfoa.firstname || ' ' || sfoa.lastname custname, sfoa.city, sfoa.region, sfoa.postcode, sfoa.iso3_code country,
				  regexp_substr(sfoa.street, '[[:print:]]+') as address_line1,
                  regexp_substr(sfoa.street, '[[:cntrl:]]+([[:print:]]+)',1,1,null,1) as address_line2,
                  sfoa.company as address_line3,
                  sfoab.firstname || ' ' || sfoab.lastname billto_custname, sfoab.city billto_city,
                  regexp_substr(sfoab.street, '[[:print:]]+') as billto_address_line1,
                  regexp_substr(sfoab.street, '[[:cntrl:]]+([[:print:]]+)',1,1,null,1) as billto_address_line2,
                  sfoab.company as billto_address_line3,
				  sfoab.region billto_state, sfoab.postcode billto_postcode, cb.iso3_code billto_country,
				  row_number() over (partition by sfo.increment_id order by sfo.increment_id, sfoi.item_id) order_line,
				  sfo.total_refunded, sfo.tax_refunded total_tax_refunded, sfo.discount_refunded total_discount_refunded, sfo.shipping_refunded,
				  sfoi.amount_refunded, sfoi.tax_refunded, sfoi.tax_amount item_tax_amount, sfoi.discount_refunded, sfo.store_id, sfp.po_number,
				  nvl(sm.ilscarnam, 'UPS') ILS_Carrier_Name, nvl(sm.ilscar,'UPSG') ILS_Carrier_Code, sfo.product_group, sfo.shipping_method,
				  sfoi.base_tax_amount, sfoi.base_discount_amount, sfo.base_shipping_tax_amount, sfo.base_tax_amount p_base_tax_amount, sfsg.WAREHOUSE, sfo.base_customer_balance_amount
				from stg_sales_flat_order sfo
				  join stg_sales_flat_order_item sfoi on sfo.entity_id = sfoi.order_id
				  join sku_product_type spt on spt.product_type = 'Service' and sfoi.sku = spt.sku
                  join (SELECT main.*, sub.ISO3_CODE,  row_number() over (PARTITION BY PARENT_ID order by ADDRESS_TYPE desc) as seqnum 
						FROM stg_sales_flat_order_address main JOIN directory_country sub ON sub.country_id = main.country_id) sfoa 
				  		on sfo.entity_id = sfoa.parent_id AND sfoa.seqnum = 1
                  join stg_sales_flat_order_address sfoab on sfo.entity_id = sfoab.parent_id and sfoab.address_type = 'billing'
                  join directory_country cb on cb.country_id = sfoab.country_id
            	  join sales_flat_shipping_group sfsg ON sfo.entity_id = sfsg.order_id AND sfsg.TYPE = 'service' 
				  left join sales_flat_order_payment sfp on sfo.entity_id = sfp.parent_id and sfp.method='purchaseorder'
		          left join avery_magento_ils_ship_mapping sm on sfo.shipping_method = sm.ship_code and sm.use_for_ils = 'Y'
				where (lower(sfo.coupon_code) not in ('gorilla007') or sfo.coupon_code is null)
                      and sfo.state not in ( 'canceled' , 'closed' , 'complete' , 'holded' , 'payment_review' )
                      and sfo.entity_id in (<entity_ids>)  ) main
		order by increment_id, item_id
        """

        val selectFund = """SELECT main.*, nvl(sfsg.sum_base_shipping_amount,0) sum_base_shipping_amount,
			   nvl(sfsg.sum_base_ship_dicnt_amt,0) sum_base_ship_dicnt_amt,
			   nvl(sfsg.sum_base_shipping_tax_amount,0) sum_base_shipping_tax_amount,
			   CASE WHEN order_line = 1 THEN
			   	CASE WHEN main.shipping_method = 'split_group' THEN nvl(main.item_tax_amount, 0) + nvl(sfsg.sum_base_shipping_tax_amount, 0) ELSE nvl(main.item_tax_amount, 0) + nvl(main.base_shipping_tax_amount, 0) END
			   ELSE 0 END as cal_item_tax_amount, sfsg.warehouse
		FROM
				(select sfo.increment_id, sfo.entity_id, sfo.shipping_amount freight, sfo.shipping_discount_amount freight_discount,
				  sfo.discount_amount promotion, sfo.tax_amount tax, sfo.CREATED_AT_PST , sfo.total_item_count, sfo.subtotal, sfo.grand_total,
				  sfo.usps_track_number, sfoi.sku, sfoi.name, sfoi.item_id, DECODE(spt.product_type, 'Roll', 1, sfoi.qty_ordered) qty_ordered, sfoi.price, sfoi.row_total,
				  sfoa.firstname || ' ' || sfoa.lastname custname, sfoa.city, sfoa.region, sfoa.postcode, c.iso3_code country,
				  regexp_substr(sfoa.street, '[[:print:]]+') as address_line1,
                  regexp_substr(sfoa.street, '[[:cntrl:]]+([[:print:]]+)',1,1,null,1) as address_line2,
                  sfoa.company as address_line3,
                  sfoab.firstname || ' ' || sfoab.lastname billto_custname, sfoab.city billto_city,
                  regexp_substr(sfoab.street, '[[:print:]]+') as billto_address_line1,
                  regexp_substr(sfoab.street, '[[:cntrl:]]+([[:print:]]+)',1,1,null,1) as billto_address_line2,
                  sfoab.company as billto_address_line3,
				  sfoab.region billto_state, sfoab.postcode billto_postcode, cb.iso3_code billto_country,
				  row_number() over (partition by sfo.increment_id order by sfo.increment_id, sfoi.item_id) order_line,
				  sfo.total_refunded, sfo.tax_refunded total_tax_refunded, sfo.discount_refunded total_discount_refunded, sfo.shipping_refunded,
				  sfoi.tax_amount item_tax_amount, sfo.store_id, sfp.po_number,
				  nvl(sm.ilscarnam, 'UPS') ILS_Carrier_Name, nvl(sm.ilscar,'UPSG') ILS_Carrier_Code, sfo.product_group, sfo.shipping_method,
				  sfoi.base_tax_amount, sfoi.base_discount_amount, sfo.base_shipping_tax_amount, sfo.base_tax_amount p_base_tax_amount,
				  nvl(aoif.SHIPPING_AMOUNT,0) FR_IT_SHIPPING_AMOUNT, nvl(aoif.SHIPPING_DISCOUNT_AMOUNT * -1, 0) FR_IT_SHIPPING_DISCOUNT_AMOUNT,
				  nvl(spt.product_type, ' ') itemType, nvl(asois.VERSION_ID,'0') versionId, nvl(asois.HP_REQUEST_NUM,'0') hPReqNum,
				  asois.tracking_no , decode(asois.ship_date, NULL, NULL, to_char(asois.ship_date, 'yyyy-MM-dd') || '-08:00') ship_date,
				  sfo.ChannelAdvisor_Order_Id, sfo.MarketPlace_Site_Id, sfo.MarketPlace_Order_Id, sfo.base_customer_balance_amount
				from stg_sales_flat_order sfo
				  join (SELECT main.sku, main.name, DECODE(main.PARENT_ITEM_ID, NULL, main.item_id, DECODE(proofs.sku, null, PARENT_ITEM_ID, main.ITEM_ID)) item_id, main.ORDER_ID,
						       sub.qty_ordered, sub.price, sub.row_total, sub.tax_amount, sub.base_tax_amount, sub.base_discount_amount
						FROM stg_sales_flat_order_item main
						 JOIN (SELECT ITEM_ID, ORDER_ID, sum(qty_ordered) qty_ordered, sum(price) price, sum(row_total) row_total, sum(tax_amount) tax_amount, sum(base_tax_amount) base_tax_amount, sum(base_discount_amount) base_discount_amount
						 	   FROM (SELECT DECODE(parent_item_id, NULL, ITEM_ID, DECODE(sub.sku, NULL, parent_item_id, ITEM_ID)) ITEM_ID, ORDER_ID,
										   DECODE(Product_type, 'simple', qty_ordered, 0) qty_ordered,
									       DECODE(sub.sku, NULL, DECODE(parent_item_id, null, price, 0), price) price,
									       DECODE(main.sku, '727826476000030', row_total, DECODE(Product_type, 'configurable', row_total, DECODE(parent_item_id, null, 0, row_total))) row_total,
									       DECODE(main.sku, '727826476000030', tax_amount, DECODE(Product_type, 'configurable', tax_amount, DECODE(parent_item_id, null, 0, tax_amount))) tax_amount,
									       DECODE(main.sku, '727826476000030', base_tax_amount, DECODE(Product_type, 'configurable', base_tax_amount, DECODE(parent_item_id, null, 0, base_tax_amount))) base_tax_amount,
									       DECODE(main.sku, '727826476000030', base_discount_amount, DECODE(Product_type, 'configurable', base_discount_amount, DECODE(base_discount_amount, null, 0, base_discount_amount))) base_discount_amount
									FROM stg_sales_flat_order_item main
									 LEFT JOIN (SELECT sku FROM catalog_product_entity cpe JOIN eav_attribute_set eav ON eav.ATTRIBUTE_SET_NAME = 'AveryRollProof' AND cpe.attribute_set_id = eav.attribute_set_id WHERE cpe.TYPE_ID = 'simple') sub ON main.SKU = sub.sku
									 WHERE ORDER_ID in (<entity_ids>)) main
								 GROUP BY ITEM_ID, ORDER_ID) sub ON main.ORDER_ID = sub.ORDER_ID AND (sub.item_id = main.item_id OR (sub.item_id = main.PARENT_ITEM_ID AND main.sku NOT IN (SELECT sku FROM catalog_product_entity cpe JOIN eav_attribute_set eav ON eav.ATTRIBUTE_SET_NAME = 'AveryRollProof' AND cpe.attribute_set_id = eav.attribute_set_id WHERE cpe.TYPE_ID = 'simple')))
						 LEFT JOIN (SELECT sku FROM catalog_product_entity cpe JOIN eav_attribute_set eav ON eav.ATTRIBUTE_SET_NAME = 'AveryRollProof' AND cpe.attribute_set_id = eav.attribute_set_id WHERE cpe.TYPE_ID = 'simple') proofs ON main.sku = proofs.sku
						WHERE main.PRODUCT_TYPE = 'simple') sfoi on sfo.entity_id = sfoi.order_id
				  join stg_sales_flat_order_address sfoa on sfo.entity_id = sfoa.parent_id and sfoa.address_type = 'shipping'
				  join sku_product_type spt on spt.product_type in ('Sheet','Roll') and sfoi.sku = spt.sku
				  join directory_country c on c.country_id = sfoa.country_id
				  join stg_sales_flat_order_address sfoab on sfo.entity_id = sfoab.parent_id and sfoab.address_type = 'billing'
				  LEFT join AVERY_SALES_ORDER_ITEM_SHIP asois ON sfo.entity_id = asois.ORDER_ID AND sfoi.ITEM_ID = asois.ORDER_ITEM_ID AND asois.ORDER_TYPE in ('sheet','roll')
				  LEFT JOIN AVERY_ORDER_ITEM_FREIGHT aoif ON sfo.entity_id = aoif.ORDER_ID AND sfoi.ITEM_ID = aoif.ORDER_ITEM_ID
				  left join directory_country cb on cb.country_id = sfoab.country_id
				  left join sales_flat_order_payment sfp on sfo.entity_id = sfp.parent_id and sfp.method='purchaseorder'
		          left join avery_magento_ils_ship_mapping sm on sfo.shipping_method = sm.ship_code and sm.use_for_ils = 'Y'
				where sfo.entity_id in (<entity_ids>)
				) main
		join (select sum(nvl(base_shipping_amount, 0)) sum_base_shipping_amount, sum(nvl(base_shipping_discount_amount, 0)) sum_base_ship_dicnt_amt, sum(nvl(base_shipping_tax_amount, 0)) + sum(nvl(base_shipping_hidden_tax_amt, 0)) sum_base_shipping_tax_amount, order_id, warehouse from sales_flat_shipping_group where type in ('sheet','roll')
        AND ORDER_ID IN (<entity_ids>) group by order_id, warehouse) sfsg ON main.entity_id = sfsg.order_id
		order by increment_id, item_id
        """

        val selectOla = """SELECT main.*, nvl(sfsg.sum_base_shipping_amount,0) sum_base_shipping_amount,
			   nvl(sfsg.sum_base_ship_dicnt_amt,0) sum_base_ship_dicnt_amt,
			   nvl(sfsg.sum_base_shipping_tax_amount,0) sum_base_shipping_tax_amount,
			   CASE WHEN order_line = 1 THEN
			   	CASE WHEN main.shipping_method = 'split_group' THEN nvl(main.item_tax_amount, 0) + nvl(sfsg.sum_base_shipping_tax_amount, 0) ELSE nvl(main.item_tax_amount, 0) + nvl(main.base_shipping_tax_amount, 0) END
			   ELSE 0 END as cal_item_tax_amount, sfsg.warehouse
		FROM
				(select sfo.increment_id, sfo.entity_id, sfo.shipping_amount freight, sfo.shipping_discount_amount freight_discount,
				  sfo.discount_amount promotion, sfo.tax_amount tax, sfo.CREATED_AT_PST , sfo.total_item_count, sfo.subtotal, sfo.grand_total,
				  sfo.usps_track_number, sfoi.material_sku sku, sfoi.name, sfoi.item_id, sfoi.qty_ordered, sfoi.price, sfoi.row_total,
				  sfoa.firstname || ' ' || sfoa.lastname custname, sfoa.city, sfoa.region, sfoa.postcode, c.iso3_code country,
				  regexp_substr(sfoa.street, '[[:print:]]+') as address_line1,
                  regexp_substr(sfoa.street, '[[:cntrl:]]+([[:print:]]+)',1,1,null,1) as address_line2,
                  sfoa.company as address_line3,
                  sfoab.firstname || ' ' || sfoab.lastname billto_custname, sfoab.city billto_city,
                  regexp_substr(sfoab.street, '[[:print:]]+') as billto_address_line1,
                  regexp_substr(sfoab.street, '[[:cntrl:]]+([[:print:]]+)',1,1,null,1) as billto_address_line2,
                  sfoab.company as billto_address_line3,
				  sfoab.region billto_state, sfoab.postcode billto_postcode, cb.iso3_code billto_country,
				  row_number() over (partition by sfo.increment_id order by sfo.increment_id, sfoi.item_id) order_line,
				  sfo.total_refunded, sfo.tax_refunded total_tax_refunded, sfo.discount_refunded total_discount_refunded, sfo.shipping_refunded,
				  sfoi.amount_refunded, sfoi.tax_refunded, sfoi.tax_amount item_tax_amount, sfoi.discount_refunded, sfo.store_id, sfp.po_number,
				  nvl(sm.ilscarnam, 'UPS') ILS_Carrier_Name, nvl(sm.ilscar,'UPSG') ILS_Carrier_Code, sfo.product_group, sfo.shipping_method,
				  sfoi.base_tax_amount, sfoi.base_discount_amount, sfo.base_shipping_tax_amount, sfo.base_tax_amount p_base_tax_amount,
				  nvl(aoif.SHIPPING_AMOUNT,0) FR_IT_SHIPPING_AMOUNT, nvl(aoif.SHIPPING_DISCOUNT_AMOUNT * -1, 0) FR_IT_SHIPPING_DISCOUNT_AMOUNT,
				  nvl(spt.product_type, ' ') itemType, nvl(asois.VERSION_ID,'0') versionId, nvl(asois.HP_REQUEST_NUM,'0') hPReqNum,
				  decode(asois.ship_date, NULL, NULL, to_char(asois.ship_date, 'yyyy-MM-dd') || '-08:00') ship_date, asois.tracking_no,
                  trim(sfoa.prefix) etsyid, sfo.customer_id, sfo.ChannelAdvisor_Order_Id, sfo.MarketPlace_Site_Id, sfo.MarketPlace_Order_Id,
                  sfo.base_customer_balance_amount
				from stg_sales_flat_order sfo
				  join stg_sales_flat_order_item sfoi on sfo.entity_id = sfoi.order_id
				  join stg_sales_flat_order_address sfoa on sfo.entity_id = sfoa.parent_id and sfoa.address_type = 'shipping'
				  join sku_product_type spt on spt.product_type = 'OnlineAisle' and sfoi.sku = spt.sku
				  join directory_country c on c.country_id = sfoa.country_id
				  join stg_sales_flat_order_address sfoab on sfo.entity_id = sfoab.parent_id and sfoab.address_type = 'billing'
                  LEFT JOIN AVERY_ORDER_ITEM_FREIGHT aoif ON sfo.entity_id = aoif.ORDER_ID AND sfoi.ITEM_ID = aoif.ORDER_ITEM_ID
                  left join AVERY_SALES_ORDER_ITEM_SHIP asois ON sfo.entity_id = asois.ORDER_ID AND sfoi.ITEM_ID = asois.ORDER_ITEM_ID AND asois.ORDER_TYPE in ('oa')
				  left join directory_country cb on cb.country_id = sfoab.country_id
				  left join sales_flat_order_payment sfp on sfo.entity_id = sfp.parent_id and sfp.method='purchaseorder'
		          left join avery_magento_ils_ship_mapping sm on sfo.shipping_method = sm.ship_code and sm.use_for_ils = 'Y'
				where (lower(sfo.coupon_code) not in ('gorilla007') or sfo.coupon_code is null)
                      and sfo.entity_id in (<entity_ids>)  ) main
		join (select sum(nvl(base_shipping_amount, 0)) sum_base_shipping_amount, sum(nvl(base_shipping_discount_amount, 0)) sum_base_ship_dicnt_amt, sum(nvl(base_shipping_tax_amount, 0)) + sum(nvl(base_shipping_hidden_tax_amt, 0)) sum_base_shipping_tax_amount, order_id, warehouse from sales_flat_shipping_group where type = 'oa'
        AND ORDER_ID IN (<entity_ids>) group by order_id, warehouse) sfsg ON main.entity_id = sfsg.order_id
		order by increment_id, item_id
        """

        val selectCM = """select sfoa.company ship_company, sfoab.company bill_company, sfoi.item_id order_item_id, sfoi.parent_item_id order_parent_item_id,
            sfoi.product_type, sfo.entity_id, sfo.increment_id ,
            sfo.created_at , sfc.increment_id cm_no , DECODE(spt.product_type, 'OnlineAisle', sfci.material_sku, sfci.sku) sku , sfci.name , DECODE(spt.product_type, 'Roll', 1, sfci.qty) qty,
            sfci.row_total * -1 as row_total , nvl(sfci.discount_amount,0) line_discount_amount , sfc.shipping_amount * -1 shipping_amount,
            sfc.tax_amount * -1 tax_amount , sfc.discount_amount * -1 discount_amount , sfc.adjustment * -1 adjustment,
            sfc.store_id , sfoa.firstname || ' ' || sfoa.lastname custname , sfoa.city , sfoa.region , sfoa.postcode,
            regexp_substr(sfoa.street, '[[:print:]]+') as address_line1,
            regexp_substr(sfoa.street, '[[:cntrl:]]+([[:print:]]+)',1,1,null,1) as address_line2,
            sfoa.company as address_line3,
            c.iso3_code country  , sfoab.firstname || ' ' || sfoab.lastname billto_custname, sfoab.city billto_city,
            regexp_substr(sfoab.street, '[[:print:]]+') as billto_address_line1,
            regexp_substr(sfoab.street, '[[:cntrl:]]+([[:print:]]+)',1,1,null,1) as billto_address_line2,
            sfoab.company as billto_address_line3,
            sfoab.region billto_state , sfoab.postcode billto_postcode , cb.iso3_code billto_country,
            row_number() over (partition by sfo.increment_id order by sfo.increment_id, sfoi.item_id) order_line,
            nvl(aoif.SHIPPING_AMOUNT,0) * -1 line_shipping_amt , nvl(aoif.SHIPPING_DISCOUNT_AMOUNT,0) * -1 line_shipping_discount,
            nvl(sfci.BASE_TAX_AMOUNT,0) * -1 line_base_tax_amt,
            nvl(sfci.BASE_DISCOUNT_AMOUNT,0) * -1 line_base_discount_amt, sfp.po_number,
            nvl(sm.ilscarnam, 'UPS') ILS_Carrier_Name, nvl(sm.ilscar,'UPSG') ILS_Carrier_Code, sfo.product_group, sfo.shipping_method,
            sfo.customer_id, sfo.customer_email, sfci.sku onlineAisleSKU, sfsg.warehouse, sfp.ecost_number,
            CASE WHEN nvl(sfc.SHIPPING_TAX_AMOUNT, 0) = 0 THEN 0
            ELSE
	            CASE WHEN sfo.shipping_method = 'split_group' THEN nvl(sfsg.sum_base_shipping_tax_amount, 0) * -1
	            ELSE nvl(sfo.base_shipping_tax_amount, 0) * -1 END 
	        END cal_tax_first_line, spt.product_type productType, sfo.base_customer_balance_refunded
        from stg_sales_flat_creditmemo sfc
            join stg_sales_flat_order sfo on sfo.entity_id = sfc.order_id  and  (lower(sfo.coupon_code) not in ('gorilla007') or sfo.coupon_code is null)
            and sfo.entity_id in (<entity_ids>)
            join stg_sales_flat_order_address sfoab	on sfo.entity_id = sfoab.parent_id and sfoab.address_type = 'billing'
            left join stg_sales_flat_order_address sfoa on sfo.entity_id = sfoa.parent_id and sfoa.address_type = 'shipping'
            left join stg_sales_flat_creditmemo_item sfci  on sfc.entity_id = sfci.parent_id
            left join sku_product_type spt on sfci.sku = spt.sku
            left join (SELECT  order_id, decode(TYPE,'sheet','Sheet','roll','Roll','oa','OnlineAisle','service','Service','retail','Retail','estore','Estore') order_type, warehouse,
            					nvl(base_shipping_tax_amount, 0) + nvl(base_shipping_hidden_tax_amt, 0) sum_base_shipping_tax_amount
						FROM sales_flat_shipping_group) sfsg 
					 on sfo.entity_id = sfsg.order_id AND sfsg.order_type = spt.PRODUCT_TYPE 
            left join AVERY_ORDER_ITEM_FREIGHT aoif  on  sfc.ORDER_id   = aoif.ORDER_ID  and aoif.order_item_id = sfci.order_item_id
            left join stg_sales_flat_order_item sfoi on sfo.entity_id = sfoi.order_id and  sfci.order_item_id  = sfoi.item_id
            left join directory_country c on c.country_id = sfoa.country_id
            left join directory_country cb on cb.country_id = sfoab.country_id
            left join sales_flat_order_payment sfp on sfo.entity_id = sfp.parent_id and sfp.method='purchaseorder'
            left join avery_magento_ils_ship_mapping sm on sfo.shipping_method = sm.ship_code and sm.use_for_ils = 'Y'
        """


        @Throws(Exception::class)
        fun getInvData(db: Database, orderList: List<String>?): HashMap<String, ILSOrder> {

            val orders = HashMap<String, ILSOrder>()
            var stmt: Statement? = null
            val rs: ResultSet?

            val destConn = db.connectionProvider.get()

            try {
                if (orderList == null || orderList.size == 0) return orders


                stmt = destConn.createStatement()

                // build sql
                var selectSql = selectFund
                if (orderList.size > 0) {
                    selectSql = selectSql.replace("<entity_ids>", StringUtils.getSqlStringToIntList(orderList))
                }


                var ilsOrder: ILSOrder?
                var lastOrderNumber = ""
                stmt.execute(selectSql)
                rs = stmt.getResultSet()
                while (rs.next()) {
                    val orderNumber = rs.getString("increment_id")
                    val freight = rs.getBigDecimal("freight")
                    var freightDisc = rs.getBigDecimal("freight_discount")
                    var promotion = rs.getBigDecimal("promotion")
                    val storeId = rs.getInt("store_id")
                    val params =  ilsParameters()[storeId]!!


                    // ils rules - make freightDisc negative; subtract freightDisc from promotion
                    freightDisc = freightDisc.multiply(BigDecimal(-1))
                    promotion = promotion.subtract(freightDisc)

                    if (lastOrderNumber != orderNumber) {

                        // Order Detail
                        val ilsOrderDetail = ILSOrderDetail(
                                rs.getString("sku"),
                                rs.getString("sku"),
                                rs.getString("name"),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                rs.getBigDecimal("qty_ordered"),
                                params["UOM"].toString(),
                                rs.getBigDecimal("row_total"),
                                rs.getString("warehouse"),
                                null,
                                rs.getBigDecimal("base_tax_amount"),
                                rs.getBigDecimal("base_discount_amount"),
                                null, null, null, null,
                                BigInteger.valueOf(rs.getLong(("item_id"))),
                                rs.getBigDecimal("cal_item_tax_amount"),
                                rs.getBigDecimal("fr_it_shipping_amount"),
                                rs.getBigDecimal("fr_it_shipping_discount_amount"),
                                rs.getString("itemType"),
                                rs.getString("versionId"),
                                rs.getString("hPReqNum"),
                                rs.getString("tracking_no"),
                                rs.getString("ship_date"),
                                null,null, "WePrint"
                        )


                        ilsOrder = ILSOrder(false,false, false, false, false,
                                BigInteger.valueOf(rs.getLong("entity_id")),
                                params["Trading Partner ID"].toString(),
                                "190", orderNumber, orderNumber,
                                null,
                                if (rs.getString("po_number") == null)
                                    params["Customer Account"].toString()
                                else
                                    rs.getString("po_number").toString(),
                                rs.getTimestamp("CREATED_AT_PST"), params["ASB Partner ID"].toString(),
                                BigInteger.valueOf(rs.getLong("total_item_count")), "ED", rs.getTimestamp("CREATED_AT_PST"),
                                params["PO Type"].toString(),
                                if (rs.getString("po_number") == null)
                                    "ASB WEB"
                                else
                                    rs.getString("po_number").toString()
                                , rs.getString("ILS_Carrier_Code"), rs.getString("ILS_Carrier_Name"),
                                "", Date(),
                                rs.getBigDecimal("subtotal"), freight,
                                freightDisc, promotion, rs.getBigDecimal("tax"), rs.getBigDecimal("grand_total"),
                                null, null, null, null, null, null, null,
                                false, false, false,
                                rs.getString("billto_custname"), rs.getString("billto_address_line1"), rs.getString("billto_address_line2"), rs.getString("billto_address_line3"),
                                rs.getString("billto_city"), rs.getString("billto_state"),
                                rs.getString("billto_postcode"), rs.getString("billto_country"), null,
                                rs.getString("custname"), rs.getString("address_line1"), rs.getString("address_line2"), rs.getString("address_line3"), rs.getString("city"),
                                rs.getString("region"), rs.getString("postcode"), rs.getString("country"),
                                null, null, mutableListOf<ILSOrderDetail>(ilsOrderDetail), mutableListOf<ILSOrderSpecialCharge>(),
                                rs.getString("po_number") != null, BigInteger.valueOf(rs.getLong("store_id")),
                                rs.getString("shipping_method"), rs.getBigDecimal("base_shipping_tax_amount"),
                                rs.getBigDecimal("p_base_tax_amount"), null, null, null, null,
                                if (rs.getObject("ChannelAdvisor_Order_Id") == null)
                                    null
                                else
                                    BigInteger.valueOf(rs.getLong("ChannelAdvisor_Order_Id")),
                                if (rs.getObject("MarketPlace_Site_Id") == null)
                                    null
                                else
                                    BigInteger.valueOf(rs.getLong("MarketPlace_Site_Id")),
                                rs.getString("MarketPlace_Order_Id"),
                                rs.getBigDecimal("base_customer_balance_amount")
                        )


                        if(rs.getBigDecimal("sum_base_ship_dicnt_amt") != null && rs.getBigDecimal("sum_base_ship_dicnt_amt").compareTo(BigDecimal(0)) != 0) {
                            ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "7 ",
                                "FREIGHT DISCOUNT", rs.getBigDecimal("sum_base_ship_dicnt_amt")))
                        }

                        if(rs.getBigDecimal("base_customer_balance_amount") != null && rs.getBigDecimal("base_customer_balance_amount").compareTo(BigDecimal(0)) != 0) {
                            ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "G ",
                                "STORE CREDIT", rs.getBigDecimal("base_customer_balance_amount")))
                        }

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "8 ",
                                "PROMOTION", rs.getBigDecimal("sum_base_ship_dicnt_amt")))

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "9 ",
                                "TAX", rs.getBigDecimal("sum_base_shipping_tax_amount")))

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "0 ",
                                "DIFY FREIGHT", rs.getBigDecimal("sum_base_shipping_amount")))

                        orders.put(orderNumber, ilsOrder)

                    } else {
                        ilsOrder = orders[orderNumber]

                        // Order Detail
                        // Order Detail
                        val ilsOrderDetail = ILSOrderDetail(
                                rs.getString("sku"),
                                rs.getString("sku"),
                                rs.getString("name"),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                rs.getBigDecimal("qty_ordered"),
                                params["UOM"].toString(),
                                rs.getBigDecimal("row_total"),
                                rs.getString("warehouse"),
                                null,
                                rs.getBigDecimal("base_tax_amount"),
                                rs.getBigDecimal("base_discount_amount"),
                                null, null, null, null,
                                BigInteger.valueOf(rs.getLong(("item_id"))),
                                rs.getBigDecimal("item_tax_amount"),
                                rs.getBigDecimal("fr_it_shipping_amount"),
                                rs.getBigDecimal("fr_it_shipping_discount_amount"),
                                rs.getString("itemType"),
                                rs.getString("versionId"),
                                rs.getString("hPReqNum"),
                                rs.getString("tracking_no"),
                                rs.getString("ship_date"),
                                null,null, "WePrint")

                        ilsOrder!!.orderDetails?.add(ilsOrderDetail)

                    }

                    lastOrderNumber = orderNumber
                }
                rs.close()


            } catch (e: Exception) {

                throw Exception("ILSOrderDAO.getInvData : exception " + e)
            } finally {
                try {
                    if (stmt != null) stmt.close()
                } catch (e: Exception) {
                }

            }

            return orders
        }

        @Throws(Exception::class)
        fun getOlaData(db: Database, orderList: List<String>?, customerID: String): HashMap<String, ILSOrder> {

            val orders = HashMap<String, ILSOrder>()
            var stmt: Statement? = null
            val rs: ResultSet?


            val destConn = db.connectionProvider.get()

            try {
                if (orderList == null || orderList.size == 0) return orders

                stmt = destConn.createStatement()

                // build sql
                var selectSql = selectOla
                if (orderList.size > 0) {
                    selectSql = selectSql.replace("<entity_ids>", StringUtils.getSqlStringToIntList(orderList))
                }


                var ilsOrder: ILSOrder?
                var lastOrderNumber = ""
                stmt.execute(selectSql)
                rs = stmt.getResultSet()
                while (rs.next()) {
                    val orderNumber = rs.getString("increment_id")
                    val freight = rs.getBigDecimal("freight")
                    var freightDisc = rs.getBigDecimal("freight_discount")
                    var promotion = rs.getBigDecimal("promotion")
                    val storeId = rs.getInt("store_id")
                    val params =  ilsParameters()[storeId]!!


                    // ils rules - make freightDisc negative; subtract freightDisc from promotion
                    freightDisc = freightDisc.multiply(BigDecimal(-1))
                    promotion = promotion.subtract(freightDisc)

                    if (lastOrderNumber != orderNumber) {

                        // Order Detail
                        val ilsOrderDetail = ILSOrderDetail(
                                rs.getString("sku"),
                                rs.getString("sku"),
                                rs.getString("name"),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                rs.getBigDecimal("qty_ordered"),
                                params["UOM"].toString(),
                                rs.getBigDecimal("row_total"),
                                rs.getString("warehouse"),
                                null,
                                rs.getBigDecimal("base_tax_amount"),
                                rs.getBigDecimal("base_discount_amount"),
                                null, null, null, null,
                                BigInteger.valueOf(rs.getLong(("item_id"))),
                                rs.getBigDecimal("cal_item_tax_amount"),
                                rs.getBigDecimal("fr_it_shipping_amount"),
                                rs.getBigDecimal("fr_it_shipping_discount_amount"),
                                rs.getString("itemType"),
                                rs.getString("versionId"),
                                rs.getString("hPReqNum"),
                                rs.getString("tracking_no"),
                                rs.getString("ship_date")
                                , null,null, "OnlineAisle")


                        ilsOrder = ILSOrder(false,false,true,
                                rs.getString("customer_id").equals(customerID), false,
                                BigInteger.valueOf(rs.getLong("entity_id")),
                                params["Trading Partner ID"].toString(),
                                "190", orderNumber, orderNumber,
                                null,
                                if (rs.getString("po_number") == null)
                                    params["Customer Account"].toString()
                                else
                                    rs.getString("po_number").toString(),
                                rs.getTimestamp("CREATED_AT_PST"), params["ASB Partner ID"].toString(),
                                BigInteger.valueOf(rs.getLong("total_item_count")), "ED", rs.getTimestamp("CREATED_AT_PST"),
                                params["PO Type"].toString(),
                                if (rs.getString("po_number") == null)
                                    "ASB WEB"
                                else
                                    rs.getString("po_number").toString()
                                , rs.getString("ILS_Carrier_Code"), rs.getString("ILS_Carrier_Name"),
                                "", Date(),
                                rs.getBigDecimal("subtotal"), freight,
                                freightDisc, promotion, rs.getBigDecimal("tax"), rs.getBigDecimal("grand_total"),
                                null, null, null, null, null, null, null,
                                false, false, false,
                                rs.getString("billto_custname"), rs.getString("billto_address_line1"), rs.getString("billto_address_line2"), rs.getString("billto_address_line3"),
                                rs.getString("billto_city"), rs.getString("billto_state"),
                                rs.getString("billto_postcode"), rs.getString("billto_country"), null,
                                rs.getString("custname"), rs.getString("address_line1"), rs.getString("address_line2"), rs.getString("address_line3"), rs.getString("city"),
                                rs.getString("region"), rs.getString("postcode"), rs.getString("country"),
                                null, null, mutableListOf<ILSOrderDetail>(ilsOrderDetail), mutableListOf<ILSOrderSpecialCharge>(),
                                rs.getString("po_number") != null, BigInteger.valueOf(rs.getLong("store_id")),
                                rs.getString("shipping_method"), rs.getBigDecimal("base_shipping_tax_amount"),
                                rs.getBigDecimal("p_base_tax_amount"), null, null,
                                rs.getString("etsyid"), null,
                                if (rs.getObject("ChannelAdvisor_Order_Id") == null)
                                    null
                                else
                                    BigInteger.valueOf(rs.getLong("ChannelAdvisor_Order_Id")),
                                if (rs.getObject("MarketPlace_Site_Id") == null)
                                    null
                                else
                                    BigInteger.valueOf(rs.getLong("MarketPlace_Site_Id")),
                                rs.getString("MarketPlace_Order_Id"),
                                rs.getBigDecimal("base_customer_balance_amount")
                        )


                        if(rs.getBigDecimal("sum_base_ship_dicnt_amt") != null && rs.getBigDecimal("sum_base_ship_dicnt_amt").compareTo(BigDecimal(0)) != 0) {
                            ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "7 ",
                                "FREIGHT DISCOUNT", rs.getBigDecimal("sum_base_ship_dicnt_amt")))
                        }

                        if(rs.getBigDecimal("base_customer_balance_amount") != null && rs.getBigDecimal("base_customer_balance_amount").compareTo(BigDecimal(0)) != 0) {
                            ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "G ",
                                "STORE CREDIT", rs.getBigDecimal("base_customer_balance_amount")))
                        }

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "8 ",
                                "PROMOTION", rs.getBigDecimal("sum_base_ship_dicnt_amt")))

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "9 ",
                                "TAX", rs.getBigDecimal("sum_base_shipping_tax_amount")))

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "0 ",
                                "DIFY FREIGHT", rs.getBigDecimal("sum_base_shipping_amount")))

                        orders.put(orderNumber, ilsOrder)

                    } else {
                        ilsOrder = orders[orderNumber]

                        // Order Detail
                        // Order Detail
                        val ilsOrderDetail = ILSOrderDetail(
                                rs.getString("sku"),
                                rs.getString("sku"),
                                rs.getString("name"),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                rs.getBigDecimal("qty_ordered"),
                                params["UOM"].toString(),
                                rs.getBigDecimal("row_total"),
                                rs.getString("warehouse"),
                                null,
                                rs.getBigDecimal("base_tax_amount"),
                                rs.getBigDecimal("base_discount_amount"),
                                null, null, null, null,
                                BigInteger.valueOf(rs.getLong(("item_id"))),
                                rs.getBigDecimal("item_tax_amount"),
                                rs.getBigDecimal("fr_it_shipping_amount"),
                                rs.getBigDecimal("fr_it_shipping_discount_amount"),
                                rs.getString("itemType"),
                                rs.getString("versionId"),
                                rs.getString("hPReqNum"),
                                rs.getString("tracking_no"),
                                rs.getString("ship_date"), null,null, "OnlineAisle")

                        ilsOrder!!.orderDetails?.add(ilsOrderDetail)

                    }

                    lastOrderNumber = orderNumber
                }
                rs.close()


            } catch (e: Exception) {

                throw Exception("ILSOrderDAO.getOlaData : exception " + e)
            } finally {
                try {
                    if (stmt != null) stmt.close()
                } catch (e: Exception) {
                }

            }

            return orders
        }

        fun getRtData(db: Database, orderList: List<String>?): HashMap<String, ILSOrder> {

            val orders = HashMap<String, ILSOrder>()
            var stmt: Statement? = null
            var pstmt: PreparedStatement? = null
            val rs: ResultSet?
            var rsChk: ResultSet?


            val destConn = db.connectionProvider.get()

            try {
                if (orderList == null || orderList.size == 0) return orders

                val checkForDuplicateSQL = "select CUSPO from ILSPDSI61.IXSB001P@" + DIFYProperty.ILS_GATEWAY +
                        " where CUSPO = ? or CUSPO = ?"

                stmt = destConn.createStatement()
                pstmt = destConn.prepareStatement(checkForDuplicateSQL)

                // build sql
                var selectSql = selectRetail
                if (orderList.size > 0) {
                    selectSql = selectSql.replace("<entity_ids>", StringUtils.getSqlStringToIntList(orderList))
                }


                var ilsOrder: ILSOrder?
                var lastOrderNumber = ""
                stmt.execute(selectSql)
                rs = stmt.getResultSet()
                while (rs.next()) {
                    val orderNumber = rs.getString("increment_id")
                    val freight = rs.getBigDecimal("freight")
                    var freightDisc = rs.getBigDecimal("freight_discount")
                    var promotion = rs.getBigDecimal("promotion")
                    val storeId = rs.getInt("store_id")
                    val params =  ilsParameters()[storeId]!!


                    // ils rules - make freightDisc negative; subtract freightDisc from promotion
                    freightDisc = freightDisc.multiply(BigDecimal(-1))
                    promotion = promotion.subtract(freightDisc)

                    if (lastOrderNumber != orderNumber) {

                        // Order Detail
                        val ilsOrderDetail = ILSOrderDetail(
                                rs.getString("sku"),
                                rs.getString("sku"),
                                rs.getString("name"),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                rs.getBigDecimal("qty_ordered"),
                                params["UOM"].toString(),
                                rs.getBigDecimal("row_total"),
                                rs.getString("warehouse"),
                                null,
                                rs.getBigDecimal("base_tax_amount"),
                                rs.getBigDecimal("base_discount_amount"),
                                null, null, null, null,
                                BigInteger.valueOf(rs.getLong(("item_id"))),
                                rs.getBigDecimal("cal_item_tax_amount"),
                                null,null,null,null,null,null,null
                                , null,null, "Retail")


                        ilsOrder = ILSOrder(rs.getString("product_group").contains("retail"),false,false, false, false,
                                BigInteger.valueOf(rs.getLong("entity_id")),
                                params["Trading Partner ID"].toString(),
                                "190", orderNumber, orderNumber,
                                null,
                                if (rs.getString("po_number") == null)
                                    params["Customer Account"].toString()
                                else
                                    rs.getString("po_number").toString(),
                                rs.getTimestamp("CREATED_AT_PST"), params["ASB Partner ID"].toString(),
                                BigInteger.valueOf(rs.getLong("total_item_count")), "ED", rs.getTimestamp("CREATED_AT_PST"),
                                params["PO Type"].toString(),
                                if (rs.getString("po_number") == null)
                                    "ASB WEB"
                                else
                                    rs.getString("po_number").toString()
                                , rs.getString("ILS_Carrier_Code"), rs.getString("ILS_Carrier_Name"),
                                "", rs.getDate("production_due_date"),
                                rs.getBigDecimal("subtotal"), freight,
                                freightDisc, promotion, rs.getBigDecimal("tax"), rs.getBigDecimal("grand_total"),
                                null, null, null, null, null, null, null,
                                false, false, false,
                                rs.getString("billto_custname"), rs.getString("billto_address_line1"), rs.getString("billto_address_line2"), rs.getString("billto_address_line3"),
                                rs.getString("billto_city"), rs.getString("billto_state"),
                                rs.getString("billto_postcode"), rs.getString("billto_country"), null,
                                rs.getString("custname"), rs.getString("address_line1"), rs.getString("address_line2"), rs.getString("address_line3"), rs.getString("city"),
                                rs.getString("region"), rs.getString("postcode"), rs.getString("country"),
                                null, null, mutableListOf<ILSOrderDetail>(ilsOrderDetail), mutableListOf<ILSOrderSpecialCharge>(),
                                rs.getString("po_number") != null, BigInteger.valueOf(rs.getLong("store_id")),
                                rs.getString("shipping_method"), rs.getBigDecimal("base_shipping_tax_amount"),
                                rs.getBigDecimal("p_base_tax_amount"), null, null, null, null, null, null, null,
                                rs.getBigDecimal("base_customer_balance_amount")
                        )


                        if(rs.getBigDecimal("sum_base_ship_dicnt_amt") != null && rs.getBigDecimal("sum_base_ship_dicnt_amt").compareTo(BigDecimal(0)) != 0) {
                            ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "7 ",
                                "FREIGHT DISCOUNT", rs.getBigDecimal("sum_base_ship_dicnt_amt")))
                        }

                        if(rs.getBigDecimal("base_customer_balance_amount") != null && rs.getBigDecimal("base_customer_balance_amount").compareTo(BigDecimal(0)) != 0) {
                            ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "G ",
                                "STORE CREDIT", rs.getBigDecimal("base_customer_balance_amount")))
                        }

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "8 ",
                            "PROMOTION", rs.getBigDecimal("sum_base_ship_dicnt_amt")))

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "9 ",
                            "TAX", rs.getBigDecimal("sum_base_shipping_tax_amount")))

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "0 ",
                            "DIFY FREIGHT", rs.getBigDecimal("sum_base_shipping_amount")))


                        // check for duplicate order
                        pstmt.setString(1, orderNumber + "A")
                        pstmt.setString(2, "R" + orderNumber + "A")
                        rsChk = pstmt.executeQuery()

                        if (rsChk.next())
                            ilsOrder.isDuplicate = true

                        rsChk.close()

                        orders.put(orderNumber, ilsOrder)

                    } else {
                        ilsOrder = orders[orderNumber]

                        // Order Detail
                        // Order Detail
                        val ilsOrderDetail = ILSOrderDetail(
                                rs.getString("sku"),
                                rs.getString("sku"),
                                rs.getString("name"),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                rs.getBigDecimal("qty_ordered"),
                                params["UOM"].toString(),
                                rs.getBigDecimal("row_total"),
                                rs.getString("warehouse"),
                                null,
                                rs.getBigDecimal("base_tax_amount"),
                                rs.getBigDecimal("base_discount_amount"),
                                null, null, null, null,
                                BigInteger.valueOf(rs.getLong(("item_id"))),
                                rs.getBigDecimal("item_tax_amount"),
                                null,null,null,null,null,null,null, null,null, "Retail")

                        ilsOrder!!.orderDetails?.add(ilsOrderDetail)

                    }

                    lastOrderNumber = orderNumber
                }
                rs.close()


            } catch (e: Exception) {

                throw Exception("ILSOrderDAO.getRtData : exception " + e)
            } finally {
                try {
                    if (stmt != null) stmt.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmt != null) pstmt.close()
                } catch (e: Exception) {
                }

            }

            return orders
        }

        fun getESData(db: Database, orderList: List<String>?): HashMap<String, ILSOrder> {

            val orders = HashMap<String, ILSOrder>()
            var stmt: Statement? = null
            var pstmt: PreparedStatement? = null
            val rs: ResultSet?
            var rsChk: ResultSet?


            val destConn = db.connectionProvider.get()

            try {
                if (orderList == null || orderList.size == 0) return orders

                val checkForDuplicateSQL = "select CUSPO from ILSPDSI61.IXSB001P@" + DIFYProperty.ILS_GATEWAY +
                        " where CUSPO = ? or CUSPO = ?"

                stmt = destConn.createStatement()
                pstmt = destConn.prepareStatement(checkForDuplicateSQL)

                // build sql
                var selectSql = selectEStore
                if (orderList.size > 0) {
                    selectSql = selectSql.replace("<entity_ids>", StringUtils.getSqlStringToIntList(orderList))
                }


                var ilsOrder: ILSOrder?
                var lastOrderNumber = ""
                stmt.execute(selectSql)
                rs = stmt.getResultSet()
                while (rs.next()) {
                    val orderNumber = rs.getString("increment_id")
                    val freight = rs.getBigDecimal("freight")
                    var freightDisc = rs.getBigDecimal("freight_discount")
                    var promotion = rs.getBigDecimal("promotion")
                    val storeId = rs.getInt("store_id")
                    val params =  ilsParameters()[storeId]!!


                    // ils rules - make freightDisc negative; subtract freightDisc from promotion
                    freightDisc = freightDisc.multiply(BigDecimal(-1))
                    promotion = promotion.subtract(freightDisc)

                    if (lastOrderNumber != orderNumber) {

                        // Order Detail
                        val ilsOrderDetail = ILSOrderDetail(
                                rs.getString("sku"),
                                rs.getString("sku"),
                                rs.getString("name"),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                rs.getBigDecimal("qty_ordered"),
                                params["UOM"].toString(),
                                rs.getBigDecimal("row_total"),
                                rs.getString("warehouse"),
                                null,
                                rs.getBigDecimal("base_tax_amount"),
                                rs.getBigDecimal("base_discount_amount"),
                                null, null, null, null,
                                BigInteger.valueOf(rs.getLong(("item_id"))),
                                rs.getBigDecimal("cal_item_tax_amount"),
                                null,null,null,null,null,null,null
                                , null,null, "Estore")


                        ilsOrder = ILSOrder(false,false,false, false,
                                rs.getString("product_group").contains("estore"),
                                BigInteger.valueOf(rs.getLong("entity_id")),
                                params["Trading Partner ID"].toString(),
                                "190", orderNumber, orderNumber,
                                null,
                                if (rs.getString("po_number") == null)
                                    params["Customer Account"].toString()
                                else
                                    rs.getString("po_number").toString(),
                                rs.getTimestamp("CREATED_AT_PST"), params["ASB Partner ID"].toString(),
                                BigInteger.valueOf(rs.getLong("total_item_count")), "ED", rs.getTimestamp("CREATED_AT_PST"),
                                params["PO Type"].toString(),
                                if (rs.getString("po_number") == null)
                                    "ASB WEB"
                                else
                                    rs.getString("po_number").toString()
                                , rs.getString("ILS_Carrier_Code"), rs.getString("ILS_Carrier_Name"),
                                "", rs.getDate("production_due_date"),
                                rs.getBigDecimal("subtotal"), freight,
                                freightDisc, promotion, rs.getBigDecimal("tax"), rs.getBigDecimal("grand_total"),
                                null, null, null, null, null, null, null,
                                false, false, false,
                                rs.getString("billto_custname"), rs.getString("billto_address_line1"), rs.getString("billto_address_line2"), rs.getString("billto_address_line3"),
                                rs.getString("billto_city"), rs.getString("billto_state"),
                                rs.getString("billto_postcode"), rs.getString("billto_country"), null,
                                rs.getString("custname"), rs.getString("address_line1"), rs.getString("address_line2"), rs.getString("address_line3"), rs.getString("city"),
                                rs.getString("region"), rs.getString("postcode"), rs.getString("country"),
                                null, null, mutableListOf<ILSOrderDetail>(ilsOrderDetail), mutableListOf<ILSOrderSpecialCharge>(),
                                false, BigInteger.valueOf(rs.getLong("store_id")),
                                rs.getString("shipping_method"), rs.getBigDecimal("base_shipping_tax_amount"),
                                rs.getBigDecimal("p_base_tax_amount"), null, null, null,
                                rs.getString("ecost_number"), null, null, null,
                                rs.getBigDecimal("base_customer_balance_amount")
                        )

                        if(rs.getBigDecimal("sum_base_ship_dicnt_amt") != null && rs.getBigDecimal("sum_base_ship_dicnt_amt").compareTo(BigDecimal(0)) != 0) {
                            ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "7 ",
                                "FREIGHT DISCOUNT", rs.getBigDecimal("sum_base_ship_dicnt_amt")))
                        }

                        if(rs.getBigDecimal("base_customer_balance_amount") != null && rs.getBigDecimal("base_customer_balance_amount").compareTo(BigDecimal(0)) != 0) {
                            ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "G ",
                                "STORE CREDIT", rs.getBigDecimal("base_customer_balance_amount")))
                        }

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "8 ",
                                "PROMOTION", rs.getBigDecimal("sum_base_ship_dicnt_amt")))

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "9 ",
                                "TAX", rs.getBigDecimal("sum_base_shipping_tax_amount")))

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "0 ",
                                "DIFY FREIGHT", rs.getBigDecimal("sum_base_shipping_amount")))


                        // check for duplicate order
                        pstmt.setString(1, orderNumber + "A")
                        pstmt.setString(2, "R" + orderNumber + "A")
                        rsChk = pstmt.executeQuery()

                        if (rsChk.next())
                            ilsOrder.isDuplicate = true

                        rsChk.close()

                        orders.put(orderNumber, ilsOrder)

                    } else {
                        ilsOrder = orders[orderNumber]

                        // Order Detail
                        // Order Detail
                        val ilsOrderDetail = ILSOrderDetail(
                                rs.getString("sku"),
                                rs.getString("sku"),
                                rs.getString("name"),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                rs.getBigDecimal("qty_ordered"),
                                params["UOM"].toString(),
                                rs.getBigDecimal("row_total"),
                                rs.getString("warehouse"),
                                null,
                                rs.getBigDecimal("base_tax_amount"),
                                rs.getBigDecimal("base_discount_amount"),
                                null, null, null, null,
                                BigInteger.valueOf(rs.getLong(("item_id"))),
                                rs.getBigDecimal("item_tax_amount"),
                                null,null,null,null,null,null,null, null,null, "Estore")

                        ilsOrder!!.orderDetails?.add(ilsOrderDetail)

                    }

                    lastOrderNumber = orderNumber
                }
                rs.close()


            } catch (e: Exception) {

                throw Exception("ILSOrderDAO.getESData : exception " + e)
            } finally {
                try {
                    if (stmt != null) stmt.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmt != null) pstmt.close()
                } catch (e: Exception) {
                }

            }

            return orders
        }

        fun getSvcData(db: Database, orderList: List<String>?): HashMap<String, ILSOrder> {

            val orders = HashMap<String, ILSOrder>()
            var stmt: Statement? = null
            var pstmt: PreparedStatement? = null
            val rs: ResultSet?
            var rsChk: ResultSet?


            val destConn = db.connectionProvider.get()

            try {
                if (orderList == null || orderList.size == 0) return orders

                val checkForDuplicateSQL = "select CUSPO from ILSPDSI61.IXSB001P@" + DIFYProperty.ILS_GATEWAY +
                        " where CUSPO = ? or CUSPO = ?"

                stmt = destConn.createStatement()
                pstmt = destConn.prepareStatement(checkForDuplicateSQL)

                // build sql
                var selectSql = selectService
                if (orderList.size > 0) {
                    selectSql = selectSql.replace("<entity_ids>", StringUtils.getSqlStringToIntList(orderList))
                }


                var ilsOrder: ILSOrder?
                var lastOrderNumber = ""
                stmt.execute(selectSql)
                rs = stmt.getResultSet()
                while (rs.next()) {
                    val orderNumber = rs.getString("increment_id")
                    val freight = rs.getBigDecimal("freight")
                    var freightDisc = rs.getBigDecimal("freight_discount")
                    var promotion = rs.getBigDecimal("promotion")
                    val storeId = rs.getInt("store_id")
                    val params =  ilsParameters()[storeId]!!


                    // ils rules - make freightDisc negative; subtract freightDisc from promotion
                    freightDisc = freightDisc.multiply(BigDecimal(-1))
                    promotion = promotion.subtract(freightDisc)

                    if (lastOrderNumber != orderNumber) {

                        // Order Detail
                        val ilsOrderDetail = ILSOrderDetail(
                                rs.getString("sku"),
                                rs.getString("sku"),
                                rs.getString("name"),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                rs.getBigDecimal("qty_ordered"),
                                params["UOM"].toString(),
                                rs.getBigDecimal("row_total"),
                                rs.getString("warehouse"),
                                null,
                                rs.getBigDecimal("base_tax_amount"),
                                rs.getBigDecimal("base_discount_amount"),
                                null, null, null, null,
                                BigInteger.valueOf(rs.getLong(("item_id"))),
                                rs.getBigDecimal("cal_item_tax_amount"),
                                null,null,null,null,null,null,null
                                , null,null, "Service")


                        ilsOrder = ILSOrder(false, rs.getString("product_group").contains("service"),false, false, false,
                                BigInteger.valueOf(rs.getLong("entity_id")),
                                params["Trading Partner ID"].toString(),
                                "190", orderNumber, orderNumber,
                                null,
                                if (rs.getString("po_number") == null)
                                    params["Customer Account"].toString()
                                else
                                    rs.getString("po_number").toString(),
                                rs.getTimestamp("CREATED_AT_PST"), params["ASB Partner ID"].toString(),
                                BigInteger.valueOf(rs.getLong("total_item_count")), "ED", rs.getTimestamp("CREATED_AT_PST"),
                                params["PO Type"].toString(),
                                if (rs.getString("po_number") == null)
                                    "ASB WEB"
                                else
                                    rs.getString("po_number").toString()
                                , rs.getString("ILS_Carrier_Code"), rs.getString("ILS_Carrier_Name"),
                                "", Date(),
                                rs.getBigDecimal("subtotal"), freight,
                                freightDisc, promotion, rs.getBigDecimal("tax"), rs.getBigDecimal("grand_total"),
                                null, null, null, null, null, null, null,
                                false, false, false,
                                rs.getString("billto_custname"), rs.getString("billto_address_line1"), rs.getString("billto_address_line2"), rs.getString("billto_address_line3"),
                                rs.getString("billto_city"), rs.getString("billto_state"),
                                rs.getString("billto_postcode"), rs.getString("billto_country"), null,
                                rs.getString("custname"), rs.getString("address_line1"), rs.getString("address_line2"), rs.getString("address_line3"), rs.getString("city"),
                                rs.getString("region"), rs.getString("postcode"), rs.getString("country"),
                                null, null, mutableListOf<ILSOrderDetail>(ilsOrderDetail), mutableListOf<ILSOrderSpecialCharge>(),
                                rs.getString("po_number") != null, BigInteger.valueOf(rs.getLong("store_id")),
                                rs.getString("shipping_method"), rs.getBigDecimal("base_shipping_tax_amount"),
                                rs.getBigDecimal("p_base_tax_amount"), null, null, null, null, null, null, null,
                                rs.getBigDecimal("base_customer_balance_amount")
                        )

                        if(rs.getBigDecimal("sum_base_ship_dicnt_amt") != null && rs.getBigDecimal("sum_base_ship_dicnt_amt").compareTo(BigDecimal(0)) != 0) {
                            ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "7 ",
                                "FREIGHT DISCOUNT", rs.getBigDecimal("sum_base_ship_dicnt_amt")))
                        }

                        if(rs.getBigDecimal("base_customer_balance_amount") != null && rs.getBigDecimal("base_customer_balance_amount").compareTo(BigDecimal(0)) != 0) {
                            ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "G ",
                            "STORE CREDIT", rs.getBigDecimal("base_customer_balance_amount")))
                        }

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "8 ",
                                "PROMOTION", rs.getBigDecimal("sum_base_ship_dicnt_amt")))

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "9 ",
                                "TAX", rs.getBigDecimal("sum_base_shipping_tax_amount")))

                        ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ilsOrder.customerPONumber.toString(), "0 ",
                                "DIFY FREIGHT", rs.getBigDecimal("sum_base_shipping_amount")))


                        // check for duplicate order
                        pstmt.setString(1, orderNumber + "S")
                        pstmt.setString(2, "R" + orderNumber + "S")
                        rsChk = pstmt.executeQuery()

                        if (rsChk.next())
                            ilsOrder.isDuplicate = true

                        rsChk.close()

                        orders.put(orderNumber, ilsOrder)

                    } else {
                        ilsOrder = orders[orderNumber]

                        // Order Detail
                        // Order Detail
                        val ilsOrderDetail = ILSOrderDetail(
                                rs.getString("sku"),
                                rs.getString("sku"),
                                rs.getString("name"),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                rs.getBigDecimal("qty_ordered"),
                                params["UOM"].toString(),
                                rs.getBigDecimal("row_total"),
                                rs.getString("warehouse"),
                                null,
                                rs.getBigDecimal("base_tax_amount"),
                                rs.getBigDecimal("base_discount_amount"),
                                null, null, null, null,
                                BigInteger.valueOf(rs.getLong(("item_id"))),
                                rs.getBigDecimal("item_tax_amount"),
                                null,null,null,null,null,null,null, null,null, "Service")

                        ilsOrder!!.orderDetails?.add(ilsOrderDetail)

                    }

                    lastOrderNumber = orderNumber
                }
                rs.close()


            } catch (e: Exception) {

                throw Exception("ILSOrderDAO.getSvcData : exception " + e)
            } finally {
                try {
                    if (stmt != null) stmt.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmt != null) pstmt.close()
                } catch (e: Exception) {
                }

            }

            return orders
        }

        @Throws(Exception::class)
        fun getProData(db: Database, orderList: List<String>?): HashMap<String, ILSOrder> {

            val orders = HashMap<String, ILSOrder>()
            var stmt: Statement? = null
            var pstmt: PreparedStatement? = null
            val rs: ResultSet?
            var rsChk: ResultSet?


            val destConn = db.connectionProvider.get()

            try {
                if (orderList == null || orderList.size == 0) return orders

                val checkForDuplicateSQL = "select CUSPO from ILSPDSI61.IXSB001P@" + DIFYProperty.ILS_GATEWAY +
                        " where CUSPO = ? or CUSPO = ?"

                stmt = destConn.createStatement()
                pstmt = destConn.prepareStatement(checkForDuplicateSQL)

                // build sql
                var selectSql = selectAvyPro
                if (orderList.size > 0) {
                    selectSql += " and sfo.increment_id in (" + StringUtils.getSqlString(orderList) + ") "
                }
                selectSql += " order by sfo.increment_id, sfoi.item_id"

                var ilsOrder: ILSOrder?
                var lastOrderNumber = ""
                stmt.execute(selectSql)
                rs = stmt.getResultSet()
                while (rs.next()) {
                    val orderNumber = rs.getString("increment_id")
                    val freight = rs.getBigDecimal("freight")
                    var freightDisc = rs.getBigDecimal("freight_discount")
                    var promotion = rs.getBigDecimal("promotion")
                    val storeId = rs.getInt("store_id")
                    val params =  ilsParameters()[storeId]!!


                    // ils rules - make freightDisc negative; subtract freightDisc from promotion
                    freightDisc = freightDisc.multiply(BigDecimal(-1))
                    promotion = promotion.subtract(freightDisc)

                    if (lastOrderNumber != orderNumber) {

                        // Order Detail
                        val ilsOrderDetail = ILSOrderDetail(
                                rs.getString("sku"),
                                rs.getString("sku"),
                                rs.getString("name"),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                rs.getBigDecimal("qty_ordered"),
                                params["UOM"].toString(),
                                rs.getBigDecimal("row_total"),
                                params["Shipping Warehouse"].toString(),
                                null,
                                rs.getBigDecimal("base_tax_amount"),
                                rs.getBigDecimal("base_discount_amount"),
                                null, null, null, null,
                                BigInteger.valueOf(rs.getLong(("item_id"))),
                                rs.getBigDecimal("item_tax_amount"),
                                null,null,null,null,null,null,null, null,null, "Pro")


                        ilsOrder = ILSOrder(false,false,false, false, false,
                                BigInteger.valueOf(rs.getLong("entity_id")),
                                params["Trading Partner ID"].toString(),
                                "190", orderNumber, orderNumber,
                                null,
                                if (rs.getString("po_number") == null)
                                    params["Customer Account"].toString()
                                else
                                    rs.getString("po_number").toString(),
                                rs.getTimestamp("CREATED_AT_PST"), params["ASB Partner ID"].toString(),
                                BigInteger.valueOf(rs.getLong("total_item_count")), "ED", rs.getTimestamp("CREATED_AT_PST"),
                                params["PO Type"].toString(),
                                if (rs.getString("po_number") == null)
                                    "ASB WEB"
                                else
                                    rs.getString("po_number").toString()
                                , rs.getString("ILS_Carrier_Code"), rs.getString("ILS_Carrier_Name"),
                                "", Date(),
                                rs.getBigDecimal("subtotal"), freight,
                                freightDisc, promotion, rs.getBigDecimal("tax"), rs.getBigDecimal("grand_total"),
                                null, null, null, null, null, null, null,
                                false, false, false,
                                rs.getString("billto_custname"), rs.getString("billto_address_line1"), rs.getString("billto_address_line2"), rs.getString("billto_address_line3"),
                                rs.getString("billto_city"), rs.getString("billto_state"),
                                rs.getString("billto_postcode"), rs.getString("billto_country"), null,
                                rs.getString("custname"), rs.getString("address_line1"), rs.getString("address_line2"), rs.getString("address_line3"), rs.getString("city"),
                                rs.getString("region"), rs.getString("postcode"), rs.getString("country"),
                                null, null, mutableListOf<ILSOrderDetail>(ilsOrderDetail), mutableListOf<ILSOrderSpecialCharge>(),
                                rs.getString("po_number") != null, BigInteger.valueOf(rs.getLong("store_id")),
                                rs.getString("shipping_method"), rs.getBigDecimal("base_shipping_tax_amount"),
                                rs.getBigDecimal("p_base_tax_amount"), null, null, null, null, null, null, null, null
                        )


                        // special charges
                        if (ilsOrder.shippingDiscount != null && ilsOrder.shippingDiscount != BigDecimal(0)) {
                            ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("AVYPRO", ilsOrder.customerPONumber.toString(), "7 ",
                                    "FREIGHT DISCOUNT", ilsOrder.shippingDiscount as BigDecimal))
                        }
                        if (ilsOrder.discount != null && ilsOrder.discount != BigDecimal(0)) {
                            ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("AVYPRO", ilsOrder.customerPONumber.toString(), "8 ",
                                    "PROMOTION", ilsOrder.discount as BigDecimal))
                        }
                        if (ilsOrder.tax != null && ilsOrder.tax != BigDecimal(0)) {
                            ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("AVYPRO", ilsOrder.customerPONumber.toString(), "9 ",
                                    "TAX", ilsOrder.tax as BigDecimal))
                        }
                        if (ilsOrder.shipping != null && ilsOrder.shipping != BigDecimal(0)) {
                            ilsOrder.specialCharges!!.add(ILSOrderSpecialCharge("AVYPRO", ilsOrder.customerPONumber.toString(), "0 ",
                                    "DIFY FREIGHT", ilsOrder.shipping as BigDecimal))
                        }


                        // check for duplicate order
                        pstmt.setString(1, orderNumber)
                        pstmt.setString(2, "R" +  orderNumber)
                        rsChk = pstmt.executeQuery()

                        if (rsChk.next())
                            ilsOrder.isDuplicate = true

                        rsChk.close()

                        orders.put(orderNumber, ilsOrder)

                    } else {
                        ilsOrder = orders[orderNumber]

                        // Order Detail
                        // Order Detail
                        val ilsOrderDetail = ILSOrderDetail(
                                rs.getString("sku"),
                                rs.getString("sku"),
                                rs.getString("name"),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                rs.getBigDecimal("qty_ordered"),
                                params["UOM"].toString(),
                                rs.getBigDecimal("row_total"),
                                params["Shipping Warehouse"].toString(),
                                null,
                                rs.getBigDecimal("base_tax_amount"),
                                rs.getBigDecimal("base_discount_amount"),
                                null, null, null, null,
                                BigInteger.valueOf(rs.getLong(("item_id"))),
                                rs.getBigDecimal("item_tax_amount"),
                                null,null,null,null,null,null,null, null,null, "Pro")

                        ilsOrder!!.orderDetails?.add(ilsOrderDetail)

                    }

                    lastOrderNumber = orderNumber
                }
                rs.close()


            } catch (e: Exception) {

                throw Exception("ILSOrderDAO.getData : exception " + e)
            } finally {
                try {
                    if (stmt != null) stmt.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmt != null) pstmt.close()
                } catch (e: Exception) {
                }

            }

            return orders
        }


        @Throws(Exception::class)
        fun getCmData(db: Database, orderList: List<String>?): HashMap<String, ILSOrder> {

            val orders = HashMap<String, ILSOrder>()
            var stmt: Statement? = null
            var pstmt: PreparedStatement? = null
            val rs: ResultSet?
            var rsChk: ResultSet?


            val destConn = db.connectionProvider.get()

            try {
                if (orderList == null || orderList.size == 0) return orders

                val checkForDuplicateSQL = "select CUSPO from ILSPDSI61.IXSB001P@" + DIFYProperty.ILS_GATEWAY +
                        " where CUSPO = ?"

                stmt = destConn.createStatement()
                pstmt = destConn.prepareStatement(checkForDuplicateSQL)


                // build sql
                var selectSql = selectCM
                if (orderList.size > 0) {
                    selectSql = selectSql.replace("<entity_ids>", StringUtils.getSqlStringToIntList(orderList))
                }
                selectSql += " order by sfc.increment_id, sfo.increment_id, sfoi.item_id"

                var ilsOrder: ILSOrder?
                stmt.execute(selectSql)
                rs = stmt.getResultSet()
                val lastCreditMemoNumbers = ArrayList<String>()

                while (rs.next()) {

                    val orderNumber = rs.getString("increment_id")
                    val creditMemoNumber = rs.getString("cm_no")
                    val shippingRefunded = rs.getBigDecimal("shipping_amount")
                    val taxRefunded = rs.getBigDecimal("tax_amount")
                    val discountRefunded = rs.getBigDecimal("discount_amount")
                    val adjustment = rs.getBigDecimal("adjustment")
                    val storeId = rs.getInt("store_id")
                    val params =  ilsParameters()[storeId]!!
                    val key = creditMemoNumber

                    if (!lastCreditMemoNumbers.contains(key)) {

                        // Order Detail
                        val ilsOrderDetail = ILSOrderDetail(
                                rs.getString("sku"),
                                rs.getString("sku"),
                                rs.getString("name"),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                rs.getBigDecimal("qty"),
                                params["UOM"].toString(),
                                rs.getBigDecimal("row_total"),
                                rs.getString("warehouse"),
                                null,
                                rs.getBigDecimal("line_base_tax_amt"),
                                rs.getBigDecimal("line_discount_amount"),
                                rs.getBigDecimal("line_shipping_amt"),
                                rs.getBigDecimal("line_shipping_discount"),
                                BigInteger.valueOf(rs.getLong("order_parent_item_id")),
                                rs.getString("product_type"),
                                BigInteger.valueOf(rs.getLong(("order_item_id"))),
                                null,null,null,null,null,null,null,null, rs.getString("onlineAisleSKU"),
                                rs.getBigDecimal("cal_tax_first_line"),
                                rs.getString("productType")
                        )


                        ilsOrder = ILSOrder(
                                rs.getString("product_group").contains("retail"),
                                rs.getString("product_group").contains("service"),
                                rs.getString("product_group").contains("oa"),
                                false,
                                rs.getString("product_group").contains("estore"),
                                BigInteger.valueOf(rs.getLong("entity_id")),
                                params["Trading Partner ID"].toString(),
                                "190", orderNumber, orderNumber, creditMemoNumber,
                                if (rs.getString("po_number") == null)
                                    params["Customer Account"].toString()
                                else
                                    rs.getString("po_number").toString(),
                                rs.getTimestamp("created_at"), params["ASB Partner ID"].toString(),
                                null, "ED", rs.getTimestamp("created_at"),
                                params["PO Type"].toString(),
                                if (rs.getString("po_number") == null)
                                    "ASB WEB"
                                else
                                    rs.getString("po_number").toString()
                                , rs.getString("ILS_Carrier_Code"), rs.getString("ILS_Carrier_Name"),
                                "CREDIT", Date(),
                                null, null,
                                null, null, BigDecimal.ZERO, null,
                                null, shippingRefunded, BigDecimal.ZERO, discountRefunded, taxRefunded, null,
                                adjustment, true, false, false,
                                rs.getString("billto_custname"), rs.getString("billto_address_line1"),
                                if(rs.getString("billto_address_line2") == null) "" else rs.getString("billto_address_line2"),
                                if(rs.getString("billto_address_line3") == null) "" else rs.getString("billto_address_line3"),
                                rs.getString("billto_city"), rs.getString("billto_state"),
                                rs.getString("billto_postcode"), rs.getString("billto_country"), rs.getString("bill_company"),
                                rs.getString("custname"), rs.getString("address_line1"),
                                if(rs.getString("address_line2") == null) "" else rs.getString("address_line2"),
                                if(rs.getString("address_line3") == null) "" else rs.getString("address_line3"),
                                rs.getString("city"), rs.getString("region"), rs.getString("postcode"), rs.getString("country"),
                                rs.getString("ship_company"), null, mutableListOf<ILSOrderDetail>(ilsOrderDetail), mutableListOf<ILSOrderSpecialCharge>(),
                                rs.getString("po_number") != null, BigInteger.valueOf(rs.getLong("store_id")),
                                rs.getString("shipping_method"), null, null,
                                BigInteger.valueOf(rs.getLong("customer_id")), rs.getString("customer_email"), null,
                                rs.getString("ecost_number"), null, null, null,
                                rs.getBigDecimal("base_customer_balance_refunded")
                        )


                        // check for duplicate order
                        pstmt.setString(1, "C" +
                                if(rs.getString("product_group").contains("retail"))
                                    orderNumber + "A" + creditMemoNumber
                                else if(rs.getString("product_group").contains("estore"))
                                    orderNumber + "A" + creditMemoNumber
                                else if(rs.getString("product_group").contains("service"))
                                    orderNumber + "S" + creditMemoNumber
                                else
                                    orderNumber + "-" + creditMemoNumber)
                        rsChk = pstmt.executeQuery()

                        if (rsChk.next())
                            ilsOrder.isDuplicate = true


                        rsChk.close()

                        orders.put(creditMemoNumber, ilsOrder)

                    } else {
                        ilsOrder = orders[creditMemoNumber]

                        // Order Detail
                        val ilsOrderDetail = ILSOrderDetail(
                                rs.getString("sku"),
                                rs.getString("sku"),
                                rs.getString("name"),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                BigInteger.valueOf(rs.getLong("order_line")),
                                rs.getBigDecimal("qty"),
                                params["UOM"].toString(),
                                rs.getBigDecimal("row_total"),
                                rs.getString("warehouse"),
                                null,
                                rs.getBigDecimal("line_base_tax_amt"),
                                rs.getBigDecimal("line_discount_amount"),
                                rs.getBigDecimal("line_shipping_amt"),
                                rs.getBigDecimal("line_shipping_discount"),
                                BigInteger.valueOf(rs.getLong("order_parent_item_id")),
                                rs.getString("product_type"),
                                BigInteger.valueOf(rs.getLong(("order_item_id"))),
                                null, null,null,null,null,null,null,null, rs.getString("onlineAisleSKU"),
                                rs.getBigDecimal("cal_tax_first_line"),
                                rs.getString("productType")
                        )

                        ilsOrder!!.orderDetails?.add(ilsOrderDetail)

                    }

                    lastCreditMemoNumbers.add(creditMemoNumber);

                }
                rs.close()

                // calculate totals
                val orderIter = orders.values.iterator()
                while (orderIter.hasNext()) {
                    val ord = orderIter.next()
                    var subTotal = BigDecimal(0)
                    var grandTotal = BigDecimal(0)
                    var lineDiscount: BigDecimal? = BigDecimal(0)
                    var shippingRefund = BigDecimal(0)

                    val iter = ord.orderDetails!!.iterator()
                    while (iter.hasNext()) {
                        val ordDtl = iter.next()
                        subTotal = subTotal.add(ordDtl.poUnitPrice ?: BigDecimal(0))
                        lineDiscount = lineDiscount!!.add(ordDtl.discountRefunded ?: BigDecimal(0))
                    }
                    ord.subtotalRefunded = subTotal
                    ord.shippingDiscountRefunded = ord.discountRefunded!!.add(lineDiscount!!.negate())
                    ord.discountRefunded = lineDiscount
                    grandTotal = grandTotal.add(ord.subtotalRefunded)
                    grandTotal = grandTotal.add(ord.shippingRefunded)
                    grandTotal = grandTotal.add(ord.shippingDiscountRefunded)
                    grandTotal = grandTotal.add(lineDiscount)
                    grandTotal = grandTotal.add(ord.taxRefunded)
                    grandTotal = grandTotal.add(ord.adjustment)
                    // grandTotal = grandTotal.add(ord.getAdjustmentNegative());
                    ord.totalRefunded = grandTotal

                    // combine shipping and shipping discount
                    if (ord.shippingRefunded != null)
                        shippingRefund = shippingRefund.add(ord.shippingRefunded)
//                    if (ord.shippingDiscountRefunded != null)
//                        shippingRefund = shippingRefund.add(ord.shippingDiscountRefunded)

                    // add freight discount or store credit
                    if (ord.storeCreditAmount != null && ord.storeCreditAmount!!.compareTo(BigDecimal(0)) != 0) {
                        ord.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ord.customerPONumber!!, "G ",
                            "STORE CREDIT", ord.storeCreditAmount!!))
                    }

                    if (ord.shippingDiscountRefunded != null && ord.shippingDiscountRefunded!!.compareTo(BigDecimal(0)) != 0) {
                        ord.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ord.customerPONumber!!, "7 ",
                                    "FREIGHT DISCOUNT", ord.shippingDiscountRefunded!!))
                    }

                    // special charges
                    if (ord.taxRefunded != null && ord.taxRefunded!!.compareTo(BigDecimal(0)) != 0) {
                        ord.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ord.customerPONumber!!, "9 ", "TAX", ord.taxRefunded!!))
                    }
                    if (shippingRefund.compareTo(BigDecimal(0)) != 0) {
                        ord.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ord.customerPONumber!!, "0 ", "DIFY FREIGHT", shippingRefund))
                    }

                    var combinedAdjustments: BigDecimal? = BigDecimal(0)
                    if (ord.adjustment != null) {
                        combinedAdjustments = combinedAdjustments!!.add(ord.adjustment)
                    }

                    combinedAdjustments = combinedAdjustments!!.add(lineDiscount)

                    if (combinedAdjustments != null) {
                        ord.specialCharges!!.add(ILSOrderSpecialCharge("DIFY", ord.customerPONumber!!, "8 ", "PROMOTION", combinedAdjustments))
                    }

                }

            } catch (e: Exception) {

                throw Exception("ILSOrderDAO.getCmData : exception " + e)
            } finally {
                try {
                    if (stmt != null) stmt.close()
                } catch (e: Exception) {
                }
                try {
                    if (pstmt != null) pstmt.close()
                } catch (e: Exception) {
                }

            }

            sumSupportiveSkus(orders.values, db);

            return orders
        }

        private fun ilsParameters(): HashMap<Int, HashMap<String, String>> {
            val hmParam = HashMap<Int, HashMap<String, String>>()

            // DIFY
            val hmDIFY = HashMap<String, String>()
            hmDIFY.put("Trading Partner ID", "DIFY")
            hmDIFY.put("ASB Partner ID", "DIFY")
            hmDIFY.put("Customer Account", "729000")
            hmDIFY.put("PO Type", "SS")
            hmDIFY.put("UOM", "EA")
            hmDIFY.put("Shipping Warehouse", "TJ")
            hmParam.put(DIFYProperty.DIFY_STORE_ID, hmDIFY)

            // AVERYPRO
            val hmAVERYPRO = HashMap<String, String>()
            hmAVERYPRO.put("Trading Partner ID", "AVYPRO")
            hmAVERYPRO.put("ASB Partner ID", "AVERYPRO")
            hmAVERYPRO.put("Customer Account", "333333")
            hmAVERYPRO.put("PO Type", "AP")
            hmAVERYPRO.put("UOM", "CA")
            hmAVERYPRO.put("Shipping Warehouse", "T4")
            hmParam.put(DIFYProperty.AVYPRO_STORE_ID, hmAVERYPRO)

            // DIFY Retail
            val hmDIFYRetail = HashMap<String, String>()
            hmDIFYRetail.put("Trading Partner ID", "DIFY")
            hmDIFYRetail.put("ASB Partner ID", "DIFY")
            hmDIFYRetail.put("Customer Account", "730000")
            hmDIFYRetail.put("PO Type", "AP")
            hmDIFYRetail.put("UOM", "EA")//?
            hmDIFYRetail.put("Shipping Warehouse", "T4")
            hmParam.put(Integer.valueOf(0), hmDIFYRetail)

            return hmParam
        }

        @Throws(Exception::class)
        fun checkProductGroup(db: Database, sku: String): String {

            var pstmt: PreparedStatement
            var rsChk: ResultSet
            var rtnVal: String = ""

            val selectSql = "SELECT PRODUCT_TYPE FROM sku_product_type WHERE SKU = ? "
            val destConn = db.connectionProvider.get()

            try {

                pstmt = destConn.prepareStatement(selectSql)
                pstmt.setString(1, sku)
                rsChk = pstmt.executeQuery()
                if (rsChk.next())
                    rtnVal = rsChk.getString("PRODUCT_TYPE")
                else
                    throw Exception("ILSOrderDAO.checkProductGroup : SKU is not existing. " + sku)

                rsChk.close()

            } catch (e: Exception) {
                throw Exception("ILSOrderDAO.checkProductGroup : exception " + e)
            }

            return rtnVal
        }

        private val HARD_PROOF_SKU1 = "727826467100030"
        private val HARD_PROOF_SKU2 = "727826476100030"
        private fun sumSupportiveSkus(orders: MutableCollection<ILSOrder>, db: Database) {

            try {

                for (ilsOrder in orders) {
                    val toRemoveList = ArrayList<ILSOrderDetail>()

                    val detailIter = ilsOrder.orderDetails!!.iterator()

                    while (detailIter.hasNext()) {

                        val detail = detailIter.next()

                        if (detail.orderItemId == null || detail.productType == null) {
                            toRemoveList.add(detail)
                        }

                        // discard child records for sheet types
                        if ("simple".equals(detail.productType, ignoreCase = true)
                                && detail.poUnitPrice == null
                                && detail.parentItemId!!.compareTo(BigInteger.valueOf(0)) == 1) {

                            toRemoveList.add(detail)

                        } else if ("simple".equals(detail.productType, ignoreCase = true)
                                && detail.poUnitPrice != null
                                && detail.parentItemId!!.compareTo(BigInteger.valueOf(0)) == 1) {

                            val iterVirtualDetail = ilsOrder.orderDetails.iterator()

                            while (iterVirtualDetail.hasNext()) {
                                val virtualDetail = iterVirtualDetail.next()

                                if ("bundle".equals(virtualDetail.productType, ignoreCase = true)) {
                                    var itemShipChargeTot = BigDecimal(0)
                                    itemShipChargeTot = itemShipChargeTot.add(
                                            if (virtualDetail.lineShippingCharge != null)
                                                virtualDetail.lineShippingCharge
                                            else
                                                BigDecimal(0))
                                    itemShipChargeTot = itemShipChargeTot.add(
                                            if (detail.lineShippingCharge != null)
                                                detail.lineShippingCharge
                                            else
                                                BigDecimal(0))

                                    detail.lineShippingCharge = itemShipChargeTot

                                    toRemoveList.add(virtualDetail)
                                } else if ("virtual".equals(virtualDetail.productType, ignoreCase = true)
                                        && virtualDetail.parentItemId!!.toInt() == detail.parentItemId.toInt()) {

                                    var poUnitPriceSum = BigDecimal(0)
                                    var taxRefundSum = BigDecimal(0)
                                    var discountRefundedSum = BigDecimal(0)
                                    if (detail.customerItemNum != HARD_PROOF_SKU1 && detail.customerItemNum != HARD_PROOF_SKU2) {
                                        if (virtualDetail.poUnitPrice != null) {
                                            poUnitPriceSum = poUnitPriceSum.add(virtualDetail.poUnitPrice!!)
                                        }

                                        if (detail.poUnitPrice != null) {
                                            poUnitPriceSum = poUnitPriceSum.add(detail.poUnitPrice!!)
                                        }

                                        if (detail.taxRefunded != null) {
                                            taxRefundSum = taxRefundSum.add(detail.taxRefunded!!)
                                        }
                                        if (virtualDetail.taxRefunded != null) {
                                            taxRefundSum = taxRefundSum.add(virtualDetail.taxRefunded!!)
                                        }

                                        if (detail.discountRefunded != null) {
                                            discountRefundedSum = discountRefundedSum.add(detail.discountRefunded!!)
                                        }
                                        if (virtualDetail.discountRefunded != null) {
                                            discountRefundedSum = discountRefundedSum.add(virtualDetail.discountRefunded!!)
                                        }
                                    } else {

                                        if (detail.poUnitPrice != null) {
                                            poUnitPriceSum = poUnitPriceSum.add(detail.poUnitPrice!!)

                                        }

                                        if (detail.taxRefunded != null) {
                                            taxRefundSum = taxRefundSum.add(detail.taxRefunded!!)
                                        }

                                        if (detail.discountRefunded != null) {
                                            discountRefundedSum = discountRefundedSum.add(detail.discountRefunded!!)
                                        }
                                        // no freight charge - hardproof
                                        detail.lineShippingCharge = BigDecimal(0)

                                    }

                                    detail.poUnitPrice = poUnitPriceSum
                                    detail.taxRefunded = taxRefundSum
                                    detail.discountRefunded = discountRefundedSum

                                    toRemoveList.add(virtualDetail)
                                }
                            }
                        }// actual sku for roll

                        // sample packs and dvd/cd applicator. DO nothing.
                    }

                    // remove non-actual skus, and fix line numbering
                    ilsOrder.orderDetails.removeAll(toRemoveList)
                    ilsOrder.numofLinesonPO = BigInteger.valueOf(ilsOrder.orderDetails.size.toLong())
                    var lineNumber: BigInteger = BigInteger.valueOf(1)
                    for (ilsOrderDetail in ilsOrder.orderDetails) {
                        ilsOrderDetail.poLineNumber = lineNumber
                        ilsOrderDetail.itemSequence = lineNumber
                        lineNumber += BigInteger.ONE
                    }

                    for (ilsOrderDetail in ilsOrder.orderDetails) {

                        var shippingDiscountAmount: BigDecimal?
                        var shippingAmount: BigDecimal?


                        //println("order_item_id: " + ilsOrderDetail.orderItemId + " " + "order detail: " + ilsOrderDetail.lineShippingCharge + " " + ilsOrderDetail.lineShippingDiscount)

                        if (ilsOrderDetail.parentItemId == BigInteger.ZERO) {
                            shippingDiscountAmount = getShippingDiscountAmount(db, ilsOrderDetail.orderItemId!!.toInt())
                            shippingAmount = getShippingAmount(db, ilsOrderDetail.orderItemId.toInt())
                        } else {
                            shippingDiscountAmount = getShippingDiscountAmount(db, ilsOrderDetail.parentItemId!!.toInt())
                            shippingAmount = getShippingAmount(db, ilsOrderDetail.parentItemId.toInt())
                        }

                        if (ilsOrderDetail.originalItemNum!!.contains(HARD_PROOF_SKU1) || ilsOrderDetail.originalItemNum.contains(HARD_PROOF_SKU2)) {
                            ilsOrderDetail.lineShippingCharge = BigDecimal(0)
                            ilsOrderDetail.lineShippingDiscount = BigDecimal(0)
                        } else {
                            //ilsOrderDetail.setLineShippingCharge( (shippingAmount).subtract(shippingDiscountAmount).negate());
                            ilsOrderDetail.lineShippingCharge = shippingAmount.negate()
                            ilsOrderDetail.lineShippingDiscount = shippingDiscountAmount
                        }

                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        protected fun getShippingAmount(db: Database, orderItemId: Int): BigDecimal {

            val sql = " SELECT shipping_amount  FROM  avery_order_item_freight  where order_item_id   =  ? "

            var pstmt: PreparedStatement? = null
            var rs: ResultSet? = null

            try {
                // get connection object for each database
                val destConn = db.connectionProvider.get()

                pstmt = destConn.prepareStatement(sql)
                pstmt!!.setInt(1, orderItemId)

                rs = pstmt.executeQuery()

                return if (rs!!.next()) {

                    //System.out.println("shipping_amount: " + rs.getBigDecimal("shipping_amount"));
                    rs.getBigDecimal("shipping_amount")

                } else BigDecimal(0)

            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                try {
                    if (rs != null)
                        rs.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmt != null)
                        pstmt.close()
                } catch (e: Exception) {
                }
            }

            return BigDecimal(0)
        }

        protected fun getShippingDiscountAmount(db: Database, orderItemId: Int): BigDecimal {

            val sql = " SELECT shipping_discount_amount  FROM  avery_order_item_freight  where order_item_id   =  ? "

            var pstmt: PreparedStatement? = null
            var rs: ResultSet? = null

            try {
                // get connection object for each database
                val destConn = db.connectionProvider.get()

                pstmt = destConn.prepareStatement(sql)
                pstmt!!.setInt(1, orderItemId)

                rs = pstmt.executeQuery()

                return if (rs!!.next()) {

                    //System.out.println("shipping_amount: " + rs.getBigDecimal("shipping_discount_amount"));
                    rs.getBigDecimal("shipping_discount_amount")

                } else BigDecimal(0)

            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                try {
                    if (rs != null)
                        rs.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmt != null)
                        pstmt.close()
                } catch (e: Exception) {
                }
            }

            return BigDecimal(0)
        }



    }

    fun getBillToHTML(): String {
        var addr = ""
        addr += (if (billToName != null) billToName else "") + "<br/>"
        addr += (if (billToCompanyName != null) billToCompanyName else "") + "<br/>"
        addr += (if (billToAddress1 != null) billToAddress1 + " " + billToAddress2 + " " + billToAddress3 else "") + "<br/>"
        addr += if (billToCity != null) billToCity + ", " else ""
        addr += if (billToState != null) billToState + " " else ""
        addr += (if (billToZip != null) billToZip else "") + "<br/>"
        addr += if (billToCountry != null) billToCountry else ""
        return addr
    }

    fun getShipToHTML(): String {
        var addr = ""
        addr += (if (customerName != null) customerName else "") + "<br/>"
        addr += (if (shipCompanyName != null) shipCompanyName else "") + "<br/>"
        addr += (if (customerAddress1 != null) customerAddress1 + " " + customerAddress2 + " " + customerAddress3 else "") + "<br/>"
        addr += if (customerCity != null) customerCity + ", " else ""
        addr += if (customerState != null) customerState + " " else ""
        addr += (if (customerZip != null) customerZip else "") + "<br/>"
        addr += if (customerCountry != null) customerCountry else ""
        return addr
    }

}
