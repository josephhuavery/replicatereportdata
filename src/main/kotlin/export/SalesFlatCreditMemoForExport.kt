package com.avery.dify.export;
// Generated Jul 5, 2017 12:55:35 PM by Hibernate Tools 5.2.3.Final

import com.avery.dify.util.StringUtils
import com.github.davidmoten.rx.jdbc.Database
import rx.Observable
import java.util.Date
import java.math.BigDecimal
import java.math.BigInteger
import java.sql.PreparedStatement
import java.util.concurrent.TimeUnit

import java.sql.ResultSet

data class SalesFlatCreditMemoForExport  (
        val entityId: BigInteger,
        val storeId: BigInteger?,
        val adjustmentPositive: BigDecimal?,
        val baseShippingTaxAmount: BigDecimal?,
        val storeToOrderRate: BigDecimal?,
        val baseDiscountAmount: BigDecimal?,
        val baseToOrderRate: BigDecimal?,
        val grandTotal: BigDecimal?,
        val baseAdjustmentNegative: BigDecimal?,
        val baseSubtotalInclTax: BigDecimal?,
        val shippingAmount: BigDecimal?,
        val subtotalInclTax: BigDecimal?,
        val adjustmentNegative: BigDecimal?,
        val baseShippingAmount: BigDecimal?,
        val storeToBaseRate: BigDecimal?,
        val baseToGlobalRate: BigDecimal?,
        val baseAdjustment: BigDecimal?,
        val baseSubtotal: BigDecimal?,
        val discountAmount: BigDecimal?,
        val subtotal: BigDecimal?,
        val adjustment: BigDecimal?,
        val baseGrandTotal: BigDecimal?,
        val baseAdjustmentPositive: BigDecimal?,
        val baseTaxAmount: BigDecimal?,
        val shippingTaxAmount: BigDecimal?,
        val taxAmount: BigDecimal?,
        val orderId: BigInteger,
        val emailSent: BigInteger?,
        val creditmemoStatus: BigInteger?,
        val state: BigInteger?,
        val shippingAddressId: BigInteger?,
        val billingAddressId: BigInteger?,
        val invoiceId: BigInteger?,
        val storeCurrencyCode: String?,
        val orderCurrencyCode: String?,
        val baseCurrencyCode: String?,
        val globalCurrencyCode: String?,
        val transactionId: String?,
        val incrementId: String?,
        val createdAt: Date?,
        val updatedAt: Date?,
        val hiddenTaxAmount: BigDecimal?,
        val baseHiddenTaxAmount: BigDecimal?,
        val shippingHiddenTaxAmount: BigDecimal?,
        val baseShippingHiddenTaxAmnt: BigDecimal?,
        val shippingInclTax: BigDecimal?,
        val baseShippingInclTax: BigDecimal?,
        val baseCustomerBalanceAmount: BigDecimal?,
        val customerBalanceAmount: BigDecimal?,
        val bsCustomerBalTotalRefunded: BigDecimal?,
        val customerBalTotalRefunded: BigDecimal?,
        val baseGiftCardsAmount: BigDecimal?,
        val giftCardsAmount: BigDecimal?,
        val gwBasePrice: BigDecimal?,
        val gwPrice: BigDecimal?,
        val gwItemsBasePrice: BigDecimal?,
        val gwItemsPrice: BigDecimal?,
        val gwCardBasePrice: BigDecimal?,
        val gwCardPrice: BigDecimal?,
        val gwBaseTaxAmount: BigDecimal?,
        val gwTaxAmount: BigDecimal?,
        val gwItemsBaseTaxAmount: BigDecimal?,
        val gwItemsTaxAmount: BigDecimal?,
        val gwCardBaseTaxAmount: BigDecimal?,
        val gwCardTaxAmount: BigDecimal?,
        val baseRewardCurrencyAmount: BigDecimal?,
        val rewardCurrencyAmount: BigDecimal?,
        val rewardPointsBalance: BigInteger?,
        val rewardPointsBalanceRefund: BigInteger?,
        val discountDescription: String?,
        val productGroup: String?
) {
    companion object {

        // **** Oracle column name limit to 30 chars. Use only 29 because of Hibernate
        val selectSQL = """select
                entity_id,
                store_id,
                adjustment_positive,
                base_shipping_tax_amount,
                store_to_order_rate,
                base_discount_amount,
                base_to_order_rate,
                grand_total,
                base_adjustment_negative,
                base_subtotal_incl_tax,
                shipping_amount,
                subtotal_incl_tax,
                adjustment_negative,
                base_shipping_amount,
                store_to_base_rate,
                base_to_global_rate,
                base_adjustment,
                base_subtotal,
                discount_amount,
                subtotal,
                adjustment,
                base_grand_total,
                base_adjustment_positive,
                base_tax_amount,
                shipping_tax_amount,
                tax_amount,
                order_id,
                email_sent,
                creditmemo_status,
                state,
                shipping_address_id,
                billing_address_id,
                invoice_id,
                store_currency_code,
                order_currency_code,
                base_currency_code,
                global_currency_code,
                transaction_id,
                increment_id,
                created_at,
                updated_at,
                hidden_tax_amount,
                base_hidden_tax_amount,
                shipping_hidden_tax_amount,
                base_shipping_hidden_tax_amnt,
                shipping_incl_tax,
                base_shipping_incl_tax,
                base_customer_balance_amount,
                customer_balance_amount,
                bs_customer_bal_total_refunded,
                customer_bal_total_refunded,
                base_gift_cards_amount,
                gift_cards_amount,
                gw_base_price,
                gw_price,
                gw_items_base_price,
                gw_items_price,
                gw_card_base_price,
                gw_card_price,
                gw_base_tax_amount,
                gw_tax_amount,
                gw_items_base_tax_amount,
                gw_items_tax_amount,
                gw_card_base_tax_amount,
                gw_card_tax_amount,
                base_reward_currency_amount,
                reward_currency_amount,
                reward_points_balance,
                reward_points_balance_refund,
                discount_description,
                (select sub.product_group from stg_sales_flat_order sub where order_id = sub.entity_id) product_group
        from STG_SALES_FLAT_CREDITMEMO
        """
        val updateWePrintStatusCMSQL = """
        update stg_sales_flat_creditmemo set ils_weprint_sent = ? where increment_id in (incrementidlist)
        """
        val updateRetailStatusCMSQL = """
        update stg_sales_flat_creditmemo set ils_retail_sent = ? where increment_id in (incrementidlist)
        """
        val updateEStoreStatusCMSQL = """
        update stg_sales_flat_creditmemo set ils_estore_sent = ? where increment_id in (incrementidlist)
        """
        val updateMixedStatusCMSQL = """
        update stg_sales_flat_creditmemo set ils_weprint_sent = ?, ils_retail_sent = ?, ils_service_sent = ?, ils_onlineaisle_sent = ? where increment_id in (incrementidlist)
        """
        val updateAveryProStatusCMSQL = """
        update stg_sales_flat_creditmemo set ils_averypro_sent = ? where increment_id in (incrementidlist)
        """
        val updateServiceStatusCMSQL = """
        update stg_sales_flat_creditmemo set ils_service_sent = ? where increment_id in (incrementidlist)
        """
        val updateOnlineStatusCMSQL = """
        update stg_sales_flat_creditmemo set ils_onlineaisle_sent = ? where increment_id in (incrementidlist)
        """

        val checkCMItemSQL = """select c.increment_id as creditmemo_number
            from stg_sales_flat_creditmemo c
            inner join stg_sales_flat_order o
            on c.order_id = o.entity_id
            where
             c.INCREMENT_ID = ?
             AND EXISTS (select * 
                from stg_sales_flat_creditmemo_item ci 
                inner join stg_sales_flat_order_item oi
                on ci.order_item_id = oi.item_id
                where ci.parent_id = c.entity_id and (oi.parent_item_id is null) and (oi.product_type='bundle'))
             AND NOT EXISTS (select * 
                from stg_sales_flat_creditmemo_item ci 
                inner join stg_sales_flat_order_item oi
                on ci.order_item_id = oi.item_id
                where ci.parent_id = c.entity_id and (oi.parent_item_id is not null) and (oi.product_type='simple'))
        """

        fun selectOracleforExport(db: Database, additionalWhere: String = ""): List<SalesFlatCreditMemoForExport> =
                db.select(selectSQL + additionalWhere)
                        .autoMap(SalesFlatCreditMemoForExport::class.java)
                        .toList().toBlocking().single()

        @Throws(Exception::class)
        fun updateMasterFlagCM(db : Database, sentCmItem: HashMap<Int, List<String>>) {
            var pstmt: PreparedStatement? = null
            val rs: ResultSet? = null
            try {
                // get connection object for each database
                val destConn = db.connectionProvider.get()
                var sqlQuery: String = ""

                val set = sentCmItem.entries
                val iterator = set.iterator()
                while(iterator.hasNext()) {
                    val mentry = iterator.next()
                    when (mentry.key) {
                        2 -> sqlQuery = updateWePrintStatusCMSQL
                        3 -> sqlQuery = updateRetailStatusCMSQL
                        23 -> sqlQuery = updateMixedStatusCMSQL
                        4 -> sqlQuery = updateAveryProStatusCMSQL
                        5 -> sqlQuery = updateServiceStatusCMSQL
                        6 -> sqlQuery = updateOnlineStatusCMSQL
                        7 -> sqlQuery = updateEStoreStatusCMSQL
                    }

                    pstmt = destConn.prepareStatement(sqlQuery.replace("incrementidlist", StringUtils.getSqlString(mentry.value)))


                    when (mentry.key) {
                        2 -> pstmt.setString(1, "Y")
                        3 -> pstmt.setString(1, "Y")
                        23 -> {
                            pstmt.setString(1, "Y")
                            pstmt.setString(2, "Y")
                            pstmt.setString(3, "Y")
                            pstmt.setString(4, "Y")
                        }
                        4 -> pstmt.setString(1, "Y")
                        5 -> pstmt.setString(1, "Y")
                        6 -> pstmt.setString(1, "Y")
                        7 -> pstmt.setString(1, "Y")
                    }
                    if(mentry.value.size != 0)
                        pstmt.executeUpdate()
                }
                pstmt!!.close()

            } catch (e: Exception) {

                throw Exception("SalesFlatOrderForExport.updateMasterFlagCM : exception " + e)
            } finally {
                try {
                    if (rs != null)
                        rs.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmt != null)
                        pstmt.close()
                } catch (e: Exception) {
                }

            }
        }


        @Throws(Exception::class)
        fun updateDupFlagCM(db : Database, duplicatedItem: HashMap<Int, List<String>>) {
            var pstmt: PreparedStatement? = null
            val rs: ResultSet? = null
            try {
                // get connection object for each database
                val destConn = db.connectionProvider.get()
                var sqlQuery: String = ""

                val set = duplicatedItem.entries
                val iterator = set.iterator()
                while(iterator.hasNext()) {
                    val mentry = iterator.next()

                    when (mentry.key) {
                        2 -> sqlQuery = updateWePrintStatusCMSQL
                        3 -> sqlQuery = updateRetailStatusCMSQL
                        23 -> sqlQuery = updateMixedStatusCMSQL
                        4 -> sqlQuery = updateAveryProStatusCMSQL
                        5 -> sqlQuery = updateServiceStatusCMSQL
                        6 -> sqlQuery = updateOnlineStatusCMSQL
                        7 -> sqlQuery = updateEStoreStatusCMSQL
                    }

                    pstmt = destConn.prepareStatement(sqlQuery.replace("incrementidlist", StringUtils.getSqlString(mentry.value)))


                    when (mentry.key) {
                        2 -> pstmt.setString(1, "D")
                        3 -> pstmt.setString(1, "D")
                        23 -> {
                            pstmt.setString(1, "D")
                            pstmt.setString(2, "D")
                            pstmt.setString(3, "D")
                            pstmt.setString(4, "D")
                        }
                        4 -> pstmt.setString(1, "D")
                        5 -> pstmt.setString(1, "D")
                        6 -> pstmt.setString(1, "D")
                        7 -> pstmt.setString(1, "D")
                    }

                    if(mentry.value.size != 0)
                        pstmt.executeUpdate()
                }
                pstmt!!.close()

            } catch (e: Exception) {

                throw Exception("SalesFlatOrderForExport.updateDupFlagCM : exception " + e)
            } finally {
                try {
                    if (rs != null)
                        rs.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmt != null)
                        pstmt.close()
                } catch (e: Exception) {
                }

            }
        }

        fun checkCMItem(db : Database, CMIncrementID:String) : List<String> =
            db.select(checkCMItemSQL)
                .parameter(CMIncrementID)
                .getAs(String::class.java).toList().toBlocking().single()
    }
}

