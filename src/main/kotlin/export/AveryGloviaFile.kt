/**
 * Created by kkim on 7/14/2017.
 */
package com.avery.dify.export;

import com.github.davidmoten.rx.jdbc.Database
import rx.Observable
import java.math.BigInteger
import java.util.*
import java.util.function.Predicate
import java.util.stream.Collectors


data class AveryGloviaFile  (
    val gloviaId: BigInteger,
    val orderNum: String?,
    val sku: String?,
    val orderNumLineNum: String?,
    val insTimestamp: Date
) {
    companion object {
        val selectSQL = """select
        		glovia_id,
				order_num,
				sku,
				order_num_line_num,
				ins_timestamp
        from avery_glovia_file
        """

        val selectSQLTEST = """
        SELECT glovia_id,
				order_num,
				sku,
				order_num_line_num,
				ins_timestamp FROM (
            SELECT
              GLOVIA_ID, ORDER_NUM, SKU, ORDER_NUM_LINE_NUM, INS_TIMESTAMP,
              ROW_NUMBER() OVER(ORDER BY INS_TIMESTAMP DESC) rn
            FROM avery_glovia_file
          )
          WHERE rn BETWEEN 1 AND 5
          ORDER BY INS_TIMESTAMP desc
        """

        val insertSQL = """insert into avery_glovia_file_test (
		        glovia_id,
				order_num,
				sku,
				order_num_line_num
        ) values (
                :gloviaId,
                :orderNum,
                :sku,
                :orderNumLineNum
        )
        """

        val updateSQL = """update avery_glovia_file set
                order_num = :orderNum,
                sku = :sku,
                order_num_line_num = :orderNumLineNum
        where glovia_id = :gloviaId
        """

        val deleteSQL = """delete from avery_glovia_file
        where glovia_id = :gloviaId
        """
        val deleteAllSQL = """delete from avery_glovia_file"""

        val insertGloviaOrder = """insert into avery_glovia_file (glovia_id, order_num, sku, ITEM_ID, ORDER_NUM_LINE_NUM, QTY) values (avery_glovia_seq.nextval, ?, ?, ?, ?, ?)"""


        @JvmOverloads
        fun select(db: Database, additionalWhere : String = "") : Observable<AveryGloviaFile> =
            db.select(selectSQL + additionalWhere)
                .autoMap(AveryGloviaFile::class.java)


        fun selectTest(db: Database, additionalWhere : String = "") : Observable<AveryGloviaFile> =
            db.select(selectSQLTEST + additionalWhere)
                .autoMap(AveryGloviaFile::class.java)

        fun deleteAll(db : Database) : Int =
            db.update(deleteAllSQL).execute()

        @JvmStatic
        fun insertItem(db: Database,  items: Observable<AveryGloviaFile>){

            items.subscribe {
                it.insert(db)
            }

        }

        @JvmStatic
        fun insertItemFromOrder(db: Database,  items: List<GloviaOrder>,  itemsExc: List<String>){

            val destConn = db.connectionProvider.get()

            val stmt = destConn.prepareStatement(insertGloviaOrder)

            for (glovia in items) {
                if (itemsExc.stream().filter(Predicate { p: String -> p == glovia.item }).collect(Collectors.toList<String>()).size == 0) {
                    val orderNo = String.format("%-10s", glovia.orderNo).replace(' ', '0')
                    val lineLen = 20 - orderNo.length // 20 is max Glovia can
                    // take.
                    val orderLine = String.format("%0" + lineLen + "d",
                        glovia.orderLine)

                    stmt.setString(1, glovia.orderNo)
                    stmt.setString(2, glovia.item)
                    stmt.setString(3, glovia.itemId.toString())
                    stmt.setString(4, orderNo + orderLine)
                    stmt.setInt(5, glovia.qty.toInt())
                    stmt.executeUpdate()
                }
            }

        }

    }


    fun insert(db: Database) : Int =
        db.update(insertSQL)
            .parameter("gloviaId",gloviaId)
            .parameter("orderNum",orderNum)
            .parameter("sku",sku)
            .parameter("orderNumLineNum",orderNumLineNum)
            .execute()

    fun update(db: Database) : Int =
        db.update(updateSQL)

            .parameter("gloviaId",gloviaId)
            .parameter("orderNum",orderNum)
            .parameter("sku",sku)
            .parameter("orderNumLineNum",orderNumLineNum)
            .execute()

    fun delete(db: Database) : Int =
        db.update(deleteSQL)
            .parameter("gloviaId", gloviaId)
            .execute()

}
