/**
 * Created by kkim on 7/14/2017.
 */
package com.avery.dify.export;

import com.github.davidmoten.rx.jdbc.Database
import rx.Observable
import java.util.Date
import java.math.BigDecimal
import java.math.BigInteger
import java.sql.PreparedStatement
import java.util.concurrent.TimeUnit


data class SalesFlatCreditmemoComment  (
        val createdAt: Date,
        val COMMENT_: String

) {
    companion object {
            val selectSQL = """SELECT new_Time(sub.CREATED_AT,'GMT','PST') CREATED_AT, sub.COMMENT_
                                FROM STG_SALES_FLAT_CREDITMEMO main
                                 INNER JOIN SALES_FLAT_CREDITMEMO_COMMENT sub
                                     ON main.entity_id = sub.PARENT_ID
                                WHERE main.increment_id = <increment_id>
        """

        fun select(db: Database, additionalWhere : String = "") : List<SalesFlatCreditmemoComment> =
                db.select( selectSQL.replace("<increment_id>",additionalWhere))
                        .autoMap(SalesFlatCreditmemoComment::class.java)
                        .toList().toBlocking().single()

    }


}
