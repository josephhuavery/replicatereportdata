/**
 * Created by kkim on 7/14/2017.
 */
package com.avery.dify.export;

import com.avery.dify.util.DIFYProperty
import com.avery.dify.xmlmodel.ILSOrderAck
import com.github.davidmoten.rx.jdbc.Database
import rx.Observable
import java.util.Date
import java.math.BigDecimal
import java.math.BigInteger
import java.sql.PreparedStatement
import java.util.concurrent.TimeUnit
import jdk.nashorn.internal.objects.NativeDate.getTime
import java.sql.ResultSet
import java.sql.Statement
import java.sql.Timestamp
import com.avery.dify.util.DIFYProperty.AVYPRO_STORE_ID
import com.avery.dify.util.DIFYProperty.DIFY_STORE_ID
import com.avery.dify.xmlmodel.ILSOrderAckHeader




data class OrderStatus  (
        var orderStatusId: BigInteger?,
        var sourceSystem: String,
        var receivedAt: Date,
        var filename: String,
        var numCredit: BigInteger?,
        var numOrders: BigInteger?,
        var numReplace: BigInteger?,
        var orderId: String?,
        var orderType: String?,
        var orderDisp: String?,
        var orderDispComment: String?,
        var gloviaUpdatedAt: Date?,
        var gloviaUpdateStatus: Char?,
        var gloviaFilename: String?,
        var ilsUpdatedAt: Date?,
        var ilsUpdateStatus: Char?,
        var ilsFilename: String?,
        var ilsAckReceivedAt: Date?,
        var ilsAckDisp: String?,
        var ilsAckDispComment: String?,
        var glvAckReceivedAt: Date?,
        var glvAckDisp: String?,
        var glvAckDispComment: String?,
        var storeId: BigInteger,
        var isNewOrder: Boolean,
        var productGroup: String
) {
    companion object {
        val selectSQL = """select
        		order_status_id,
                source_system,
                received_at,
                filename,
                num_credit,
                num_orders,
                num_replace,
                order_id,
                order_type,
                order_disp,
                order_disp_comment,
                glovia_updated_at,
                glovia_update_status,
                glovia_filename,
                ils_updated_at,
                ils_update_status,
                ils_filename,
                ils_ack_received_at,
                ils_ack_disp,
                ils_ack_disp_comment,
                glv_ack_received_at,
                glv_ack_disp,
                glv_ack_disp_comment,
                store_id
        from stg_order_status
        """

        val insertSQL = """insert into stg_order_status (
		        order_status_id,
                source_system,
                received_at,
                filename,
                num_credit,
                num_orders,
                num_replace,
                order_id,
                order_type,
                order_disp,
                order_disp_comment,
                glovia_updated_at,
                glovia_update_status,
                glovia_filename,
                ils_updated_at,
                ils_update_status,
                ils_filename,
                ils_ack_received_at,
                ils_ack_disp,
                ils_ack_disp_comment,
                glv_ack_received_at,
                glv_ack_disp,
                glv_ack_disp_comment,
                store_id
        ) values (
                :order_status_id,
                :source_system,
                :received_at,
                :filename,
                :num_credit,
                :num_orders,
                :num_replace,
                :order_id,
                :order_type,
                :order_disp,
                :order_disp_comment,
                :glovia_updated_at,
                :glovia_update_status,
                :glovia_filename,
                :ils_updated_at,
                :ils_update_status,
                :ils_filename,
                :ils_ack_received_at,
                :ils_ack_disp,
                :ils_ack_disp_comment,
                :glv_ack_received_at,
                :glv_ack_disp,
                :glv_ack_disp_comment,
                :store_id
        )
        """

        val updateSQL = """update stg_order_status set
                order_status_id = :order_status_id,
                source_system = :source_system,
                received_at = :received_at,
                filename = :filename,
                num_credit = :num_credit,
                num_orders = :num_orders,
                num_replace = :num_replace,
                order_id = :order_id,
                order_type = :order_type,
                order_disp = :order_disp,
                order_disp_comment = :order_disp_comment,
                glovia_updated_at = :glovia_updated_at,
                glovia_update_status = :glovia_update_status,
                glovia_filename = :glovia_filename,
                ils_updated_at = :ils_updated_at,
                ils_update_status = :ils_update_status,
                ils_filename = :ils_filename,
                ils_ack_received_at = :ils_ack_received_at,
                ils_ack_disp = :ils_ack_disp,
                ils_ack_disp_comment = :ils_ack_disp_comment,
                glv_ack_received_at = :glv_ack_received_at,
                glv_ack_disp = :glv_ack_disp,
                glv_ack_disp_comment = :glv_ack_disp_comment,
                store_id = :store_id
        where order_id = :order_id
        """

        val deleteSQL = """delete from stg_order_status
        where order_id = :order_id
        """
        val deleteAllSQL = """delete from stg_order_status"""

        val insertIntoSql = """
        insert into STG_ORDER_STATUS
			(ORDER_STATUS_ID,SOURCE_SYSTEM,RECEIVED_AT,FILENAME,NUM_CREDIT,NUM_ORDERS,NUM_REPLACE,ORDER_ID,
			ORDER_TYPE,ORDER_DISP,ORDER_DISP_COMMENT,GLOVIA_UPDATED_AT,GLOVIA_UPDATE_STATUS,GLOVIA_FILENAME,
			ILS_UPDATED_AT,ILS_UPDATE_STATUS,ILS_FILENAME,ILS_ACK_RECEIVED_AT,ILS_ACK_DISP,ILS_ACK_DISP_COMMENT,STORE_ID)
			values (stg_order_status_seq.nextval,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?,?)
        """

        val updateGlvAckSQL = """update STG_ORDER_STATUS set GLV_ACK_RECEIVED_AT=?,GLV_ACK_DISP=?,GLV_ACK_DISP_COMMENT=?
			where GLOVIA_FILENAME = (select GLOVIA_FILENAME from STG_ORDER_STATUS where ORDER_ID=? and STORE_ID=? and GLV_ACK_RECEIVED_AT is null and GLOVIA_FILENAME is not null and SOURCE_SYSTEM=?)
			and ORDER_ID=? and STORE_ID=?
        """

        val updateIlsAckSQL = """update STG_ORDER_STATUS set ILS_ACK_RECEIVED_AT=?,ILS_ACK_DISP=?,ILS_ACK_DISP_COMMENT=?
			where GLOVIA_FILENAME = (select ILS_FILENAME from STG_ORDER_STATUS where ORDER_ID=? and STORE_ID=? and GLV_ACK_RECEIVED_AT is null and ILS_FILENAME is not null)
			and ORDER_ID=? and STORE_ID=?
        """

        fun select(db: Database, additionalWhere : String = "") : Observable<GloviaOrder> =
                db.select(selectSQL + additionalWhere)
                        .autoMap(GloviaOrder::class.java)

        fun deleteAll(db : Database) : Int =
                db.update(deleteAllSQL).execute()

        @Throws(Exception::class)
        fun logOrderStatus(db : Database, orderList: List<OrderStatus>?) {
            var pstmt: PreparedStatement? = null
            val rs: ResultSet? = null
            try {
                if (orderList == null || orderList.size == 0)
                    return

                // get connection object for each database
                val destConn = db.connectionProvider.get()


                pstmt = destConn.prepareStatement(insertIntoSql)

                val iter = orderList.iterator()
                while (iter.hasNext()) {
                    val order = iter.next()
                    pstmt!!.setString(1, order.sourceSystem)
                    pstmt.setTimestamp(2, Timestamp(order.receivedAt.getTime()))
                    pstmt.setString(3, order.filename)
                    if (order.numCredit == null)
                        pstmt.setNull(4, java.sql.Types.INTEGER)
                    else
                        pstmt.setInt(4, order.numCredit!!.toInt())
                    if (order.numOrders == null)
                        pstmt.setNull(5, java.sql.Types.INTEGER)
                    else
                        pstmt.setInt(5, order.numOrders!!.toInt())
                    if (order.numReplace == null)
                        pstmt.setNull(6, java.sql.Types.INTEGER)
                    else
                        pstmt.setInt(6, order.numReplace!!.toInt())
                    pstmt.setString(7, order.orderId)
                    pstmt.setString(8, order.orderType)
                    pstmt.setString(9, order.orderDisp)
                    pstmt.setString(10, order.orderDispComment)
                    if (order.gloviaUpdatedAt == null)
                        pstmt.setNull(11, java.sql.Types.TIMESTAMP)
                    else
                        pstmt.setTimestamp(11, Timestamp(order.gloviaUpdatedAt!!.getTime()))
                    pstmt.setString(12, order.gloviaUpdateStatus.toString())
                    pstmt.setString(13, order.gloviaFilename)
                    if (order.ilsUpdatedAt == null)
                        pstmt.setNull(14, java.sql.Types.TIMESTAMP)
                    else
                        pstmt.setTimestamp(14, Timestamp(order.ilsUpdatedAt!!.getTime()))
                    pstmt.setString(15, order.ilsUpdateStatus.toString())
                    pstmt.setString(16, order.ilsFilename)
                    if (order.ilsAckReceivedAt == null)
                        pstmt.setNull(17, java.sql.Types.TIMESTAMP)
                    else
                        pstmt.setTimestamp(17, Timestamp(order.ilsAckReceivedAt!!.getTime()))
                    pstmt.setString(18, order.ilsAckDisp)
                    pstmt.setString(19, order.ilsAckDispComment)
                    pstmt.setInt(20, order.storeId.toInt())
                    pstmt.executeUpdate()

                }

                pstmt!!.close()

            } catch (e: Exception) {

                throw Exception("OrderStatusDAO.getData : exception " + e)
            } finally {
                try {
                    if (rs != null)
                        rs.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmt != null)
                        pstmt.close()
                } catch (e: Exception) {
                }

            }
        }


        @Throws(Exception::class)
        fun updateGlvAckOrderStatus(ackList: List<ILSOrderAck>?, db: Database) {
            var pstmtUpdate: PreparedStatement? = null
            var rs: ResultSet? = null
            try {
                if (ackList == null || ackList.size == 0)
                    return

                // get connection object for each database
                val dbConnMgr = db.connectionProvider.get()

                pstmtUpdate = dbConnMgr.prepareStatement(updateGlvAckSQL)

                // loop thru acknowledgments and update order status
                val iter = ackList.iterator()
                while (iter.hasNext()) {
                    val header = iter.next().header
                    val orderId = header.poNumber
                    val disp = header.poDisposition
                    val dispComments = header.poDispositionComments
                    val sourceSystem =
                            if (disp != null && disp.contains("CREATED"))
                                "Magento"
                            else
                                "UPS"
                    val storeId =
                            if (header.tradingPartnerID == "DIFY")
                                DIFYProperty.DIFY_STORE_ID
                            else
                                DIFYProperty.AVYPRO_STORE_ID

                    // get Order Status record

                    pstmtUpdate!!.setTimestamp(1, Timestamp(Date().getTime()))
                    pstmtUpdate.setString(2, disp)
                    pstmtUpdate.setString(3, dispComments)
                    pstmtUpdate.setString(4, orderId)
                    pstmtUpdate.setInt(5, storeId)
                    pstmtUpdate.setString(6, sourceSystem)
                    pstmtUpdate.setString(7, orderId)
                    pstmtUpdate.setInt(8, storeId)
                    pstmtUpdate.executeUpdate()

                }

                rs!!.close()
                pstmtUpdate!!.close()

            } catch (e: Exception) {

                throw Exception(
                        "OrderStatusDAO.updateGlvAckOrderStatus : exception " + e)
            } finally {
                try {
                    if (rs != null)
                        rs.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmtUpdate != null)
                        pstmtUpdate.close()
                } catch (e: Exception) {
                }

            }
        }

        @Throws(Exception::class)
        fun updateIlsAckOrderStatus(ackList: List<ILSOrderAck>?, db: Database) {
            var pstmtUpdate: PreparedStatement? = null
            var rs: ResultSet? = null
            try {
                if (ackList == null || ackList.size == 0)
                    return

                // get connection object for each database
                val dbConnMgr = db.connectionProvider.get()

                pstmtUpdate = dbConnMgr.prepareStatement(updateIlsAckSQL)

                // loop thru acknowledgments and update order status
                val iter = ackList.iterator()
                while (iter.hasNext()) {
                    val header = iter.next().header
                    val orderId = header.poNumber
                    val disp = header.poDisposition
                    val dispComments = header.poDispositionComments
                    val storeId =
                            if (header.tradingPartnerID == "DIFY")
                                DIFYProperty.DIFY_STORE_ID
                            else
                                DIFYProperty.AVYPRO_STORE_ID

                    // get Order Status record
                    pstmtUpdate!!.setTimestamp(1, Timestamp(Date().getTime()))
                    pstmtUpdate.setString(2, disp)
                    pstmtUpdate.setString(3, dispComments)
                    pstmtUpdate.setString(4, orderId)
                    pstmtUpdate.setInt(5, storeId)
                    pstmtUpdate.setString(6, orderId)
                    pstmtUpdate.setInt(7, storeId)
                    pstmtUpdate.executeUpdate()


                }

                rs!!.close()
                pstmtUpdate!!.close()

            } catch (e: Exception) {
                throw Exception(
                        "OrderStatusDAO.updateIlsAckOrderStatus : exception " + e)
            } finally {
                try {
                    if (rs != null)
                        rs.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmtUpdate != null)
                        pstmtUpdate.close()
                } catch (e: Exception) {
                }

            }
        }
    }

    fun insert(db: Database) : Int =
            db.update(insertSQL)
                    .parameter("order_status_id", orderStatusId)
                    .parameter("source_system", sourceSystem)
                    .parameter("received_at", receivedAt)
                    .parameter("filename", filename)
                    .parameter("num_credit", numCredit)
                    .parameter("num_orders", numOrders)
                    .parameter("num_replace", numReplace)
                    .parameter("order_id", orderId)
                    .parameter("order_type", orderType)
                    .parameter("order_disp", orderDisp)
                    .parameter("order_disp_comment", orderDispComment)
                    .parameter("glovia_updated_at", gloviaUpdatedAt)
                    .parameter("glovia_update_status", gloviaUpdateStatus)
                    .parameter("glovia_filename", gloviaFilename)
                    .parameter("ils_updated_at", ilsUpdatedAt)
                    .parameter("ils_update_status", ilsUpdateStatus)
                    .parameter("ils_filename", ilsFilename)
                    .parameter("ils_ack_received_at", ilsAckReceivedAt)
                    .parameter("ils_ack_disp", ilsAckDisp)
                    .parameter("ils_ack_disp_comment", ilsAckDispComment)
                    .parameter("glv_ack_received_at", glvAckReceivedAt)
                    .parameter("glv_ack_disp", glvAckDisp)
                    .parameter("glv_ack_disp_comment", glvAckDispComment)
                    .parameter("store_id", storeId)
                    .execute()

    fun update(db: Database) : Int =
            db.update(updateSQL)
                    .parameter("order_status_id", orderStatusId)
                    .parameter("source_system", sourceSystem)
                    .parameter("received_at", receivedAt)
                    .parameter("filename", filename)
                    .parameter("num_credit", numCredit)
                    .parameter("num_orders", numOrders)
                    .parameter("num_replace", numReplace)
                    .parameter("order_id", orderId)
                    .parameter("order_type", orderType)
                    .parameter("order_disp", orderDisp)
                    .parameter("order_disp_comment", orderDispComment)
                    .parameter("glovia_updated_at", gloviaUpdatedAt)
                    .parameter("glovia_update_status", gloviaUpdateStatus)
                    .parameter("glovia_filename", gloviaFilename)
                    .parameter("ils_updated_at", ilsUpdatedAt)
                    .parameter("ils_update_status", ilsUpdateStatus)
                    .parameter("ils_filename", ilsFilename)
                    .parameter("ils_ack_received_at", ilsAckReceivedAt)
                    .parameter("ils_ack_disp", ilsAckDisp)
                    .parameter("ils_ack_disp_comment", ilsAckDispComment)
                    .parameter("glv_ack_received_at", glvAckReceivedAt)
                    .parameter("glv_ack_disp", glvAckDisp)
                    .parameter("glv_ack_disp_comment", glvAckDispComment)
                    .parameter("store_id", storeId)
                    .execute()

    fun delete(db: Database) : Int =
            db.update(deleteSQL)
                    .parameter("order_id", orderId)
                    .execute()

}

