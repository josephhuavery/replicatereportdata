package com.avery.dify.export;

import com.avery.dify.util.StringUtils
import com.github.davidmoten.rx.jdbc.Database
import rx.Observable
import java.util.Date
import java.math.BigDecimal
import java.math.BigInteger
import java.sql.PreparedStatement
import java.util.concurrent.TimeUnit

import java.sql.ResultSet

data class SalesFlatInvoiceForExport  (
        val entityId: BigInteger,
        val storeId: BigInteger,
        val orderId: BigInteger,
        val state: BigInteger,
        val incrementId: String?,
        val productGroup: String?
) {
    companion object {

        val selectSQL = """select
                main.entity_id,
                main.store_id,
                main.order_id,
                main.state,
                sub.INCREMENT_ID,
                sub.product_group
        from SALES_FLAT_INVOICE main
        """
        val updateWePrintStatusINVSQL = """
        update SALES_FLAT_INVOICE set ils_weprint_sent = ? where order_id in (orderidlist)
        """
        val updateWePrintStatusINVOLASQL = """
        update SALES_FLAT_INVOICE set ils_onlineaisle_sent = ? where order_id in (orderidlist)
        """


        fun selectOracleforExport(db: Database, additionalWhere: String = ""): List<SalesFlatInvoiceForExport> =
                db.select(selectSQL +
                        if (additionalWhere.equals(""))
                            " JOIN stg_sales_flat_order sub ON main.order_id = sub.entity_id AND (sub.PRODUCT_GROUP LIKE '%roll%' OR sub.PRODUCT_GROUP LIKE '%sheet%') and sub.term_method <> 'purchaseorder' WHERE main.state = 2 and main.store_id = 1 and main.ils_weprint_sent is null  AND (main.PRODUCT_GROUP LIKE '%roll%' OR main.PRODUCT_GROUP LIKE '%sheet%') "
                        else
                            " JOIN (SELECT * FROM stg_sales_flat_order ${additionalWhere} AND (PRODUCT_GROUP LIKE '%roll%' OR PRODUCT_GROUP LIKE '%sheet%') and term_method <> 'purchaseorder') sub ON main.order_id = sub.entity_id WHERE main.state = 2 and main.store_id = 1  AND (main.PRODUCT_GROUP LIKE '%roll%' OR main.PRODUCT_GROUP LIKE '%sheet%') ")
                        .autoMap(SalesFlatInvoiceForExport::class.java)
                        .toList().toBlocking().single()

        fun selectOlaforExport(db: Database, additionalWhere: String = ""): List<SalesFlatInvoiceForExport> =
                db.select(selectSQL +
                        if (additionalWhere.equals(""))
                            " JOIN stg_sales_flat_order sub ON main.order_id = sub.entity_id AND sub.PRODUCT_GROUP LIKE '%oa%' and sub.term_method <> 'purchaseorder' WHERE main.state = 2 and main.store_id = 1 and main.ils_onlineaisle_sent is null AND main.PRODUCT_GROUP LIKE '%oa%'  "
                        else
                            " JOIN (SELECT * FROM stg_sales_flat_order ${additionalWhere} AND PRODUCT_GROUP LIKE '%oa%' and term_method <> 'purchaseorder') sub ON main.order_id = sub.entity_id WHERE main.state = 2 and main.store_id = 1 AND main.PRODUCT_GROUP LIKE '%oa%'  ")
                        .autoMap(SalesFlatInvoiceForExport::class.java)
                        .toList().toBlocking().single()

        @Throws(Exception::class)
        fun updateMasterFlagINV(db : Database, sentInvItem: List<String>) {
            var pstmt: PreparedStatement? = null
            val rs: ResultSet? = null
            try {
                // get connection object for each database
                val destConn = db.connectionProvider.get()
                var sqlQuery = updateWePrintStatusINVSQL

                pstmt = destConn.prepareStatement(sqlQuery.replace("orderidlist", StringUtils.getSqlString(sentInvItem)))

                pstmt.setString(1, "Y")
                pstmt.executeUpdate()
                pstmt!!.close()

            } catch (e: Exception) {

                throw Exception("SalesFlatInvoiceForExport.updateMasterFlagINV : exception " + e)
            } finally {
                try {
                    if (rs != null)
                        rs.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmt != null)
                        pstmt.close()
                } catch (e: Exception) {
                }

            }
        }

        @Throws(Exception::class)
        fun updateMasterFlagINVOLA(db : Database, sentOlaItem: List<String>) {
            var pstmt: PreparedStatement? = null
            val rs: ResultSet? = null
            try {
                // get connection object for each database
                val destConn = db.connectionProvider.get()
                var sqlQuery = updateWePrintStatusINVOLASQL

                pstmt = destConn.prepareStatement(sqlQuery.replace("orderidlist", StringUtils.getSqlString(sentOlaItem)))

                pstmt.setString(1, "Y")
                pstmt.executeUpdate()
                pstmt!!.close()

            } catch (e: Exception) {

                throw Exception("SalesFlatInvoiceForExport.updateMasterFlagINVOLA : exception " + e)
            } finally {
                try {
                    if (rs != null)
                        rs.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmt != null)
                        pstmt.close()
                } catch (e: Exception) {
                }

            }
        }

    }
}

