package com.avery.dify.export;

import com.avery.dify.util.StringUtils
import com.github.davidmoten.rx.jdbc.Database
import rx.Observable
import java.util.Date
import java.math.BigDecimal
import java.math.BigInteger
import java.sql.PreparedStatement
import java.util.concurrent.TimeUnit

import java.sql.ResultSet

data class SalesFlatOrderPaymentForExport  (
        val entityId: BigInteger,
        val storeId: BigInteger,
        val orderId: BigInteger,
        val incrementId: String?,
        val productGroup: String?
) {
    companion object {

        val selectSQL = """select
                main.entity_id,
                sub.store_id,
                main.PARENT_ID order_id,
                sub.INCREMENT_ID,
                sub.product_group
        from SALES_FLAT_ORDER_PAYMENT main
        """
        val updateWePrintStatusPMTSQL = """
        update SALES_FLAT_ORDER_PAYMENT set ils_weprint_sent = ? where PARENT_ID in (orderidlist) and METHOD = 'purchaseorder'
        """
        val updateWePrintStatusPMTOLASQL = """
        update SALES_FLAT_ORDER_PAYMENT set ils_onlineaisle_sent = ? where PARENT_ID in (orderidlist) and METHOD = 'purchaseorder'
        """

        fun selectOracleforExport(db: Database, additionalWhere: String = ""): List<SalesFlatOrderPaymentForExport> =
                db.select(selectSQL +
                        if (additionalWhere.equals(""))
                            " JOIN stg_sales_flat_order sub ON main.PARENT_ID = sub.entity_id AND (sub.PRODUCT_GROUP LIKE '%roll%' OR sub.PRODUCT_GROUP LIKE '%sheet%') WHERE main.METHOD = 'purchaseorder' and main.ils_weprint_sent is NULL "
                        else
                            " JOIN (SELECT * FROM stg_sales_flat_order ${additionalWhere} AND (PRODUCT_GROUP LIKE '%roll%' OR PRODUCT_GROUP LIKE '%sheet%')) sub ON main.PARENT_ID = sub.entity_id WHERE main.METHOD = 'purchaseorder' ")
                        .autoMap(SalesFlatOrderPaymentForExport::class.java)
                        .toList().toBlocking().single()

        fun selectOlaforExport(db: Database, additionalWhere: String = ""): List<SalesFlatOrderPaymentForExport> =
                db.select(selectSQL +
                        if (additionalWhere.equals(""))
                            " JOIN stg_sales_flat_order sub ON main.PARENT_ID = sub.entity_id AND sub.PRODUCT_GROUP LIKE '%oa%' WHERE main.METHOD = 'purchaseorder' and main.ils_onlineaisle_sent is NULL "
                        else
                            " JOIN (SELECT * FROM stg_sales_flat_order ${additionalWhere} AND PRODUCT_GROUP LIKE '%oa%') sub ON main.PARENT_ID = sub.entity_id WHERE main.METHOD = 'purchaseorder' ")
                        .autoMap(SalesFlatOrderPaymentForExport::class.java)
                        .toList().toBlocking().single()

        @Throws(Exception::class)
        fun updateMasterFlagPMT(db : Database, sentPmtItem: List<String>) {
            var pstmt: PreparedStatement? = null
            val rs: ResultSet? = null
            try {
                // get connection object for each database
                val destConn = db.connectionProvider.get()
                var sqlQuery = updateWePrintStatusPMTSQL

                pstmt = destConn.prepareStatement(sqlQuery.replace("orderidlist", StringUtils.getSqlString(sentPmtItem)))

                pstmt.setString(1, "Y")
                pstmt.executeUpdate()
                pstmt!!.close()

            } catch (e: Exception) {

                throw Exception("SalesFlatOrderPaymentForExport.updateMasterFlagPMT : exception " + e)
            } finally {
                try {
                    if (rs != null)
                        rs.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmt != null)
                        pstmt.close()
                } catch (e: Exception) {
                }

            }
        }

        @Throws(Exception::class)
        fun updateMasterFlagPMTOLA(db : Database, sentOlaItem: List<String>) {
            var pstmt: PreparedStatement? = null
            val rs: ResultSet? = null
            try {
                // get connection object for each database
                val destConn = db.connectionProvider.get()
                var sqlQuery = updateWePrintStatusPMTOLASQL

                pstmt = destConn.prepareStatement(sqlQuery.replace("orderidlist", StringUtils.getSqlString(sentOlaItem)))

                pstmt.setString(1, "Y")
                pstmt.executeUpdate()
                pstmt!!.close()

            } catch (e: Exception) {

                throw Exception("SalesFlatOrderPaymentForExport.updateMasterFlagPMTOLA : exception " + e)
            } finally {
                try {
                    if (rs != null)
                        rs.close()
                } catch (e: Exception) {
                }

                try {
                    if (pstmt != null)
                        pstmt.close()
                } catch (e: Exception) {
                }

            }
        }

    }
}

