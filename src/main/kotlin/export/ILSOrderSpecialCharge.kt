/**
 * Created by kkim on 7/14/2017.
 */
package com.avery.dify.export;


import com.github.davidmoten.rx.jdbc.Database
import rx.Observable
import java.util.Date
import java.math.BigDecimal
import java.math.BigInteger
import java.sql.PreparedStatement
import java.util.concurrent.TimeUnit

data class ILSOrderSpecialCharge  (
        val internalPartnerId: String,
        val customerPONumber: String,
        val specialChargeCode: String,
        val specialChargeDescription: String,
        val specialChargeAmount: BigDecimal
) {
    companion object {
        val selectSQL = """select
                        'DIFY' ccn, '190' mas_loc,
                        'WC' wc,
                        '729000' primary_cell,
                        sfoi.qty_ordered qty,
                        sfoi.sku item,
                        sfo.created_at start_date,
                        sfo.created_at due_date,
                        'ED' dc, 'SS' pre_seq,
                        sfo.increment_id order_no,
                        row_number() over ( partition by sfo.INCREMENT_ID order by sfoi.sku) order_line
                from stg_sales_flat_order sfo join stg_sales_flat_order_item sfoi on sfo.entity_id = sfoi.order_id
                where sfo.increment_id in (increment_id_list) AND sfoi.PRODUCT_TYPE = 'simple'
                order by increment_id , sku
        """

        val insertSQL = """insert into avery_glovia_file (
		        glovia_id,
				order_num,
				sku,
				order_num_line_num
        ) values (
                :glovia_id,
                :order_num,
                :sku,
                :order_num_line_num
        )
        """

        val updateSQL = """update avery_glovia_file set
                order_num = :orderNum,
                sku = :sku,
                order_num_line_num = :orderNumLineNum
        where glovia_id = :gloviaId
        """

        val deleteSQL = """delete from avery_glovia_file
        where glovia_id = :gloviaId
        """
        val deleteAllSQL = """delete from avery_glovia_file"""


        fun select(db: Database, additionalWhere : String = "") : List<GloviaOrder> =
                db.select( selectSQL.replace("increment_id_list",additionalWhere))
                        .autoMap(GloviaOrder::class.java)
                        .toList().toBlocking().single()

        fun deleteAll(db : Database) : Int =
                db.update(deleteAllSQL).execute()

    }


}
