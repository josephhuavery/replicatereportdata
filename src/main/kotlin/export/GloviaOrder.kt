/**
 * Created by kkim on 7/14/2017.
 */
package com.avery.dify.export;

import com.github.davidmoten.rx.jdbc.Database
import rx.Observable
import java.util.Date
import java.math.BigDecimal
import java.math.BigInteger
import java.sql.PreparedStatement
import java.util.concurrent.TimeUnit


data class GloviaOrder  (
        val ccn: String,
        val masLoc: String,
        val wc: String,
        val primaryCell: String,
        val qty: BigDecimal,
        val item: String,
        val startDate: Date,
        val dueDate: Date,
        val dc: String,
        val priSeq: String,
        val orderNo: String,
        val productType: String,
        val orderLine: BigInteger,
        val itemId: BigInteger,
        val warehouse: String,
        val warehouse_ccn: String
) {
    companion object {
            val selectSQL = """select
                        'DIFY' ccn, '190' mas_loc,
                        'WC' wc,
                        '729000' primary_cell,
                        sfoi.qty_ordered qty,
                        DECODE(spt.product_type, 'OnlineAisle', sfoi.material_sku, sfoi.sku) item,
                        sfo.created_at start_date,
                        sfo.created_at due_date,
                        'ED' dc, 'SS' pre_seq,
                        sfo.increment_id order_no,
                        spt.product_type,
                        row_number() over ( partition by sfo.INCREMENT_ID order by sfoi.sku,sfoi.ITEM_ID) order_line,
                        nvl(sfoi.PARENT_ITEM_ID, sfoi.ITEM_ID) ITEM_ID,
        				sgp.WAREHOUSE, sgp.CCN warehouse_ccn
                from stg_sales_flat_order sfo
                join stg_sales_flat_order_item sfoi on sfo.entity_id = sfoi.order_id
                join sku_product_type spt on sfoi.sku = spt.sku AND spt.PRODUCT_TYPE NOT IN ('Retail','Service','Estore')
                JOIN (SELECT  main.order_id, decode(main.TYPE,'sheet','Sheet','roll','Roll','oa','OnlineAisle') order_type, main.warehouse, gcn.CCN 
						FROM sales_flat_shipping_group main
						JOIN AVERY_GLOVIA_CCN_MAPPING gcn on gcn.warehouse = main.warehouse
						WHERE main.TYPE IN ('sheet','roll','oa')) sgp 
					 on sfo.entity_id = sgp.order_id AND sgp.order_type = spt.PRODUCT_TYPE 
                where sfo.increment_id in (increment_id_list) AND sfoi.PRODUCT_TYPE = 'simple' AND GLOVIA_FILE_SENT IS null
                order by increment_id , item
        """

            val selectCancelSQL = """select
                        'DIFY' ccn, '190' mas_loc,
                        'WC' wc,
                        agf.order_num_line_num primary_cell,
                        agf.qty qty,
                        DECODE(spt.product_type, 'OnlineAisle', sfoi.material_sku, sfoi.sku) item,
                        sfo.created_at start_date,
                        sfo.created_at due_date,
                        'ED' dc, 'SS' pre_seq,
                        sfo.increment_id order_no,
                        spt.product_type,
                        row_number() over ( partition by sfo.INCREMENT_ID order by sfoi.sku,sfoi.ITEM_ID) order_line,
                        agf.item_id ITEM_ID,
        				sgp.WAREHOUSE, sgp.CCN warehouse_ccn
                from (SELECT *
			           FROM stg_sales_flat_order main
			           where main.state = 'canceled' AND main.GLOVIA_FILE_SENT = 'Y' AND entity_id NOT IN (SELECT ORDER_ID FROM AVERY_SALES_ORDER_ITEM_SHIP)
) sfo
                join stg_sales_flat_order_item sfoi on sfo.entity_id = sfoi.order_id
                JOIN avery_glovia_file agf ON sfo.INCREMENT_ID = agf.order_num AND sfoi.item_id = agf.item_id    
                join sku_product_type spt on sfoi.sku = spt.sku AND spt.PRODUCT_TYPE NOT IN ('Retail','Service','Estore')
                JOIN (SELECT  main.order_id, decode(main.TYPE,'sheet','Sheet','roll','Roll','oa','OnlineAisle') order_type, main.warehouse, gcn.CCN 
						FROM sales_flat_shipping_group main
						JOIN AVERY_GLOVIA_CCN_MAPPING gcn on gcn.warehouse = main.warehouse
						WHERE main.TYPE IN ('sheet','roll','oa')) sgp 
					 on sfo.entity_id = sgp.order_id AND sgp.order_type = spt.PRODUCT_TYPE 
                order by sfo.increment_id , sfoi.item_id
        """

        val selectExclusionSQL = """SELECT sku FROM AVERY_SPECIALTY_SKU WHERE GLOVIA_EXCLUDE = 'Y'"""

        val insertSQL = """insert into avery_glovia_file (
		        glovia_id,
				order_num,
				sku,
				order_num_line_num
        ) values (
                :glovia_id,
                :order_num,
                :sku,
                :order_num_line_num
        )
        """

        val updateSQL = """update avery_glovia_file set
                order_num = :orderNum,
                sku = :sku,
                order_num_line_num = :orderNumLineNum
        where glovia_id = :gloviaId
        """

        val deleteSQL = """delete from avery_glovia_file
        where glovia_id = :gloviaId
        """
        val deleteAllSQL = """delete from avery_glovia_file"""


        fun select(db: Database, additionalWhere : String = "") : List<GloviaOrder> =
                db.select( selectSQL.replace("increment_id_list",additionalWhere))
                        .autoMap(GloviaOrder::class.java)
                        .toList().toBlocking().single()

        fun selectCan(db: Database) : List<GloviaOrder> =
            db.select( selectCancelSQL)
                    .autoMap(GloviaOrder::class.java)
                    .toList().toBlocking().single()

        fun selectExclusionList(db: Database) : List<String> =
                db.select( selectExclusionSQL)
                        .getAs(String::class.java)
                        .toBlocking().toIterable().toList()

        fun deleteAll(db : Database) : Int =
                db.update(deleteAllSQL).execute()

    }


}
