package com.avery;

import com.avery.dify.ProcessFiles;
import com.avery.dify.util.DIFYProperty;
import com.avery.dify.util.SendEmail;
import com.github.davidmoten.rx.jdbc.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ReplicateReportData {
    private static Logger logger = LoggerFactory.getLogger(ReplicateReportData.class);

    public static void main(String[] args) throws Exception {

        Config config = CommandLine.populateCommand(new Config(), args);
        if (config.help) {
            CommandLine.usage(new Config(), System.err);
            return;
        }

        switch(config.environment){
            case 0:
                logger.info("{} Job Start. Window Test Environment", new Date());
                break;
            case 1:
                logger.info("{} Job Start. Dev Server Environment", new Date());
                break;
            case 2:
                logger.info("{} Job Start. Production Environment", new Date());
                break;
            default:
                return;
        }

        long start = System.currentTimeMillis();

        DIFYProperty prop = new DIFYProperty(config.environment);
        // Oracle's own JDBC driver does not support auto discovery

        try {

            ReplicateReportData rrd = new ReplicateReportData();

            rrd.ILSdbChecker();

            Class.forName(DIFYProperty.DB_DRIVER);

            String srcUrl = DIFYProperty.SRC_DB_URL;
            String srcUsername = DIFYProperty.SRC_DB_UID;
            String srcPassword = DIFYProperty.SRC_DB_PWD;

            String destUrl = DIFYProperty.DEST_DB_URL;
            String destUsername = DIFYProperty.DEST_DB_UID;
            String destPassword = DIFYProperty.DEST_DB_PWD;

            Database srcDatabase = Database.from(srcUrl, srcUsername, srcPassword);
            Database destDatabase = Database.from(destUrl, destUsername, destPassword);

            String additionalwhere = "";
            if (config.orders != null && config.orders.length > 0) {
                for (int i = 0; i < config.orders.length; i++) {
                    config.orders[i] = "'" + config.orders[i] + "'";
                }
                additionalwhere = " where increment_id in (" + String.join(",", config.orders) + ")";
            }

            if (config.runtype == 0) {
                logger.info("{} Start import/export process.", new Date());

                rrd.importProcess(srcDatabase, destDatabase, additionalwhere, config);
                rrd.exportProcess(destDatabase, additionalwhere, config);
            } else if (config.runtype == 1) {
                logger.info("{} Start import process only.", new Date());
                rrd.importProcess(srcDatabase, destDatabase, additionalwhere, config);
            } else {
                logger.info("{} Start export process only.", new Date());
                rrd.exportProcess(destDatabase, additionalwhere, config);
            }

            long end = System.currentTimeMillis();
            logger.info("{} All process has been finished. Elapsed time total : " + (end-start)/1000 + " second", new Date());

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            SendEmail.sendMessage("kkim@avery.com,jhu@avery.com",
                    DIFYProperty.EMAIL_SENDER,
                    DIFYProperty.EMAIL_HOST,
                    DIFYProperty.SERVER +" : DIFY Replication Error ",
                    sw.toString());
        }

    }

    public void ILSdbChecker() throws Exception {
        Class.forName(DIFYProperty.ILS_DB_DRIVER);
        Database ilsDatabase = Database.from(DIFYProperty.ILS_DB_URL, DIFYProperty.ILS_DB_UID, DIFYProperty.ILS_DB_PWD);
        String check = ilsDatabase.select("SELECT IBMREQD FROM SYSIBM.SYSDUMMY1").getAs(String.class).toBlocking().single();
        logger.info("{} ILS Database is online.", new Date());
        ilsDatabase.close();
    }

    public void importProcess(Database srcDatabase, Database destDatabase, String additionalwhere, Config config) throws Exception {

        if (additionalwhere.equals("")) {
            logger.info("{} Start DB import process. (Days count base)", new Date());

            if (config.offset != 0) {
                additionalwhere =
                        com.avery.dify.OrdersReplicator.replicateParallel(srcDatabase, destDatabase,
                                config.offset, TimeUnit.DAYS, 500);
                logger.info("{} Replicated with AdditionalWhere:\n {}", new Date(), additionalwhere);
            } else {
                additionalwhere =
                        com.avery.dify.OrdersReplicator.replicateParallel(srcDatabase, destDatabase,
                                config.hours, TimeUnit.HOURS, 500);
                logger.info("{} Replicated with AdditionalWhere:\n {}", new Date(), additionalwhere);
            }

            logger.info("{} End DB import process.", new Date());

        } else {

            logger.info("{} Start DB import process. (Order number base)", new Date());

            com.avery.dify.OrdersReplicator.replicateParallel(srcDatabase,destDatabase,
                    additionalwhere, 500);

            logger.info("{} End DB import process.", new Date());

        }
    }

    public void exportProcess(Database destDatabase, String additionalwhere, Config config) throws Exception {

        logger.info("{} Start file export process.", new Date());
        if (config.sending == 1)
            logger.info("{} Export process with file sending.", new Date());
        else
            logger.info("{} Export process without file sending.", new Date());

        //Start export process
        ProcessFiles pf = new ProcessFiles();
        pf.process(destDatabase, additionalwhere, config);

        logger.info("{} End file export process.", new Date());
    }
}
