package com.avery.dify;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.avery.dify.export.OrderStatus;
import com.github.davidmoten.rx.jdbc.Database;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.avery.dify.xmlmodel.ILSOrderAck;
import com.avery.dify.xmlmodel.ILSOrderAckHeader;
import com.avery.dify.xmlmodel.ILSOrderAckItem;
import com.avery.dify.util.DIFYProperty;
import com.avery.dify.util.MQUtil;
import com.avery.dify.util.SendEmail;
import com.avery.dify.util.XmlUtils;

public class GetILSAcknowledgments {

    private SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");

    public void processAck(Database destDatabase) throws Exception {
        try {

            List<ILSOrderAck> ackList = getAcknowledgements();

            OrderStatus.Companion.updateIlsAckOrderStatus(ackList, destDatabase);
        }
        catch (Exception e) {
            throw new Exception("GetILSAcknowledgments.processAck(): " + e.getMessage());
        }
    }

    private List<ILSOrderAck> getAcknowledgements() throws Exception {
        List<ILSOrderAck> ackList = new ArrayList<ILSOrderAck>();
        MQUtil mqAck = null;
        try {
            mqAck = new MQUtil(DIFYProperty.ILS_DIFY_ACK_QUEUE, "GET");

            JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] {ILSOrderAck.class, ILSOrderAckHeader.class, ILSOrderAckItem.class});
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            int numAcks = mqAck.queueDepth();
            for (int j = 0; j < numAcks; j++) {
                // get message on queue
                Document doc = mqAck.getMsg();

                // save message
                try {

                    String fileName = "ILS_ACK_" + df.format(new Date()) + ".xml";
                    String filePath = DIFYProperty.ILS_ACK_DIR + File.separator + fileName;
                    XmlUtils.writeXmlToFile(doc, filePath);
                }
                catch (Exception e) {
                    SendEmail.sendMessage(DIFYProperty.ADMIN_RECIPIENTS,
                            DIFYProperty.EMAIL_SENDER,
                            DIFYProperty.EMAIL_HOST,
                            DIFYProperty.SERVER + ": Error Saving ILS Acknowledgement",
                            e.getMessage());
                }

                // parse message
                NodeList listOfAck = doc.getElementsByTagName("POAcknowledgement");
                for (int i = 0; i < listOfAck.getLength(); i++) {
                    Node POAck = listOfAck.item(i);
                    ILSOrderAck ack = (ILSOrderAck)jaxbUnmarshaller.unmarshal(POAck);
                    ackList.add(ack);
                }
            }
        }
        catch (Exception e) {
            throw new Exception("GetILSAcknowledgements.getAcknowledgements() Exception: " + e.getMessage());
        }
        finally {
            mqAck.closeQueue();
            mqAck.closeQueueManager();
        }
        return ackList;
    }

}
