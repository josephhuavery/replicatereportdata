package com.avery.dify.xmlmodel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "com.avery.ocp.dify.model.ILSOrderAck")
public class ILSOrderAckItem {
    private String itemNumber;
    private String avyItemNumber;
    private String itemSequence;
    private String qty;
    private String itemDisposition;
    private String itemDispositionComments;

    public String getItemNumber() {
        return itemNumber;
    }
    @XmlElement(name = "ItemNumber")
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }
    public String getAvyItemNumber() {
        return avyItemNumber;
    }
    @XmlElement(name = "AvyItemNumber")
    public void setAvyItemNumber(String avyItemNumber) {
        this.avyItemNumber = avyItemNumber;
    }
    public String getItemSequence() {
        return itemSequence;
    }
    @XmlElement(name = "ItemSequence")
    public void setItemSequence(String itemSequence) {
        this.itemSequence = itemSequence;
    }
    public String getQty() {
        return qty;
    }
    @XmlElement(name = "Qty")
    public void setQty(String qty) {
        this.qty = qty;
    }
    public String getItemDisposition() {
        return itemDisposition;
    }
    @XmlElement(name = "ItemDisposition")
    public void setItemDisposition(String itemDisposition) {
        this.itemDisposition = itemDisposition;
    }
    public String getItemDispositionComments() {
        return itemDispositionComments;
    }
    @XmlElement(name = "ItemDispositionComments")
    public void setItemDispositionComments(String itemDispositionComments) {
        this.itemDispositionComments = itemDispositionComments;
    }

}
