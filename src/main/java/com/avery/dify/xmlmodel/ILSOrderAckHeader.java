package com.avery.dify.xmlmodel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "com.avery.ocp.dify.model.ILSOrderAck")
public class ILSOrderAckHeader {
    private String poNumber;
    private String asbOrderNumber;
    private String acknowledgementTimestamp;
    private String tradingPartnerID;
    private String companyCode;
    private String customerAccountNum;
    private String poDisposition;
    private String poDispositionComments;


    public String getPoNumber() {
        return poNumber;
    }
    @XmlElement(name = "PONumber")
    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }
    public String getAsbOrderNumber() {
        return asbOrderNumber;
    }
    @XmlElement(name = "ASBOrderNumber")
    public void setAsbOrderNumber(String asbOrderNumber) {
        this.asbOrderNumber = asbOrderNumber;
    }
    public String getAcknowledgementTimestamp() {
        return acknowledgementTimestamp;
    }
    @XmlElement(name = "AcknowledgementTimestamp")
    public void setAcknowledgementTimestamp(String acknowledgementTimestamp) {
        this.acknowledgementTimestamp = acknowledgementTimestamp;
    }
    public String getTradingPartnerID() {
        return tradingPartnerID;
    }
    @XmlElement(name = "TradingPartnerID")
    public void setTradingPartnerID(String tradingPartnerID) {
        this.tradingPartnerID = tradingPartnerID;
    }
    public String getCompanyCode() {
        return companyCode;
    }
    @XmlElement(name = "CompanyCode")
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }
    public String getCustomerAccountNum() {
        return customerAccountNum;
    }
    @XmlElement(name = "CustomerAccountNum")
    public void setCustomerAccountNum(String customerAccountNum) {
        this.customerAccountNum = customerAccountNum;
    }
    public String getPoDisposition() {
        return poDisposition;
    }
    @XmlElement(name = "PODisposition")
    public void setPoDisposition(String poDisposition) {
        this.poDisposition = poDisposition;
    }
    public String getPoDispositionComments() {
        return poDispositionComments;
    }
    @XmlElement(name = "PODispositionComments")
    public void setPoDispositionComments(String poDispositionComments) {
        this.poDispositionComments = poDispositionComments;
    }

}
