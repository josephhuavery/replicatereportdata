package com.avery.dify.xmlmodel;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(name="POAcknowledgement")
public class ILSOrderAck {
    private String asbPartnerId;
    private ILSOrderAckHeader header;
    private ArrayList<ILSOrderAckItem> items;


    public String getAsbPartnerId() {
        return asbPartnerId;
    }
    @XmlElement(name = "ASBPartnerID")
    public void setAsbPartnerId(String asbPartnerId) {
        this.asbPartnerId = asbPartnerId;
    }
    public ILSOrderAckHeader getHeader() {
        return header;
    }
    @XmlElement(name = "POAcknowledgementHeader")
    public void setHeader(ILSOrderAckHeader header) {
        this.header = header;
    }
    public ArrayList<ILSOrderAckItem> getItems() {
        return items;
    }

    @XmlElementWrapper(name = "POAcknowledgementDetail")
    @XmlElement(name = "Item")
    public void setItems(ArrayList<ILSOrderAckItem> items) {
        this.items = items;
    }

}
