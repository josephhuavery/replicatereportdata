package com.avery.dify.xmlmodel;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PO" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Header"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="OrderType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="CarrierCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="CarrierName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="CustomerPONum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="ASBOrderNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="CustomerAccountNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="DateOrderReceived" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *                             &lt;element name="TradingPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="ASBPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="NumOfLinesOnThePO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="OrderSourceCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="PODate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *                             &lt;element name="POOrderType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="StoreNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="UPSTracking" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="ShippedDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *                             &lt;element name="CostCenterString" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="MarketPlaceSiteId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *                             &lt;element name="ChannelAdvisorOrderId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *                             &lt;element name="MarketPlaceOrderId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="Error" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Detail" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="ItemType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="VersionID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="HPReqNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="CustItemNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="CustomerPONumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="TradingPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="CustomerAccountNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="ItemDescription1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="OriginalItemNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="ArtworkNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="POLineNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="ItemSequence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="POQty" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="POUOM" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="POUnitPrice" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="ShippingWarehouse" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="TrackingNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="ShippedDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *                             &lt;element name="FreightChargeAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="FreightDiscountAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="TaxChargeAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="PromoDiscountAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="Error" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="SpecialCharge" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="CustomerPONumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="TradingPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="CustomerAccountNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="SpecialChargeAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="SpecialChargeCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="SpecialChargeDesc" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="Error" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="ShippingAddress"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="CustomerPONumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="TradingPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="CustomerAccountNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="AddressLine1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="AddressLine2" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="AddressLine3" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="Error" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "po"
})
@XmlRootElement(name = "PurchaseOrders")
public class PurchaseOrders {

    @XmlElement(name = "PO", required = true)
    protected List<PurchaseOrders.PO> po;

    /**
     * Gets the value of the po property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the po property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPO().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrders.PO }
     *
     *
     */
    public List<PurchaseOrders.PO> getPO() {
        if (po == null) {
            po = new ArrayList<PurchaseOrders.PO>();
        }
        return this.po;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Header"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="OrderType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="CarrierCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="CarrierName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="CustomerPONum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="ASBOrderNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="CustomerAccountNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="DateOrderReceived" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
     *                   &lt;element name="TradingPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="ASBPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="NumOfLinesOnThePO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="OrderSourceCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="PODate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
     *                   &lt;element name="POOrderType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="StoreNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="UPSTracking" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="ShippedDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
     *                   &lt;element name="CostCenterString" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="MarketPlaceSiteId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
     *                   &lt;element name="ChannelAdvisorOrderId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
     *                   &lt;element name="MarketPlaceOrderId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="Error" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Detail" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="ItemType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="VersionID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="HPReqNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="CustItemNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="CustomerPONumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="TradingPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="CustomerAccountNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="ItemDescription1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="OriginalItemNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="ArtworkNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="POLineNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="ItemSequence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="POQty" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="POUOM" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="POUnitPrice" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="ShippingWarehouse" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="TrackingNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="ShippedDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
     *                   &lt;element name="FreightChargeAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="FreightDiscountAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="TaxChargeAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="PromoDiscountAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="Error" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="SpecialCharge" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="CustomerPONumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="TradingPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="CustomerAccountNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="SpecialChargeAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="SpecialChargeCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="SpecialChargeDesc" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="Error" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="ShippingAddress"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="CustomerPONumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="TradingPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="CustomerAccountNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="AddressLine1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="AddressLine2" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="AddressLine3" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="Error" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "header",
            "detail",
            "specialCharge",
            "shippingAddress"
    })
    public static class PO {

        @XmlElement(name = "Header", required = true)
        protected PurchaseOrders.PO.Header header;
        @XmlElement(name = "Detail")
        protected List<PurchaseOrders.PO.Detail> detail;
        @XmlElement(name = "SpecialCharge")
        protected List<PurchaseOrders.PO.SpecialCharge> specialCharge;
        @XmlElement(name = "ShippingAddress", required = true)
        protected PurchaseOrders.PO.ShippingAddress shippingAddress;
        @XmlAttribute(name = "Status")
        protected String status;

        /**
         * Gets the value of the header property.
         *
         * @return
         *     possible object is
         *     {@link PurchaseOrders.PO.Header }
         *
         */
        public PurchaseOrders.PO.Header getHeader() {
            return header;
        }

        /**
         * Sets the value of the header property.
         *
         * @param value
         *     allowed object is
         *     {@link PurchaseOrders.PO.Header }
         *
         */
        public void setHeader(PurchaseOrders.PO.Header value) {
            this.header = value;
        }

        /**
         * Gets the value of the detail property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the detail property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDetail().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PurchaseOrders.PO.Detail }
         *
         *
         */
        public List<PurchaseOrders.PO.Detail> getDetail() {
            if (detail == null) {
                detail = new ArrayList<PurchaseOrders.PO.Detail>();
            }
            return this.detail;
        }

        /**
         * Gets the value of the specialCharge property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the specialCharge property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSpecialCharge().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PurchaseOrders.PO.SpecialCharge }
         *
         *
         */
        public List<PurchaseOrders.PO.SpecialCharge> getSpecialCharge() {
            if (specialCharge == null) {
                specialCharge = new ArrayList<PurchaseOrders.PO.SpecialCharge>();
            }
            return this.specialCharge;
        }

        /**
         * Gets the value of the shippingAddress property.
         *
         * @return
         *     possible object is
         *     {@link PurchaseOrders.PO.ShippingAddress }
         *
         */
        public PurchaseOrders.PO.ShippingAddress getShippingAddress() {
            return shippingAddress;
        }

        /**
         * Sets the value of the shippingAddress property.
         *
         * @param value
         *     allowed object is
         *     {@link PurchaseOrders.PO.ShippingAddress }
         *
         */
        public void setShippingAddress(PurchaseOrders.PO.ShippingAddress value) {
            this.shippingAddress = value;
        }

        /**
         * Gets the value of the status property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setStatus(String value) {
            this.status = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="ItemType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="VersionID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="HPReqNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="CustItemNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="CustomerPONumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="TradingPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="CustomerAccountNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="ItemDescription1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="OriginalItemNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="ArtworkNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="POLineNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="ItemSequence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="POQty" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="POUOM" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="POUnitPrice" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="ShippingWarehouse" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="TrackingNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="ShippedDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
         *         &lt;element name="FreightChargeAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="FreightDiscountAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="TaxChargeAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="PromoDiscountAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="Error" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "itemType",
                "versionID",
                "hpReqNum",
                "custItemNumber",
                "customerPONumber",
                "tradingPartnerID",
                "companyCode",
                "customerAccountNum",
                "itemDescription1",
                "originalItemNumber",
                "artworkNumber",
                "poLineNum",
                "itemSequence",
                "poQty",
                "pouom",
                "poUnitPrice",
                "shippingWarehouse",
                "trackingNumber",
                "shippedDate",
                "freightChargeAmount",
                "freightDiscountAmount",
                "taxChargeAmount",
                "promoDiscountAmount"
        })
        public static class Detail {

            @XmlElement(name = "ItemType", required = true, defaultValue = "sheet")
            protected String itemType;
            @XmlElement(name = "VersionID", required = true, defaultValue = "0")
            protected String versionID;
            @XmlElement(name = "HPReqNum", required = true, defaultValue = "0")
            protected String hpReqNum;
            @XmlElement(name = "CustItemNumber", required = true)
            protected String custItemNumber;
            @XmlElement(name = "CustomerPONumber", required = true)
            protected String customerPONumber;
            @XmlElement(name = "TradingPartnerID", required = true)
            protected String tradingPartnerID;
            @XmlElement(name = "CompanyCode", required = true, defaultValue = "190")
            protected String companyCode;
            @XmlElement(name = "CustomerAccountNum", required = true)
            protected String customerAccountNum;
            @XmlElement(name = "ItemDescription1", required = true)
            protected String itemDescription1;
            @XmlElement(name = "OriginalItemNumber", required = true)
            protected String originalItemNumber;
            @XmlElement(name = "ArtworkNumber", required = true)
            protected String artworkNumber;
            @XmlElement(name = "POLineNum", required = true)
            protected String poLineNum;
            @XmlElement(name = "ItemSequence", required = true)
            protected String itemSequence;
            @XmlElement(name = "POQty", required = true)
            protected String poQty;
            @XmlElement(name = "POUOM", required = true)
            protected String pouom;
            @XmlElement(name = "POUnitPrice", required = true)
            protected String poUnitPrice;
            @XmlElement(name = "ShippingWarehouse", required = true, defaultValue = "MR")
            protected String shippingWarehouse;
            @XmlElement(name = "TrackingNumber", required = true)
            protected String trackingNumber;
            @XmlElement(name = "ShippedDate", required = true)
            @XmlSchemaType(name = "date")
            protected String shippedDate;
            @XmlElement(name = "FreightChargeAmount", required = true)
            protected String freightChargeAmount;
            @XmlElement(name = "FreightDiscountAmount", required = true)
            protected String freightDiscountAmount;
            @XmlElement(name = "TaxChargeAmount", required = true)
            protected String taxChargeAmount;
            @XmlElement(name = "PromoDiscountAmount", required = true)
            protected String promoDiscountAmount;
            @XmlAttribute(name = "Error")
            protected String error;
            @XmlAttribute(name = "Warning")
            protected String warning;

            /**
             * Gets the value of the itemType property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getItemType() {
                return itemType;
            }

            /**
             * Sets the value of the itemType property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setItemType(String value) {
                this.itemType = value;
            }

            /**
             * Gets the value of the versionID property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getVersionID() {
                return versionID;
            }

            /**
             * Sets the value of the versionID property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setVersionID(String value) {
                this.versionID = value;
            }

            /**
             * Gets the value of the hpReqNum property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getHPReqNum() {
                return hpReqNum;
            }

            /**
             * Sets the value of the hpReqNum property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setHPReqNum(String value) {
                this.hpReqNum = value;
            }

            /**
             * Gets the value of the custItemNumber property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCustItemNumber() {
                return custItemNumber;
            }

            /**
             * Sets the value of the custItemNumber property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCustItemNumber(String value) {
                this.custItemNumber = value;
            }

            /**
             * Gets the value of the customerPONumber property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCustomerPONumber() {
                return customerPONumber;
            }

            /**
             * Sets the value of the customerPONumber property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCustomerPONumber(String value) {
                this.customerPONumber = value;
            }

            /**
             * Gets the value of the tradingPartnerID property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getTradingPartnerID() {
                return tradingPartnerID;
            }

            /**
             * Sets the value of the tradingPartnerID property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setTradingPartnerID(String value) {
                this.tradingPartnerID = value;
            }

            /**
             * Gets the value of the companyCode property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCompanyCode() {
                return companyCode;
            }

            /**
             * Sets the value of the companyCode property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCompanyCode(String value) {
                this.companyCode = value;
            }

            /**
             * Gets the value of the customerAccountNum property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCustomerAccountNum() {
                return customerAccountNum;
            }

            /**
             * Sets the value of the customerAccountNum property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCustomerAccountNum(String value) {
                this.customerAccountNum = value;
            }

            /**
             * Gets the value of the itemDescription1 property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getItemDescription1() {
                return itemDescription1;
            }

            /**
             * Sets the value of the itemDescription1 property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setItemDescription1(String value) {
                this.itemDescription1 = value;
            }

            /**
             * Gets the value of the originalItemNumber property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getOriginalItemNumber() {
                return originalItemNumber;
            }

            /**
             * Sets the value of the originalItemNumber property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setOriginalItemNumber(String value) {
                this.originalItemNumber = value;
            }

            /**
             * Gets the value of the artworkNumber property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getArtworkNumber() {
                return artworkNumber;
            }

            /**
             * Sets the value of the artworkNumber property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setArtworkNumber(String value) {
                this.artworkNumber = value;
            }

            /**
             * Gets the value of the poLineNum property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getPOLineNum() {
                return poLineNum;
            }

            /**
             * Sets the value of the poLineNum property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setPOLineNum(String value) {
                this.poLineNum = value;
            }

            /**
             * Gets the value of the itemSequence property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getItemSequence() {
                return itemSequence;
            }

            /**
             * Sets the value of the itemSequence property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setItemSequence(String value) {
                this.itemSequence = value;
            }

            /**
             * Gets the value of the poQty property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getPOQty() {
                return poQty;
            }

            /**
             * Sets the value of the poQty property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setPOQty(String value) {
                this.poQty = value;
            }

            /**
             * Gets the value of the pouom property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getPOUOM() {
                return pouom;
            }

            /**
             * Sets the value of the pouom property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setPOUOM(String value) {
                this.pouom = value;
            }

            /**
             * Gets the value of the poUnitPrice property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getPOUnitPrice() {
                return poUnitPrice;
            }

            /**
             * Sets the value of the poUnitPrice property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setPOUnitPrice(String value) {
                this.poUnitPrice = value;
            }

            /**
             * Gets the value of the shippingWarehouse property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getShippingWarehouse() {
                return shippingWarehouse;
            }

            /**
             * Sets the value of the shippingWarehouse property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setShippingWarehouse(String value) {
                this.shippingWarehouse = value;
            }

            /**
             * Gets the value of the trackingNumber property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getTrackingNumber() {
                return trackingNumber;
            }

            /**
             * Sets the value of the trackingNumber property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setTrackingNumber(String value) {
                this.trackingNumber = value;
            }

            /**
             * Gets the value of the shippedDate property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getShippedDate() {
                return shippedDate;
            }

            /**
             * Sets the value of the shippedDate property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setShippedDate(String value) {
                this.shippedDate = value;
            }

            /**
             * Gets the value of the freightChargeAmount property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getFreightChargeAmount() {
                return freightChargeAmount;
            }

            /**
             * Sets the value of the freightChargeAmount property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setFreightChargeAmount(String value) {
                this.freightChargeAmount = value;
            }

            /**
             * Gets the value of the freightDiscountAmount property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getFreightDiscountAmount() {
                return freightDiscountAmount;
            }

            /**
             * Sets the value of the freightDiscountAmount property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setFreightDiscountAmount(String value) {
                this.freightDiscountAmount = value;
            }

            /**
             * Gets the value of the taxChargeAmount property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getTaxChargeAmount() {
                return taxChargeAmount;
            }

            /**
             * Sets the value of the taxChargeAmount property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setTaxChargeAmount(String value) {
                this.taxChargeAmount = value;
            }

            /**
             * Gets the value of the promoDiscountAmount property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getPromoDiscountAmount() {
                return promoDiscountAmount;
            }

            /**
             * Sets the value of the promoDiscountAmount property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setPromoDiscountAmount(String value) {
                this.promoDiscountAmount = value;
            }

            /**
             * Gets the value of the error property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getError() {
                return error;
            }

            /**
             * Sets the value of the error property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setError(String value) {
                this.error = value;
            }

            /**
             * Gets the value of the warning property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getWarning() {
                return warning;
            }

            /**
             * Sets the value of the warning property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setWarning(String value) {
                this.warning = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="OrderType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="CarrierCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="CarrierName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="CustomerPONum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="ASBOrderNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="CustomerAccountNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="DateOrderReceived" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
         *         &lt;element name="TradingPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="ASBPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="NumOfLinesOnThePO" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="OrderSourceCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="PODate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
         *         &lt;element name="POOrderType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="StoreNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="UPSTracking" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="ShippedDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
         *         &lt;element name="CostCenterString" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="MarketPlaceSiteId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
         *         &lt;element name="ChannelAdvisorOrderId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
         *         &lt;element name="MarketPlaceOrderId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="Error" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "orderType",
                "carrierCode",
                "carrierName",
                "companyCode",
                "customerPONum",
                "asbOrderNum",
                "customerAccountNum",
                "dateOrderReceived",
                "tradingPartnerID",
                "asbPartnerID",
                "numOfLinesOnThePO",
                "orderSourceCode",
                "poDate",
                "poOrderType",
                "storeNum",
                "upsTracking",
                "shippedDate",
                "costCenterString",
                "marketPlaceSiteId",
                "channelAdvisorOrderId",
                "marketPlaceOrderId"
        })
        public static class Header {

            @XmlElement(name = "OrderType", required = true, defaultValue = "shipment")
            protected String orderType;
            @XmlElement(name = "CarrierCode", required = true, defaultValue = "UPSG")
            protected String carrierCode;
            @XmlElement(name = "CarrierName", required = true, defaultValue = "UPS")
            protected String carrierName;
            @XmlElement(name = "CompanyCode", required = true, defaultValue = "190")
            protected String companyCode;
            @XmlElement(name = "CustomerPONum", required = true)
            protected String customerPONum;
            @XmlElement(name = "ASBOrderNum", required = true)
            protected String asbOrderNum;
            @XmlElement(name = "CustomerAccountNum", required = true)
            protected String customerAccountNum;
            @XmlElement(name = "DateOrderReceived", required = true)
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar dateOrderReceived;
            @XmlElement(name = "TradingPartnerID", required = true)
            protected String tradingPartnerID;
            @XmlElement(name = "ASBPartnerID", required = true)
            protected String asbPartnerID;
            @XmlElement(name = "NumOfLinesOnThePO", required = true)
            protected String numOfLinesOnThePO;
            @XmlElement(name = "OrderSourceCode", required = true, defaultValue = "ED")
            protected String orderSourceCode;
            @XmlElement(name = "PODate", required = true)
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar poDate;
            @XmlElement(name = "POOrderType", required = true, defaultValue = "SS")
            protected String poOrderType;
            @XmlElement(name = "StoreNum", required = true, defaultValue = "ASB WEB")
            protected String storeNum;
            @XmlElement(name = "UPSTracking", required = true)
            protected String upsTracking;
            @XmlElement(name = "ShippedDate", required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar shippedDate;
            @XmlElement(name = "CostCenterString", required = true, defaultValue = "")
            protected String costCenterString;
            @XmlElement(name = "MarketPlaceSiteId")
            protected Integer marketPlaceSiteId;
            @XmlElement(name = "ChannelAdvisorOrderId")
            protected Integer channelAdvisorOrderId;
            @XmlElement(name = "MarketPlaceOrderId")
            protected String marketPlaceOrderId;
            @XmlAttribute(name = "Error")
            protected String error;
            @XmlAttribute(name = "Warning")
            protected String warning;

            /**
             * Gets the value of the orderType property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getOrderType() {
                return orderType;
            }

            /**
             * Sets the value of the orderType property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setOrderType(String value) {
                this.orderType = value;
            }

            /**
             * Gets the value of the carrierCode property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCarrierCode() {
                return carrierCode;
            }

            /**
             * Sets the value of the carrierCode property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCarrierCode(String value) {
                this.carrierCode = value;
            }

            /**
             * Gets the value of the carrierName property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCarrierName() {
                return carrierName;
            }

            /**
             * Sets the value of the carrierName property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCarrierName(String value) {
                this.carrierName = value;
            }

            /**
             * Gets the value of the companyCode property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCompanyCode() {
                return companyCode;
            }

            /**
             * Sets the value of the companyCode property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCompanyCode(String value) {
                this.companyCode = value;
            }

            /**
             * Gets the value of the customerPONum property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCustomerPONum() {
                return customerPONum;
            }

            /**
             * Sets the value of the customerPONum property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCustomerPONum(String value) {
                this.customerPONum = value;
            }

            /**
             * Gets the value of the asbOrderNum property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getASBOrderNum() {
                return asbOrderNum;
            }

            /**
             * Sets the value of the asbOrderNum property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setASBOrderNum(String value) {
                this.asbOrderNum = value;
            }

            /**
             * Gets the value of the customerAccountNum property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCustomerAccountNum() {
                return customerAccountNum;
            }

            /**
             * Sets the value of the customerAccountNum property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCustomerAccountNum(String value) {
                this.customerAccountNum = value;
            }

            /**
             * Gets the value of the dateOrderReceived property.
             *
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *
             */
            public XMLGregorianCalendar getDateOrderReceived() {
                return dateOrderReceived;
            }

            /**
             * Sets the value of the dateOrderReceived property.
             *
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *
             */
            public void setDateOrderReceived(XMLGregorianCalendar value) {
                this.dateOrderReceived = value;
            }

            /**
             * Gets the value of the tradingPartnerID property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getTradingPartnerID() {
                return tradingPartnerID;
            }

            /**
             * Sets the value of the tradingPartnerID property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setTradingPartnerID(String value) {
                this.tradingPartnerID = value;
            }

            /**
             * Gets the value of the asbPartnerID property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getASBPartnerID() {
                return asbPartnerID;
            }

            /**
             * Sets the value of the asbPartnerID property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setASBPartnerID(String value) {
                this.asbPartnerID = value;
            }

            /**
             * Gets the value of the numOfLinesOnThePO property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getNumOfLinesOnThePO() {
                return numOfLinesOnThePO;
            }

            /**
             * Sets the value of the numOfLinesOnThePO property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setNumOfLinesOnThePO(String value) {
                this.numOfLinesOnThePO = value;
            }

            /**
             * Gets the value of the orderSourceCode property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getOrderSourceCode() {
                return orderSourceCode;
            }

            /**
             * Sets the value of the orderSourceCode property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setOrderSourceCode(String value) {
                this.orderSourceCode = value;
            }

            /**
             * Gets the value of the poDate property.
             *
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *
             */
            public XMLGregorianCalendar getPODate() {
                return poDate;
            }

            /**
             * Sets the value of the poDate property.
             *
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *
             */
            public void setPODate(XMLGregorianCalendar value) {
                this.poDate = value;
            }

            /**
             * Gets the value of the poOrderType property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getPOOrderType() {
                return poOrderType;
            }

            /**
             * Sets the value of the poOrderType property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setPOOrderType(String value) {
                this.poOrderType = value;
            }

            /**
             * Gets the value of the storeNum property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getStoreNum() {
                return storeNum;
            }

            /**
             * Sets the value of the storeNum property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setStoreNum(String value) {
                this.storeNum = value;
            }

            /**
             * Gets the value of the upsTracking property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getUPSTracking() {
                return upsTracking;
            }

            /**
             * Sets the value of the upsTracking property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setUPSTracking(String value) {
                this.upsTracking = value;
            }

            /**
             * Gets the value of the shippedDate property.
             *
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *
             */
            public XMLGregorianCalendar getShippedDate() {
                return shippedDate;
            }

            /**
             * Sets the value of the shippedDate property.
             *
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *
             */
            public void setShippedDate(XMLGregorianCalendar value) {
                this.shippedDate = value;
            }

            /**
             * Gets the value of the costCenterString property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCostCenterString() {
                return costCenterString;
            }

            /**
             * Sets the value of the costCenterString property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCostCenterString(String value) {
                this.costCenterString = value;
            }

            /**
             * Gets the value of the marketPlaceSiteId property.
             *
             */
            public Integer getMarketPlaceSiteId() {
                return marketPlaceSiteId;
            }

            /**
             * Sets the value of the marketPlaceSiteId property.
             *
             */
            public void setMarketPlaceSiteId(Integer value) {
                this.marketPlaceSiteId = value;
            }

            /**
             * Gets the value of the channelAdvisorOrderId property.
             *
             */
            public Integer getChannelAdvisorOrderId() {
                return channelAdvisorOrderId;
            }

            /**
             * Sets the value of the channelAdvisorOrderId property.
             *
             */
            public void setChannelAdvisorOrderId(Integer value) {
                this.channelAdvisorOrderId = value;
            }

            /**
             * Gets the value of the marketPlaceOrderId property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getMarketPlaceOrderId() {
                return marketPlaceOrderId;
            }

            /**
             * Sets the value of the marketPlaceOrderId property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setMarketPlaceOrderId(String value) {
                this.marketPlaceOrderId = value;
            }

            /**
             * Gets the value of the error property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getError() {
                return error;
            }

            /**
             * Sets the value of the error property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setError(String value) {
                this.error = value;
            }

            /**
             * Gets the value of the warning property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getWarning() {
                return warning;
            }

            /**
             * Sets the value of the warning property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setWarning(String value) {
                this.warning = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="CustomerPONumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="TradingPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="CustomerAccountNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="AddressLine1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="AddressLine2" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="AddressLine3" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Zip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="Error" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "customerPONumber",
                "tradingPartnerID",
                "companyCode",
                "customerAccountNum",
                "name",
                "addressLine1",
                "addressLine2",
                "addressLine3",
                "city",
                "state",
                "zip",
                "country"
        })
        public static class ShippingAddress {

            @XmlElement(name = "CustomerPONumber", required = true)
            protected String customerPONumber;
            @XmlElement(name = "TradingPartnerID", required = true)
            protected String tradingPartnerID;
            @XmlElement(name = "CompanyCode", required = true, defaultValue = "190")
            protected String companyCode;
            @XmlElement(name = "CustomerAccountNum", required = true)
            protected String customerAccountNum;
            @XmlElement(name = "Name", required = true)
            protected String name;
            @XmlElement(name = "AddressLine1", required = true)
            protected String addressLine1;
            @XmlElement(name = "AddressLine2", required = true)
            protected String addressLine2;
            @XmlElement(name = "AddressLine3", required = true, defaultValue = "")
            protected String addressLine3;
            @XmlElement(name = "City", required = true)
            protected String city;
            @XmlElement(name = "State", required = true)
            protected String state;
            @XmlElement(name = "Zip", required = true)
            protected String zip;
            @XmlElement(name = "Country", required = true)
            protected String country;
            @XmlAttribute(name = "Error")
            protected String error;
            @XmlAttribute(name = "Warning")
            protected String warning;

            /**
             * Gets the value of the customerPONumber property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCustomerPONumber() {
                return customerPONumber;
            }

            /**
             * Sets the value of the customerPONumber property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCustomerPONumber(String value) {
                this.customerPONumber = value;
            }

            /**
             * Gets the value of the tradingPartnerID property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getTradingPartnerID() {
                return tradingPartnerID;
            }

            /**
             * Sets the value of the tradingPartnerID property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setTradingPartnerID(String value) {
                this.tradingPartnerID = value;
            }

            /**
             * Gets the value of the companyCode property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCompanyCode() {
                return companyCode;
            }

            /**
             * Sets the value of the companyCode property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCompanyCode(String value) {
                this.companyCode = value;
            }

            /**
             * Gets the value of the customerAccountNum property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCustomerAccountNum() {
                return customerAccountNum;
            }

            /**
             * Sets the value of the customerAccountNum property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCustomerAccountNum(String value) {
                this.customerAccountNum = value;
            }

            /**
             * Gets the value of the name property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the addressLine1 property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getAddressLine1() {
                return addressLine1;
            }

            /**
             * Sets the value of the addressLine1 property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setAddressLine1(String value) {
                this.addressLine1 = value;
            }

            /**
             * Gets the value of the addressLine2 property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getAddressLine2() {
                return addressLine2;
            }

            /**
             * Sets the value of the addressLine2 property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setAddressLine2(String value) {
                this.addressLine2 = value;
            }

            /**
             * Gets the value of the addressLine3 property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getAddressLine3() {
                return addressLine3;
            }

            /**
             * Sets the value of the addressLine3 property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setAddressLine3(String value) {
                this.addressLine3 = value;
            }

            /**
             * Gets the value of the city property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCity() {
                return city;
            }

            /**
             * Sets the value of the city property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCity(String value) {
                this.city = value;
            }

            /**
             * Gets the value of the state property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getState() {
                return state;
            }

            /**
             * Sets the value of the state property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setState(String value) {
                this.state = value;
            }

            /**
             * Gets the value of the zip property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getZip() {
                return zip;
            }

            /**
             * Sets the value of the zip property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setZip(String value) {
                this.zip = value;
            }

            /**
             * Gets the value of the country property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCountry() {
                return country;
            }

            /**
             * Sets the value of the country property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCountry(String value) {
                this.country = value;
            }

            /**
             * Gets the value of the error property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getError() {
                return error;
            }

            /**
             * Sets the value of the error property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setError(String value) {
                this.error = value;
            }

            /**
             * Gets the value of the warning property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getWarning() {
                return warning;
            }

            /**
             * Sets the value of the warning property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setWarning(String value) {
                this.warning = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         *
         * <p>The following schema fragment specifies the expected content contained within this class.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="CustomerPONumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="TradingPartnerID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="CustomerAccountNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="SpecialChargeAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="SpecialChargeCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="SpecialChargeDesc" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="Error" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "customerPONumber",
                "tradingPartnerID",
                "companyCode",
                "customerAccountNum",
                "specialChargeAmount",
                "specialChargeCode",
                "specialChargeDesc"
        })
        public static class SpecialCharge {

            @XmlElement(name = "CustomerPONumber", required = true)
            protected String customerPONumber;
            @XmlElement(name = "TradingPartnerID", required = true)
            protected String tradingPartnerID;
            @XmlElement(name = "CompanyCode", required = true, defaultValue = "190")
            protected String companyCode;
            @XmlElement(name = "CustomerAccountNum", required = true)
            protected String customerAccountNum;
            @XmlElement(name = "SpecialChargeAmount", required = true)
            protected String specialChargeAmount;
            @XmlElement(name = "SpecialChargeCode", required = true, defaultValue = "FRT")
            protected String specialChargeCode;
            @XmlElement(name = "SpecialChargeDesc", required = true, defaultValue = "FREIGHT")
            protected String specialChargeDesc;
            @XmlAttribute(name = "Error")
            protected String error;
            @XmlAttribute(name = "Warning")
            protected String warning;

            /**
             * Gets the value of the customerPONumber property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCustomerPONumber() {
                return customerPONumber;
            }

            /**
             * Sets the value of the customerPONumber property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCustomerPONumber(String value) {
                this.customerPONumber = value;
            }

            /**
             * Gets the value of the tradingPartnerID property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getTradingPartnerID() {
                return tradingPartnerID;
            }

            /**
             * Sets the value of the tradingPartnerID property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setTradingPartnerID(String value) {
                this.tradingPartnerID = value;
            }

            /**
             * Gets the value of the companyCode property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCompanyCode() {
                return companyCode;
            }

            /**
             * Sets the value of the companyCode property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCompanyCode(String value) {
                this.companyCode = value;
            }

            /**
             * Gets the value of the customerAccountNum property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCustomerAccountNum() {
                return customerAccountNum;
            }

            /**
             * Sets the value of the customerAccountNum property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCustomerAccountNum(String value) {
                this.customerAccountNum = value;
            }

            /**
             * Gets the value of the specialChargeAmount property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getSpecialChargeAmount() {
                return specialChargeAmount;
            }

            /**
             * Sets the value of the specialChargeAmount property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setSpecialChargeAmount(String value) {
                this.specialChargeAmount = value;
            }

            /**
             * Gets the value of the specialChargeCode property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getSpecialChargeCode() {
                return specialChargeCode;
            }

            /**
             * Sets the value of the specialChargeCode property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setSpecialChargeCode(String value) {
                this.specialChargeCode = value;
            }

            /**
             * Gets the value of the specialChargeDesc property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getSpecialChargeDesc() {
                return specialChargeDesc;
            }

            /**
             * Sets the value of the specialChargeDesc property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setSpecialChargeDesc(String value) {
                this.specialChargeDesc = value;
            }

            /**
             * Gets the value of the error property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getError() {
                return error;
            }

            /**
             * Sets the value of the error property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setError(String value) {
                this.error = value;
            }

            /**
             * Gets the value of the warning property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getWarning() {
                return warning;
            }

            /**
             * Sets the value of the warning property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setWarning(String value) {
                this.warning = value;
            }

        }

    }

}
