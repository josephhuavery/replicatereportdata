package com.avery.dify;

import com.avery.Config;
import com.avery.ReplicateReportData;
import com.avery.dify.util.DIFYProperty;
import com.github.davidmoten.rx.jdbc.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

import java.util.Date;

public class ReplicateNightly {
    private static Logger logger = LoggerFactory.getLogger(ReplicateNightly.class);

    // Should not be run at the same time as ReplicateReportData
    public static void main(String[] args) throws ClassNotFoundException {
        long start = System.currentTimeMillis();

        Config config = CommandLine.populateCommand(new Config(), args);
        if (config.help) {
            CommandLine.usage(new Config(), System.err);
            return;
        }

        switch(config.environment){
            case 0:
                logger.info("{} Job Start. Window Test Environment", new Date());
                break;
            case 1:
                logger.info("{} Job Start. Dev Server Environment", new Date());
                break;
            case 2:
                logger.info("{} Job Start. Production Environment", new Date());
                break;
            default:
                return;
        }
        DIFYProperty prop = new DIFYProperty(config.environment);
        // Oracle's own JDBC driver does not support auto discovery
        Class.forName(DIFYProperty.DB_DRIVER);

        String srcUrl = DIFYProperty.SRC_DB_URL;
        String srcUsername = DIFYProperty.SRC_DB_UID;
        String srcPassword = DIFYProperty.SRC_DB_PWD;

        String destUrl = DIFYProperty.DEST_DB_URL;
        String destUsername = DIFYProperty.DEST_DB_UID;
        String destPassword = DIFYProperty.DEST_DB_PWD;

        Database srcDatabase = Database.from(srcUrl, srcUsername, srcPassword);
        Database destDatabase = Database.from(destUrl, destUsername, destPassword);

        OrdersReplicator.replicateNightly(srcDatabase, destDatabase);
        int count = destDatabase.update("{call update_customer_type()}").execute();

        long end = System.currentTimeMillis();
        logger.info("Result of calling update_customer_type=" + count);
        logger.info("{} Nightly replicate has been finished. Elapsed time total : " + (end-start)/1000 + " second", new Date());
    }
}
