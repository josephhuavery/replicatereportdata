/*
 * XmlUtils.java
 *
 * This software is the confidential and proprietary information of
 * Avery Dennison ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Avery Dennison.
 */
package com.avery.dify.util;

import org.apache.xalan.serialize.Serializer;
import org.apache.xalan.serialize.SerializerFactory;
import org.apache.xalan.templates.OutputProperties;

// Misc imports
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

// Java imports
import java.io.StringWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;


/**
 * Contains collection of XML utilities
 * Feb 09,2002
 * @author Yantra
 */
public class XmlUtils {
    /**
    * Checks if node name equals given string
    *
    * @param  node Node Object
    * @param  name String value that needs to be compared with node
    * name
    * @return <code>true</code> If node name equals given string;
    * <code>false</code> otherwise
    * @throws         IllegalArgumentException If invalid input
    */
    protected static boolean isSameName(Node node, String name)
        throws IllegalArgumentException {
        //Validate node
        if (node == null) {
            throw new IllegalArgumentException("Node cannot be " +
                " null in XmlUtils.isSameName method");
        }

        //Validate node name
        if (name == null) {
            throw new IllegalArgumentException("Node name cannot be " +
                " null in XmlUtils.isSameName method");
        }

        return (node.getNodeName().equals(name));
    }

    /**
    * Constructs XML String from given document object. This
    * method returns null if document is empty
    *
    * @param  document XML Document
    * @return XML String
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static String getXmlString(Document document)
            throws IllegalArgumentException, Exception {
    	return getXmlStringFormatted(document, true);
    }

    public static String getXmlStringFormatted(Document document, boolean indent)
        throws IllegalArgumentException, Exception {
        //Root node
        Node rootNode = null;

        //Validate document
        if (document == null) {
            throw new IllegalArgumentException("Document cannot " +
                "be null in XmlUtils.getXMLString method");
        }

        //Get root element
        rootNode = document.getDocumentElement();

        //If root element does not exist throw null
        if (rootNode == null) {
            return null;
        }

        OutputFormat format = new OutputFormat(document);
        format.setIndenting(false);
        if (indent) {
	        format.setLineWidth(512);
	        format.setIndenting(true);
	        format.setIndent(2);
        }
        
        StringWriter stringOut = new StringWriter();
        XMLSerializer serial = new XMLSerializer(stringOut, format);
        serial.asDOMSerializer();
        serial.serialize(document.getDocumentElement());

        String output = stringOut.toString();
		if (output.contains("�")) 
			output = output.replaceAll("�", "&#x00AE;");
		else if (output.contains("�")) 
			output = output.replaceAll("�", "&#x00A9;");
		else if (output.contains("�")) 
			output = output.replaceAll("�", "&#x2122;");

        return output;
    }

    /**
    * Constructs XML String from given element object
    *
    * @param  element Element object
    * @return XML String
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static String getElementString(Element inputElement)
        throws IllegalArgumentException, Exception {
        //Import element content and construct Document
        Document document = getDocument(inputElement, true);

        //Convert document as element string
        String xmlString = getXmlString(document);

        //Remove Processing Instruction from xml string if exists
        xmlString = removeProcessingInstruction(xmlString);

        return xmlString;
    }

    /**
     * Removes processing instruction from input XML String
     * @param XML String thay may contain processing instruction
     * @return XML String
     */
    public static String removeProcessingInstruction(String xmlString) {
        if ((xmlString != null) &&
                (xmlString.toLowerCase().trim().startsWith("<?xml"))) {
            int processInstructionEndIndex = xmlString.indexOf("?>");

            if (processInstructionEndIndex != -1) {
                xmlString = xmlString.substring(processInstructionEndIndex + 2);
            }
        }

        return xmlString;
    }

    /**
     * Creates and empty Document object
     * @throws Exception for all exceptions
     */
    public static Document getDocument() throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder builder = factory.newDocumentBuilder();

        return builder.newDocument();
    }

    /**
    * Constructs Document object from XML String.
    * Uses DOMParser to generate a document from a string coded
    * as a well-formed XML. This method should be sparingly used
    * to build XMLs as all XMLs should be attempted to be built
    * using Nodes and elements from scratch. The advantage of
    * this is that all encoding of special characters is
    * automatically taken care of, and is also the recommended
    * way of building XML.
    * @param  inputValue XML String
    * @return XML Document object
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static Document getDocument(String inputValue)
        throws IllegalArgumentException, Exception {
        //Validate input value
        if (inputValue == null) {
            throw new IllegalArgumentException("Input string" +
                " cannot be null in XmlUtils.getDocument method");
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(new ByteArrayInputStream(
                    inputValue.getBytes()));

        return document;
    }

    /**
    * Finds all elements with specified tag name and returns node
    * list. Do not use this method if you have to access parent elements
    * using the the result node.
    * @param  element Element object
    * @param  elementName Element name to search for
    * @return NodeList List of nodes with given name
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static NodeList getElementsByTagName(Element element,
        String elementName) throws IllegalArgumentException, Exception {
        Document document;

        //Validate input element
        if (element == null) {
            throw new IllegalArgumentException("Element" +
                " cannot be null in XmlUtils.getDocument method");
        }

        //Validate child element name
        if (elementName == null) {
            throw new IllegalArgumentException("Element name cannot" +
                " be null in XmlUtils.getChildListByName method");
        }

        document = getDocument(element, true);

        return document.getElementsByTagName(elementName);
    }

    /**
    * Imports content of element to a new Document object
    * @param  element Element object
    * @param deep Include child nodes of this element true/false
    * @return XML Document object
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static Document getDocument(Element element, boolean deep)
        throws IllegalArgumentException, Exception {
        //Validate input element
        if (element == null) {
            throw new IllegalArgumentException("Element" +
                " cannot be null in XmlUtils.getDocument method");
        }

        //Create a new document
        Document document = getDocument();

        //Import data from input element
        Element resultElement = (Element) document.importNode(element, deep);

        //Append imported content
        document.appendChild(resultElement);

        return document;
    }

    /**
    * Loads and constructs XML Document object from file
    *
    * @param  fileName XML Filename with path
    * @return XML Document object
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static Document getXmlFromFile(String fileName)
        throws IllegalArgumentException, Exception {
        //Validate file name
        if (fileName == null) {
            throw new IllegalArgumentException("Filename cannot be" +
                " null in XmlUtils.getXMLfromfile method");
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(new File(fileName));

        return document;
    }

    /**
    * Writes content of XML Document to a file
    *
    * @param  document XML Document Object
    * @param  filename Output filename with path
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static void writeXmlToFile(Document document, String fileName)
        throws IllegalArgumentException, Exception {
        //Validate document
        if (document == null) {
            throw new IllegalArgumentException("Document cannot " +
                "be null in XmlUtils.writeXMLtofile method");
        }

        //Validate file name
        if (fileName == null) {
            throw new IllegalArgumentException("Filename cannot " +
                "be null in XmlUtils.writeXMLtofile method");
        }

        String xmlString = getXmlString(document);
        FileOutputStream fileHandle = new FileOutputStream(fileName);
        DataOutputStream dosHandle = new DataOutputStream(fileHandle);
        dosHandle.writeBytes(xmlString);
        dosHandle.close();
        fileHandle.close();
    }

    /**
    * Sets a new attribute to element
    *
    * @param  element Element Object
    * @param  attributeName Attribute Name
    * @param  attributeValue Attribute Value
    * @return Element object after adding new attribute
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static Element setAttribute(Element element, String attributeName,
        String attributeValue) throws IllegalArgumentException, Exception {
        //Validate element
        if (element == null) {
            throw new IllegalArgumentException("Element cannot " +
                " be null in XmlUtils.setAttribute method");
        }

        //Validate attribute name
        if (attributeName == null) {
            throw new IllegalArgumentException("Attribute Name " +
                " cannot be null in XmlUtils.setAttribute method");
        }

        element.setAttribute(attributeName, attributeValue);

        return element;
    }

    /**
     * Sets a new attribute to element
     *
     * @param  element Element Object
     * @param  attributeName Attribute Name
     * @param  attributeValue Attribute Value
     * @param  defaultValue Default Value
     * @return Element object after adding new attribute
     * @throws IllegalArgumentException for Invalid input
     * @throws Exception for all others
     */
     public static Element setAttribute(Element element, String attributeName,
         String attributeValue, String defaultValue) throws IllegalArgumentException, Exception {
         //Validate element
         if (element == null) {
             throw new IllegalArgumentException("Element cannot " +
                 " be null in XmlUtils.setAttribute method");
         }

         //Validate attribute name
         if (attributeName == null) {
             throw new IllegalArgumentException("Attribute Name " +
                 " cannot be null in XmlUtils.setAttribute method");
         }

         //Validate attribute value
         if (attributeValue == null) {
             throw new IllegalArgumentException("Attribute Value " +
                 " cannot be null in XmlUtils.setAttribute method");
         }

         //Validate default value
         if (defaultValue == null) {
             throw new IllegalArgumentException("Default Value " +
                 " cannot be null in XmlUtils.setAttribute method");
         }

         if (attributeValue.length() == 0)
            element.setAttribute(attributeName, defaultValue);
         else
         	element.setAttribute(attributeName, attributeValue);

         return element;
     }

    /**
    * Gets value of an attribute from node
    *
    * @param  node Node Object
    * @param  attributeName Attribute Name
    * @return Attribute Value
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static String getAttribute(Node node, String attributeName)
        throws IllegalArgumentException, Exception {
        //Validate node
        if (node == null) {
            throw new IllegalArgumentException("Node cannot " +
                " be null in XmlUtils.getAttribute method");
        }

        //Validate attribute name
        if (attributeName == null) {
            throw new IllegalArgumentException("Attribute Name " +
                " cannot be null in XmlUtils.getAttribute method");
        }

        NamedNodeMap attributeList = node.getAttributes();
        Node attribute = attributeList.getNamedItem(attributeName);

        //Validate attribute name
        if (attribute == null) {
            throw new IllegalArgumentException("Invalid Attribute Name " +
                attributeName + " in XmlUtils.getAttribute method");
        }

        return ((Attr) attribute).getValue();
    }

    /**
    * Gets value of an attribute from node. Returns default value
    * if attribute not found.
    *
    * @param  node Node Object
    * @param  attributeName Attribute Name
    * @param  defaultValue Default value if attribute not found
    * @return Attribute Value
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static String getAttribute(Node node, String attributeName,
        String defaultValue) throws IllegalArgumentException, Exception {
        //Validate node
        if (node == null) {
            throw new IllegalArgumentException("Node cannot " +
                " be null in XmlUtils.getAttribute method");
        }

        //Validate attribute name
        if (attributeName == null) {
            throw new IllegalArgumentException("Attribute Name " +
                " cannot be null in XmlUtils.getAttribute method");
        }

        NamedNodeMap attributeList = node.getAttributes();
        Node attribute = attributeList.getNamedItem(attributeName);

        //Validate attribute name
        if (attribute == null) {
            return defaultValue;
        }

        return ((Attr) attribute).getValue();
    }

    /**
    * Calls setAttribute for all enumerations in the map. This way
    * multiple attributes can be set in one go.
    *
    * @param  element Element
    * @param  attribute Map Set of attributes to be set
    * @return Element object after attribute setting
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static Element setManyAttributes(Element element, Map<String, String> attributeMap)
        throws IllegalArgumentException, Exception {
        //Validate element
        if (element == null) {
            throw new IllegalArgumentException("Element cannot " +
                " be null in XmlUtils.setManyAttributes method");
        }

        //Validate attribute map
        if (attributeMap == null) {
            throw new IllegalArgumentException("Attribute map " +
                "cannot be null in XmlUtils.setManyAttributes method");
        }

        Object[] keyList = attributeMap.keySet().toArray();

        for (int index = 0; index < keyList.length; index++) {
            String attributeName = StringUtils.getStringValue(keyList[index]);

            String attributeValue = StringUtils.getStringValue(attributeMap.get(
                        keyList[index]));

            element = setAttribute(element, attributeName, attributeValue);
        }

        return element;
    }

    /**
    * Loads all attributes in given element to a Map
    *
    * @param  element Element
    * @return Map object containing all attributes of given element
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static Map<String, String> getManyAttributes(Element element)
        throws IllegalArgumentException, Exception {
        //Validate element
        if (element == null) {
            throw new IllegalArgumentException("Element cannot " +
                " be null in XmlUtils.getManyAttributes method");
        }

        NamedNodeMap attributeList = element.getAttributes();
        Map<String, String> map = Collections.synchronizedMap(new HashMap<String, String>());

        for (int index = 0; index < attributeList.getLength(); index++) {
            Node attributeNode = attributeList.item(index);

            if (attributeNode == null) {
                continue;
            }

            Attr attribute = (Attr) attributeNode;
            map.put(attribute.getName(), attribute.getValue());
        }

        return map;
    }

    /**
    * Goes through the first child and its siblings, till the name
    * of the node matches the name of the childName passed as input,
    * when it returns the current node. When constructing an XML, you
    * may like to check if the XML already has the child node you
    * are trying to construct or if you would like to create the
    * node and return it in the event that you do not have
    * the node.( For example, lets say you have to add an element
    * to the Orderlines node which is a child of the order node.
    * You are currently at the order node. So when you ask for
    * the child node - Orderlines from this function you ask
    * him to create one in the event that it did not already have one)
    *
    * @param  node Node object
    * @param  childName Name of child node looking for
    * @param  createNode If true, appends a child node with given name
    * when not found
    * @return Node Child Node
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception if node not found and createNode is false; and
    * all others
    */
    public static Node getChildNodeByName(Node node, String childName,
        boolean createNode) throws IllegalArgumentException, Exception {
        //Validate node
        if (node == null) {
            throw new IllegalArgumentException("Node cannot " +
                "be null in XmlUtils.getChildNodebyName method");
        }

        //Validate child name
        if (childName == null) {
            throw new IllegalArgumentException("Child name cannot" +
                " be null in XmlUtils.getChildNodebyName method");
        }

        NodeList childList = node.getChildNodes();

        if (childList != null) {
            for (int childIndex = 0; childIndex < childList.getLength();
                    childIndex++) {
                if (isSameName(childList.item(childIndex), childName)) {
                    return childList.item(childIndex);
                }
            }
        }

        if (createNode) {
            Node newNode = node.getOwnerDocument().createElement(childName);
            node.appendChild(newNode);

            return newNode;
        }

        throw new Exception("Node " + childName + " not found in " +
            "XmlUtils.getChildNodebyName method");
    }

    /**
    * Goes through each sibling of node, till the name
    * of the node matches the siblingName passed as input,
    *
    * @param  node Node object
    * @param  siblingName Name of sibling node looking for
    * @param  createNode If true, adds a sibling with given
    * sibling name when not found
    * @return Node Sibling Node
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception if node not found and createNode is false;
    * and all others
    */
    public static Node getNextSiblingByName(Node node, String siblingName,
        boolean createNode) throws IllegalArgumentException, Exception {
        //Validate node
        if (node == null) {
            throw new IllegalArgumentException("Node cannot be " +
                " null in XmlUtils.getChildNodebyName method");
        }

        //Validate sibling name
        if (siblingName == null) {
            throw new IllegalArgumentException("Sibling name cannot" +
                " be null in XmlUtils.getChildNodebyName method");
        }

        Node siblingNode = node.getNextSibling();

        if (siblingNode == null) {
            if (createNode) {
                Node newNode = node.getOwnerDocument().createElement(siblingName);
                node.getParentNode().appendChild(newNode);

                return newNode;
            } else {
                throw new Exception("Node " + siblingName + "not  " +
                    "found in XmlUtils.getNextSiblingbyName method");
            }
        }

        if (isSameName(siblingNode, siblingName)) {
            return siblingNode;
        }

        return getNextSiblingByName(siblingNode, siblingName, createNode);
    }

    /**
     * Determines if a child exists with given name
     * @throws IllegalArgumentException if input is invalid
     * @throws Exception for all errors
     */
    public static boolean hasChildNode(Node node, String inputChildNodeName)
        throws IllegalArgumentException, Exception {
        //Validate node
        if (node == null) {
            throw new IllegalArgumentException("Node cannot be " +
                " null in XmlUtils.hasChildNode method");
        }

        //Validate Child node name
        if (inputChildNodeName == null) {
            throw new IllegalArgumentException("Input Child Node " +
                " name cannot be null in XmlUtils.hasChildNode method");
        }

        NodeList nodeList = node.getChildNodes();

        if ((nodeList == null) || (nodeList.getLength() < 1)) {
            return false;
        }

        //Compare each child name and return true if match found
        for (int index = 0; index < nodeList.getLength(); index++) {
            Node childNode = nodeList.item(index);
            String childNodeName = childNode.getNodeName();

            if (StringUtils.isSame(childNodeName, inputChildNodeName)) {
                return true;
            }
        }

        //If no children found with given name then return false
        return false;
    }

    /**
     * This method converts input stream
     * as an XML document
     * @param inputStream Input Stream
     * @return Content of input stream as an XML Document object
     * @throws IllegalArgumentException if input is not valid
     * @throws Exception incase of any exceptions
     */
    public static Document getDocument(InputStream inputStream)
        throws IllegalArgumentException, Exception {
        //Validate input stream
        if (inputStream == null) {
            throw new IllegalArgumentException(
                "Input Stream cannot be null in " + "XmlUtils.getDocument");
        }

        //Create document factory
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //Create document builder
        DocumentBuilder builder = factory.newDocumentBuilder();

        //Parse input stream as document
        Document document = builder.parse(inputStream);

        //return output document
        return document;
    }

    /**
     * This method returns node value for given child.  If there is no
     * text available for given node, then this method returns null
     * @return Node value of input node
     * @throws IllegalArgumentException if input is invalid
     * @throws Exception incase of any other exceptions
     */
    public static String getNodeValue(Node inputNode)
        throws IllegalArgumentException, Exception {
        //Child count
        int childCount = 0;

        //Validate input stream
        if (inputNode == null) {
            throw new IllegalArgumentException("Input Node cannot be null in " +
                "XmlUtils.getNodeValue");
        }

        //Return null if child not found
        NodeList childList = inputNode.getChildNodes();

        if ((childList == null) || (childList.getLength() < 1)) {
            return null;
        }

        //Get child count
        childCount = childList.getLength();

        //For each child
        for (int childIndex = 0; childIndex < childCount; childIndex++) {
            //Get each child
            Node childNode = childList.item(childIndex);

            //Check if text node
            if (childNode.getNodeType() == Node.TEXT_NODE) {
                //Return node value
                return childNode.getNodeValue();
            }
        }

        //If no text node found return null
        return null;
    }

    /**
     * This method sets value to given node
     * @param inputNode Node to which value needs to be set
     * @param nodeValue Value to set
     * @throws IllegalArgumentException if input is invalid
     * @throws Exception incase of any other exceptions
     */
    public static void setNodeValue(Node inputNode, String nodeValue)
        throws IllegalArgumentException, Exception {
        //Child list
        NodeList childList = null;

        //Validate input stream
        if (inputNode == null) {
            throw new IllegalArgumentException("Input Node cannot be null in " +
                "XmlUtils.setNodeValue");
        }

        //Get child list
        childList = inputNode.getChildNodes();

        //If child nodes found
        if ((childList != null) && (childList.getLength() > 0)) {
            //Get child count
            int childCount = childList.getLength();

            //For each child
            for (int childIndex = 0; childIndex < childCount; childIndex++) {
                //Check if text node
                if (childList.item(childIndex).getNodeType() == Node.TEXT_NODE) {
                    //Set value to text node
                    childList.item(childIndex).setNodeValue(nodeValue);

                    return;
                }
            }
        }

        //Create text node and set node value
        inputNode.appendChild(inputNode.getOwnerDocument().createTextNode(nodeValue));
    }

    /**
     * This method imports specified child node for target document
     * and returns reference to Node
     * @param targetDocument Target document for which the child node
     * needs to be imported
     * @param inputNode Source Node whose child node needs
     * to be imported
     * @param childNodeName Name of child
     * @param deep Include nodes while importing true/false
     * @return Reference to imported child node
     * @throws Exception incase of any exception
     */
    public static Node importChildNode(Document targetDocument, Node inputNode,
        String childNodeName, boolean deep)
        throws IllegalArgumentException, Exception {
        //Validate target document
        if (targetDocument == null) {
            throw new IllegalArgumentException("Target document cannot be null" +
                " in XmlUtils.importChildNode method");
        }

        //Validate input node
        if (inputNode == null) {
            throw new IllegalArgumentException("Input node cannot be null" +
                " in XmlUtils.importChildNode method");
        }

        //Validate child name
        if (StringUtils.isNullOrEmpty(childNodeName)) {
            throw new IllegalArgumentException("Child node namecannot be null" +
                " in XmlUtils.importChildNode method");
        }

        //Get child node
        Node childNode = XmlUtils.getChildNodeByName(inputNode, childNodeName,
                false);

        //Import child
        Node resultNode = targetDocument.importNode(childNode, deep);

        //Return reference to imported child
        return resultNode;
    }

    /**
     * Creates a copy of element with attributes(not deep) with
     * given name
     * @param elementName Element Name
     * @param newElementName New name for element
     * @throws Exception incase of any exception
     */
    public static Element prepareElement(Element elementName,
        String newElementName) throws IllegalArgumentException, Exception {
        //Validate element name
        if (elementName == null) {
            throw new IllegalArgumentException("Input node cannot be null" +
                " in XmlUtils.prepareElement method");
        }

        //Create an element with given name
        Element resultElement = elementName.getOwnerDocument().createElement(newElementName);

        //Import all attributes to new element
        XmlUtils.setManyAttributes(resultElement,
            XmlUtils.getManyAttributes(elementName));

        //Return result element
        return resultElement;
    }

    /**
     * Renames the attribute with the new name
     * @param elementObject the element object
     * @param oldName Old name of the attribute
     * @param newName New name of the attribute
     * @throws Exception incase of any exception
     */
    public static void renameAttribute(Element elementObject, String oldName,
        String newName) throws IllegalArgumentException, Exception {
        //Validate element Object
        if (elementObject == null) {
            throw new IllegalArgumentException("Element object cannot be null" +
                " in XmlUtils.renameAttribute method");
        }

        //Validate old name
        if (oldName == null) {
            throw new IllegalArgumentException("Old name cannot be null" +
                " in XmlUtils.renameAttribute method");
        }

        //Validate new name
        if (newName == null) {
            throw new IllegalArgumentException("New name cannot be null" +
                " in XmlUtils.renameAttribute method");
        }

        //the attribute value
        String attributeValue = null;

        //get the value of the attribute
        attributeValue = XmlUtils.getAttribute(elementObject, oldName, null);

        //remove the old attribute
        elementObject.removeAttribute(oldName);

        //if the attribute value is not null then set the attribute
        if (attributeValue != null) {
            XmlUtils.setAttribute(elementObject, newName, attributeValue);
        }
    }

    /** XmlSort takes the source Xml and sorts the Nodes according to the Xsl in the YFS_HOME
     * directory
     * @param xmlDoc Source Xml file where the nodes need to be sorted
     * @throws IllegalArgumentException Illegal Argument exception
     * @throws Exception Exception
     * @return Source Xml with the nodes sorted
     */
    public static Document XmlSort(Document xmlDoc, String xsl)
        throws IllegalArgumentException, Exception {
        // Instantiate a DocumentBuilderFactory.
        DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();

        // And setNamespaceAware, which is required when parsing xsl files
        dFactory.setNamespaceAware(true);

        //Use the DocumentBuilderFactory to create a DocumentBuilder.
        DocumentBuilder dBuilder = dFactory.newDocumentBuilder();

        //Use the DocumentBuilder to parse the XSL stylesheet.
        Document xslDoc = dBuilder.parse(ClassLoader.getSystemResourceAsStream(
                    xsl));

        // Use the DOM Document to define a DOMSource object.
        DOMSource xslDomSource = new DOMSource(xslDoc);

        //Use transformation caching for performance
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Templates cachedXSLT = tFactory.newTemplates(xslDomSource);
        Transformer transformer = cachedXSLT.newTransformer();

        // Use the DOM Document to define a DOMSource object.
        DOMSource xmlDomSource = new DOMSource(xmlDoc);

        // Create an empty DOMResult for the Result.
        DOMResult domResult = new DOMResult();

        // Perform the transformation, placing the output in the DOMResult.
        transformer.transform(xmlDomSource, domResult);

        // Instantiate an Xalan XML serializer and use it to serialize the output DOM to System.out
        // using a default output format.
        Serializer serializer = SerializerFactory.getSerializer(OutputProperties.getDefaultMethodProperties(
                    "xml"));

        File file = new File("temp.xml");
        FileOutputStream fos = new FileOutputStream(file);
        serializer.setOutputStream(fos);
        serializer.asDOMSerializer().serialize(domResult.getNode());

        Document document = dBuilder.parse(file);

        file.delete();
        fos.close();

        return document;
    }

    /**
    * Loads and constructs XML Document object from file resource
    *
    * @param  fileName XML Filename with relative path
    * @return XML Document object
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static Document getXmlFromResource(String fileName)
        throws IllegalArgumentException, Exception {
        //Validate file name
        if (fileName == null) {
            throw new IllegalArgumentException("Filename cannot be" +
                " null in XmlUtils.getXMLfromResource method");
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(ClassLoader.getSystemResourceAsStream(
                    fileName));

        return document;
    }
}
