/*
 * Created on Jul 26, 2005
 *
 * Copyright (c) 2005 Avery Dennison
 *
 * This software is the confidential and proprietary information of
 * Avery Dennison ("Confidential Information"). */
package com.avery.dify.util;


import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Generic wrapper class for sending emails with or without attachements
 * 
 * @author RuleMeister, Inc.
 * @version 1.0
 */
public class SendEmail {

	
	/**
	 * Sends a simple email message
	 * 
	 * @param emailto Recipient addresses (comma or semi-colon separated list)
	 * @param emailfrom Sender address
	 * @param smtphost Email Server
	 * @param msgSubject Email subject text
	 * @param msgText Email message text
	 * @throws Exception any email errors
	 */
	public static synchronized void sendMessage(String emailto, String emailfrom, 
			String smtphost, String msgSubject, String msgText) throws Exception {
		sendMessage(emailto, emailfrom, smtphost, msgSubject, msgText, false);
	}
	
	public static synchronized void sendMessage(String emailto, String emailfrom, 
		String smtphost, String msgSubject, String msgText, boolean isHtml) throws Exception {
        boolean debug = false; 
        
        // set the host
        Properties props = new Properties();
        props.put("mail.smtp.host", smtphost);

        // create some properties and get the default Session
        Session session = Session.getDefaultInstance(props, null);
        session.setDebug(debug);
        try {
            // create a message
            Message msg = new MimeMessage(session);
            
            // set the from
            InternetAddress from = new InternetAddress(emailfrom);
            msg.setFrom(from);

            // set the recipients
            StringTokenizer st = new StringTokenizer(emailto, ";,");
        	while (st.hasMoreTokens()) 
        	    msg.addRecipient(Message.RecipientType.TO, 
        				 new InternetAddress(st.nextToken()));
            
        	// cc administrator
            StringTokenizer stadm = new StringTokenizer(DIFYProperty.ADMIN_RECIPIENTS, ";,");
        	while (stadm.hasMoreTokens()) 
        	    msg.addRecipient(Message.RecipientType.CC, 
        				 new InternetAddress(stadm.nextToken()));
        	
            msg.setSubject(msgSubject);
            if (isHtml)
                msg.setContent(msgText, "text/html");
            else
            	msg.setContent(msgText, "text/plain");
            
            // send message
            Transport.send(msg);

		} catch (AddressException e) {
			throw new Exception(SendEmail.class.getName() + ".SendMessage(): " + 			
				"There was an error parsing the addresses. " + 
				e.getMessage());
		} catch (SendFailedException e) {
			throw new Exception(SendEmail.class.getName() + ".SendMessage(): " + 			
				"Error sending the message. " + 
				e.getMessage());
		} catch (MessagingException e) {
			throw new Exception(SendEmail.class.getName() + ".SendMessage(): " + 			
				"Unexpected Error. " + 
				e.getMessage());
		}

    }

	public static synchronized void sendMessageWOcc(String emailto, String emailfrom,
												String smtphost, String msgSubject, String msgText, boolean isHtml) throws Exception {
		boolean debug = false;

		// set the host
		Properties props = new Properties();
		props.put("mail.smtp.host", smtphost);

		// create some properties and get the default Session
		Session session = Session.getDefaultInstance(props, null);
		session.setDebug(debug);
		try {
			// create a message
			Message msg = new MimeMessage(session);

			// set the from
			InternetAddress from = new InternetAddress(emailfrom);
			msg.setFrom(from);

			// set the recipients
			StringTokenizer st = new StringTokenizer(emailto, ";,");
			while (st.hasMoreTokens())
				msg.addRecipient(Message.RecipientType.TO,
						new InternetAddress(st.nextToken()));

			msg.setSubject(msgSubject);
			if (isHtml)
				msg.setContent(msgText, "text/html");
			else
				msg.setContent(msgText, "text/plain");

			// send message
			Transport.send(msg);

		} catch (AddressException e) {
			throw new Exception(SendEmail.class.getName() + ".SendMessage(): " +
					"There was an error parsing the addresses. " +
					e.getMessage());
		} catch (SendFailedException e) {
			throw new Exception(SendEmail.class.getName() + ".SendMessage(): " +
					"Error sending the message. " +
					e.getMessage());
		} catch (MessagingException e) {
			throw new Exception(SendEmail.class.getName() + ".SendMessage(): " +
					"Unexpected Error. " +
					e.getMessage());
		}

	}

	/**
	 * Sends an email message with attachments.
	 * 
	 * @param emailto Recipient addresses (comma or semi-colon separated list)
	 * @param emailfrom Sender address
	 * @param smtphost Email Server
	 * @param msgSubject Email subject text
	 * @param msgText Email message text
	 * @param fileList List of file locations for attachments
	 * @param doNotReply if true, add do not reply text to bottom of email message
	 * @throws Exception any email errors
	 */
	public static synchronized void sendMultiPartMessage(
		String emailto, String emailfrom, String smtphost, String msgSubject, 
		String msgText, List<String> fileList, boolean isHtml) throws Exception {
	
		if (msgSubject == null) msgSubject = "No Subject";
		if (msgText == null) msgText = "No Message";
	
		try {
	        Properties props = new Properties();
	        props.put("mail.smtp.host", smtphost);

	        // create some properties and get the default Session
	        Session session = Session.getDefaultInstance(props, null);
	        session.setDebug(false);
	
			// Construct the message
			MimeMessage message = new MimeMessage(session);
	
			// Set the from address
			Address fromAddress = new InternetAddress(emailfrom);
			message.setFrom(fromAddress);
	
			// Parse and set the recipient addresses
            StringTokenizer st = new StringTokenizer(emailto, ";,");
        	while (st.hasMoreTokens()) 
        	    message.addRecipient(Message.RecipientType.TO, 
        				 new InternetAddress(st.nextToken()));

        	// cc administrators
            StringTokenizer stadm = new StringTokenizer(DIFYProperty.ADMIN_RECIPIENTS, ";,");
        	while (stadm.hasMoreTokens()) 
        	    message.addRecipient(Message.RecipientType.CC, 
        				 new InternetAddress(stadm.nextToken()));


        	// Set the subject and text
			message.setSubject(msgSubject);

			// Create multipart message object
			Multipart mp = new MimeMultipart();

			// attache message text to first mime part
			MimeBodyPart mbp = new MimeBodyPart();
			if (isHtml)
				mbp.setContent(msgText, "text/html");
			else
				mbp.setContent(msgText, "text/plain");				
			mp.addBodyPart(mbp);

			// attach files to message
			Iterator<String> iter = fileList.iterator();
			while (iter.hasNext()) {
				String filename = (String) iter.next();
				
				File file = new File(filename);
				if (file.exists()) {
					// create the message attachment part
					MimeBodyPart mbp2 = new MimeBodyPart();
		
					// attach the file to the message
					FileDataSource fds = new FileDataSource(filename);
					mbp2.setDataHandler(new DataHandler(fds));
					mbp2.setFileName(fds.getName());

					mp.addBodyPart(mbp2);
				}				
			}

			
			// attach message content
			message.setContent(mp);
	
			// send the message
			Transport.send(message);
	
		} catch (AddressException e) {
			throw new Exception(SendEmail.class.getName() + ".SendMultiPartMessage(): " + 			
				"There was an error parsing the addresses. " + 
				e.getMessage());
		} catch (SendFailedException e) {
			throw new Exception(SendEmail.class.getName() + ".SendMultiPartMessage(): " + 			
				"Error sending the message. " + 
				e.getMessage());
		} catch (MessagingException e) {
			throw new Exception(SendEmail.class.getName() + ".SendMultiPartMessage(): " + 			
				"Unexpected Error. " + 
				e.getMessage());
		}
	
	}

}

