/*
 * StringUtils.java
 *
 * This software is the confidential and proprietary information of
 * Avery Dennison ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Avery Dennison.
 */
package com.avery.dify.util;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// Java imports
import java.util.StringTokenizer;


/**
 * Contains Collection of String Utilities
 * Feb 09,2002
 * @author Yantra
 */
public class StringUtils {
    /**
     * Returns boolean Y/N Code
     * @param flag input boolean value
     * @return Y if input is true else returns N
     */
    public static String getStringValue(boolean flag) {
        if (flag) {
            return "Y";
        } else {
            return "N";
        }
    }

    /**
     * This method converts given value(Y/N) to boolean.
     * This method throws exception if input value is not
     * Y or N.
     * @param inputValue Input Value(Valid values are Y and N)
     * @param nullOK Is null accepted? if null is accepted and
     * and if input value is null, this method returns false
     * @return Equivalent boolean value of input
     * @throws Exception incase of any exception
     */
    public static boolean getBooleanValue(String inputValue, boolean nullOK)
        throws Exception {
        //Is input value null
        if (StringUtils.isNullOrEmpty(inputValue)) {
            //If null not accepted throw exception
            if (!nullOK) {
                throw new Exception("Input value cannot be empty " +
                    "in StringUtils.getBooleanValue");
            } else {
                //Return default value if null
                return false;
            }
        }

        //Convert input value to upper case for comparision
        inputValue = inputValue.toUpperCase();

        //If input value is Yes return true
        if (StringUtils.isSame(inputValue, "Y")) {
            return true;
        }
        //If input value if No return false
        else if (StringUtils.isSame(inputValue, "N")) {
            return false;
        }
        //If input valud is invalid throws exception
        else {
            throw new Exception("Invalid input value:" + inputValue +
                "in StringUtils.getBooleanValue");
        }
    }

    /**
     * This method compares two strings with equals method.
     * This method considers null as empty string.
     * @param firstValue First String to compare;
     * Considers null as empty string for comparision
     * @param secondValue Second String to compare;
     * Considers null as empty string for comparision
     * @return true - if equals; else - false
     */
    public static boolean isSame(String firstValue, String secondValue) {
        if (firstValue == null) {
            firstValue = "";
        }

        if (secondValue == null) {
            secondValue = "";
        }

        return firstValue.equals(secondValue);
    }

    /**
     * This method converts object as a string value.  If input object
     * is null this method then returns default value
     * @param object Object
     * @param defaultValue Default Value
     * @return Returns object as string; if object is null returns
     * defaultValue
     */
    public static String getStringValue(Object object, String defaultValue) {
        //If input object is null return default value
        if (object == null) {
            return defaultValue;
        }

        return object.toString();
    }

    public static String getStringValue(Object object, String defaultValue, String appendValue) {
        //If input object is null return default value
        if (object == null) {
            return defaultValue + appendValue;
        }

        return object.toString() + appendValue;
    }

    public static String getStringValue(Date object, SimpleDateFormat df, String defaultValue) {
        //If input object is null return default value
        if (object == null) {
            return defaultValue;
        }

        return df.format(object);
    }

    /**
     * This method converts object as an integer value. If input object
     * is null or invalid this method then returns default value
     * @param object Object
     * @param defaultValue Default Value
     * @return Returns object as integer; if object is null or invalid
     * returns defaultValue
     */
    public static int getIntegerValue(Object object, int defaultValue) {
        String stringValue = getStringValue(object, "");

        try {
            return Integer.parseInt(stringValue);
        } catch (Exception exception) {
            return defaultValue;
        }
    }

    /**
     * Appends spaces to given string to meet maximum length
     * @param inputValue Input string to which spaces needs to be
     * added.
     * @param maxLength Maximum Length of string.
     * @return input value with spaces appended
     * @throws IllegalArgumentException for Invalid input
     */
    public static String appendSpaces(String inputValue, int maxLength)
        throws IllegalArgumentException {
        int spacesToAdd = 0;

        //Validate input value
        if (inputValue == null) {
            throw new IllegalArgumentException("Input value cannot " +
                "be null in StringUtils.appendSpace method");
        }

        //Validate Maximum length
        if (maxLength < 0) {
            throw new IllegalArgumentException("Invalid maximum " +
                "length be null in StringUtils.appendSpace method");
        }

        spacesToAdd = maxLength - inputValue.length();

        if (spacesToAdd <= 0) {
            return inputValue;
        }

        for (int spaceIndex = 0; spaceIndex < spacesToAdd; spaceIndex++) {
            //append space
            inputValue = inputValue + " ";
        }

        return inputValue;
    }

    /**
     * Checks if input value is null or empty string
     * @param inputValue Input Value
     * @return true - if null or empty string; false - otherwise
     */
    public static boolean isNullOrEmpty(String inputValue) {
        if (inputValue == null) {
            return true;
        }

        if (inputValue.length() < 1) {
            return true;
        }

        return false;
    }

    /**
    * Converts object to string. Returns null if object is null
    * else casts object as String
    *
    * @param inputObject Input Object
    * @return          Returns null if input object is null;
    * else casts object as String
    */
    public static String getStringValue(Object inputObject) {
        if (inputObject == null) {
            return null;
        }

        return (String) inputObject;
    }

    /** Takes in numerical string with commas and returns a string with commas removed
     * @param str input string with comma as delimiter
     * @return numerical string with commas removed
     */
    public static String getStringValue(String str) {
        StringTokenizer st = new StringTokenizer(str, ",");
        StringBuffer sb = new StringBuffer();

        while (st.hasMoreTokens()) {
            sb.append(st.nextToken());
        }

        return sb.toString();
    }

    /**
    * Parses input value based on delimiter and returns desired token
    *
    * @param inputValue String to be parsed
    * @param delimiter  Delimiter for parsing
    * @param tokenNumber  Desired token number(0 for First token,
    * 1 for Second token etc)
    * @return          Returns desired token
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    protected static String getToken(String inputValue, String delimiter,
        int tokenNumber) throws IllegalArgumentException, Exception {
        String resultValue = null;

        //Validate Input Value
        if (inputValue == null) {
            throw new IllegalArgumentException("Input value cannot" +
                "be null in StringUtils.getToken method");
        }

        //Validate Delimiter
        if (delimiter == null) {
            throw new IllegalArgumentException("Delimiter cannot be" +
                " null in StringUtils.getToken method");
        }

        //Validate token number
        if (tokenNumber < 0) {
            throw new IllegalArgumentException("Invalid token number" +
                " in StringUtils.getToken method");
        }

        StringTokenizer st = new StringTokenizer(inputValue, delimiter);

        for (int tokenIndex = 0; tokenIndex < tokenNumber; tokenIndex++) {
            st.nextToken();
        }

        if (st.hasMoreTokens()) {
            resultValue = st.nextToken();
        }

        return resultValue;
    }

    /**
     * Parses given string and returns array of tokens
     * @param inputValue String to be parsed
     * @param delimiter  Delimiter for parsing
     * @return Array of tokens
     * @throws IllegalArgumentException for Invalid input
     * @throws Exception for all others
     */
    public static String[] parseString(String inputValue, String delimiter)
        throws IllegalArgumentException, Exception {
        //Validate Input Value
        if (inputValue == null) {
            throw new IllegalArgumentException("Input value cannot" +
                "be null in StringUtils.parseString method");
        }

        //Validate delimiter
        if (delimiter == null) {
            throw new IllegalArgumentException("Delimiter cannot" +
                "be null in StringUtils.parseString method");
        }

        //Validate delimiter
        if (delimiter.length() < 1) {
            throw new IllegalArgumentException("Invalid Delimiter " +
                " in StringUtils.parseString method");
        }

        //Parse
        StringTokenizer tokenizer = new StringTokenizer(inputValue, delimiter);

        //Create an array for tokens
        String[] result = new String[tokenizer.countTokens()];

        int tokenIndex = 0;

        while (tokenizer.hasMoreTokens()) {
            result[tokenIndex] = tokenizer.nextToken();
            tokenIndex++;
        }

        //Return result array
        return result;
    }

    /**
    * A method that uses a stringtokenizer to parse a NameValue pair
    * and returns a map.
    * A NameValue Pair is in the following format:
    * OrderNo=Y100089&EnterpriseCode=DEFAULT&ShiptoID=493848234
    *
    * @param inputValue String to be parsed
    * @return          Map object that contains name value pairs
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static java.util.Map<String, String> getMap(String inputValue)
        throws IllegalArgumentException, Exception {
        //Validate Input Value
        if (inputValue == null) {
            throw new IllegalArgumentException("Input value cannot" +
                "be null in StringUtils.getMap method");
        }

        //Validate Input Value
        if (inputValue.indexOf("&") <= -1) {
            throw new IllegalArgumentException("Invalid Input value" +
                " in StringUtils.getMap method");
        }

        if (inputValue.indexOf("=") <= -1) {
            throw new IllegalArgumentException("Invalid Input value" +
                " in StringUtils.getMap method");
        }

        Map<String, String> map = Collections.synchronizedMap(new HashMap<String, String>());
        StringTokenizer st = new StringTokenizer(inputValue, "&");

        while (st.hasMoreTokens()) {
            String nameValuePair = st.nextToken();
            map.put(getToken(nameValuePair, "=", 0),
                getToken(nameValuePair, "=", 1));
        }

        return map;
    }

    /**
    * Constructs name value pair string in the following format
    * from Map object
    * OrderNo=Y100089&EnterpriseCode=DEFAULT&ShiptoID=493848234
    *
    * @param map   Input Map object
    * @return      Name value pair string
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static String getMapContent(java.util.Map<String, String> map)
        throws IllegalArgumentException, Exception {
        //Validate map
        if (map == null) {
            throw new IllegalArgumentException("Input value cannot" +
                " be null in StringUtils.getMap method");
        }

        String result = "";
        Object[] keyList = map.keySet().toArray();

        for (int index = 0; index < keyList.length; index++) {
            String name = getStringValue(keyList[index]);
            String value = getStringValue(map.get(keyList[index]));

            if (result.length() > 1) {
                result = result + "&";
            }

            result = result + name + "=" + value;
        }

        return result;
    }

	/**
	* A method mainly for stripping out commas from string representations
	*  of numbers
	* @param inputValue String to be parsed
	* @param ch character to be stripped out
	* @return String
	*/
	public static String strip(String inputValue, int ch) {
		int pos;
		while ((pos = inputValue.indexOf(ch)) != -1)
			inputValue = inputValue.substring(0, pos) + inputValue.substring(pos + 1);
		return inputValue;
	}
	
	
	public static String getSqlString(List<String>list) {
		String sql = "";
		Iterator<String>iter = list.iterator();
		while (iter.hasNext()) {
			String item = (String)iter.next();
			if (sql.length() > 0) sql += ",";
			sql += "'" + item + "'";
		}
		return sql;
	}

    public static String getSqlStringToIntList(List<String>list) {
        String sql = "";
        Iterator<String>iter = list.iterator();
        while (iter.hasNext()) {
            String item = (String)iter.next();
            if (sql.length() > 0) sql += ",";
            sql += item;
        }
        return sql;
    }

	public static String getSqlNumList(List<Integer>list) {
		String sql = "";
		Iterator<Integer>iter = list.iterator();
		while (iter.hasNext()) {
			Integer item = (Integer)iter.next();
			if (sql.length() > 0) sql += ",";
			sql += item + "";
		}
		return sql;
	}

}
