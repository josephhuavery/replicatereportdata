/*
 * Created on Oct 7, 2005
 *
 * Copyright (c) 2005 Avery Dennison
 *
 * This software is the confidential and proprietary information of
 * Avery Dennison ("Confidential Information"). 
 */

package com.avery.dify.util;

/**
 * This class is used to load and access property values stored in 
 * this application's properties file.
 * 
 * @author RuleMeister, Inc.
 * @version 1.0
 */
public class DIFYProperties_Prod {
	
	public final Integer DIFY_STORE_ID = 1;
	public final Integer AVYPRO_STORE_ID = 11;
	
	
	// Database Constants
	// ---------------------------------------
	public final String SRC_DB_URL = "jdbc:mysql://magentoprod2-aurora-cluster.cluster-cqvhamgdg5wz.us-east-1.rds.amazonaws.com:3306/avery_prod?characterEncoding=utf8&dumpQueriesOnException=true";
	public final String SRC_DB_UID = "difyexport";
	public final String SRC_DB_PWD = "4RFCQoZcJwHeJciBA2x";
	public final String DEST_DB_URL = "jdbc:oracle:thin:@dbpdadc42:1521:bkprod";
	public final String DEST_DB_UID = "magento_stg";
	public final String DEST_DB_PWD = "difybkPr0d";
	public final String ILS_GATEWAY = "ilsgateway";

	final public String ILS_DB_URL = "jdbc:as400://PROD01";
	final public String ILS_DB_UID = "DWPROD";
	final public String ILS_DB_PWD = "DWPROD";

	// Email Constants
	// ---------------------------------------
	public final String SERVER = "DIFY Production";
	public final String EMAIL_HOST = "smtp.cclind.com";
	public final String EMAIL_RECIPIENTS = "DIFYnotificationemail@avery.com";
	public final String EMAIL_US_RECIPIENTS = "DIFYnotificationemail@avery.com";
	public final String EMAIL_SPECIAL_CM_RECIPIENTS = "WePrint_CreditMemo@avery.com";
	public final String EMAIL_DUPLICATE_ORDER_RECIPIENTS = "kkim@avery.com";
//	public final String EMAIL_DUPLICATE_ORDER_RECIPIENTS = "kkim@avery.com,yli@avery.com,megonzalez@avery.com,spadron@avery.com,aaguilar@avery.com,lcarrillo@avery.com,fcamarena@avery.com";
	public final String EMAIL_SHIPMENT_RPT_RECIPIENTS = "kkim@avery.com";
//	public final String EMAIL_SHIPMENT_RPT_RECIPIENTS = "aaguilar@avery.com,megonzalez@avery.com,evito@avery.com,tpreusch@avery.com,dprocuk@avery.com";
	public final String EMAIL_SHIPMENT_RPT_AVYPRO_RECIPIENTS = "kkim@avery.com,yli@avery.com";
	public final String EMAIL_ORDER_DTL_RPT_RECIPIENTS = "WePrintDailyReport@avery.com";
//	public final String EMAIL_ORDER_DTL_RPT_RECIPIENTS = "mpark@avery.com,megonzalez@avery.com,MCastellanos@avery.com,jherz@avery.com,evito@avery.com,tpreusch@avery.com,jdawkins@avery.com,amaleki@avery.com,dwilson@avery.com,nbecker@avery.com,agabele@avery.com,spadron@avery.com,yli@avery.com,dsillas@avery.com,kkim@avery.com,cdidomenico@avery.com,pjani@avery.com,erodriguez@avery.com";
	public final String EMAIL_ORDER_DTL_RPT_RETAIL_RECIPIENTS = "RetailDailyReport@avery.com";
	public final String EMAIL_ORDER_DTL_RPT_AVYPRO_RECIPIENTS = "kkim@avery.com,yli@avery.com,jdawkins@avery.com,mpark@avery.com,lisanchez@avery.com,dfox@avery.com,tamin@avery.com";
	public final String ADMIN_RECIPIENTS = "DIFYAdmin@avery.com";
	public final String EMAIL_SENDER = "dify@avery.com";
	public final String DO_NOT_REPLY_TEXT = "Please do not reply to this email";
	
	
	// FTP Mappings
	// ---------------------------------------
	public final String GLOVIA_FTP_HOST = "glovia";
	public final String GLOVIA_FTP_PORT = "22";
	public final String GLOVIA_FTP_USER = "asapint";
	public final String GLOVIA_FTP_PWD = "as@pint1";
	public final String GLOVIA_FTP_NEW_TGT_DIR = "/staging/eai/wo/in";
	public final String GLOVIA_FTP_SHP_TGT_DIR = "/staging/eai/wc/in";
	public final String GLOVIA_FTP_CAN_TGT_DIR = "/staging/eai/canc/in";
	
	// MQ Constants
	// ---------------------------------------
	public final String ILS_DIFY_QUEUE = "AVY.DIFY.QL.AVY.ILS.ORDER.OUT";
	public final String ILS_DIFY_ACK_QUEUE = "AVY.ILS.QL.AVY.DIFY.CONFIRM.IN";
	public final String GLV_DIFY_ACK_QUEUE = "AVY.GLV.QL.AVY.DIFY.CONFIRM.IN";


	// File System
	// ---------------------------------------
	public final String MQManager = "AVYWBPRD";
	public final String MQHostname = "MPPDBOU02";
	public final String MQPort = "1432";
	public final String MQChannel = "SYSTEM.ADMIN.SVRCONN";
	public final String MSGSize = "4194304";
	public final String POLL_FREQ = "600000";
	public final String ACK_POLL_FREQ = "300000";

	public final String UPLOAD_DIR = "/u12/app/DIFY/data_orders/in";
	public final String WORK_DIR = "/u12/app/DIFY/data_orders/work";
	public final String ERROR_DIR = "/u12/app/DIFY/data_orders/err";
	public final String ARCHIVE_DIR = "/u12/app/DIFY/data_orders/archive";

	public final String GLOVIA_OUT_DIR = "/u12/app/DIFY/data_orders/glovia/out";
	public final String GLOVIA_ARCHIVE_DIR = "/u12/app/DIFY/data_orders/glovia/archive";
	public final String GLOVIA_ACK_DIR = "/u12/app/DIFY/data_orders/glovia/ack";

	public final String ILS_ARCHIVE_DIR = "/u12/app/DIFY/data_orders/ils/archive";
	public final String ILS_ACK_DIR = "/u12/app/DIFY/data_orders/ils/ack";

	public final String REPORT_DIR = "/u12/app/DIFY/data_orders/reports";

	public final String ETSY_CUST_ID = "1156241";
	// Logging
	// ---------------------------------------
	public final String DEBUG = "Y";

	
	// Error Codes
	// ---------------------------------------
	public final String ERR_SYS_NO_UPLOAD_DIR = "SYS1001";
	public final String ERR_SYS_NO_ARCHIVE_DIR = "SYS1002";
	public final String ERR_SYS_NO_ERROR_DIR = "SYS1003";
	public final String ERR_SYS_NO_WORK_DIR = "SYS1004";
	public final String ERR_SYS_FILE_RENAME = "SYS1005";
	public final String ERR_SYS_FILE_MOVE = "SYS1006";

}
