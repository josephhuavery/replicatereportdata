package com.avery.dify.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class SpreadsheetUtil {
	
	/**
	 * @param zipfile - the template file
	 * @param tmpfile - the XML file with the sheet data
	 * @param entry   - the name of the sheet entry to substitute, e.g. xl/worksheets/sheet1.xml
	 * @param out     - the stream to write the result to
	 */
	public void substitute(File zipfile, Map<String, File> sheets, OutputStream out) throws IOException {
		ZipFile zip = new ZipFile(zipfile);

		ZipOutputStream zos = new ZipOutputStream(out);

		@SuppressWarnings("unchecked")
		Enumeration<ZipEntry> en = (Enumeration<ZipEntry>) zip.entries();
		while (en.hasMoreElements()) {
			ZipEntry ze = en.nextElement();
			System.out.println(ze.getName());
			
			if (!sheets.containsKey(ze.getName())) {
				zos.putNextEntry(new ZipEntry(ze.getName()));
				InputStream is = zip.getInputStream(ze);
				copyStream(is, zos);
				is.close();
			}
			else {
				File tmpfile = sheets.get(ze.getName());
				zos.putNextEntry(new ZipEntry(ze.getName()));
				InputStream is = new FileInputStream(tmpfile);
				copyStream(is, zos);
				is.close();				
			}
		}

		zos.close();
	}
	
	public File addSheet(String name, XSSFWorkbook book, Map<String, File> sheets) throws IOException {
		// create the sheet
		XSSFSheet sheet = book.createSheet(name);

		// get the refname. do the substring() here since we done need the full name anywhere else
		String ref = sheet.getPackagePart().getPartName().getName().substring(1);
		//System.out.println("AddSheet:" + ref);

		// create the temp file as sheet#.xml using the map size to get the right sheet index
		File tmp = File.createTempFile("sheet" + (sheets.size() + 1), ".xml");
		sheets.put(ref, tmp);
		
		return tmp;
	}
	
	/**
	 * Create a library of cell styles.
	 */
	public Map<String, XSSFCellStyle> createStyles(XSSFWorkbook wb) {
		Map<String, XSSFCellStyle> styles = new HashMap<String, XSSFCellStyle>();
		XSSFDataFormat fmt = wb.createDataFormat();

		XSSFCellStyle style1 = wb.createCellStyle();
		style1.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		style1.setDataFormat(fmt.getFormat("0.0%"));
		styles.put("percent", style1);

		XSSFCellStyle style2 = wb.createCellStyle();
		style2.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		style2.setDataFormat(fmt.getFormat("##,##0.0000"));
		styles.put("cost", style2);

		XSSFCellStyle style3 = wb.createCellStyle();
		style3.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		style3.setDataFormat(fmt.getFormat("##,##0"));
		styles.put("currency", style3);

		XSSFCellStyle style4 = wb.createCellStyle();
		style4.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		style4.setDataFormat(fmt.getFormat("mm/dd/yyyy"));
		styles.put("date", style4);

		XSSFCellStyle style5 = wb.createCellStyle();
		XSSFFont headerFont = wb.createFont();
		headerFont.setBold(true);
		style5.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		style5.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style5.setFont(headerFont);
		styles.put("header", style5);

		XSSFCellStyle style6 = wb.createCellStyle();
		style6.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		style6.setDataFormat(fmt.getFormat("##,##0.0"));
		styles.put("num1", style6);

		XSSFCellStyle style7 = wb.createCellStyle();
		style7.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		style7.setDataFormat(fmt.getFormat("##,##0.00"));
		styles.put("num2", style7);

		return styles;
	}

	public void copyStream(InputStream in, OutputStream out)
			throws IOException {
		byte[] chunk = new byte[1024];
		int count;
		while ((count = in.read(chunk)) >= 0) {
			out.write(chunk, 0, count);
		}
	}
	
}
