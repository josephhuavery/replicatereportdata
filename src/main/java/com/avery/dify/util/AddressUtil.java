package com.avery.dify.util;

import java.util.StringTokenizer;


public class AddressUtil {

	private static String getAbbreviation(String in) {
		if (in.equalsIgnoreCase("Alley")) return "Aly";
		else if (in.equalsIgnoreCase("Annex")) return "Anx";
		else if (in.equalsIgnoreCase("Apartment")) return "Apt";
		else if (in.equalsIgnoreCase("Arcade")) return "Arc";
		else if (in.equalsIgnoreCase("Avenue")) return "Ave";
		else if (in.equalsIgnoreCase("Basement")) return "Bsmt";
		else if (in.equalsIgnoreCase("Bayou")) return "Byu";
		else if (in.equalsIgnoreCase("Beach")) return "Bch";
		else if (in.equalsIgnoreCase("Bend")) return "Bnd";
		else if (in.equalsIgnoreCase("Bluff")) return "Blf";
		else if (in.equalsIgnoreCase("Bottom")) return "Btm";
		else if (in.equalsIgnoreCase("Boulevard")) return "Blvd";
		else if (in.equalsIgnoreCase("Branch")) return "Br";
		else if (in.equalsIgnoreCase("Bridge")) return "Brg";
		else if (in.equalsIgnoreCase("Brook")) return "Brk";
		else if (in.equalsIgnoreCase("Building")) return "Bldg";
		else if (in.equalsIgnoreCase("Burg")) return "Bg";
		else if (in.equalsIgnoreCase("Bypass")) return "Byp";
		else if (in.equalsIgnoreCase("Camp")) return "Cp";
		else if (in.equalsIgnoreCase("Canyon")) return "Cyn";
		else if (in.equalsIgnoreCase("Cape")) return "Cpe";
		else if (in.equalsIgnoreCase("Causeway")) return "Cswy";
		else if (in.equalsIgnoreCase("Center")) return "Ctr";
		else if (in.equalsIgnoreCase("Circle")) return "Cir";
		else if (in.equalsIgnoreCase("Cliff")) return "Clfs";
		else if (in.equalsIgnoreCase("Cliffs")) return "Clfs";
		else if (in.equalsIgnoreCase("Club")) return "Clb";
		else if (in.equalsIgnoreCase("Corner")) return "Cor";
		else if (in.equalsIgnoreCase("Corners")) return "Cors";
		else if (in.equalsIgnoreCase("Course")) return "Crse";
		else if (in.equalsIgnoreCase("Court")) return "Ct";
		else if (in.equalsIgnoreCase("Courts")) return "Cts";
		else if (in.equalsIgnoreCase("Cove")) return "Cv";
		else if (in.equalsIgnoreCase("Creek")) return "Crk";
		else if (in.equalsIgnoreCase("Crescent")) return "Cres";
		else if (in.equalsIgnoreCase("Crossing")) return "Xing";
		else if (in.equalsIgnoreCase("Dale")) return "Dl";
		else if (in.equalsIgnoreCase("Dam")) return "Dm";
		else if (in.equalsIgnoreCase("Department")) return "Dept";
		else if (in.equalsIgnoreCase("Divide")) return "Dv";
		else if (in.equalsIgnoreCase("Drive")) return "Dr";
		else if (in.equalsIgnoreCase("Estate")) return "Est";
		else if (in.equalsIgnoreCase("Expressway")) return "Expy";
		else if (in.equalsIgnoreCase("Extension")) return "Ext";
		else if (in.equalsIgnoreCase("Falls")) return "Fls";
		else if (in.equalsIgnoreCase("Ferry")) return "Fry";
		else if (in.equalsIgnoreCase("Field")) return "Fld";
		else if (in.equalsIgnoreCase("Fields")) return "Flds";
		else if (in.equalsIgnoreCase("Flat")) return "Flt";
		else if (in.equalsIgnoreCase("Floor")) return "Fl";
		else if (in.equalsIgnoreCase("Ford")) return "Frd";
		else if (in.equalsIgnoreCase("Forest")) return "Frst";
		else if (in.equalsIgnoreCase("Forge")) return "Frg";
		else if (in.equalsIgnoreCase("Fork")) return "Frk";
		else if (in.equalsIgnoreCase("Forks")) return "Frks";
		else if (in.equalsIgnoreCase("Fort")) return "Ft";
		else if (in.equalsIgnoreCase("Freeway")) return "Fwy";
		else if (in.equalsIgnoreCase("Front")) return "Frnt";
		else if (in.equalsIgnoreCase("Garden")) return "Gdns";
		else if (in.equalsIgnoreCase("Gardens")) return "Gdns";
		else if (in.equalsIgnoreCase("Gateway")) return "Gtwy";
		else if (in.equalsIgnoreCase("Glen")) return "Gln";
		else if (in.equalsIgnoreCase("Green")) return "Grn";
		else if (in.equalsIgnoreCase("Grove")) return "Grv";
		else if (in.equalsIgnoreCase("Hanger")) return "Hngr";
		else if (in.equalsIgnoreCase("Harbor")) return "Hbr";
		else if (in.equalsIgnoreCase("Haven")) return "Hvn";
		else if (in.equalsIgnoreCase("Heights")) return "Hts";
		else if (in.equalsIgnoreCase("Highway")) return "Hwy";
		else if (in.equalsIgnoreCase("Hill")) return "Hl";
		else if (in.equalsIgnoreCase("Hills")) return "Hls";
		else if (in.equalsIgnoreCase("Hollow")) return "Holw";
		else if (in.equalsIgnoreCase("Inlet")) return "Inlt";
		else if (in.equalsIgnoreCase("Island")) return "Is";
		else if (in.equalsIgnoreCase("Islands")) return "Iss";
		else if (in.equalsIgnoreCase("Junction")) return "Jct";
		else if (in.equalsIgnoreCase("Key")) return "Ky";
		else if (in.equalsIgnoreCase("Knoll")) return "Knls";
		else if (in.equalsIgnoreCase("Knolls")) return "Knls";
		else if (in.equalsIgnoreCase("Lake")) return "Lk";
		else if (in.equalsIgnoreCase("Lakes")) return "Lks";
		else if (in.equalsIgnoreCase("Landing")) return "Lndg";
		else if (in.equalsIgnoreCase("Lane")) return "Ln";
		else if (in.equalsIgnoreCase("Light")) return "Lgt";
		else if (in.equalsIgnoreCase("Loaf")) return "Lf";
		else if (in.equalsIgnoreCase("Lobby")) return "Lbby";
		else if (in.equalsIgnoreCase("Lock")) return "Lcks";
		else if (in.equalsIgnoreCase("Locks")) return "Lcks";
		else if (in.equalsIgnoreCase("Lodge")) return "Ldg";
		else if (in.equalsIgnoreCase("Lower")) return "Lowr";
		else if (in.equalsIgnoreCase("Manor")) return "Mnr";
		else if (in.equalsIgnoreCase("Meadow")) return "Mdws";
		else if (in.equalsIgnoreCase("Meadows")) return "Mdws";
		else if (in.equalsIgnoreCase("Mill")) return "Ml";
		else if (in.equalsIgnoreCase("Mills")) return "Mls";
		else if (in.equalsIgnoreCase("Mission")) return "Msn";
		else if (in.equalsIgnoreCase("Mount")) return "Mt";
		else if (in.equalsIgnoreCase("Mountain")) return "Mtn";
		else if (in.equalsIgnoreCase("Neck")) return "Nck";
		else if (in.equalsIgnoreCase("Office")) return "Ofc";
		else if (in.equalsIgnoreCase("Orchard")) return "Orch";
		else if (in.equalsIgnoreCase("Parkway")) return "Pkwy";
		else if (in.equalsIgnoreCase("Penthouse")) return "Ph";
		else if (in.equalsIgnoreCase("Pine")) return "Pnes";
		else if (in.equalsIgnoreCase("Pines")) return "Pnes";
		else if (in.equalsIgnoreCase("Place")) return "Pl";
		else if (in.equalsIgnoreCase("Plain")) return "Pln";
		else if (in.equalsIgnoreCase("Plains")) return "Plns";
		else if (in.equalsIgnoreCase("Plaza")) return "Plz";
		else if (in.equalsIgnoreCase("Point")) return "Pt";
		else if (in.equalsIgnoreCase("Port")) return "Prt";
		else if (in.equalsIgnoreCase("Prairie")) return "Pr";
		else if (in.equalsIgnoreCase("Radial")) return "Radl";
		else if (in.equalsIgnoreCase("Ranch")) return "Rnch";
		else if (in.equalsIgnoreCase("Rapid")) return "Rpds";
		else if (in.equalsIgnoreCase("Rapids")) return "Rpds";
		else if (in.equalsIgnoreCase("Rest")) return "Rst";
		else if (in.equalsIgnoreCase("Ridge")) return "Rdg";
		else if (in.equalsIgnoreCase("River")) return "Riv";
		else if (in.equalsIgnoreCase("Road")) return "Rd";
		else if (in.equalsIgnoreCase("Room")) return "Rm";
		else if (in.equalsIgnoreCase("Shoal")) return "Shl";
		else if (in.equalsIgnoreCase("Shoals")) return "Shls";
		else if (in.equalsIgnoreCase("Shore")) return "Shr";
		else if (in.equalsIgnoreCase("Shores")) return "Shrs";
		else if (in.equalsIgnoreCase("Space")) return "Spc";
		else if (in.equalsIgnoreCase("Spring")) return "Spg";
		else if (in.equalsIgnoreCase("Springs")) return "Spgs";
		else if (in.equalsIgnoreCase("Square")) return "Sq";
		else if (in.equalsIgnoreCase("Station")) return "Sta";
		else if (in.equalsIgnoreCase("Stravenue")) return "Stra";
		else if (in.equalsIgnoreCase("Stream")) return "Strm";
		else if (in.equalsIgnoreCase("Street")) return "St";
		else if (in.equalsIgnoreCase("Suite")) return "Ste";
		else if (in.equalsIgnoreCase("Summit")) return "Smt";
		else if (in.equalsIgnoreCase("Terrace")) return "Ter";
		else if (in.equalsIgnoreCase("Trace")) return "Trce";
		else if (in.equalsIgnoreCase("Track")) return "Trak";
		else if (in.equalsIgnoreCase("Trafficway")) return "Trfy";
		else if (in.equalsIgnoreCase("Trail")) return "Trl";
		else if (in.equalsIgnoreCase("Trailer")) return "Trlr";
		else if (in.equalsIgnoreCase("Tunnel")) return "Tunl";
		else if (in.equalsIgnoreCase("Turnpike")) return "Tpke";
		else if (in.equalsIgnoreCase("Union")) return "Un";
		else if (in.equalsIgnoreCase("Upper")) return "Uppr";
		else if (in.equalsIgnoreCase("Valley")) return "Vly";
		else if (in.equalsIgnoreCase("Viaduct")) return "Via";
		else if (in.equalsIgnoreCase("View")) return "Vw";
		else if (in.equalsIgnoreCase("Village")) return "Vlg";
		else if (in.equalsIgnoreCase("Ville")) return "Vl";
		else if (in.equalsIgnoreCase("Vista")) return "Vis";
		else if (in.equalsIgnoreCase("Way")) return "Way";
		else if (in.equalsIgnoreCase("Well")) return "Wl";
		else if (in.equalsIgnoreCase("Wells")) return "Wls";
		return null;
	}
	
	public static String abbrevAddress(String address, int maxLength) {
		String addr = "";
		if (address == null) return null;		
		int inLength = address.length();
		int currLength = inLength;
		if (inLength <= maxLength)
			return address;
		
		String abbrev = "";
		StringTokenizer st = new StringTokenizer(address, " ");
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (currLength > maxLength) {
				abbrev = AddressUtil.getAbbreviation(token);
				if (abbrev != null) {
					int diff = token.length() - abbrev.length();
					currLength -= diff;
				}
				else abbrev = token;
			} 
			else abbrev = token;
			if (addr.length() > 0) addr += " ";
			addr += abbrev;
		}
		
		if (addr.length() > maxLength) addr = addr.substring(0, maxLength);
		return addr;
	}

	public static String abbrevName(String name, int maxLength) {
		String abbrevName = "";
		if (name == null) return null;		
		int inLength = name.length();
		if (inLength <= maxLength)
			return name;

		int firstSpace = name.indexOf(" ");
		String firstName = name.substring(0, firstSpace - 1);
		String lastName = name.substring(firstSpace + 1);
		String firstInitial = firstName.substring(0, 1);
		abbrevName = firstInitial + ". " + lastName;

		if (abbrevName.length() > maxLength) abbrevName = lastName;
		if (abbrevName.length() > maxLength) abbrevName = abbrevName.substring(0, maxLength);

		return abbrevName;
	}
	
	public static void main(String[] args) {
		String test = "1234567890 Street 12345678901 Valley";
		System.out.println(AddressUtil.abbrevAddress(test, 34));

		String testName = "Harrison Skywalkerson";
		System.out.println(AddressUtil.abbrevName(testName, 15));

	}

}
