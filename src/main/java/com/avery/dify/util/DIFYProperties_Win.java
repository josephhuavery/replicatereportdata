package com.avery.dify.util;

public class DIFYProperties_Win {
	public final Integer DIFY_STORE_ID = 1;
	public final Integer AVYPRO_STORE_ID = 11;
	
	
	// Database Constants
	// ---------------------------------------
	public final String SRC_DB_URL = "jdbc:mysql://10.104.0.74:3306/test?characterEncoding=utf8&dumpQueriesOnException=true";
	public final String SRC_DB_UID = "magdevit";
	public final String SRC_DB_PWD = "h5UqZd$L@W57c2^8";
//	public final String SRC_DB_URL = "jdbc:mysql://magentoprod2-aurora-cluster.cluster-cqvhamgdg5wz.us-east-1.rds.amazonaws.com:3306/avery_prod?characterEncoding=utf8&dumpQueriesOnException=true";
//	public final String SRC_DB_UID = "difyexport";
//	public final String SRC_DB_PWD = "4RFCQoZcJwHeJciBA2x";
	public final String DEST_DB_URL = "jdbc:oracle:thin:@//dbdvavyc25:1521/DWTEST";
//	public final String DEST_DB_UID = "magentodev_stg";
//	public final String DEST_DB_PWD = "mgtdev_stag1ng";
//	public final String ILS_GATEWAY = "ilsgateway";
	public final String DEST_DB_UID = "magento_stg";
	public final String DEST_DB_PWD = "MGT_Stag1ng";
	public final String ILS_GATEWAY = "ilsgateway";

	final public String ILS_DB_URL = "jdbc:as400://DEV01";
	final public String ILS_DB_UID = "DWTEST";
	final public String ILS_DB_PWD = "DWTEST";

	// Email Constants
	// ---------------------------------------
	public final String SERVER = "DIFY Development";
	public final String EMAIL_HOST = "smtp.cclind.com";
	public final String EMAIL_RECIPIENTS = "DIFYnotificationemail@avery.com";
	public final String EMAIL_US_RECIPIENTS = "DIFYnotificationemail@avery.com";
	public final String EMAIL_SPECIAL_CM_RECIPIENTS = "kkim@avery.com";
	public final String EMAIL_DUPLICATE_ORDER_RECIPIENTS = "kkim@avery.com";
	public final String EMAIL_SHIPMENT_RPT_RECIPIENTS = "kkim@avery.com";
	public final String EMAIL_SHIPMENT_RPT_AVYPRO_RECIPIENTS = "kkim@avery.com";
	public final String EMAIL_ORDER_DTL_RPT_RECIPIENTS = "kkim@avery.com";
	public final String EMAIL_ORDER_DTL_RPT_RETAIL_RECIPIENTS = "kkim@avery.com";
	public final String EMAIL_ORDER_DTL_RPT_AVYPRO_RECIPIENTS = "kkim@avery.com";
	public final String ADMIN_RECIPIENTS = "kkim@avery.com";
	public final String EMAIL_SENDER = "dify@avery.com";
	public final String DO_NOT_REPLY_TEXT = "Please do not reply to this email";
	
	
	// FTP Mappings
	// ---------------------------------------
	public final String GLOVIA_FTP_HOST = "gloviadev";
	public final String GLOVIA_FTP_PORT = "22";
	public final String GLOVIA_FTP_USER = "asapint";
	public final String GLOVIA_FTP_PWD = "as@pint1";
	public final String GLOVIA_FTP_NEW_TGT_DIR = "/staging/eai/wo/in";
	public final String GLOVIA_FTP_SHP_TGT_DIR = "/staging/eai/wc/in";
	public final String GLOVIA_FTP_CAN_TGT_DIR = "/staging/eai/canc/in";
	

	// MQ Constants
	// ---------------------------------------
	public final String ILS_DIFY_QUEUE = "AVY.DIFY.QL.AVY.ILS.ORDER.OUT";
	public final String ILS_DIFY_ACK_QUEUE = "AVY.ILS.QL.AVY.DIFY.CONFIRM.IN";
	public final String GLV_DIFY_ACK_QUEUE = "AVY.GLV.QL.AVY.DIFY.CONFIRM.IN";


	// File System
	// ---------------------------------------
	public final String MQManager = "AVYWBDEV";
	public final String MQHostname = "MPDVADC01";
	public final String MQPort = "1432";
	public final String MQChannel = "SYSTEM.ADMIN.SVRCONN";
	public final String MSGSize = "4194304";
	public final String POLL_FREQ = "600000";
	public final String ACK_POLL_FREQ = "300000";
	
	public final String UPLOAD_DIR = "c:\\temp\\DIFY\\data_orders\\in";
	public final String WORK_DIR = "c:\\temp\\DIFY\\data_orders\\work";
	public final String ERROR_DIR = "c:\\temp\\DIFY\\data_orders\\err";
	public final String ARCHIVE_DIR = "c:\\temp\\DIFY\\data_orders\\archive";

	public final String GLOVIA_OUT_DIR = "c:\\temp\\DIFY\\data_orders\\glovia\\out";
	public final String GLOVIA_ARCHIVE_DIR = "c:\\temp\\DIFY\\data_orders\\glovia\\archive";
	public final String GLOVIA_ACK_DIR = "c:\\temp\\DIFY\\data_orders\\glovia\\ack";

	public final String ILS_ARCHIVE_DIR = "c:\\temp\\DIFY\\data_orders\\ils\\archive";
	public final String ILS_ACK_DIR = "c:\\temp\\DIFY\\data_orders\\ils\\ack";

	public final String REPORT_DIR = "c:\\temp\\DIFY\\data_orders\\reports";

	public final String ETSY_CUST_ID = "977184";

	// Logging
	// ---------------------------------------
	public final String DEBUG = "Y";

	
	// Error Codes
	// ---------------------------------------
	public final String ERR_SYS_NO_UPLOAD_DIR = "SYS1001";
	public final String ERR_SYS_NO_ARCHIVE_DIR = "SYS1002";
	public final String ERR_SYS_NO_ERROR_DIR = "SYS1003";
	public final String ERR_SYS_NO_WORK_DIR = "SYS1004";
	public final String ERR_SYS_FILE_RENAME = "SYS1005";
	public final String ERR_SYS_FILE_MOVE = "SYS1006";

}
