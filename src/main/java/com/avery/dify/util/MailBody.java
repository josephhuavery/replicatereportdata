package com.avery.dify.util;

import com.avery.dify.export.ILSOrder;
import com.avery.dify.export.ILSOrderDetail;
import com.avery.dify.export.SalesFlatCreditmemoComment;

import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class MailBody {

    public static String buildOrderEmail(ILSOrder orderBn) {
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat dfs = new SimpleDateFormat("MM/dd/yyyy HH:mm z");

        String subject = "Duplicate Order " + orderBn.getOrderId() + " Received from Magento at " + dfs.format(new Date());

        StringBuffer sb = new StringBuffer();
        sb.append("<html>");
        sb.append("<head></head>");
        sb.append("<style type=\"text/css\">");
        sb.append("<!--");
        sb.append("body,table {font: 14px Verdana, Arial, Helvetica, sans-serif;}");
        sb.append(".header    {font: 12px Verdana, Arial, Helvetica, sans-serif; font-weight: bold; color: #FFFFFF; background-color: #0000FF;}");
        sb.append(".label1    {font: 12px Verdana, Arial, Helvetica, sans-serif; font-weight: bold;}");
        sb.append("-->");
        sb.append("</style>");
        sb.append("<body>");
        sb.append(subject);
        sb.append("<br/>");
        sb.append("<br/>");
        sb.append("<table  width=\"780\" cellpadding=\"3\" cellspacing=\"0\" border=\"1\">");
        sb.append("  <tr>");
        sb.append("    <th width=\"195\" class=\"header\">Order Number</th>");
        sb.append("    <th width=\"195\" class=\"header\">Tracking Number</th>");
        sb.append("    <th width=\"195\" class=\"header\">Order Date</th>");
        sb.append("    <th width=\"195\" class=\"header\">Ship Date</th>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        if (orderBn.isRetailOrder()) {
            sb.append(orderBn.getCustomerPONumber()).append("A</td>");
        } else if (orderBn.isEStoreOrder()) {
            sb.append(orderBn.getCustomerPONumber()).append("A</td>");
        } else if (orderBn.isServiceOrder()) {
            sb.append(orderBn.getCustomerPONumber()).append("S</td>");
        } else if (orderBn.isOnlineAisleOrder()) {
            sb.append(orderBn.getCustomerPONumber()).append("B</td>");
        } else {
            sb.append(orderBn.getCustomerPONumber()).append("</td>");
        }
        sb.append("    <td align=\"center\">").append(orderBn.getCarrierTrackingNum() != null? orderBn.getCarrierTrackingNum() : "").append("</td>");
        sb.append("    <td align=\"center\">").append(orderBn.getPoDate()!=null? df.format(orderBn.getPoDate()) : "").append("</td>");
        sb.append("    <td align=\"center\">").append(orderBn.getShipDate()!=null? df.format(orderBn.getShipDate()) : "").append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <th width=\"390\" class=\"header\" colspan=\"2\">Bill To</th>");
        sb.append("    <th width=\"390\" class=\"header\" colspan=\"2\">Ship To</th>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <td colspan=\"2\">").append(orderBn.getBillToHTML()).append("</td>");
        sb.append("    <td colspan=\"2\">").append(orderBn.isServiceOrder() ? orderBn.getBillToHTML() : orderBn.getShipToHTML()).append("</td>");
        sb.append("  </tr>");
        sb.append("</table>");
        sb.append("<br/>");
        sb.append("<table width=\"780\" cellpadding=\"2\" cellspacing=\"0\" border=\"1\">");
        sb.append("  <tr>");
        sb.append("    <th width=\"130\" class=\"header\">SKU</th>");
        sb.append("    <th width=\"350\" class=\"header\">Description</th>");
        sb.append("    <th width=\"60\"  class=\"header\">Qty</th>");
        sb.append("    <th width=\"60\"  class=\"header\">Unit</th>");
        sb.append("    <th width=\"60\"  class=\"header\">Unit Price</th>");
        sb.append("    <th width=\"100\" class=\"header\">Extended Amount</th>");
        sb.append("  </tr>");

        Iterator<ILSOrderDetail> iter = orderBn.getOrderDetails().iterator();
        while (iter.hasNext()) {
            ILSOrderDetail orderDtl = (ILSOrderDetail) iter.next();
            sb.append("  <tr>");
            sb.append("    <td>").append(orderDtl.getCustomerItemNum()).append("</td>");
            sb.append("    <td>").append(orderDtl.getItemDescription()).append("</td>");
            sb.append("    <td align=\"right\">").append(orderDtl.getPoQuantity()).append("</td>");
            sb.append("    <td align=\"center\">").append("EA").append("</td>");
            sb.append("    <td align=\"right\">").append(orderDtl.getPoUnitPrice().divide(orderDtl.getPoQuantity()).setScale(2, RoundingMode.HALF_UP)).append("</td>");
            sb.append("    <td align=\"right\">").append(orderDtl.getPoUnitPrice().setScale(2, RoundingMode.HALF_UP)).append("</td>");
            sb.append("  </tr>");
        }

        sb.append("  <tr>");
        sb.append("    <td>&nbsp;</td>");
        sb.append("    <td>&nbsp;</td>");
        sb.append("    <td align=\"right\">&nbsp;</td>");
        sb.append("    <td align=\"center\">&nbsp;</td>");
        sb.append("    <td align=\"right\">&nbsp;</td>");
        sb.append("    <td align=\"right\">&nbsp;</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <td></td><td class=\"label1\" align=\"right\">SUBTOTAL:</td><td></td><td></td><td></td><td align=\"right\">").append(orderBn.getSubtotal().setScale(2, RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <td></td><td class=\"label1\" align=\"right\">SHIPPING:</td><td></td><td></td><td></td><td align=\"right\">").append(orderBn.getShipping().setScale(2, RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <td></td><td class=\"label1\" align=\"right\">SHIPPING DISCOUNT:</td><td></td><td></td><td></td><td align=\"right\">").append(orderBn.getShippingDiscount().setScale(2, RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <td></td><td class=\"label1\" align=\"right\">PROMOTION:</td><td></td><td></td><td></td><td align=\"right\">").append(orderBn.getDiscount().setScale(2, RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <td></td><td class=\"label1\" align=\"right\">TAX:</td><td></td><td></td><td></td><td align=\"right\">").append(orderBn.getTax().setScale(2, RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <td></td><td class=\"label1\" align=\"right\">ADJUSTMENT:</td><td></td><td></td><td></td><td align=\"right\">").append(orderBn.getAdjustment() == null ? "0.00" : orderBn.getAdjustment().setScale(2, RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <td></td><td class=\"label1\" align=\"right\">GRAND TOTAL:</td><td></td><td></td><td></td><td align=\"right\">").append(orderBn.getTotal().setScale(2, RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("</table>");
        sb.append("<body>");

        return sb.toString();
    }


    public static String buildOrderEmailCM(ILSOrder orderBn) {
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat dfs = new SimpleDateFormat("MM/dd/yyyy HH:mm z");

        String subject = "Duplicate Credit Memo "
                + orderBn.getCreditMemoNumber() + " Received from Magento at "
                + dfs.format(new Date());

        StringBuffer sb = new StringBuffer();
        sb.append("<html>");
        sb.append("<head></head>");
        sb.append("<style type=\"text/css\">");
        sb.append("<!--");
        sb.append("body,table {font: 14px Verdana, Arial, Helvetica, sans-serif;}");
        sb.append(".header    {font: 12px Verdana, Arial, Helvetica, sans-serif; font-weight: bold; color: #FFFFFF; background-color: #0000FF;}");
        sb.append(".label1    {font: 12px Verdana, Arial, Helvetica, sans-serif; font-weight: bold;}");
        sb.append("-->");
        sb.append("</style>");
        sb.append("<body>");
        sb.append(subject);
        sb.append("<br/>");
        sb.append("<br/>");
        sb.append("<table  width=\"780\" cellpadding=\"3\" cellspacing=\"0\" border=\"1\">");
        sb.append("  <tr>");
        sb.append("    <th width=\"195\" class=\"header\">Order Number</th>");
        sb.append("    <th width=\"195\" class=\"header\">Tracking Number</th>");
        sb.append("    <th width=\"195\" class=\"header\">Order Date</th>");
        sb.append("    <th width=\"195\" class=\"header\">Ship Date</th>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <td align=\"center\">");

        if (orderBn.isRetailOrder()) {
            sb.append(orderBn.getCustomerPONumber()).append("A</td>");
        } else if (orderBn.isEStoreOrder()) {
            sb.append(orderBn.getCustomerPONumber()).append("A</td>");
        } else if (orderBn.isServiceOrder()) {
            sb.append(orderBn.getCustomerPONumber()).append("S</td>");
        } else if (orderBn.isOnlineAisleOrder()) {
            sb.append(orderBn.getCustomerPONumber()).append("B</td>");
        } else {
            sb.append(orderBn.getCustomerPONumber()).append("</td>");
        }

        sb.append("    <td align=\"center\">")
                .append(orderBn.getCarrierTrackingNum()).append("</td>");
        sb.append("    <td align=\"center\">")
                .append(orderBn.getPoDate() != null ? df.format(orderBn
                        .getPoDate()) : "").append("</td>");
        sb.append("    <td align=\"center\">")
                .append(orderBn.getShipDate() != null ? df.format(orderBn
                        .getShipDate()) : "").append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <th width=\"390\" class=\"header\" colspan=\"2\">Bill To</th>");
        sb.append("    <th width=\"390\" class=\"header\" colspan=\"2\">Ship To</th>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <td colspan=\"2\">").append(orderBn.getBillToHTML()).append("</td>");
        sb.append("    <td colspan=\"2\">").append(orderBn.isServiceOrder() ? orderBn.getBillToHTML() : orderBn.getShipToHTML()).append("</td>");
        sb.append("  </tr>");
        sb.append("</table>");
        sb.append("<br/>");
        sb.append("<table width=\"780\" cellpadding=\"2\" cellspacing=\"0\" border=\"1\">");
        sb.append("  <tr>");
        sb.append("    <th width=\"130\" class=\"header\">SKU</th>");
        sb.append("    <th width=\"350\" class=\"header\">Description</th>");
        sb.append("    <th width=\"60\"  class=\"header\">Qty</th>");
        sb.append("    <th width=\"60\"  class=\"header\">Unit</th>");
        sb.append("    <th width=\"60\"  class=\"header\">Unit Price</th>");
        sb.append("    <th width=\"100\" class=\"header\">Extended Amount</th>");
        sb.append("  </tr>");

        Iterator<ILSOrderDetail> iter = orderBn.getOrderDetails().iterator();
        while (iter.hasNext()) {
            ILSOrderDetail orderDtl = (ILSOrderDetail) iter.next();
            sb.append("  <tr>");
            sb.append("    <td>").append(orderDtl.getCustomerItemNum())
                    .append("</td>");
            sb.append("    <td>").append(orderDtl.getItemDescription())
                    .append("</td>");
            sb.append("    <td align=\"right\">")
                    .append(orderDtl.getPoQuantity()).append("</td>");
            sb.append("    <td align=\"center\">").append("EA").append("</td>");
            sb.append("    <td align=\"right\">")
                    .append(orderDtl.getPoUnitPrice()
                            .divide(orderDtl.getPoQuantity())
                            .setScale(2, RoundingMode.HALF_UP)).append("</td>");
            sb.append("    <td align=\"right\">")
                    .append(orderDtl.getPoUnitPrice().setScale(2,
                            RoundingMode.HALF_UP)).append("</td>");
            sb.append("  </tr>");
        }

        sb.append("  <tr>");
        sb.append("    <td>&nbsp;</td>");
        sb.append("    <td>&nbsp;</td>");
        sb.append("    <td align=\"right\">&nbsp;</td>");
        sb.append("    <td align=\"center\">&nbsp;</td>");
        sb.append("    <td align=\"right\">&nbsp;</td>");
        sb.append("    <td align=\"right\">&nbsp;</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append(
                "    <td></td><td class=\"label1\" align=\"right\">SUBTOTAL:</td><td></td><td></td><td></td><td align=\"right\">")
                .append(orderBn.getSubtotalRefunded().setScale(2,
                        RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append(
                "    <td></td><td class=\"label1\" align=\"right\">SHIPPING:</td><td></td><td></td><td></td><td align=\"right\">")
                .append(orderBn.getShippingRefunded().setScale(2,
                        RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append(
                "    <td></td><td class=\"label1\" align=\"right\">SHIPPING DISCOUNT:</td><td></td><td></td><td></td><td align=\"right\">")
                .append(orderBn.getShippingDiscountRefunded().setScale(2,
                        RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append(
                "    <td></td><td class=\"label1\" align=\"right\">PROMOTION:</td><td></td><td></td><td></td><td align=\"right\">")
                .append(orderBn.getDiscountRefunded().setScale(2,
                        RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append(
                "    <td></td><td class=\"label1\" align=\"right\">TAX:</td><td></td><td></td><td></td><td align=\"right\">")
                .append(orderBn.getTaxRefunded().setScale(2,
                        RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append(
                "    <td></td><td class=\"label1\" align=\"right\">ADJUSTMENT:</td><td></td><td></td><td></td><td align=\"right\">")
                .append(orderBn.getAdjustment().setScale(2,
                        RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append(
                "    <td></td><td class=\"label1\" align=\"right\">GRAND TOTAL:</td><td></td><td></td><td></td><td align=\"right\">")
                .append(orderBn.getTotalRefunded().setScale(2,
                        RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("</table>");
        sb.append("<body>");

        return sb.toString();
    }


    public static String buildNoDetailCreditMemo(ILSOrder orderBn, List<SalesFlatCreditmemoComment> CMComments, int naming) {
        SimpleDateFormat dfs = new SimpleDateFormat("MM/dd/yyyy HH:mm z");
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

        String subject = "Credit Memo " + orderBn.getCreditMemoNumber()
                + " with only special charge refunds received from Magento at "
                + dfs.format(new Date());

        StringBuffer sb = new StringBuffer();
        sb.append("<html>");
        sb.append("<head></head>");
        sb.append("<style type=\"text/css\">");
        sb.append("<!--");
        sb.append("body,table {font: 14px Verdana, Arial, Helvetica, sans-serif;}");
        sb.append(".header    {font: 12px Verdana, Arial, Helvetica, sans-serif; font-weight: bold; color: #FFFFFF; background-color: #0000FF;}");
        sb.append(".label1    {font: 12px Verdana, Arial, Helvetica, sans-serif; font-weight: bold;}");
        sb.append("-->");
        sb.append("</style>");
        sb.append("<body>");
        sb.append(subject);
        sb.append("<br/>");
        sb.append("<br/>");
        sb.append("<table  width=\"780\" cellpadding=\"3\" cellspacing=\"0\" border=\"1\">");
        sb.append("  <tr>");
        sb.append("    <th width=\"195\" class=\"header\">Order Number</th>");
        sb.append("    <th width=\"195\" class=\"header\">Tracking Number</th>");
        sb.append("    <th width=\"195\" class=\"header\">Order Date</th>");
        sb.append("    <th width=\"195\" class=\"header\">Ship Date</th>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <td align=\"center\">");

        if (naming == 2) {
            sb.append(orderBn.getCustomerPONumber()).append("A</td>");
        } else if (naming == 3) {
            sb.append(orderBn.getCustomerPONumber()).append("S</td>");
        } else if (naming == 4) {
            sb.append(orderBn.getCustomerPONumber()).append("B</td>");
        } else {
            sb.append(orderBn.getCustomerPONumber()).append("</td>");
        }

        sb.append("    <td align=\"center\">").append(orderBn.getCarrierTrackingNum() != null? orderBn.getCarrierTrackingNum() : "").append("</td>");
        sb.append("    <td align=\"center\">").append(orderBn.getPoDate() != null ? df.format(orderBn.getPoDate()) : "").append("</td>");
        sb.append("    <td align=\"center\">").append(orderBn.getShipDate() != null ? df.format(orderBn.getShipDate()) : "").append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <th width=\"390\" class=\"header\" colspan=\"2\">Bill To</th>");
        sb.append("    <th width=\"390\" class=\"header\" colspan=\"2\">Ship To</th>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("    <td colspan=\"2\">").append(orderBn.getBillToHTML()).append("</td>");
        sb.append("    <td colspan=\"2\">").append(orderBn.isServiceOrder() ? orderBn.getBillToHTML() : orderBn.getShipToHTML()).append("</td>");
        sb.append("  </tr>");
        sb.append("</table>");
        sb.append("<br/>");
        sb.append("<table width=\"780\" cellpadding=\"2\" cellspacing=\"0\" border=\"1\">");
        sb.append("  <tr>");
        sb.append("    <th width=\"130\" class=\"header\">SKU</th>");
        sb.append("    <th width=\"350\" class=\"header\">Description</th>");
        sb.append("    <th width=\"60\"  class=\"header\">Qty</th>");
        sb.append("    <th width=\"60\"  class=\"header\">Unit</th>");
        sb.append("    <th width=\"60\"  class=\"header\">Unit Price</th>");
        sb.append("    <th width=\"100\" class=\"header\">Extended Amount</th>");
        sb.append("  </tr>");

        sb.append("  <tr>");
        sb.append("    <td>&nbsp;</td>");
        sb.append("    <td>&nbsp;</td>");
        sb.append("    <td align=\"right\">&nbsp;</td>");
        sb.append("    <td align=\"center\">&nbsp;</td>");
        sb.append("    <td align=\"right\">&nbsp;</td>");
        sb.append("    <td align=\"right\">&nbsp;</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append(
                "    <td></td><td class=\"label1\" align=\"right\">SHIPPING:</td><td></td><td></td><td></td><td align=\"right\">")
                .append(orderBn.getShippingRefunded().setScale(2,
                        RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append(
                "    <td></td><td class=\"label1\" align=\"right\">SHIPPING DISCOUNT:</td><td></td><td></td><td></td><td align=\"right\">")
                .append(orderBn.getShippingDiscountRefunded().setScale(2,
                        RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append(
                "    <td></td><td class=\"label1\" align=\"right\">PROMOTION:</td><td></td><td></td><td></td><td align=\"right\">")
                .append(orderBn.getDiscountRefunded().setScale(2,
                        RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append(
                "    <td></td><td class=\"label1\" align=\"right\">TAX:</td><td></td><td></td><td></td><td align=\"right\">")
                .append(orderBn.getTaxRefunded().setScale(2,
                        RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append(
                "    <td></td><td class=\"label1\" align=\"right\">ADJUSTMENT:</td><td></td><td></td><td></td><td align=\"right\">")
                .append(orderBn.getAdjustment().setScale(2,
                        RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append(
                "    <td></td><td class=\"label1\" align=\"right\">GRAND TOTAL:</td><td></td><td></td><td></td><td align=\"right\">")
                .append(orderBn.getTotalRefunded().setScale(2,
                        RoundingMode.HALF_UP)).append("</td>");
        sb.append("  </tr>");
        sb.append("</table>");
        sb.append("<br/>");
        sb.append("<table width=\"780\" cellpadding=\"2\" cellspacing=\"0\" border=\"1\">");
        sb.append("  <tr>");
        sb.append("    <th width=\"100\" class=\"header\">Customer ID</th>");
        sb.append("    <th width=\"200\" class=\"header\">Customer Name</th>");
        sb.append("    <th width=\"480\"  class=\"header\">Customer Email</th>");
        sb.append("  </tr>");
        sb.append("<tr><td>").append(orderBn.getCustomerID()).append("</td>");
        sb.append("<td>").append(orderBn.getCustomerName()).append("</td>");
        sb.append("<td>").append(orderBn.getCustomerEmail()).append("</td></tr>");
        sb.append("</table>");

        if (CMComments.size() > 0){
            Iterator<SalesFlatCreditmemoComment> iter = CMComments.iterator();
            sb.append("<br/><table width=\"780\" cellpadding=\"2\" cellspacing=\"0\" border=\"1\">");
            sb.append("  <tr>");
            sb.append("    <th width=\"600\" class=\"header\">Comment</th>");
            sb.append("    <th width=\"180\" class=\"header\">Comment Date</th>");
            sb.append("  </tr>");
            while (iter.hasNext()) {
                SalesFlatCreditmemoComment ccmnt = (SalesFlatCreditmemoComment)iter.next();
                sb.append("  <tr><td>").append(ccmnt.getCOMMENT_()).append("  </td><td>").append(ccmnt.getCreatedAt()).append("  </td></tr>");
            }
            sb.append("</table>");
        }

        sb.append("<body>");

        return sb.toString();
    }
}
