/*
 * Created on Aug 11, 2005
 *
 * Copyright (c) 2005 Avery Dennison
 *
 * This software is the confidential and proprietary information of
 * Avery Dennison ("Confidential Information").
 */

package com.avery.dify.util;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;

import com.avery.dify.ExportToILS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;


/**
 * This class is used to connect and access MQ 
 * 
 * @author RuleMeister, Inc.
 * @version 1.0
 */
public class MQUtil {

	private static Logger logger = LoggerFactory.getLogger(MQUtil.class);

    /** Data member to hold MQ mgr. */
    private MQQueueManager qmgr = null;
    /** Data member to hold MQ queue. */
    private MQQueue queue = null;
    private int msgNum = 0;
	private int transferDepth = 500;
	private int sleepTime = 60000;
	private int maxTries = 3;
	private int numTries = 0;

    
    /** Default constructor. */
    public MQUtil() {}
    
    public MQUtil(String queue) throws Exception {
    	connectQueueMgr();
    	connectQueue(queue);
    }
    
    public MQUtil(String qMgr, String host, int port, String queue) throws Exception {
    	connectQueueMgr(qMgr, host, port);
    	connectQueue(queue);
    }

    public MQUtil(String queue, String type) throws Exception {
    	connectQueueMgr();
    	if (type.equalsIgnoreCase("GET"))
        	connectQueueForGet(queue);
    	else
    		connectQueue(queue);
    }
	
    /** This method is called to make an MQ Queue Manager connection.
     * @throws Exception if MQ Queue connection fails
     */
	public void connectQueueMgr() throws Exception {
		try {
			logger.info("Connect queue manager.", new Date());
			if (qmgr == null) {
	            // setup queue manager connection
	            String qManager = DIFYProperty.MQManager;
	            String hostName = DIFYProperty.MQHostname;
	            
	            MQEnvironment.hostname = hostName;
	            MQEnvironment.port = Integer.parseInt(DIFYProperty.MQPort);
	            MQEnvironment.channel = DIFYProperty.MQChannel;
	            qmgr = new MQQueueManager(qManager) ;
			}
		}
        catch (Exception e) {
            throw new Exception("Exception in " + MQUtil.class.getName() + ".connectQueueMgr(): " + 
            		e.getMessage());
        }
	}

	public void connectQueueMgr(String qMgr, String host, int port) throws Exception {
		try {
			logger.info("Connect queue manager.", new Date());
			if (qmgr == null) {
	            // setup queue manager connection
	            String qManager = qMgr;
	            MQEnvironment.hostname = host;
	            MQEnvironment.port = port;
	            MQEnvironment.channel = DIFYProperty.MQChannel;
	            qmgr = new MQQueueManager(qManager) ;
			}
		}
        catch (Exception e) {
            throw new Exception("Exception in " + MQUtil.class.getName() + ".connectQueueMgr(): " + 
            		e.getMessage());
        }
	}

	
    /** This method is called to make a MQ Queue connection.
     * @throws Exception if MQ Queue connection fails
     */
    public void connectQueue(String queueName) throws Exception {
        try {
			logger.info("Connect queue " + queueName, new Date());
        	numTries = 0;
        	int openOptions = MQC.MQOO_OUTPUT | MQC.MQOO_INQUIRE;
            queue = qmgr.accessQueue(queueName, openOptions, null, null, null);
        	msgNum = queueDepth();
        	int availDepth = availableQueueDepth();
			if (availDepth < transferDepth) {
				throw new Exception("Insufficient Queue Depth to perform transfer - " + availDepth + " of " +
						maxQueueDepth() + " available.");
			}
        }
        catch (Exception e) {
            throw new Exception("Exception in " + MQUtil.class.getName() + ".connectQueues(): \n" + 
            		e.getMessage());
        }
    }

    public void connectQueueForGet(String queueName) throws Exception {
        try {
			logger.info("Connect queue " + queueName, new Date());
        	numTries = 0;
        	int openOptions = MQC.MQOO_INQUIRE + MQC.MQOO_FAIL_IF_QUIESCING
    				+ MQC.MQOO_INPUT_SHARED;
            queue = qmgr.accessQueue(queueName, openOptions, null, null, null);
        }
        catch (Exception e) {
            throw new Exception("Exception in " + MQUtil.class.getName() + ".connectQueues(): \n" + 
            		e.getMessage());
        }
    }

    /** This method is called to close a MQ Queue connection 
     * @throws Exception if MQ Queue connection close fails
     */
    public void closeQueue() throws Exception {
        try {
            // close queues
            if (queue != null) {
            	queue.close() ;
            	queue = null;
            }
        }
        catch (MQException e) {
            throw new Exception("Exception in " + MQUtil.class.getName() + ".closeQueue(): " + 
            		e.getMessage());
        }
    }

    /** This method is called to close a MQ Queue connection 
     * @throws Exception if MQ Queue connection close fails
     */
    public void closeQueueManager() throws Exception {
        try {
            // close queue manager
            if (qmgr != null) {
            	qmgr.disconnect() ;
            	qmgr = null;
            }
        }
        catch (Exception e) {
            throw new Exception("Exception in " + MQUtil.class.getName() + ".closeQueueManager(): " + 
            		e.getMessage());
        }
    }
    
    public int queueDepth() throws Exception {
    	try {
    		return queue.getCurrentDepth();
    	}
	    catch (Exception e) {
	        throw new Exception("Exception in " + MQUtil.class.getName() + ".queueDepth(): " + 
	        		e.getMessage());
	    }
    }

    public int maxQueueDepth() throws Exception {
    	try {
    		return queue.getMaximumDepth();
    	}
	    catch (Exception e) {
	        throw new Exception("Exception in " + MQUtil.class.getName() + ".queueDepth(): " + 
	        		e.getMessage());
	    }
    }

    public int availableQueueDepth() throws Exception {
    	try {
    		return queue.getMaximumDepth() - queue.getCurrentDepth();
    	}
	    catch (Exception e) {
	        throw new Exception("Exception in " + MQUtil.class.getName() + ".availableQueueDepth(): " + 
	        		e.getMessage());
	    }
    }

    /** This method is called to send messages to the MQ queue
	 *
     * @throws Exception if put message fails
     */
    public void putMsg(String s) throws Exception {
        MQMessage mBuf = null;
        MQPutMessageOptions pmo = null;    	
        try {
    		// check if queue has room for the next <transferDepth> messages
    		if ((msgNum % transferDepth)==0) {
    			boolean qReady = false;
    			while (!qReady) {
    				int availDepth = availableQueueDepth();
    				if (availDepth >= transferDepth) {
    					qReady = true;
    					numTries = 0;
    				}
    				if (availDepth < transferDepth) {
    					logger.info("Insufficient Queue Depth - " + availDepth + " of " +
    							maxQueueDepth() + " available.");
						logger.info("    Attempt " + numTries);
						logger.info("    Program will sleep for " + sleepTime + " ms");
    					Thread.sleep(sleepTime);
    					qReady = false;
    					numTries++;
    					if (numTries > maxTries) throw new Exception("Unable to write to queue.");
    				}
    			}
    		}

            pmo = new MQPutMessageOptions() ;
            pmo.options = MQC.MQPMO_NONE ;

            mBuf = new MQMessage() ;
            mBuf.clearMessage() ;
            mBuf.correlationId = MQC.MQCI_NONE ;
            mBuf.messageId = MQC.MQMI_NONE ;
            mBuf.format = MQC.MQFMT_STRING ;
            mBuf.writeString(s) ;
            
            queue.put(mBuf, pmo) ;
            msgNum++;
    		if ((msgNum%100)==0)
    			logger.info(" ---> Exporting message " + msgNum, new Date());
        }
        catch (Exception e) {
            throw new Exception("Exception in " + MQUtil.class.getName() + ".putMsg(): " + 
            		e.getMessage());
        }
        finally {
        	mBuf = null;
        	pmo = null;
        }
    }

    public Document getMsg() throws Exception {
    	int depth = 0;
    	Document doc = null;
        MQMessage mBuf = null;
        MQGetMessageOptions gmo = null;    	
        try {
    		depth = queue.getCurrentDepth();
    		logger.info("Current depth: " + depth + "\n", new Date());
    		if (depth == 0) {
    			return null;
    		}

            gmo = new MQGetMessageOptions() ;
            gmo.waitInterval = 5000;
            gmo.options = MQC.MQGMO_NO_WAIT + MQC.MQGMO_FAIL_IF_QUIESCING
    				+ MQC.MQGMO_CONVERT;

            mBuf = new MQMessage() ;
            mBuf.clearMessage() ;
            mBuf.correlationId = MQC.MQCI_NONE ;
            mBuf.messageId = MQC.MQMI_NONE ;
            mBuf.format = MQC.MQFMT_STRING ;
            
            queue.get(mBuf, gmo) ;
            
            byte[] buffer = new byte[mBuf.getTotalMessageLength()];
            mBuf.readFully(buffer);

            String message = new String(buffer);
            message = message.substring(message.indexOf("<?xml version=\"1.0\" encoding=\"UTF-8\""));			
            doc = XmlUtils.getDocument(message);
        }	
        catch (IOException e) {
			logger.error("IOException during GET: " + e.getMessage(), new Date());
            throw new Exception(MQUtil.class.getName() + ": IOException during GET: " + e.getMessage());
		} 
        catch (MQException e) {
			if (e.completionCode == 2 && e.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE) {
				if (depth > 0) {
					logger.error("All messages read.");
				}
			} else {
				logger.error("GET Exception: " + e);
	            throw new Exception(MQUtil.class.getName() + ": GET Exception: " + e.getMessage());
			}
		}
        catch (Exception e) {
            throw new Exception("Exception in " + MQUtil.class.getName() + ".getMsg(): " + e.getMessage());
        }
        finally {
        	mBuf = null;
        	gmo = null;
        }
        return doc;
    }

    public void putTextFile(String filepath, String filename) throws Exception {
        MQMessage mBuf = null;
        MQPutMessageOptions pmo = null;    	
        BufferedReader br = null;
        try {
    		// check if queue has room for the next <transferDepth> messages
    		if ((msgNum % transferDepth)==0) {
    			boolean qReady = false;
    			while (!qReady) {
    				int availDepth = availableQueueDepth();
    				if (availDepth >= transferDepth) {
    					qReady = true;
    					numTries = 0;
    				}
    				if (availDepth < transferDepth) {
    					logger.info("Insufficient Queue Depth - " + availDepth + " of " +
    							maxQueueDepth() + " available.");
						logger.info("    Attempt " + numTries);
						logger.info("    Program will sleep for " + sleepTime + " ms");
    					Thread.sleep(sleepTime);
    					qReady = false;
    					numTries++;
    					if (numTries > maxTries) throw new Exception("Unable to write to queue.");
    				}
    			}
    		}

            pmo = new MQPutMessageOptions() ;
            pmo.options = MQC.MQPMO_NONE ;

            mBuf = new MQMessage() ;
            mBuf.clearMessage() ;
            mBuf.correlationId = MQC.MQCI_NONE ;
            mBuf.messageId = MQC.MQMI_NONE ;
            mBuf.format = MQC.MQFMT_STRING ;
            
            String strItemLine = "";
            br = new BufferedReader(new FileReader(new File(filepath + filename)));
            while ((strItemLine = br.readLine()) != null) {
                mBuf.writeString(strItemLine) ;
            }
            br.close();
            
            queue.put(mBuf, pmo) ;
            msgNum++;
    		if ((msgNum%100)==0)
    			logger.info(" ---> Exporting message " + msgNum, new Date());
        }
        catch (Exception e) {
            throw new Exception("Exception in " + MQUtil.class.getName() + ".putMsg(): " + 
            		e.getMessage());
        }
        finally {
        	mBuf = null;
        	pmo = null;
			try {if (br != null) br.close();} catch (Exception e) {}
        }
    }
    /** Test Stub
     */
    public static void main(String[] args) {
        try {
        }
        catch (Exception e) {
			logger.error(e.getMessage());
        }
    }
}
