package com.avery.dify.util;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {

    // convert from local time to GMT time
	public static Date convertPSTDateToGMT(Date syncDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(syncDate);

		TimeZone fromTimeZone = calendar.getTimeZone();
		TimeZone toTimeZone = TimeZone.getTimeZone("GMT");
		
		calendar.setTimeZone(fromTimeZone);
		calendar.add(Calendar.MILLISECOND, fromTimeZone.getRawOffset() * -1);
		if (fromTimeZone.inDaylightTime(calendar.getTime())) {
		    calendar.add(Calendar.MILLISECOND, calendar.getTimeZone().getDSTSavings() * -1);
		}
		
		calendar.add(Calendar.MILLISECOND, toTimeZone.getRawOffset());
		if (toTimeZone.inDaylightTime(calendar.getTime())) {
		    calendar.add(Calendar.MILLISECOND, toTimeZone.getDSTSavings());
		}
		
		return calendar.getTime();
    }
	
	public static Date convertGMTToPST(Date gmtDate) {
		TimeZone fromTimeZone = TimeZone.getTimeZone("GMT");
		TimeZone toTimeZone = TimeZone.getTimeZone("PST");

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(fromTimeZone);
		calendar.setTime(gmtDate);
		
		calendar.add(Calendar.MILLISECOND, fromTimeZone.getRawOffset() * -1);
		if (fromTimeZone.inDaylightTime(calendar.getTime())) {
		    calendar.add(Calendar.MILLISECOND, calendar.getTimeZone().getDSTSavings() * -1);
		}
		
		calendar.add(Calendar.MILLISECOND, toTimeZone.getRawOffset());
		if (toTimeZone.inDaylightTime(calendar.getTime())) {
		    calendar.add(Calendar.MILLISECOND, toTimeZone.getDSTSavings());
		}
		
		return calendar.getTime();
	}

}
