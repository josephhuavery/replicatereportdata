package com.avery.dify.util;

import java.io.IOException;
import java.io.Writer;
import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.util.CellReference;

public class SpreadsheetWriter {
	private final Writer _out;
	private int _rownum;
	private Map<Integer,Short> columnWidthMap=new TreeMap<Integer,Short>();
	 

	public SpreadsheetWriter(Writer out) {
		_out = out;
	}

	public void beginSheet() throws IOException {
		_out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\">");
/*
		if(!columnWidthMap.isEmpty()){
            _out.write("<cols>");
            Set keySet=columnWidthMap.keySet();
            Iterator iterator=keySet.iterator();
            while (iterator.hasNext()) {
                Integer col =  (Integer)iterator.next();
                _out.write("<col min=\""+col+"\" max=\""+col+"\" width=\""+columnWidthMap.get(col)+"\" customWidth=\"1\"/>");                           
            }
            _out.write("</cols>");
    	}
*/            

		_out.write("<sheetData>\n");
	}

	public void endSheet() throws IOException {
		_out.write("</sheetData>");
		_out.write("</worksheet>");
	}

	/**
	 * Insert a new row
	 *
	 * @param rownum
	 *            0-based row number
	 */
	public void insertRow(int rownum) throws IOException {
		_out.write("<row r=\"" + (rownum + 1) + "\">\n");
		this._rownum = rownum;
	}

	/**
	 * Insert row end marker
	 */
	public void endRow() throws IOException {
		_out.write("</row>\n");
	}

	public void createCell(int columnIndex, String value, int styleIndex)
			throws IOException {
		int width = 50;
		String ref = new CellReference(_rownum, columnIndex).formatAsString();
		_out.write("<c r=\"" + ref + "\" t=\"inlineStr\"");
		if (styleIndex != -1)
			_out.write(" s=\"" + styleIndex + "\"");
		_out.write(" w=\"" + width + "\"");
		_out.write(">");
		_out.write("<is><t><![CDATA[" + value + "]]></t></is>");
		_out.write("</c>");
	}

	public void createCell(int columnIndex, String value) throws IOException {
   	 	// value = value.replaceAll("�", " ");
		// value = value.replaceAll("�", "'");
		if (value == null) value = "";
		createCell(columnIndex, value, -1);
	}

	public void createCell(int columnIndex, double value, int styleIndex)
			throws IOException {
		String ref = new CellReference(_rownum, columnIndex).formatAsString();
		_out.write("<c r=\"" + ref + "\" t=\"n\"");
		if (styleIndex != -1)
			_out.write(" s=\"" + styleIndex + "\"");
		_out.write(">");
		_out.write("<v>" + value + "</v>");
		_out.write("</c>");
	}

	public void createNullDateCell(int columnIndex, int styleIndex)
	throws IOException {
		String ref = new CellReference(_rownum, columnIndex).formatAsString();
		_out.write("<c r=\"" + ref + "\" t=\"n\"");
		if (styleIndex != -1)
			_out.write(" s=\"" + styleIndex + "\"");
		_out.write(">");
		_out.write("<v></v>");
		_out.write("</c>");
	}

	public void createCell(int columnIndex, double value) throws IOException {
		createCell(columnIndex, value, -1);
	}

	public void createCell(int columnIndex, Calendar value, int styleIndex)
			throws IOException {
		if (value == null)
			createCell(columnIndex, "", styleIndex);
		else
			createCell(columnIndex, DateUtil.getExcelDate(value, false), styleIndex);
	}

	public void setCellWidth(int columnIndex,short width) {	 
        this.columnWidthMap.put(Integer.valueOf(columnIndex), Short.valueOf(width));
    }
 
    public void close() throws IOException{
        if(_out!=null)
                _out.close();
    }
 
}
