package com.avery.dify.util;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JschUtil {
    private JSch        jsch        = null;             
	private Session     session     = null; 
	private Channel     channel     = null; 
	private ChannelSftp channelSftp = null; 
	
	private SimpleDateFormat df     = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private static Logger logger = LoggerFactory.getLogger(JschUtil.class);


	public void open(String server, String username, String password, int port) throws Exception {
        jsch = new JSch();             
        session = jsch.getSession(username, server, port);             
        session.setPassword(password);             
        java.util.Properties config = new java.util.Properties();             
        config.put("StrictHostKeyChecking", "no");             
        session.setConfig(config);             
        session.connect();             
        channel = session.openChannel("sftp");             
        channel.connect();             
        channelSftp = (ChannelSftp)channel;             
	}

	public void close() throws Exception {
		if (jsch == null) return;
		if (channelSftp != null) channelSftp.disconnect();
		if (channel != null) channel.disconnect();
		if (session != null) session.disconnect();
		jsch = null;
	}

	public void sendFiles(String srcFolder, String tgtFolder, String archiveDir) throws Exception {
    	logger.info("[" + df.format(new Date()) + "]: Sending files in " + srcFolder);
		File dir = new File(srcFolder);    			
		File[] files = dir.listFiles();

		channelSftp.cd(tgtFolder);             

		// loop through files and send
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
    	    	if (!files[i].isDirectory()) {
					logger.info("[" + df.format(new Date()) + "]: ***** file " + files[i].getName());
    	    		channelSftp.put(new FileInputStream(files[i]), files[i].getName());  
    	    		
    	    		//String filepath = files[i].getName();
    	    		files[i].renameTo(new File(archiveDir, files[i].getName()));
    	    		//files[i].delete();
    	    	}
			}
		}
		logger.info("[" + df.format(new Date()) + "]: transfer complete ");
	}

	public void getFiles(String remoteFolder, String destFolder) throws Exception {

		channelSftp.cd(remoteFolder);             

        @SuppressWarnings("unchecked")
		Vector<ChannelSftp.LsEntry> files = channelSftp.ls("*");
		logger.info("[" + df.format(new Date()) + "]: Found %d files in dir %s%n", files.size(), remoteFolder);
 
        for (ChannelSftp.LsEntry file : files) {
            if (file.getAttrs().isDir()) {
                continue;
            }
			logger.info("[" + df.format(new Date()) + "]: ***** Getting file : %s%n", file.getFilename());
            channelSftp.get(file.getFilename(), destFolder);
            channelSftp.rm(file.getFilename());
        }

		logger.info("[" + df.format(new Date()) + "]: transfer complete ");
	}

}
