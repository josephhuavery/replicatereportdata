package com.avery.dify;
/*
 * Created on Feb 20, 2014
 *
 * Copyright (c) 2005 Avery Dennison
 *
 * This software is the confidential and proprietary information of
 * Avery Dennison ("Confidential Information").
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.avery.dify.export.AveryGloviaFile;
import com.avery.dify.export.GloviaOrder;
import com.avery.dify.export.OrderStatus;
import com.avery.dify.export.SalesFlatOrderForExport;
import com.avery.dify.util.DIFYProperty;
import com.avery.dify.util.JschUtil;
import com.avery.dify.util.StringUtils;
import com.github.davidmoten.rx.jdbc.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExportToGlovia {
    private static Logger logger = LoggerFactory.getLogger(ExportToGlovia.class);

    public void export(List<OrderStatus> orderList, Date receiptDt, Database db, boolean dosend) throws Exception {
        JschUtil jschUtil = null;
        PrintWriter pw = null;
        try {
            SimpleDateFormat df = new SimpleDateFormat("'ce_'MMddyyHHmm'dify.seq'");
            String ftpHost      = DIFYProperty.GLOVIA_FTP_HOST;
            int    ftpPort      = new Integer(DIFYProperty.GLOVIA_FTP_PORT);
            String ftpUser      = DIFYProperty.GLOVIA_FTP_USER;
            String ftpPwd       = DIFYProperty.GLOVIA_FTP_PWD;
            String ftpTgtDir    = DIFYProperty.GLOVIA_FTP_NEW_TGT_DIR;
            String outputDir    = DIFYProperty.GLOVIA_OUT_DIR;
            String archiveDir   = DIFYProperty.GLOVIA_ARCHIVE_DIR;
            String fileName     = df.format(receiptDt);
            String filepath     = outputDir + File.separator + fileName;


            final List<GloviaOrder> itemList = GloviaOrder.Companion.select(db, StringUtils.getSqlString(getOrderList(orderList)));

            final List<String> excList = GloviaOrder.Companion.selectExclusionList(db);

            try {
                AveryGloviaFile.Companion.insertItemFromOrder(db, itemList, excList);
            }
            catch (Exception e) {
                // catch error, update all dispositions here and return
                Iterator<OrderStatus> iter = orderList.iterator();
                while (iter.hasNext()) {
                    OrderStatus statusBn = (OrderStatus)iter.next();
                    statusBn.setGloviaUpdateStatus('F');
                    statusBn.setOrderDisp("Fail");
                    statusBn.setOrderDispComment(statusBn.getOrderDispComment().toString() == null ?
                            "Glovia Interface: An error occured while reading order information from the Staging Database: " + e.getMessage() + "\n" :
                            statusBn.getOrderDispComment().toString() + "Glovia Interface: An error occured while reading order information from the Staging Database: " + e.getMessage() + "\n");
                }
                return;
            }

            try {
                pw = new PrintWriter(new BufferedWriter(new FileWriter(filepath)));
                pw.println("CCN|Work Center|Quantity|SKU|Order Date|Order Number & Order Line|Product Type|");
            }
            catch (Exception e) {
                Iterator<OrderStatus> iter = orderList.iterator();
                while (iter.hasNext()) {
                    OrderStatus statusBn = (OrderStatus)iter.next();
                    statusBn.setGloviaUpdateStatus('F');
                    statusBn.setOrderDisp("Fail");
                    statusBn.setOrderDispComment(statusBn.getOrderDispComment().toString() == null ?
                            "Glovia Interface: An error occured while writing the export file: " + e.getMessage() + "\n" :
                            statusBn.getOrderDispComment().toString() + "Glovia Interface: An error occured while writing the export file: " + e.getMessage() + "\n");
                }
                return;
            }

            int total = 0;
            Iterator<GloviaOrder> iter = itemList.iterator();
            while (iter.hasNext()) {
                GloviaOrder orderBn = (GloviaOrder)iter.next();

                if (excList.stream().filter(p -> p.equals(orderBn.getItem())).collect(Collectors.toList()).size() == 0) {
                    pw.println(buildOrderLine(orderBn));
                    total++;
                }

            }

            pw.println("EOF|" + total + "|");
            pw.flush();
            pw.close();
            pw = null;

            // send file to Glovia
            try {
                if (dosend) {
                    jschUtil = new JschUtil();
                    jschUtil.open(ftpHost, ftpUser, ftpPwd, ftpPort);
                    jschUtil.sendFiles(outputDir, ftpTgtDir, archiveDir);
                    jschUtil.close();

                    logger.info("{} Order file has been sent to Glovia. Total count:" + String.valueOf(total) + ", " + fileName, new Date());
                    SalesFlatOrderForExport.Companion.updateMasterFlag(db, new HashMap<Integer, List<String>>(){{put(1, getOrderListFromGlovia(itemList));}});
                }

            }
            catch (Exception e) {
                Iterator<OrderStatus> iters = orderList.iterator();
                while (iters.hasNext()) {
                    OrderStatus statusBn = (OrderStatus)iters.next();
                    statusBn.setGloviaUpdateStatus('F');
                    statusBn.setOrderDisp("Fail");
                    statusBn.setOrderDispComment(statusBn.getOrderDispComment().toString() == null ?
                            "Glovia Interface: Could not send files to Glovia Server: " + e.getMessage() + "\n" :
                            statusBn.getOrderDispComment().toString() + " Glovia Interface: Could not send files to Glovia Server: " + e.getMessage() + "\n");
                }
                return;
            }

            // update status
            Iterator<OrderStatus> iterd = orderList.iterator();
            while (iterd.hasNext()) {
                OrderStatus statusBn = (OrderStatus)iterd.next();
                statusBn.setGloviaUpdatedAt(receiptDt);
                statusBn.setGloviaFilename(fileName);
            }

        }
        catch (Exception e) {
            throw new Exception(this.getClass().getName() + ".export(): \n" + e.getMessage());
        }
        finally {
            if (pw != null) {
                pw.flush();
                pw.close();
            }
            try {jschUtil.close();} catch (Exception e) {}
        }
    }

    private String buildOrderLine(GloviaOrder orderBn) throws Exception {
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        StringBuffer out = new StringBuffer("");

        out.append(orderBn.getWarehouse_ccn());
        out.append("|DIFY|");

        String orderNo = orderBn.getOrderNo() == null ? "" : String.format("%-10s", orderBn.getOrderNo()).replace(' ', '0');
        int lineLen = 20 - orderNo.length() ; // 20 is max Glovia can take.
        String orderLine = String.format("%0" + lineLen + "d", orderBn.getOrderLine());
        out.append(orderBn.getQty().toString()).append("|");
        out.append(orderBn.getItem()).append("|");
        out.append(orderBn.getStartDate() != null ? df.format(orderBn.getStartDate()) : "").append("|");
        out.append(orderNo + orderLine).append("|");
        out.append(orderBn.getProductType()).append("|");

        return out.toString();
    }

    private String buildOrderLineForCancel(GloviaOrder orderBn) throws Exception {
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        StringBuffer out = new StringBuffer("");

        out.append(orderBn.getWarehouse_ccn());
        out.append("|DIFY|");
        out.append(orderBn.getQty().toString()).append("|");
        out.append(orderBn.getItem()).append("|");
        out.append(orderBn.getStartDate() != null ? df.format(orderBn.getStartDate()) : "").append("|");
        out.append(orderBn.getPrimaryCell()).append("|");
        out.append(orderBn.getProductType()).append("|");

        return out.toString();
    }

    private List<String>getOrderList(List<OrderStatus> shipList) {
        List<String>orders = new ArrayList<String>();
        Iterator<OrderStatus>iter = shipList.iterator();
        while(iter.hasNext()) {
            orders.add(((OrderStatus)iter.next()).getOrderId());
        }
        return orders;
    }

    private List<String>getOrderListFromGlovia(List<GloviaOrder> shipList) {
        List<String>orders = new ArrayList<String>();
        Iterator<GloviaOrder>iter = shipList.iterator();
        while(iter.hasNext()) {
            orders.add(((GloviaOrder)iter.next()).getOrderNo());
        }
        return orders;
    }

    public void exportCancel(Date receiptDt, Database db, boolean dosend) throws Exception {
        JschUtil jschUtil = null;
        PrintWriter pw = null;
        try {
            SimpleDateFormat df = new SimpleDateFormat("'ca_'MMddyyHHmm'dify.seq'");
            String ftpHost      = DIFYProperty.GLOVIA_FTP_HOST;
            int    ftpPort      = new Integer(DIFYProperty.GLOVIA_FTP_PORT);
            String ftpUser      = DIFYProperty.GLOVIA_FTP_USER;
            String ftpPwd       = DIFYProperty.GLOVIA_FTP_PWD;
            String ftpTgtDir    = DIFYProperty.GLOVIA_FTP_CAN_TGT_DIR;
            String outputDir    = DIFYProperty.GLOVIA_OUT_DIR;
            String archiveDir   = DIFYProperty.GLOVIA_ARCHIVE_DIR;
            String fileName     = df.format(receiptDt);
            String filepath     = outputDir + File.separator + fileName;


            final List<GloviaOrder> itemList = GloviaOrder.Companion.selectCan(db);
            logger.info("{} Cancelled WePrint/OA order Total count: " + itemList.size() + ", " + fileName, new Date());

            if(itemList.size() == 0){
                return;
            }

            final List<String> excList = GloviaOrder.Companion.selectExclusionList(db);

            try {
                pw = new PrintWriter(new BufferedWriter(new FileWriter(filepath)));
                pw.println("CCN|Work Center|Quantity|SKU|Order Date|Order Number & Order Line|Product Type|");
            }
            catch (Exception e) {
                logger.error("Exception in process() ", e);
                throw e;
            }

            int total = 0;
            Iterator<GloviaOrder> iter = itemList.iterator();
            while (iter.hasNext()) {
                GloviaOrder orderBn = (GloviaOrder)iter.next();

                if (excList.stream().filter(p -> p.equals(orderBn.getItem())).collect(Collectors.toList()).size() == 0) {
                    pw.println(buildOrderLineForCancel(orderBn));
                    total++;
                }

            }

            pw.println("EOF|" + total + "|");
            pw.flush();
            pw.close();
            pw = null;

            // send file to Glovia
            try {
                if (dosend) {
                    jschUtil = new JschUtil();
                    jschUtil.open(ftpHost, ftpUser, ftpPwd, ftpPort);
                    jschUtil.sendFiles(outputDir, ftpTgtDir, archiveDir);
                    jschUtil.close();

                    logger.info("{} Cancelled order file has been sent to Glovia. Total count:" + String.valueOf(total) + ", " + fileName, new Date());
                    SalesFlatOrderForExport.Companion.updateMasterFlag(db, new HashMap<Integer, List<String>>(){{put(11, getOrderListFromGlovia(itemList));}});
                }

            }
            catch (Exception e) {
                logger.error("Exception in process() ", e);
                throw e;
            }

        }
        catch (Exception e) {
            throw new Exception(this.getClass().getName() + ".export(): \n" + e.getMessage());
        }
        finally {
            if (pw != null) {
                pw.flush();
                pw.close();
            }
            try {jschUtil.close();} catch (Exception e) {}
        }
    }

}
