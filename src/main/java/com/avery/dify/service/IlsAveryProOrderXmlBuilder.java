package com.avery.dify.service;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.avery.dify.export.ILSOrderSpecialCharge;
import com.github.davidmoten.rx.jdbc.Database;
import export.SalesFlatShippingGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.avery.dify.xmlmodel.PurchaseOrders;
import com.avery.dify.xmlmodel.PurchaseOrders.PO;
import com.avery.dify.xmlmodel.PurchaseOrders.PO.Detail;
import com.avery.dify.xmlmodel.PurchaseOrders.PO.Header;
import com.avery.dify.xmlmodel.PurchaseOrders.PO.ShippingAddress;
import com.avery.dify.xmlmodel.PurchaseOrders.PO.SpecialCharge;
import com.avery.dify.export.ILSOrder;
import com.avery.dify.export.ILSOrderDetail;

/**
 * This class is used to build the ILS Digital River Retail order XML.
 *
 * @author htran
 *
 */
public class IlsAveryProOrderXmlBuilder {

    private static Logger logger = LoggerFactory.getLogger(IlsRetailOrderXmlBuilder.class);

    private static final String US_COMPANY = "190";
    private static String CUST_ACCT_NO = "333333";
    private static String TRADE = "AVYPRO";
    private static String ABS = "AVERYPRO";
    private static String ED = "ED";
    private static String BILLING = "AP";

    private static String  ASB_WEB = "ASB WEB";
    private static String  EA = "CA";
    private static String  TJ = "T4";
    private static String  ORDER_TYPE = "";
    private static String TRANSACTION_TYPE = "";


    public PurchaseOrders buildProOrder(ILSOrder order) {
        if (order == null) {
            return null;
        }

        //setConstants();
        ORDER_TYPE = BILLING;
        TRANSACTION_TYPE = "funds";

        if(order.isTerm()){
            CUST_ACCT_NO = order.getCustomerAccountNumber();
            ASB_WEB = order.getCustomerAccountNumber();
        } else {
            CUST_ACCT_NO = "333333";
            ASB_WEB = "ASB WEB";
        }



        int lineCount = 1;
        PurchaseOrders ilsPo = new PurchaseOrders();
        List<PurchaseOrders.PO> poList = ilsPo.getPO();
        PurchaseOrders.PO po = new PurchaseOrders.PO();

        PurchaseOrders.PO.Header header = fillHeader(order);
        po.setHeader(header);

        for (ILSOrderDetail item : order.getOrderDetails()) {

            PurchaseOrders.PO.Detail detail = fillDetail(order, item, lineCount);
            if (detail != null) {
                po.getDetail().add(detail);
                lineCount++;
            }

        }

        if (header == null || po.getDetail() == null
                || po.getDetail().size() == 0) {
            return null;
        }

        header.setNumOfLinesOnThePO(String.valueOf(po.getDetail().size()));
        PurchaseOrders.PO.ShippingAddress shipAdrr = fillShipAddress(order);
        po.setShippingAddress(shipAdrr);
        po.getSpecialCharge().addAll(fillSpecialCharges(order));
        poList.add(po);

        return ilsPo;
    }

    /**
     * Fills the header part of the xml
     */
    protected PurchaseOrders.PO.Header fillHeader(ILSOrder order) {
        PurchaseOrders.PO.Header header = new PurchaseOrders.PO.Header();
        header.setOrderType(TRANSACTION_TYPE);
        header.setCarrierName(order.getCarrierName());
        header.setCarrierCode(order.getCarrierCode());
        header.setCompanyCode(order.getCompanyCode());
        header.setCustomerPONum(order.getCustomerPONumber());
        header.setASBOrderNum(order.getCustomerPONumber());
        header.setCustomerAccountNum(CUST_ACCT_NO);
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(order.getDateOrderReceived());

        XMLGregorianCalendar xmlCal = null;
        try {
            xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            header.setDateOrderReceived(xmlCal);
        } catch (DatatypeConfigurationException e) {
            logger.error("Error in fillHeader() order number {} ", order.getCustomerPONumber(), e);
        }

        header.setTradingPartnerID(TRADE);
        header.setASBPartnerID(ABS);
        header.setOrderSourceCode(ED);
        header.setPODate(xmlCal);
        header.setPOOrderType(ORDER_TYPE);
        header.setStoreNum(ASB_WEB);

        return header;

    }

    /**
     * Fills out the detail sections of the ILS order, only with Retails items.
     *
     */
    private PurchaseOrders.PO.Detail fillDetail(ILSOrder salesOrder, ILSOrderDetail orderItem, int lineCount) {

        PurchaseOrders.PO.Detail detail = new PurchaseOrders.PO.Detail();

        detail.setCustomerPONumber(salesOrder.getCustomerPONumber());
        detail.setPOLineNum(String.valueOf(lineCount));
        detail.setItemSequence(String.valueOf(lineCount));

        detail.setCustItemNumber(orderItem.getCustomerItemNum());
        detail.setTradingPartnerID(TRADE);
        detail.setCompanyCode(salesOrder.getCompanyCode());
        detail.setCustomerAccountNum(CUST_ACCT_NO);
        detail.setItemDescription1(orderItem.getItemDescription());
        detail.setOriginalItemNumber(orderItem.getOriginalItemNum());
        //detail.setArtworkNumber(DIFY);
        detail.setPOQty(String.valueOf(orderItem.getPoQuantity()));
        detail.setPOUOM(EA);
        detail.setPOUnitPrice(String.valueOf(orderItem.getPoUnitPrice()));
        detail.setTaxChargeAmount(String.valueOf(orderItem.getItemTaxAmount()));
        detail.setShippingWarehouse(TJ);

        return detail;
    }


    /**
     * Fills out the special charges section.
     */
    protected List<PurchaseOrders.PO.SpecialCharge> fillSpecialCharges(ILSOrder salesOrder) {

        List<PurchaseOrders.PO.SpecialCharge> specialCharges = new ArrayList<PurchaseOrders.PO.SpecialCharge>();

        for (ILSOrderSpecialCharge item : salesOrder.getSpecialCharges()) {

            if (item.getSpecialChargeAmount().compareTo(BigDecimal.ZERO) != 0) {
                specialCharges.add(
                        fillSpecialCharge(
                                item.getSpecialChargeCode(),
                                item.getSpecialChargeDescription(),
                                item.getCustomerPONumber(),
                                item.getSpecialChargeAmount()
                        )
                );
            }

        }

        return specialCharges;
    }

    /**
     * Fills out one special charge section.
     *
     * @param specialChargeCode
     * @param specialChargeDescription
     * @param orderNum
     * @param chargeAmount
     * @return
     */
    protected PurchaseOrders.PO.SpecialCharge fillSpecialCharge(String specialChargeCode,
                                                                String specialChargeDescription, String orderNum,
                                                                BigDecimal chargeAmount) {

        chargeAmount = chargeAmount.setScale(2, RoundingMode.HALF_UP);

        PurchaseOrders.PO.SpecialCharge charge = new PurchaseOrders.PO.SpecialCharge();
        charge.setCustomerPONumber(orderNum);
        charge.setTradingPartnerID(TRADE);
        charge.setCompanyCode(US_COMPANY);
        charge.setCustomerAccountNum(CUST_ACCT_NO);
        charge.setSpecialChargeCode(specialChargeCode);
        charge.setSpecialChargeDesc(specialChargeDescription);
        charge.setSpecialChargeAmount(String.valueOf(chargeAmount));

        return charge;
    }

    /**
     * Fills out the shipping address.
     *
     * @param order
     * @return
     */
    protected PurchaseOrders.PO.ShippingAddress fillShipAddress(ILSOrder order) {
        PurchaseOrders.PO.ShippingAddress shipAddr = new PurchaseOrders.PO.ShippingAddress();
        shipAddr.setCustomerPONumber(order.getCustomerPONumber());
        shipAddr.setTradingPartnerID(TRADE);
        shipAddr.setCompanyCode(US_COMPANY);
        shipAddr.setCustomerAccountNum(CUST_ACCT_NO);

        shipAddr.setName(order.getCustomerName());
        shipAddr.setAddressLine1(order.getCustomerAddress1());
        shipAddr.setAddressLine2(order.getCustomerAddress2());
        shipAddr.setAddressLine3(order.getCustomerAddress3());

        shipAddr.setCity(order.getCustomerCity());
        shipAddr.setState(order.getCustomerState());
        shipAddr.setZip(order.getCustomerZip());
        shipAddr.setCountry(order.getCustomerCountry());

        return shipAddr;
    }

}
