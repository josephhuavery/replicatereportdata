package com.avery.dify.service;


import com.avery.dify.export.ILSOrder;
import com.avery.dify.export.ILSOrderDetail;
import com.avery.dify.export.ILSOrderSpecialCharge;
import com.avery.dify.xmlmodel.PurchaseOrders;
import com.avery.dify.xmlmodel.PurchaseOrders.PO;
import com.avery.dify.xmlmodel.PurchaseOrders.PO.Detail;
import com.avery.dify.xmlmodel.PurchaseOrders.PO.Header;
import com.avery.dify.xmlmodel.PurchaseOrders.PO.ShippingAddress;
import com.avery.dify.xmlmodel.PurchaseOrders.PO.SpecialCharge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * This class is used to build the ILS Digital River Retail credit memo XML.
 *
 * @author kkim
 *
 */
public class IlsCMRetailOrderXmlBuilder {

    private static Logger logger = LoggerFactory.getLogger(IlsCMRetailOrderXmlBuilder.class);

    private static String ORDER_TYPE = "C";
    private static String  UPSTracking = "CREDIT";
    BigDecimal SP_DIFY_FREIGHT = new BigDecimal(0);
    BigDecimal SP_FREIGHT_DISCOUNT = new BigDecimal(0);
    BigDecimal SP_PROMOTION = new BigDecimal(0);
    BigDecimal SP_TAX = new BigDecimal(0);
    BigDecimal SP_STORECR = BigDecimal.ZERO;
    boolean isMixed = false;

    public PurchaseOrders buildCMRetailOrder(ILSOrder order, List<ILSOrderDetail> orderDetail, boolean is_Mixed) {
        if (order == null) {
            return null;
        }

        this.isMixed = is_Mixed;

        int lineCount = 1;
        PurchaseOrders ilsPo = new PurchaseOrders();
        List<PO> poList = ilsPo.getPO();
        PO po = new PO();

        Header header = fillHeader(order);
        po.setHeader(header);

        for (ILSOrderDetail item : orderDetail) {

            Detail detail = fillDetail(order, item, lineCount);
            if (detail != null) {
                po.getDetail().add(detail);

                SP_DIFY_FREIGHT = SP_DIFY_FREIGHT.add(item.getLineShippingCharge());
                SP_FREIGHT_DISCOUNT = SP_FREIGHT_DISCOUNT.add(item.getLineShippingDiscount());
                SP_PROMOTION = SP_PROMOTION.add(item.getDiscountRefunded());
                SP_TAX = SP_TAX.add(item.getTaxRefunded());
                if(lineCount == 1) {
                    SP_TAX = SP_TAX.add(item.getCalFirstLineTax());
                }
                SP_STORECR = order.getStoreCreditAmount();

                lineCount++;
            }

        }

        if (order.getAdjustment().equals(BigDecimal.ZERO) && order.getNumofLinesonPO().equals(BigInteger.ZERO)) {

            Detail dummydetail = new Detail();

            dummydetail.setCustomerPONumber(order.getCustomerPONumber() + "A" + order.getCreditMemoNumber());
            dummydetail.setCustItemNumber("999008");
            dummydetail.setTradingPartnerID("DIFY");
            dummydetail.setCompanyCode(order.getCompanyCode());
            dummydetail.setCustomerAccountNum(order.getCustomerAccountNumber());
            dummydetail.setItemDescription1("SPECIAL CHARGE MOD");
            dummydetail.setOriginalItemNumber("999008");
            dummydetail.setPOLineNum("1");
            dummydetail.setItemSequence("1");
            dummydetail.setPOQty("1");
            dummydetail.setPOUOM("EA");
            dummydetail.setPOUnitPrice("0");
            dummydetail.setShippingWarehouse("TJ");
            dummydetail.setTaxChargeAmount("0");
            dummydetail.setFreightChargeAmount("0");
            dummydetail.setFreightDiscountAmount("0");

            po.getDetail().add(dummydetail);

        }

        header.setNumOfLinesOnThePO(String.valueOf(po.getDetail().size()));
        ShippingAddress shipAdrr = fillShipAddress(order);
        po.setShippingAddress(shipAdrr);
        po.getSpecialCharge().addAll(fillSpecialCharges(order));
        poList.add(po);

        return ilsPo;
    }

    /**
     * Fills the header part of the xml
     */
    protected Header fillHeader(ILSOrder order) {
        Header header = new Header();
        header.setOrderType("funds");
        header.setCarrierName(order.getCarrierName());
        header.setCarrierCode(order.getCarrierCode());
        header.setCompanyCode(order.getCompanyCode());
        header.setCustomerPONum(order.getCustomerPONumber() + "A" + order.getCreditMemoNumber());
        header.setASBOrderNum(order.getCustomerPONumber() + "A" + order.getCreditMemoNumber());
        header.setCustomerAccountNum(order.isTerm() ? order.getCustomerAccountNumber() : "730000");
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(order.getDateOrderReceived());

        XMLGregorianCalendar xmlCal = null;
        try {
            xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            header.setDateOrderReceived(xmlCal);
        } catch (DatatypeConfigurationException e) {
            logger.error("Error in fillHeader() setDateOrderReceived {} ", order.getCustomerPONumber(), e);
        }

        header.setTradingPartnerID("DIFY");
        header.setASBPartnerID(order.getAsbPartnerID());
        header.setOrderSourceCode(order.getOrderSourceCode());
        header.setPODate(xmlCal);
        header.setPOOrderType(ORDER_TYPE);
        header.setStoreNum(order.isTerm() ? order.getStoreNum() : "AVERY.COM");
        header.setUPSTracking(UPSTracking);

        GregorianCalendar cal2 = new GregorianCalendar();
        cal2.setTime(order.getShipDate());
        try {
            xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal2);
            header.setShippedDate(xmlCal);
        } catch (DatatypeConfigurationException e) {
            logger.error("Error in fillHeader() setShippedDate {} ", order.getCustomerPONumber(), e);
        }

        return header;

    }

    /**
     * Fills out the detail sections of the ILS order, only with Retails items.
     *
     */
    private Detail fillDetail(ILSOrder salesOrder, ILSOrderDetail orderItem, int lineCount) {

        Detail detail = new Detail();

        detail.setCustomerPONumber(salesOrder.getCustomerPONumber() + "A" + salesOrder.getCreditMemoNumber());
        detail.setCustItemNumber(orderItem.getCustomerItemNum());
        detail.setTradingPartnerID("DIFY");
        detail.setCompanyCode(salesOrder.getCompanyCode());
        detail.setCustomerAccountNum(salesOrder.isTerm() ? salesOrder.getCustomerAccountNumber() : "730000");
        detail.setItemDescription1(orderItem.getItemDescription());
        detail.setOriginalItemNumber(orderItem.getOriginalItemNum());
        detail.setPOLineNum(String.valueOf(orderItem.getPoLineNumber()));
        detail.setItemSequence(String.valueOf(orderItem.getItemSequence()));
        detail.setPOQty(String.valueOf(orderItem.getPoQuantity()));
        detail.setPOUOM(orderItem.getPoUnitofMeasure());
        detail.setPOUnitPrice(String.valueOf(orderItem.getPoUnitPrice()));
        detail.setShippingWarehouse(orderItem.getShippingWarehouse());

        if(orderItem.getItemTaxAmount() != null) {
            detail.setTaxChargeAmount(String.valueOf(orderItem.getItemTaxAmount()));
        }

        return detail;
    }


    /**
     * Fills out the special charges section.
     */
    protected List<SpecialCharge> fillSpecialCharges(ILSOrder order) {

        List<SpecialCharge> specialCharges = new ArrayList<SpecialCharge>();

        for (ILSOrderSpecialCharge item : order.getSpecialCharges()) {

            specialCharges.add(
                    fillSpecialCharge(
                            item.getSpecialChargeCode(),
                            item.getSpecialChargeDescription(),
                            order.getCustomerPONumber() + "A" + order.getCreditMemoNumber(),
                            item.getSpecialChargeAmount(),
                            "DIFY", order.getCompanyCode(), order.isTerm() ? order.getCustomerAccountNumber() : "730000"
                    )
            );

        }

        return specialCharges;
    }

    /**
     * Fills out one special charge section.
     *
     * @param specialChargeCode
     * @param specialChargeDescription
     * @param orderNum
     * @param chargeAmount
     * @return
     */
    protected SpecialCharge fillSpecialCharge(String specialChargeCode,
                                              String specialChargeDescription, String orderNum,
                                              BigDecimal chargeAmount,
                                              String TRADE, String US_COMPANY, String CUST_ACCT_NO) {

        chargeAmount = chargeAmount.setScale(2, RoundingMode.HALF_UP);

        SpecialCharge charge = new SpecialCharge();
        charge.setCustomerPONumber(orderNum);
        charge.setTradingPartnerID(TRADE);
        charge.setCompanyCode(US_COMPANY);
        charge.setCustomerAccountNum(CUST_ACCT_NO);
        charge.setSpecialChargeCode(specialChargeCode);
        charge.setSpecialChargeDesc(specialChargeDescription);

        if(isMixed) {
            if(specialChargeCode.equals("7 "))  charge.setSpecialChargeAmount(String.valueOf(SP_FREIGHT_DISCOUNT));
            else if(specialChargeCode.equals("8 "))  charge.setSpecialChargeAmount(String.valueOf(SP_PROMOTION));
            else if(specialChargeCode.equals("9 "))  charge.setSpecialChargeAmount(String.valueOf(SP_TAX));
            else if(specialChargeCode.equals("0 "))  charge.setSpecialChargeAmount(String.valueOf(SP_DIFY_FREIGHT));
            else if(specialChargeCode.equals("G "))  charge.setSpecialChargeAmount(String.valueOf(SP_STORECR));
        } else {
            charge.setSpecialChargeAmount(String.valueOf(chargeAmount));
        }

        return charge;
    }

    /**
     * Fills out the shipping address.
     *
     * @param order
     * @return
     */
    protected ShippingAddress fillShipAddress(ILSOrder order) {
        ShippingAddress shipAddr = new ShippingAddress();
        shipAddr.setCustomerPONumber(order.getCustomerPONumber() + "A" + order.getCreditMemoNumber());
        shipAddr.setTradingPartnerID("DIFY");
        shipAddr.setCompanyCode(order.getCompanyCode());
        shipAddr.setCustomerAccountNum(order.isTerm() ? order.getCustomerAccountNumber() : "730000");

        shipAddr.setName(order.getCustomerName());
        shipAddr.setAddressLine1(order.getCustomerAddress1());
        shipAddr.setAddressLine2(order.getCustomerAddress2());
        shipAddr.setAddressLine3(order.getCustomerAddress3());

        shipAddr.setCity(order.getCustomerCity());
        shipAddr.setState(order.getCustomerState());
        shipAddr.setZip(order.getCustomerZip());
        shipAddr.setCountry(order.getCustomerCountry());

        return shipAddr;
    }

}
