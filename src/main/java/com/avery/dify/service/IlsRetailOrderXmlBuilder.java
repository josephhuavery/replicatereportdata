package com.avery.dify.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.avery.dify.export.ILSOrderSpecialCharge;
import com.github.davidmoten.rx.jdbc.Database;
import export.SalesFlatShippingGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.avery.dify.xmlmodel.PurchaseOrders;
import com.avery.dify.xmlmodel.PurchaseOrders.PO;
import com.avery.dify.xmlmodel.PurchaseOrders.PO.Detail;
import com.avery.dify.xmlmodel.PurchaseOrders.PO.Header;
import com.avery.dify.xmlmodel.PurchaseOrders.PO.ShippingAddress;
import com.avery.dify.xmlmodel.PurchaseOrders.PO.SpecialCharge;
import com.avery.dify.export.ILSOrder;
import com.avery.dify.export.ILSOrderDetail;

public class IlsRetailOrderXmlBuilder {

    private static Logger logger = LoggerFactory.getLogger(IlsRetailOrderXmlBuilder.class);

    private static final String US_COMPANY = "190";
    private static String CUST_ACCT_NO = "730000";
    private static String DIFY = "DIFY";
    private static String ED = "ED";
    private static String BILLING = "AP";
    private static String CREDIT_MEMO = "C";
    private static String  ASB_WEB = "AVERY.COM";
    private static String  EA = "EA";
    private static String  ORDER_TYPE = "";
    private static String  FREIGHT_DISCOUNT_CODE = "7 ";
    private static String  FREIGHT_DISCOUNT_DESC = "FREIGHT DISCOUNT";
    private static String  PROMOTION_CODE = "8 ";
    private static String  PROMOTION_DESC = "PROMOTION";
    private static String TAX_CODE = "9 ";
    private static String  TAX_DESC = "TAX";
    private static String  FREIGHT_CODE = "0 ";
    private static String  FREIGHT_DESC = "DIFY FREIGHT";
    private static String  STORECR_CODE = "G ";
    private static String  STORECR_DESC = "STORE CREDIT";
    private static String  PRODUCT_GROUP = "A"; // A : Retail
    private static String TRANSACTION_TYPE = "";


    public PurchaseOrders buildRetailOrder(ILSOrder order) {
        if (order == null) {
            return null;
        }

        //setConstants();
        ORDER_TYPE = BILLING;
        TRANSACTION_TYPE = "funds";

        if(order.isTerm()){
            CUST_ACCT_NO = order.getCustomerAccountNumber();
            ASB_WEB = order.getCustomerAccountNumber();
        } else {
            CUST_ACCT_NO = "730000";
            ASB_WEB = "AVERY.COM";
        }



        int lineCount = 1;
        PurchaseOrders ilsPo = new PurchaseOrders();
        List<PO> poList = ilsPo.getPO();
        PO po = new PO();

        Header header = fillHeader(order);
        po.setHeader(header);

        for (ILSOrderDetail item : order.getOrderDetails()) {

            Detail detail = fillDetail(order, item, lineCount);
            if (detail != null) {
                po.getDetail().add(detail);
                lineCount++;
            }

        }

        if (header == null || po.getDetail() == null
                || po.getDetail().size() == 0) {
            return null;
        }

        header.setNumOfLinesOnThePO(String.valueOf(po.getDetail().size()));
        ShippingAddress shipAdrr = fillShipAddress(order);
        po.setShippingAddress(shipAdrr);
        po.getSpecialCharge().addAll(fillSpecialCharges(order));
        poList.add(po);

        return ilsPo;
    }

    /**
     * Fills the header part of the xml
     */
    protected Header fillHeader(ILSOrder order) {
        Header header = new Header();
        header.setOrderType(TRANSACTION_TYPE);
        header.setCarrierName(order.getCarrierName());
        header.setCarrierCode(order.getCarrierCode());
        header.setCompanyCode(order.getCompanyCode());
        header.setCustomerPONum(order.getCustomerPONumber() + PRODUCT_GROUP); // append "A" for
        header.setASBOrderNum(order.getCustomerPONumber() + PRODUCT_GROUP);
        header.setCustomerAccountNum(CUST_ACCT_NO);
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(order.getDateOrderReceived());

        XMLGregorianCalendar xmlCal = null;
        try {
            xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            header.setDateOrderReceived(xmlCal);
        } catch (DatatypeConfigurationException e) {
            logger.error("Error in fillHeader() order number {} ", order.getCustomerPONumber(), e);
        }

        header.setTradingPartnerID(DIFY);
        header.setASBPartnerID(DIFY);
        header.setOrderSourceCode(ED);
        header.setPODate(xmlCal);
        header.setPOOrderType(ORDER_TYPE);
        header.setStoreNum(ASB_WEB);

        if (order.getShipDate() != null) {
            GregorianCalendar calsd = new GregorianCalendar();
            calsd.setTime(order.getShipDate());
            try {
                XMLGregorianCalendar xmlCalsd = DatatypeFactory.newInstance().newXMLGregorianCalendar(calsd);
                header.setShippedDate(xmlCalsd);
            } catch (Exception e) {
                logger.error("Error in fillHeader() setting ship date {} ", order.getOrderId(), e);
            }
        } else {
            header.setShippedDate(null);
        }

        return header;

    }

    /**
     * Fills out the detail sections of the ILS order, only with Retails items.
     *
     */
    private Detail fillDetail(ILSOrder salesOrder, ILSOrderDetail orderItem, int lineCount) {

        Detail detail = new Detail();

        detail.setCustomerPONumber(salesOrder.getCustomerPONumber() + PRODUCT_GROUP);
        detail.setPOLineNum(String.valueOf(lineCount));
        detail.setItemSequence(String.valueOf(lineCount));

        detail.setCustItemNumber(orderItem.getCustomerItemNum());
        detail.setTradingPartnerID(DIFY);
        detail.setCompanyCode(salesOrder.getCompanyCode());
        detail.setCustomerAccountNum(CUST_ACCT_NO);
        detail.setItemDescription1(orderItem.getItemDescription());
        detail.setOriginalItemNumber(orderItem.getOriginalItemNum());
        detail.setArtworkNumber(DIFY);
        detail.setPOQty(String.valueOf(orderItem.getPoQuantity()));
        detail.setPOUOM(EA);
        detail.setPOUnitPrice(String.valueOf(orderItem.getPoUnitPrice()));
        detail.setTaxChargeAmount(String.valueOf(orderItem.getItemTaxAmount()));
        detail.setShippingWarehouse(orderItem.getShippingWarehouse());

        return detail;
    }


    /**
     * Fills out the special charges section.
     */
    protected List<SpecialCharge> fillSpecialCharges(ILSOrder salesOrder) {

        List<SpecialCharge> specialCharges = new ArrayList<SpecialCharge>();

        for (ILSOrderSpecialCharge item : salesOrder.getSpecialCharges()) {

            if (item.getSpecialChargeCode().equals(FREIGHT_DISCOUNT_CODE)) {

                specialCharges.add(
                            fillSpecialCharge(
                                    item.getSpecialChargeCode(),
                                    item.getSpecialChargeDescription(),
                                    item.getCustomerPONumber() + PRODUCT_GROUP,
                                    item.getSpecialChargeAmount()
                            ));

            } else if (item.getSpecialChargeCode().equals(TAX_CODE)) {

                BigDecimal totalTax = totalTaxItems(salesOrder.getOrderDetails());
                if (item.getSpecialChargeAmount().compareTo(BigDecimal.ZERO) == 1){
                    totalTax = totalTax.add(item.getSpecialChargeAmount());
                }

                specialCharges.add(
                            fillSpecialCharge(
                                    item.getSpecialChargeCode(),
                                    item.getSpecialChargeDescription(),
                                    item.getCustomerPONumber() + PRODUCT_GROUP,
                                    totalTax));

            } else if (item.getSpecialChargeCode().equals(PROMOTION_CODE)) {

                BigDecimal baseDiscount = calcCategoryDiscountAmt(salesOrder.getOrderDetails());
                BigDecimal promoDiscount = baseDiscount.subtract(item.getSpecialChargeAmount().negate());

                promoDiscount = baseDiscount;
                if (promoDiscount.compareTo(BigDecimal.ZERO) != 0) {
                    specialCharges.add(
                            fillSpecialCharge(
                                    item.getSpecialChargeCode(),
                                    item.getSpecialChargeDescription(),
                                    item.getCustomerPONumber() + PRODUCT_GROUP,
                                    promoDiscount
                            )
                    );
                }

            } else if (item.getSpecialChargeCode().equals(FREIGHT_CODE)) {

                if (item.getSpecialChargeAmount().compareTo(BigDecimal.ZERO) != 0) {
                    specialCharges.add(
                            fillSpecialCharge(
                                    item.getSpecialChargeCode(),
                                    item.getSpecialChargeDescription(),
                                    item.getCustomerPONumber() + PRODUCT_GROUP,
                                    item.getSpecialChargeAmount()
                            )
                    );

                }
            } else if (item.getSpecialChargeCode().equals(STORECR_CODE)) {

                if (item.getSpecialChargeAmount().compareTo(BigDecimal.ZERO) != 0) {
                    specialCharges.add(
                            fillSpecialCharge(
                                    item.getSpecialChargeCode(),
                                    item.getSpecialChargeDescription(),
                                    item.getCustomerPONumber() + PRODUCT_GROUP,
                                    item.getSpecialChargeAmount()
                            )
                    );

                }
            }
        }


        return specialCharges;
    }

    /**
     * Fills out one special charge section.
     *
     * @param specialChargeCode
     * @param specialChargeDescription
     * @param orderNum
     * @param chargeAmount
     * @return
     */
    protected SpecialCharge fillSpecialCharge(String specialChargeCode,
                                              String specialChargeDescription, String orderNum,
                                              BigDecimal chargeAmount) {

        chargeAmount = chargeAmount.setScale(2, RoundingMode.HALF_UP);

        SpecialCharge charge = new SpecialCharge();
        charge.setCustomerPONumber(orderNum);
        charge.setTradingPartnerID(DIFY);
        charge.setCompanyCode(US_COMPANY);
        charge.setCustomerAccountNum(CUST_ACCT_NO);
        charge.setSpecialChargeCode(specialChargeCode);
        charge.setSpecialChargeDesc(specialChargeDescription);
        charge.setSpecialChargeAmount(String.valueOf(chargeAmount));

        if (specialChargeCode == FREIGHT_DISCOUNT_CODE && chargeAmount.doubleValue() > 0) {
            charge.setSpecialChargeAmount(String.valueOf(chargeAmount.multiply(new BigDecimal(-1))));
        } else if (specialChargeCode == PROMOTION_CODE && chargeAmount.doubleValue() > 0) {
            charge.setSpecialChargeAmount(String.valueOf(chargeAmount.multiply(new BigDecimal(-1))));
        } else if (specialChargeCode == STORECR_CODE && chargeAmount.doubleValue() > 0) {
            charge.setSpecialChargeAmount(String.valueOf(chargeAmount.multiply(new BigDecimal(-1))));
        }

        return charge;
    }

    /**
     * Fills out the shipping address.
     *
     * @param order
     * @return
     */
    protected ShippingAddress fillShipAddress(ILSOrder order) {
        ShippingAddress shipAddr = new ShippingAddress();
        shipAddr.setCustomerPONumber(order.getCustomerPONumber() + PRODUCT_GROUP);
        shipAddr.setTradingPartnerID(DIFY);
        shipAddr.setCompanyCode(US_COMPANY);
        shipAddr.setCustomerAccountNum(CUST_ACCT_NO);

        shipAddr.setName(order.getCustomerName());
        shipAddr.setAddressLine1(order.getCustomerAddress1());
        shipAddr.setAddressLine2(order.getCustomerAddress2());
        shipAddr.setAddressLine3(order.getCustomerAddress3());

        shipAddr.setCity(order.getCustomerCity());
        shipAddr.setState(order.getCustomerState());
        shipAddr.setZip(order.getCustomerZip());
        shipAddr.setCountry(order.getCustomerCountry());

        return shipAddr;
    }


    /**
     * Calculates the tax at the category level for retail items
     */
    protected BigDecimal calcCategoryDiscountAmt(List<ILSOrderDetail> orderItems) {

        BigDecimal totalCategoryDiscount = new BigDecimal(0.00);

        for (ILSOrderDetail item : orderItems) {
            if (item != null) {
                totalCategoryDiscount = totalCategoryDiscount.add(item.getDiscountRefunded());
            }
        }

        return totalCategoryDiscount;
    }

    /**
     * Calculates the tax at the category level for retail items
     */
    protected BigDecimal totalTaxItems(List<ILSOrderDetail> orderItems) {

        BigDecimal totalTax = new BigDecimal(0.00);

        for (ILSOrderDetail item : orderItems) {
            if (item != null) {
                totalTax = totalTax.add(item.getTaxRefunded());
            }
        }

        return totalTax;
    }


}
