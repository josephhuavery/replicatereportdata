package com.avery.dify;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

import com.avery.Config;
import com.avery.dify.export.*;
import com.avery.dify.util.MailBody;
import com.avery.dify.util.SendEmail;
import com.github.davidmoten.rx.jdbc.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.avery.dify.util.DIFYProperty;

public class ProcessFiles {
    private static Logger logger = LoggerFactory.getLogger(ProcessFiles.class);

    public void process(Database destDatabase, String orderNoList, Config config) throws Exception {

        try {
            Date receiptDate = new Date();

            ExportToGlovia expGlovia = new ExportToGlovia();
            ExportToILS expILS = new ExportToILS();

            String additionalWhere =
                    orderNoList.equals("") ?
                            " where (ils_retail_sent is null and PRODUCT_GROUP LIKE '%retail%' AND state NOT IN ('canceled' , 'closed' , 'complete' , 'holded' , 'payment_review'))  or (ils_estore_sent is null and PRODUCT_GROUP LIKE '%estore%' AND state NOT IN ('canceled' , 'closed' , 'complete' , 'holded' , 'payment_review')) OR (ILS_SERVICE_SENT is null and PRODUCT_GROUP LIKE '%service%' AND state NOT IN ('canceled' , 'closed' , 'complete' , 'holded' , 'payment_review')) OR (ILS_AVERYPRO_SENT is null and PRODUCT_GROUP LIKE '%pro%' AND state NOT IN ('canceled' , 'closed' , 'complete' , 'holded' , 'payment_review')) " :
                            orderNoList;

            List<SalesFlatOrderForExport> sfoList = SalesFlatOrderForExport.Companion.selectOracleforExport(destDatabase,additionalWhere);
            List<SalesFlatCreditMemoForExport> cmList = SalesFlatCreditMemoForExport.Companion.selectOracleforExport(destDatabase,
                    orderNoList.equals("") ? " where ils_weprint_sent is null and ils_retail_sent is null and ils_averypro_sent is null and ils_service_sent is null and ils_onlineaisle_sent is null and ils_estore_sent is null " : " main WHERE main.order_id IN (SELECT entity_id FROM stg_sales_flat_order " + orderNoList + " )");
            List<SalesFlatInvoiceForExport> invList = SalesFlatInvoiceForExport.Companion.selectOracleforExport(destDatabase, orderNoList);
            List<SalesFlatOrderPaymentForExport> pmtList = SalesFlatOrderPaymentForExport.Companion.selectOracleforExport(destDatabase, orderNoList);

            List<SalesFlatInvoiceForExport> olaList = SalesFlatInvoiceForExport.Companion.selectOlaforExport(destDatabase, orderNoList);
            List<SalesFlatOrderPaymentForExport> olapmtList = SalesFlatOrderPaymentForExport.Companion.selectOlaforExport(destDatabase, orderNoList);

            List<OrderStatus> orderStatusList = new ArrayList<OrderStatus>();
            List<OrderStatus> cmOrderStatusList = new ArrayList<OrderStatus>();
            List<OrderStatus> invOrderStatusList = new ArrayList<OrderStatus>();
            List<OrderStatus> pmtOrderStatusList = new ArrayList<OrderStatus>();
            List<OrderStatus> olaOrderStatusList = new ArrayList<OrderStatus>();
            List<OrderStatus> olapmtOrderStatusList = new ArrayList<OrderStatus>();

            logger.info("{} Total export target order count. Order:" + String.valueOf(sfoList.size())
                    + " || CreditMemo:" + String.valueOf(cmList.size())
                    + " || FundCaptureForWePrint:" + String.valueOf(invList.size())
                    + " || FundCapturePurchaseForWePrint:" + String.valueOf(pmtList.size())
                    + " || FundCaptureOlaForWePrint:" + String.valueOf(olaList.size())
                    + " || FundCaptureOlaPurchaseForWePrint:" + String.valueOf(olapmtList.size()), new Date());

            String newFilePath = "ImportDirectly";

            List<SalesFlatOrder> orders = null;
            try {
                orderStatusList.addAll(createOrderStatus(sfoList, newFilePath, ""));
            } catch (Exception sfoEx) {
                orderStatusList.addAll(createOrderStatus(sfoList, newFilePath, sfoEx.getMessage()));
            }

            try {
                cmOrderStatusList.addAll(createCMOrderStatus(cmList, newFilePath, "", destDatabase));
            } catch (Exception sfoEx) {
                cmOrderStatusList.addAll(createCMOrderStatus(cmList, newFilePath, sfoEx.getMessage(), destDatabase));
            }

            try {
                invOrderStatusList.addAll(createINVOrderStatus(invList, newFilePath, ""));
            } catch (Exception sfoEx) {
                invOrderStatusList.addAll(createINVOrderStatus(invList, newFilePath, sfoEx.getMessage()));
            }

            try {
                pmtOrderStatusList.addAll(createPMTOrderStatus(pmtList, newFilePath, ""));
            } catch (Exception sfoEx) {
                pmtOrderStatusList.addAll(createPMTOrderStatus(pmtList, newFilePath, sfoEx.getMessage()));
            }

            try {
                olaOrderStatusList.addAll(createINVOrderStatus(olaList, newFilePath, ""));
            } catch (Exception sfoEx) {
                olaOrderStatusList.addAll(createINVOrderStatus(olaList, newFilePath, sfoEx.getMessage()));
            }

            try {
                olapmtOrderStatusList.addAll(createPMTOrderStatus(olapmtList, newFilePath, ""));
            } catch (Exception sfoEx) {
                olapmtOrderStatusList.addAll(createPMTOrderStatus(olapmtList, newFilePath, sfoEx.getMessage()));
            }

            // export fund captures to ILS - Invoiced Credit card order
            if (invList != null && invList.size() > 0) {

                if (invOrderStatusList.size() > 0) {
                    logger.info("{} Start export process for WePrint Fund Capture. Target Invoice total:" + String.valueOf(invOrderStatusList.size()), new Date());
                    expILS.export(invOrderStatusList, receiptDate, destDatabase, 1, config.sending == 1, false);
                }

                OrderStatus.Companion.logOrderStatus(destDatabase, invOrderStatusList);
            }
            // export fund captures to ILS - Term order
            if (pmtList != null && pmtList.size() > 0) {

                if (pmtOrderStatusList.size() > 0) {
                    logger.info("{} Start export process for WePrint Fund Capture. Target Terms orders total:" + String.valueOf(pmtOrderStatusList.size()), new Date());
                    expILS.export(pmtOrderStatusList, receiptDate, destDatabase, 1, config.sending == 1, true);
                }

                OrderStatus.Companion.logOrderStatus(destDatabase, pmtOrderStatusList);
            }

            // export online aisle fund captures to ILS - Invoiced Credit card order
            if (olaList != null && olaList.size() > 0) {

                if (olaOrderStatusList.size() > 0) {
                    logger.info("{} Start export process for Online Aisle Fund Capture. Target Invoice total:" + String.valueOf(olaOrderStatusList.size()), new Date());
                    expILS.export(olaOrderStatusList, receiptDate, destDatabase, 11, config.sending == 1, false);
                }

                OrderStatus.Companion.logOrderStatus(destDatabase, olaOrderStatusList);
            }
            // export online aisle fund captures to ILS - Term order
            if (olapmtList != null && olapmtList.size() > 0) {

                if (olapmtOrderStatusList.size() > 0) {
                    logger.info("{} Start export process for OnlineAisle Fund Capture. Target Terms orders total:" + String.valueOf(olapmtOrderStatusList.size()), new Date());
                    expILS.export(olapmtOrderStatusList, receiptDate, destDatabase, 11, config.sending == 1, true);
                }

                OrderStatus.Companion.logOrderStatus(destDatabase, olapmtOrderStatusList);
            }

            // export new orders to Glovia
            if (sfoList != null && sfoList.size() > 0) {

//                // "Glovia" WePrint/OA Only (store_id = 1)
//                List<OrderStatus> wpListGl = getWePrintOrdersForGlovia(orderStatusList);
//                if (wpListGl.size() > 0) {
//                    logger.info("{} Start Glovia file export. Target order total:" + String.valueOf(wpListGl.size()), new Date());
//                    expGlovia.export(wpListGl, receiptDate, destDatabase, config.sending == 1);
//                }

                // Retail Only (store_id = 1 and ProductGroup.contains("retail")
                List<OrderStatus> wpList = getRetailOrdersList(orderStatusList);
                if (wpList.size() > 0) {
                    logger.info("{} Start Retail file export. Target order total:" + String.valueOf(wpList.size()), new Date());
                    expILS.export(wpList, receiptDate, destDatabase, 2, config.sending == 1, false);
                }

                // EStore Only (store_id = 1 and ProductGroup.contains("estore")
                List<OrderStatus> esList = getEStoreOrdersList(orderStatusList);
                if (esList.size() > 0) {
                    logger.info("{} Start EStore file export. Target order total:" + String.valueOf(esList.size()), new Date());
                    expILS.export(esList, receiptDate, destDatabase, 6, config.sending == 1, false);
                }

                // Service Only (store_id = 1 and ProductGroup.contains("service")
                List<OrderStatus> svcList = getServiceOrdersList(orderStatusList);
                if (svcList.size() > 0) {
                    logger.info("{} Start Service file export. Target order total:" + String.valueOf(svcList.size()), new Date());
                    expILS.export(svcList, receiptDate, destDatabase, 5, config.sending == 1, false);
                }

                // AveryPro Only (store_id = 11)
                List<OrderStatus> apList = getAveryProOrders(orderStatusList);
                if (apList.size() > 0) {
                    logger.info("{} Start AveryPro file export. Target order total:" + String.valueOf(apList.size()), new Date());
                    expILS.export(apList, receiptDate, destDatabase, 3, config.sending == 1, false);
                }

                // log data processing run
                OrderStatus.Companion.logOrderStatus(destDatabase, orderStatusList);
            }

            // export credit memos to ILS
            if (cmList != null && cmList.size() > 0) {

                if (cmOrderStatusList.size() > 0) {
                    logger.info("{} Start export process for CreditMemo. Target Credit Memo total:" + String.valueOf(cmOrderStatusList.size()), new Date());
                    expILS.export(cmOrderStatusList, receiptDate, destDatabase, 4, config.sending == 1, false);
                }

                // log data processing run
                OrderStatus.Companion.logOrderStatus(destDatabase, cmOrderStatusList);
            }

//            //Send cancellation data to Glovia when the job is running on a scheduled basis. Not run by order no basis.
//            if(orderNoList.equals("")){
//                // export cancelled Glovia orders
//                logger.info("{} Start Glovia cancel file export.", new Date());
//                expGlovia.exportCancel(receiptDate, destDatabase, config.sending == 1);
//            }


        } catch (Exception e) {
            logger.error("Exception in process() ", e);
            throw e;
        }
    }

    private List<OrderStatus> getWePrintOrdersForGlovia(List<OrderStatus> orderStatusList) {
        List<OrderStatus> osList = new ArrayList<OrderStatus>();
        Iterator<OrderStatus> iter = orderStatusList.iterator();
        while (iter.hasNext()) {
            OrderStatus os = (OrderStatus) iter.next();
            if (os.getStoreId() == BigInteger.valueOf(DIFYProperty.DIFY_STORE_ID)
                    && !os.getProductGroup().equalsIgnoreCase("retail") && !os.getProductGroup().equalsIgnoreCase("service") && !os.getProductGroup().equalsIgnoreCase("estore"))
                osList.add(os);
        }
        return osList;
    }

    private List<OrderStatus> getWePrintOrders(List<OrderStatus> orderStatusList) {
        List<OrderStatus> osList = new ArrayList<OrderStatus>();
        Iterator<OrderStatus> iter = orderStatusList.iterator();
        while (iter.hasNext()) {
            OrderStatus os = (OrderStatus) iter.next();
            if (os.getStoreId() == BigInteger.valueOf(DIFYProperty.DIFY_STORE_ID))
                osList.add(os);
        }
        return osList;
    }

    private List<OrderStatus> getAveryProOrders(
            List<OrderStatus> orderStatusList) {
        List<OrderStatus> osList = new ArrayList<OrderStatus>();
        Iterator<OrderStatus> iter = orderStatusList.iterator();
        while (iter.hasNext()) {
            OrderStatus os = (OrderStatus) iter.next();
            int storeId = DIFYProperty.AVYPRO_STORE_ID;
            if (os.getStoreId() == BigInteger.valueOf(DIFYProperty.AVYPRO_STORE_ID))
                osList.add(os);
        }
        return osList;
    }

    private List<OrderStatus> getEStoreOrdersList(List<OrderStatus> orderStatusList) {

        List<OrderStatus> osList = new ArrayList<OrderStatus>();
        Iterator<OrderStatus> iter = orderStatusList.iterator();
        while (iter.hasNext()) {
            OrderStatus os = (OrderStatus) iter.next();
            if (os.getStoreId() == BigInteger.valueOf(DIFYProperty.DIFY_STORE_ID)
                    && os.getProductGroup().contains("estore"))
                osList.add(os);
        }
        return osList;
    }

    private List<OrderStatus> getRetailOrdersList(List<OrderStatus> orderStatusList) {

        List<OrderStatus> osList = new ArrayList<OrderStatus>();
        Iterator<OrderStatus> iter = orderStatusList.iterator();
        while (iter.hasNext()) {
            OrderStatus os = (OrderStatus) iter.next();
            if (os.getStoreId() == BigInteger.valueOf(DIFYProperty.DIFY_STORE_ID)
                    && os.getProductGroup().contains("retail"))
                osList.add(os);
        }
        return osList;
    }

    private List<OrderStatus> getServiceOrdersList(List<OrderStatus> orderStatusList) {

        List<OrderStatus> osList = new ArrayList<OrderStatus>();
        Iterator<OrderStatus> iter = orderStatusList.iterator();
        while (iter.hasNext()) {
            OrderStatus os = (OrderStatus) iter.next();
            if (os.getStoreId() == BigInteger.valueOf(DIFYProperty.DIFY_STORE_ID)
                    && os.getProductGroup().contains("service"))
                osList.add(os);
        }
        return osList;
    }

    public List<OrderStatus> createOrderStatus(List<SalesFlatOrderForExport> sfoList,
                                               String filePath, String exception) {
        List<OrderStatus> orderStatusList = new ArrayList<OrderStatus>();
        Date receivedAt = new Date();
        int numOrders = 0;
        int numReplace = 0;

        Iterator<SalesFlatOrderForExport> iter = sfoList.iterator();
        while (iter.hasNext()) {
            SalesFlatOrderForExport sfo = (SalesFlatOrderForExport) iter.next();

            if (sfo.getBaseSubtotal().compareTo(new BigDecimal(0)) == 0) {
                numReplace++;
            } else {
                numOrders++;
            }

            OrderStatus os = new OrderStatus(
                sfo.getEntityId(),
                "Magento",
                receivedAt,
                "DBdirect",
                null,
                null,
                null,
                sfo.getIncrementId(),
                sfo.getBaseSubtotal().compareTo(new BigDecimal(0)) == 0 ? "Replacement" : "Order",
                exception != null && exception.length() > 0 ?  "Fail" : "Pass",
                exception != null && exception.length() > 0 ? exception : "" ,
                null,
                'P',
                null,null,'P',null,null,null,null,null,null,null,
                sfo.getStoreId(),
                sfo.getEntityId() == null ? true: false,
                sfo.getProductGroup()
            );

            orderStatusList.add(os);
        }

        Iterator<OrderStatus> iterOS = orderStatusList.iterator();
        while (iterOS.hasNext()) {
            OrderStatus os = (OrderStatus) iterOS.next();
            os.setNumOrders(BigInteger.valueOf(numOrders));
            os.setNumReplace(BigInteger.valueOf(numReplace));
        }

        return orderStatusList;
    }

    public List<OrderStatus> createCMOrderStatus(
            List<SalesFlatCreditMemoForExport> cmList, String filePath, String exception, Database db) {
        List<OrderStatus> cmOrderStatusList = new ArrayList<OrderStatus>();
        Date receivedAt = new Date();

        Iterator<SalesFlatCreditMemoForExport> iter = cmList.iterator();
        while (iter.hasNext()) {
            SalesFlatCreditMemoForExport cm = (SalesFlatCreditMemoForExport) iter.next();

            List<String> rtn = SalesFlatCreditMemoForExport.Companion.checkCMItem(db, cm.getIncrementId());

            if(rtn.size() == 0) {

                OrderStatus os = new OrderStatus(
                        cm.getOrderId(),
                        "Magento",
                        receivedAt,
                        "DBdirect",
                        BigInteger.valueOf(cmList.size()),
                        null,
                        null,
                        cm.getIncrementId(),
                        "Credit Memo",
                        exception != null && exception.length() > 0 ? "Fail" : "Pass",
                        exception != null && exception.length() > 0 ? exception : "",
                        null,
                        ' ',
                        null, null, 'P', null, null, null, null, null, null, null,
                        cm.getStoreId(),
                        false,
                        cm.getProductGroup()
                );

                cmOrderStatusList.add(os);

            } else {

                try {
                    SendEmail.sendMessageWOcc(DIFYProperty.EMAIL_DUPLICATE_ORDER_RECIPIENTS,
                            DIFYProperty.EMAIL_SENDER,
                            DIFYProperty.EMAIL_HOST,
                            DIFYProperty.SERVER + ": CM Item has issue.",
                            "Check CM : " + cm.getIncrementId(),
                            false);
                }
                catch (Exception em) {
                    logger.error(em.toString());
                }

            }
        }
        return cmOrderStatusList;
    }

    public List<OrderStatus> createINVOrderStatus(
            List<SalesFlatInvoiceForExport> invList, String filePath, String exception) {
        List<OrderStatus> invOrderStatusList = new ArrayList<OrderStatus>();
        Date receivedAt = new Date();

        Iterator<SalesFlatInvoiceForExport> iter = invList.iterator();
        while (iter.hasNext()) {
            SalesFlatInvoiceForExport inv = (SalesFlatInvoiceForExport) iter.next();

            OrderStatus os = new OrderStatus(
                    inv.getOrderId(),
                    "Magento",
                    receivedAt,
                    "DBdirect",
                    BigInteger.valueOf(invList.size()),
                    null,
                    null,
                    inv.getIncrementId(),
                    "Fund Capture",
                    exception != null && exception.length() > 0 ?  "Fail" : "Pass",
                    exception != null && exception.length() > 0 ? exception : "" ,
                    null,
                    ' ',
                    null,null,'P',null,null,null,null,null,null,null,
                    inv.getStoreId(),
                    false,
                    inv.getProductGroup()
            );

            invOrderStatusList.add(os);
        }
        return invOrderStatusList;
    }

    public List<OrderStatus> createPMTOrderStatus(
            List<SalesFlatOrderPaymentForExport> pmtList, String filePath, String exception) {
        List<OrderStatus> pmtOrderStatusList = new ArrayList<OrderStatus>();
        Date receivedAt = new Date();

        Iterator<SalesFlatOrderPaymentForExport> iter = pmtList.iterator();
        while (iter.hasNext()) {
            SalesFlatOrderPaymentForExport pmt = (SalesFlatOrderPaymentForExport) iter.next();

            OrderStatus os = new OrderStatus(
                    pmt.getOrderId(),
                    "Magento",
                    receivedAt,
                    "DBdirect",
                    BigInteger.valueOf(pmtList.size()),
                    null,
                    null,
                    pmt.getIncrementId(),
                    "Fund Capture",
                    exception != null && exception.length() > 0 ?  "Fail" : "Pass",
                    exception != null && exception.length() > 0 ? exception : "" ,
                    null,
                    ' ',
                    null,null,'P',null,null,null,null,null,null,null,
                    pmt.getStoreId(),
                    false,
                    pmt.getProductGroup()
            );

            pmtOrderStatusList.add(os);
        }
        return pmtOrderStatusList;
    }

}
