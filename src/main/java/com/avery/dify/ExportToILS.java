package com.avery.dify;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.avery.dify.export.*;
import com.avery.dify.export.SalesFlatCreditmemoComment;
import com.avery.dify.service.*;
import com.avery.dify.util.DIFYProperty;
import com.avery.dify.util.MQUtil;
import com.avery.dify.util.SendEmail;
import com.avery.dify.util.XmlUtils;
import com.avery.dify.util.MailBody;

import com.avery.dify.xmlmodel.PurchaseOrders;
import com.github.davidmoten.rx.jdbc.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class ExportToILS {

    private static SimpleDateFormat dfs = new SimpleDateFormat("MM/dd/yyyy HH:mm z");
    private static Logger logger = LoggerFactory.getLogger(ExportToILS.class);

    public void export(List<OrderStatus> orderStatusList, Date receiptDt, Database db, int proc_Type, boolean dosend, boolean ispmt) {
        // Load orders from Staging DB
        HashMap<String, ILSOrder> itemList = null;
        try {
            if(proc_Type == 1)
                itemList = ILSOrder.Companion.getInvData(db, getOrderList(orderStatusList, true)); //Fund Capture for WePrint Order
            if(proc_Type == 11)
                itemList = ILSOrder.Companion.getOlaData(db, getOrderList(orderStatusList, true), DIFYProperty.ETSY_CUST_ID); //Fund Capture for Online Aisle
            else if(proc_Type == 2)
                itemList = ILSOrder.Companion.getRtData(db, getOrderList(orderStatusList, true)); //Retail Order
            else if(proc_Type == 3)
                itemList = ILSOrder.Companion.getProData(db, getOrderList(orderStatusList, false)); //AveryPro Order
            else if(proc_Type == 4)
                itemList = ILSOrder.Companion.getCmData(db, getOrderList(orderStatusList, true));   //CreditMemo
            else if(proc_Type == 5)
                itemList = ILSOrder.Companion.getSvcData(db, getOrderList(orderStatusList, true));   //Service Order
            else if(proc_Type == 6)
                itemList = ILSOrder.Companion.getESData(db, getOrderList(orderStatusList, true)); //EStore Order
        }
        catch (Exception e) {
            // catch error, update all dispositions here and return
            Iterator<OrderStatus> iter = orderStatusList.iterator();
            while (iter.hasNext()) {
                OrderStatus shipmentBn = (OrderStatus)iter.next();
                shipmentBn.setIlsUpdateStatus('F');
                shipmentBn.setOrderDisp("Fail");
                shipmentBn.setOrderDispComment(shipmentBn.getOrderDispComment().toString() == null ?
                        "ILS Interface: An error occured while reading order information from the Staging Database: " + e.getMessage() + "\n" :
                        shipmentBn.getOrderDispComment().toString() + "ILS Interface: An error occured while reading order information from the Staging Database: " + e.getMessage() + "\n");
            }
            return;
        }

        List<String> WePrintSent = new ArrayList<String>();
        List<String> OnlineSent = new ArrayList<String>();
        List<String> RetailSent = new ArrayList<String>();
        List<String> MixedSent = new ArrayList<String>();
        List<String> AveryProSent = new ArrayList<String>();
        List<String> ServiceSent = new ArrayList<String>();
        List<String> EStoreSent = new ArrayList<String>();
        HashMap<Integer, List<String>> sentItem = new HashMap<Integer, List<String>>();

        List<String> WePrintDup = new ArrayList<String>();
        List<String> OnlineDup = new ArrayList<String>();
        List<String> RetailDup = new ArrayList<String>();
        List<String> MixedDup = new ArrayList<String>();
        List<String> AveryProDup = new ArrayList<String>();
        List<String> ServiceDup = new ArrayList<String>();
        List<String> EStoreDup = new ArrayList<String>();
        HashMap<Integer, List<String>> duplicatedItem = new HashMap<Integer, List<String>>();

        Iterator<OrderStatus> iter = orderStatusList.iterator();
        while (iter.hasNext()) {
            OrderStatus shipmentBn = (OrderStatus)iter.next();

            // get ILSOrder
            ILSOrder orderBn = itemList.get(shipmentBn.getOrderId());
            if (orderBn == null) {
                shipmentBn.setIlsUpdateStatus('F');
                shipmentBn.setOrderDisp("Fail");
                shipmentBn.setOrderDispComment(shipmentBn.getOrderDispComment().toString() == null ?
                        "ILS Interface: Order was not found in Staging Database" :
                        shipmentBn.getOrderDispComment().toString() + "ILS Interface: Order was not found in Staging Database");
                continue;
            }

            // do not send if duplicate
            if (orderBn.isDuplicate()) {
                shipmentBn.setIlsUpdateStatus('F');
                shipmentBn.setOrderDisp("Fail");
                if (proc_Type == 4) {
                    shipmentBn.setOrderDispComment(shipmentBn.getOrderDispComment().toString() == null ?
                            "ILS Interface: Duplicate CreditMemo found in ILS" :
                            shipmentBn.getOrderDispComment().toString() + "ILS Interface: Duplicate CreditMemo found in ILS");
                } else {
                    shipmentBn.setOrderDispComment(shipmentBn.getOrderDispComment().toString() == null ?
                            "ILS Interface: Duplicate order found in ILS" :
                            shipmentBn.getOrderDispComment().toString() + "ILS Interface: Duplicate order found in ILS");
                }

                // send duplicate order notification
                try {
                    String subject = proc_Type == 4 ? "Duplicate CreditMemo " : "Duplicate Order "
                            + orderBn.getOrderId() + " Received from Magento at " + dfs.format(new Date());
                    SendEmail.sendMessage(DIFYProperty.EMAIL_DUPLICATE_ORDER_RECIPIENTS,
                            DIFYProperty.EMAIL_SENDER,
                            DIFYProperty.EMAIL_HOST,
                            DIFYProperty.SERVER + ": " + subject,
                            proc_Type == 4 ? MailBody.buildOrderEmailCM(orderBn) : MailBody.buildOrderEmail(orderBn),
                            true);
                }
                catch (Exception em) {
                    logger.error(em.toString());
                }

                if(proc_Type == 2){
                    RetailDup.add(shipmentBn.getOrderId().toString());
                } else if(proc_Type == 3){
                    AveryProDup.add(shipmentBn.getOrderId().toString());
                } else if(proc_Type == 4){
                    if (shipmentBn.getProductGroup().matches("retail")) {
                        RetailDup.add(orderBn.getCreditMemoNumber().toString());
                    } else if (shipmentBn.getProductGroup().matches("estore")) {
                        EStoreDup.add(orderBn.getCreditMemoNumber().toString());
                    } else if (shipmentBn.getProductGroup().matches("service")) {
                        ServiceDup.add(orderBn.getCreditMemoNumber().toString());
                    } else if (shipmentBn.getProductGroup().matches("oa")) {
                        OnlineDup.add(orderBn.getCreditMemoNumber().toString());
                    } else if (shipmentBn.getProductGroup().contains("retail") || shipmentBn.getProductGroup().contains("estore") || shipmentBn.getProductGroup().contains("service") || shipmentBn.getProductGroup().contains("oa")) {
                        MixedDup.add(orderBn.getCreditMemoNumber().toString());
                    } else {
                        if (orderBn.getInternalPartnerID().equals("DIFY"))
                            WePrintDup.add(orderBn.getCreditMemoNumber().toString());
                        else
                            AveryProDup.add(orderBn.getCreditMemoNumber().toString());
                    }
                } else if(proc_Type == 5){
                    ServiceDup.add(shipmentBn.getOrderId().toString());
                } else if(proc_Type == 6){
                    EStoreDup.add(shipmentBn.getOrderId().toString());
                }

                continue;
            }

            // build xml message
            Document doc = null;
            String xmlStr = null;
            Date receiptMixedDate = null;
            try {
                if(proc_Type == 1){
                    IlsOrderXmlBuilder ilsOrderXmlBuilder = new IlsOrderXmlBuilder();
                    PurchaseOrders ilsOrders = ilsOrderXmlBuilder.buildWePrintOrder(orderBn);
                    doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber(),new Date());

                    xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                    logger.info("Document String: " + xmlStr, new Date());
                    shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 0, dosend, db);

                    if(shipmentBn.getOrderDisp() != null && shipmentBn.getOrderDisp().equals("Pass")){
                        WePrintSent.add(shipmentBn.getOrderStatusId().toString());
                    }

                }if(proc_Type == 11){
                    IlsOnlineOrderXmlBuilder ilsOnlineOrderXmlBuilder = new IlsOnlineOrderXmlBuilder();
                    PurchaseOrders ilsOrders = ilsOnlineOrderXmlBuilder.buildOnlineOrder(orderBn);
                    doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber(),new Date());

                    xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                    logger.info("Document String: " + xmlStr, new Date());
                    shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 0, dosend, db);

                    if(shipmentBn.getOrderDisp() != null && shipmentBn.getOrderDisp().equals("Pass")){
                        OnlineSent.add(shipmentBn.getOrderStatusId().toString());
                    }

                }else if(proc_Type == 2){
                    IlsRetailOrderXmlBuilder ilsRetailXmlBuilder = new IlsRetailOrderXmlBuilder();
                    PurchaseOrders ilsOrders = ilsRetailXmlBuilder.buildRetailOrder(orderBn);
                    doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber(),new Date());

                    xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                    logger.info("Document String: " + xmlStr, new Date());
                    shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 0, dosend, db);

                    if(shipmentBn.getOrderDisp() != null && shipmentBn.getOrderDisp().equals("Pass")){
                        RetailSent.add(shipmentBn.getOrderId().toString());
                    }


                } else if(proc_Type == 3){
                    IlsAveryProOrderXmlBuilder ilsAvyProXmlBuilder = new IlsAveryProOrderXmlBuilder();
                    PurchaseOrders ilsOrders = ilsAvyProXmlBuilder.buildProOrder(orderBn);
                    doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber(),new Date());

                    xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                    logger.info("Document String: " + xmlStr, new Date());
                    shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 0, dosend, db);

                    if(shipmentBn.getOrderDisp() != null && shipmentBn.getOrderDisp().equals("Pass")){
                        AveryProSent.add(shipmentBn.getOrderId().toString());
                    }

                } else if(proc_Type == 4){
                    //check if it is retail order
                    if (shipmentBn.getProductGroup().matches("retail")) {
                        IlsCMRetailOrderXmlBuilder ilsCMRetailOrderXmlBuilder = new IlsCMRetailOrderXmlBuilder();
                        PurchaseOrders ilsOrders = ilsCMRetailOrderXmlBuilder.buildCMRetailOrder(orderBn, orderBn.getOrderDetails(), false);
                        doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "A" + orderBn.getCreditMemoNumber(), new Date());

                        xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                        logger.info("Document String: " + xmlStr, new Date());
                        shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 2, dosend, db);

                        if(shipmentBn.getOrderDisp() != null && shipmentBn.getOrderDisp().equals("Pass")){
                            RetailSent.add(orderBn.getCreditMemoNumber().toString());
                        }

                    } else if (shipmentBn.getProductGroup().matches("estore")) {
                        IlsCMEStoreOrderXmlBuilder ilsCMEStoreOrderXmlBuilder = new IlsCMEStoreOrderXmlBuilder();
                        PurchaseOrders ilsOrders = ilsCMEStoreOrderXmlBuilder.buildCMEStoreOrder(orderBn, orderBn.getOrderDetails(), false);
                        doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "A" + orderBn.getCreditMemoNumber(), new Date());

                        xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                        logger.info("Document String: " + xmlStr, new Date());
                        shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 5, dosend, db);

                        if(shipmentBn.getOrderDisp() != null && shipmentBn.getOrderDisp().equals("Pass")){
                            EStoreSent.add(orderBn.getCreditMemoNumber().toString());
                        }

                    } else if (shipmentBn.getProductGroup().matches("service")) {
                        IlsCMServiceOrderXmlBuilder ilsCMServiceOrderXmlBuilder = new IlsCMServiceOrderXmlBuilder();
                        PurchaseOrders ilsOrders = ilsCMServiceOrderXmlBuilder.buildCMServiceOrder(orderBn, orderBn.getOrderDetails(), false);
                        doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "S" + orderBn.getCreditMemoNumber(), new Date());

                        xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                        logger.info("Document String: " + xmlStr, new Date());
                        shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 3, dosend, db);

                        if(shipmentBn.getOrderDisp() != null && shipmentBn.getOrderDisp().equals("Pass")){
                            ServiceSent.add(orderBn.getCreditMemoNumber().toString());
                        }

                    }  else if (shipmentBn.getProductGroup().matches("oa")) {
                        IlsCMOnlineOrderXmlBuilder ilsCMOnlineOrderXmlBuilder = new IlsCMOnlineOrderXmlBuilder();
                        PurchaseOrders ilsOrders = ilsCMOnlineOrderXmlBuilder.buildCMOnlineOrder(orderBn, orderBn.getOrderDetails(), false);
                        doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "B" + orderBn.getCreditMemoNumber(), new Date());

                        xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                        logger.info("Document String: " + xmlStr, new Date());
                        shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 4, dosend, db);

                        if(shipmentBn.getOrderDisp() != null && shipmentBn.getOrderDisp().equals("Pass")){
                            OnlineSent.add(orderBn.getCreditMemoNumber().toString());
                        }

                    } else if (shipmentBn.getProductGroup().contains("retail") || shipmentBn.getProductGroup().contains("estore") || shipmentBn.getProductGroup().contains("service") || shipmentBn.getProductGroup().contains("oa")) {
                        // Split orders
                        HashMap<String, List<ILSOrderDetail>> mixedOrder = splitOrder(db, orderBn);

                        if(mixedOrder.get("retail").size() != 0) {
                            IlsCMRetailOrderXmlBuilder ilsCMRetailOrderXmlBuilder = new IlsCMRetailOrderXmlBuilder();
                            PurchaseOrders ilsOrders = ilsCMRetailOrderXmlBuilder.buildCMRetailOrder(orderBn, mixedOrder.get("retail"), true);
                            doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "A" + orderBn.getCreditMemoNumber(), new Date());
                            xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                            logger.info("Document String: " + xmlStr, new Date());
                            shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 2, dosend, db);
                        }

                        if(mixedOrder.get("estore").size() != 0) {
                            IlsCMEStoreOrderXmlBuilder ilsCMEStoreOrderXmlBuilder = new IlsCMEStoreOrderXmlBuilder();
                            PurchaseOrders ilsOrders = ilsCMEStoreOrderXmlBuilder.buildCMEStoreOrder(orderBn, mixedOrder.get("estore"), true);
                            doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "A" + orderBn.getCreditMemoNumber(), new Date());
                            xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                            logger.info("Document String: " + xmlStr, new Date());
                            shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 5, dosend, db);
                        }

                        if(mixedOrder.get("service").size() != 0) {
                            IlsCMServiceOrderXmlBuilder ilsCMServiceOrderXmlBuilder = new IlsCMServiceOrderXmlBuilder();
                            PurchaseOrders ilsOrders = ilsCMServiceOrderXmlBuilder.buildCMServiceOrder(orderBn, mixedOrder.get("service"), true);
                            doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "S" + orderBn.getCreditMemoNumber(), new Date());
                            xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                            logger.info("Document String: " + xmlStr, new Date());
                            shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 3, dosend, db);
                        }

                        if(mixedOrder.get("oa").size() != 0) {
                            IlsCMOnlineOrderXmlBuilder ilsCMOnlineOrderXmlBuilder = new IlsCMOnlineOrderXmlBuilder();
                            PurchaseOrders ilsOrders = ilsCMOnlineOrderXmlBuilder.buildCMOnlineOrder(orderBn, mixedOrder.get("oa"), true);
                            doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "B" + orderBn.getCreditMemoNumber(), new Date());

                            xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                            logger.info("Document String: " + xmlStr, new Date());
                            shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 4, dosend, db);
                        }

                        // save others
                        if(mixedOrder.get("others").size() != 0) {
                            IlsCMNonRetailOrderXmlBuilder ilsCMNonRetailOrderXmlBuilder = new IlsCMNonRetailOrderXmlBuilder();
                            PurchaseOrders ilsOrders = ilsCMNonRetailOrderXmlBuilder.buildCMNonRetailOrder(orderBn, mixedOrder.get("others"), true);
                            doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "-" + orderBn.getCreditMemoNumber(), new Date());
                            xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                            logger.info("Document String: " + xmlStr, new Date());
                            receiptMixedDate = new Date();
                            shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptMixedDate, 1, dosend, db);
                        }

                        if(orderBn.getOrderDetails().size() == 0){
                            if(shipmentBn.getProductGroup().contains("sheet") || shipmentBn.getProductGroup().contains("roll")) {
                                IlsCMNonRetailOrderXmlBuilder ilsCMNonRetailOrderXmlBuilder = new IlsCMNonRetailOrderXmlBuilder();
                                PurchaseOrders ilsOrders = ilsCMNonRetailOrderXmlBuilder.buildCMNonRetailOrder(orderBn, mixedOrder.get("others"), false);
                                doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "-" + orderBn.getCreditMemoNumber(), new Date());
                                xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                                logger.info("Document String: " + xmlStr, new Date());
                                receiptMixedDate = new Date();
                                shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptMixedDate, 1, dosend, db);
                            } else if(shipmentBn.getProductGroup().contains("oa")) {
                                IlsCMOnlineOrderXmlBuilder ilsCMOnlineOrderXmlBuilder = new IlsCMOnlineOrderXmlBuilder();
                                PurchaseOrders ilsOrders = ilsCMOnlineOrderXmlBuilder.buildCMOnlineOrder(orderBn, mixedOrder.get("oa"), false);
                                doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "B" + orderBn.getCreditMemoNumber(), new Date());

                                xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                                logger.info("Document String: " + xmlStr, new Date());
                                shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 4, dosend, db);
                            } else if(shipmentBn.getProductGroup().contains("retail")) {
                                IlsCMRetailOrderXmlBuilder ilsCMRetailOrderXmlBuilder = new IlsCMRetailOrderXmlBuilder();
                                PurchaseOrders ilsOrders = ilsCMRetailOrderXmlBuilder.buildCMRetailOrder(orderBn, mixedOrder.get("retail"), false);
                                doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "A" + orderBn.getCreditMemoNumber(), new Date());
                                xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                                logger.info("Document String: " + xmlStr, new Date());
                                shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 2, dosend, db);
                            } else if(shipmentBn.getProductGroup().contains("service")) {
                                IlsCMServiceOrderXmlBuilder ilsCMServiceOrderXmlBuilder = new IlsCMServiceOrderXmlBuilder();
                                PurchaseOrders ilsOrders = ilsCMServiceOrderXmlBuilder.buildCMServiceOrder(orderBn, mixedOrder.get("service"), false);
                                doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "S" + orderBn.getCreditMemoNumber(), new Date());
                                xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                                logger.info("Document String: " + xmlStr, new Date());
                                shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 3, dosend, db);
                            } else if(shipmentBn.getProductGroup().contains("estore")) {
                                IlsCMEStoreOrderXmlBuilder ilsCMEStoreOrderXmlBuilder = new IlsCMEStoreOrderXmlBuilder();
                                PurchaseOrders ilsOrders = ilsCMEStoreOrderXmlBuilder.buildCMEStoreOrder(orderBn, mixedOrder.get("estore"), false);
                                doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "A" + orderBn.getCreditMemoNumber(), new Date());
                                xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                                logger.info("Document String: " + xmlStr, new Date());
                                shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 5, dosend, db);
                            }
                        }

                        if(shipmentBn.getOrderDisp() != null && shipmentBn.getOrderDisp().equals("Pass")){
                            MixedSent.add(orderBn.getCreditMemoNumber().toString());
                        }

                    } else {
                        IlsCMNonRetailOrderXmlBuilder ilsCMNonRetailOrderXmlBuilder = new IlsCMNonRetailOrderXmlBuilder();
                        PurchaseOrders ilsOrders = ilsCMNonRetailOrderXmlBuilder.buildCMNonRetailOrder(orderBn, orderBn.getOrderDetails(), false);
                        doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber() + "-" + orderBn.getCreditMemoNumber(), new Date());

                        xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                        logger.info("Document String: " + xmlStr, new Date());
                        shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 1, dosend, db);

                        if(shipmentBn.getOrderDisp() != null && shipmentBn.getOrderDisp().equals("Pass")) {
                            if (orderBn.getInternalPartnerID().equals("DIFY"))
                                WePrintSent.add(orderBn.getCreditMemoNumber().toString());
                            else
                                AveryProSent.add(orderBn.getCreditMemoNumber().toString());
                        }
                    }

                } else if(proc_Type == 5) {
                    IlsServiceOrderXmlBuilder ilsServiceXmlBuilder = new IlsServiceOrderXmlBuilder();
                    PurchaseOrders ilsOrders = ilsServiceXmlBuilder.buildServiceOrder(orderBn);
                    doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber(),new Date());

                    xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                    logger.info("Document String: " + xmlStr, new Date());
                    shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 0, dosend, db);

                    if(shipmentBn.getOrderDisp() != null && shipmentBn.getOrderDisp().equals("Pass")){
                        ServiceSent.add(shipmentBn.getOrderId().toString());
                    }

                } else if(proc_Type == 6) {
                    IlsEStoreOrderXmlBuilder ilsEStoreXmlBuilder = new IlsEStoreOrderXmlBuilder();
                    PurchaseOrders ilsOrders = ilsEStoreXmlBuilder.buildEStoreOrder(orderBn);
                    doc = writeToXmlFile(ilsOrders, orderBn.getCustomerPONumber(),new Date());

                    xmlStr = XmlUtils.getXmlStringFormatted(doc, false);
                    logger.info("Document String: " + xmlStr, new Date());
                    shipmentBn = exportSendXML(orderBn, shipmentBn, xmlStr, doc, receiptDt, 0, dosend, db);

                    if(shipmentBn.getOrderDisp() != null && shipmentBn.getOrderDisp().equals("Pass")){
                        EStoreSent.add(shipmentBn.getOrderId().toString());
                    }

                }
            }
            catch (Exception e) {
                shipmentBn.setIlsUpdateStatus('F');
                shipmentBn.setOrderDisp("Fail");
                shipmentBn.setOrderDispComment(shipmentBn.getOrderDispComment().toString() == null ?
                        "ILS Interface: An error occured while building the Order XML. " + e.getMessage() + "\n" :
                        shipmentBn.getOrderDispComment().toString() + "ILS Interface: An error occured while building the Order XML. " + e.getMessage() + "\n");
                continue;
            }

        }

        try {
            sentItem.put(2, WePrintSent);
            sentItem.put(3, RetailSent);
            sentItem.put(23, MixedSent);
            sentItem.put(4, AveryProSent);
            sentItem.put(5, ServiceSent);
            sentItem.put(6, OnlineSent);
            sentItem.put(7, EStoreSent);

            duplicatedItem.put(2, WePrintDup);
            duplicatedItem.put(3, RetailDup);
            duplicatedItem.put(23, MixedDup);
            duplicatedItem.put(4, AveryProDup);
            duplicatedItem.put(5, ServiceDup);
            duplicatedItem.put(6, OnlineDup);
            duplicatedItem.put(7, EStoreDup);

            if(dosend && proc_Type == 1) {
                if(ispmt)
                    SalesFlatOrderPaymentForExport.Companion.updateMasterFlagPMT(db, WePrintSent);
                else
                    SalesFlatInvoiceForExport.Companion.updateMasterFlagINV(db, WePrintSent);

                String totalLog = "{} Fund Capture(WePrint only) sent total ||";

                if (WePrintSent.size() != 0)    totalLog += " WePrint:" + String.valueOf(WePrintSent.size());

                logger.info(totalLog, new Date());

            } if(dosend && proc_Type == 11) {
                if(ispmt)
                    SalesFlatOrderPaymentForExport.Companion.updateMasterFlagPMTOLA(db, OnlineSent);
                else
                    SalesFlatInvoiceForExport.Companion.updateMasterFlagINVOLA(db, OnlineSent);

                String totalLog = "{} Fund Capture(Online Aisle only) sent total ||";

                if (OnlineSent.size() != 0)    totalLog += " WePrint:" + String.valueOf(OnlineSent.size());

                logger.info(totalLog, new Date());

            } else if(dosend && proc_Type == 4) {
                SalesFlatCreditMemoForExport.Companion.updateMasterFlagCM(db, sentItem);
                SalesFlatCreditMemoForExport.Companion.updateDupFlagCM(db, duplicatedItem);

                String totalLog = "{} Credit memo sent total ||";

                if (WePrintSent.size() != 0)    totalLog += " WePrint:" + String.valueOf(WePrintSent.size());
                if (RetailSent.size() != 0)    totalLog += " Retail:" + String.valueOf(RetailSent.size());
                if (AveryProSent.size() != 0)    totalLog += " AveryPro:" + String.valueOf(AveryProSent.size());
                if (ServiceSent.size() != 0)    totalLog += " Service:" + String.valueOf(ServiceSent.size());
                if (OnlineSent.size() != 0)    totalLog += " OnlineAisle:" + String.valueOf(OnlineSent.size());
                if (EStoreSent.size() != 0)    totalLog += " EStore:" + String.valueOf(EStoreSent.size());
                if (MixedSent.size() != 0)    totalLog += " Mixed:" + String.valueOf(MixedSent.size());

                logger.info(totalLog, new Date());

            } else if(dosend) {
                SalesFlatOrderForExport.Companion.updateMasterFlag(db, sentItem);
                SalesFlatOrderForExport.Companion.updateDupFlag(db, duplicatedItem);

                String totalLog = "{} Order sent total ||";

                if (RetailSent.size() != 0)    totalLog += " Retail:" + String.valueOf(RetailSent.size());
                if (MixedSent.size() != 0)    totalLog += " Mixed:" + String.valueOf(MixedSent.size());
                if (AveryProSent.size() != 0)    totalLog += " AveryPro:" + String.valueOf(AveryProSent.size());
                if (ServiceSent.size() != 0)    totalLog += " Service:" + String.valueOf(ServiceSent.size());
                if (EStoreSent.size() != 0)    totalLog += " EStore:" + String.valueOf(EStoreSent.size());

                logger.info(totalLog, new Date());

            }
        } catch (Exception e) {
            logger.error("Sent flag update error: " + e.getMessage(), new Date());
        }


    }


    private Document writeToXmlFile(PurchaseOrders po, String orderNum, Date date) {
        Document doc = null;
        try {
            DocumentBuilderFactory docFact = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuild = docFact.newDocumentBuilder();
            doc = docBuild.newDocument();

            JAXBContext jaxb = JAXBContext.newInstance(po.getClass());
            Marshaller marshal = jaxb.createMarshaller();
            marshal.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshal.marshal(po, doc);

            return doc;
        } catch (Exception e) {
            logger.error("Error in writeToXmlFile({})", orderNum, e);
        }
        return doc;
    }

    private OrderStatus exportSendXML(ILSOrder orderBn, OrderStatus shipmentBn, String xmlStr, Document doc, Date receiptDt, int naming, boolean sending, Database db){

        String fileName = "";

        // save xml message
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");

            if(orderBn.isCredit() && naming == 1) {

                fileName = orderBn.getCustomerPONumber() + "-" + orderBn.getCreditMemoNumber() + "_" + df.format(receiptDt) + ".xml";

            } else if(orderBn.isCredit() && naming == 2) {

                if(orderBn.isRetailOrder())
                    fileName = orderBn.getCustomerPONumber() + "A" + orderBn.getCreditMemoNumber() + "_" + df.format(receiptDt) + ".xml";

            } else if(orderBn.isCredit() && naming == 3) {

                if(orderBn.isServiceOrder())
                    fileName = orderBn.getCustomerPONumber() + "S" + orderBn.getCreditMemoNumber() + "_" + df.format(receiptDt) + ".xml";

            } else if(orderBn.isCredit() && naming == 4) {

                if(orderBn.isOnlineAisleOrder())
                    fileName = orderBn.getCustomerPONumber() + "B" + orderBn.getCreditMemoNumber() + "_" + df.format(receiptDt) + ".xml";

            } else if(orderBn.isCredit() && naming == 5) {

                if(orderBn.isEStoreOrder())
                    fileName = orderBn.getCustomerPONumber() + "A" + orderBn.getCreditMemoNumber() + "_" + df.format(receiptDt) + ".xml";

            } else {

                if(orderBn.isRetailOrder())
                    fileName = orderBn.getCustomerPONumber() + "A_" + df.format(receiptDt) + ".xml";
                else if(orderBn.isEStoreOrder())
                    fileName = orderBn.getCustomerPONumber() + "A_" + df.format(receiptDt) + ".xml";
                else if(orderBn.isServiceOrder())
                    fileName = orderBn.getCustomerPONumber() + "S_" + df.format(receiptDt) + ".xml";
                else if(orderBn.isOnlineAisleOrder())
                    fileName = orderBn.getCustomerPONumber() + "B_" + df.format(receiptDt) + ".xml";
                else
                    fileName = orderBn.getCustomerPONumber() + "_" + df.format(receiptDt) + ".xml";

            }
            String filePath = DIFYProperty.ILS_ARCHIVE_DIR + File.separator + fileName;
            XmlUtils.writeXmlToFile(doc, filePath);
            shipmentBn.setIlsFilename(fileName);
        }
        catch (Exception e) {
            shipmentBn.setIlsUpdateStatus('F');
            shipmentBn.setOrderDisp("Fail");
            shipmentBn.setOrderDispComment(shipmentBn.getOrderDispComment().toString() == null ?
                    "ILS Interface: Order could not be saved to filesystem. " + e.getMessage() + "\n" :
                    shipmentBn.getOrderDispComment().toString() + "ILS Interface: Order could not be saved to filesystem. " + e.getMessage() + "\n");

        }

        // export xml message
        try {
            if (sending){
                if (orderBn.isCredit() && orderBn.getNumofLinesonPO().equals(BigInteger.ZERO) && !orderBn.getAdjustment().equals(BigDecimal.ZERO)){

                    List<com.avery.dify.export.SalesFlatCreditmemoComment> CMComments = SalesFlatCreditmemoComment.Companion.select(db, orderBn.getCreditMemoNumber());

                    SendEmail.sendMessage(
                            DIFYProperty.EMAIL_SPECIAL_CM_RECIPIENTS,
                            DIFYProperty.EMAIL_SENDER,
                            DIFYProperty.EMAIL_HOST,
                            DIFYProperty.SERVER + " Special Charge Only Refund",
                            MailBody.buildNoDetailCreditMemo(orderBn, CMComments, naming), true);

                    shipmentBn.setOrderDispComment("Special charge only refunded.");
                } else {
                    exportToMQ(xmlStr);

                    logger.info("{} File has been sent to ILS. " + fileName, new Date());
                    shipmentBn.setIlsUpdatedAt(new Date());
                }
            }

        }
        catch (Exception e) {
            shipmentBn.setIlsUpdateStatus('F');
            shipmentBn.setOrderDisp("Fail");
            shipmentBn.setOrderDispComment(shipmentBn.getOrderDispComment().toString() == null ?
                    "ILS Interface: Order could not be written to Queue. " + e.getMessage() + "\n" :
                    shipmentBn.getOrderDispComment().toString() + "ILS Interface: Order could not be written to Queue. " + e.getMessage() + "\n");
        }

        return shipmentBn;

    }

    private List<String>getOrderList(List<OrderStatus> shipList, boolean isentity) {
        List<String>orders = new ArrayList<String>();
        Iterator<OrderStatus>iter = shipList.iterator();
        while(iter.hasNext()) {
            if(isentity)
                orders.add((iter.next()).getOrderStatusId().toString());
            else
                orders.add((iter.next()).getOrderId());
        }
        return orders;
    }

    private HashMap<String, List<ILSOrderDetail>> splitOrder(Database db, ILSOrder orderBn) {

        List<ILSOrderDetail> tmpRetail = new ArrayList<ILSOrderDetail>();
        List<ILSOrderDetail> tmpEStore = new ArrayList<ILSOrderDetail>();
        List<ILSOrderDetail> tmpService = new ArrayList<ILSOrderDetail>();
        List<ILSOrderDetail> tmpOnline = new ArrayList<ILSOrderDetail>();
        List<ILSOrderDetail> tmpMixed = new ArrayList<ILSOrderDetail>();
        HashMap<String, List<ILSOrderDetail>> mixedList = new HashMap<String,List<ILSOrderDetail>>();

        List<ILSOrderDetail> detailList = orderBn.getOrderDetails();
        for (ILSOrderDetail orderDetail : detailList){
            try {

                String pr_grp = ILSOrder.Companion.checkProductGroup(db, orderDetail.getOnlineAisleSKU());

                if (pr_grp.equals("Retail")){
                    tmpRetail.add(orderDetail);
                } else if (pr_grp.equals("OnlineAisle")){
                    tmpOnline.add(orderDetail);
                } else if (pr_grp.equals("Service")){
                    tmpService.add(orderDetail);
                } else if (pr_grp.equals("Estore")){
                    tmpEStore.add(orderDetail);
                } else {
                    tmpMixed.add(orderDetail);
                }
            }catch(Exception e){
                logger.error("spliting group error" + e.getMessage(), new Date());
            }
        }

        mixedList.put("others", tmpMixed);
        mixedList.put("retail", tmpRetail);
        mixedList.put("estore", tmpEStore);
        mixedList.put("service", tmpService);
        mixedList.put("oa", tmpOnline);

        return mixedList;
    }



    private void exportToMQ(String xmlStr) throws Exception {
        MQUtil mq = null;
        try {
            mq = new MQUtil(DIFYProperty.ILS_DIFY_QUEUE);
            mq.putMsg(xmlStr);
        }
        catch (Exception e) {
            try {
                SendEmail.sendMessage(
                        DIFYProperty.ADMIN_RECIPIENTS,
                        DIFYProperty.EMAIL_SENDER,
                        DIFYProperty.EMAIL_HOST,
                        "ERROR occurred while sending XML to ILS",
                        "There was an error sending XML to ILS .  Check it out.\n\n"
                                + e);
            } catch (Exception ex) {
                logger.error(ex.toString());
            }
            throw new Exception("ExportToILS.exportToMQ() Exception: " + e.getMessage());
        }
        finally {
            mq.closeQueue();
            mq.closeQueueManager();
        }
    }

}
