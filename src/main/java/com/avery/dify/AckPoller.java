package com.avery.dify;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.avery.dify.util.DIFYProperty;
import com.avery.dify.util.SendEmail;
import com.github.davidmoten.rx.jdbc.Database;

public class AckPoller {

    public void poll(Database db) throws Exception {
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        try {
            GetILSAcknowledgments ack = new GetILSAcknowledgments();
            ack.processAck(db);

            GetGlvAcknowledgments ackGlv = new GetGlvAcknowledgments();
            ackGlv.processAck(db);

            System.out.println("[" + df.format(new Date()) + "]: Ack run completed.");
        }
        catch (Exception e) {
            throw new Exception("AckPoller. Exception: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        AckPoller mp = null;
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        try {
            DIFYProperty prop = new DIFYProperty(Integer.valueOf(args[0]));
            // Oracle's own JDBC driver does not support auto discovery
            Class.forName(DIFYProperty.DB_DRIVER);

            String destUrl = DIFYProperty.DEST_DB_URL;
            String destUsername = DIFYProperty.DEST_DB_UID;
            String destPassword = DIFYProperty.DEST_DB_PWD;

            Database destDatabase = Database.from(destUrl, destUsername, destPassword);

            //get poll frequency in ms (300000 = 5 min)
            int sleepTime = new Integer(DIFYProperty.ACK_POLL_FREQ);

            mp = new AckPoller();
            while (true) {
                try {
                    mp.poll(destDatabase);
                }
                catch(Exception p) {
                    int day = cal.get(Calendar.DAY_OF_WEEK);
                    if (p.getMessage().indexOf("Unable to establish database") > -1 &&
                            (day == Calendar.SATURDAY || day == Calendar.SUNDAY)) {
                        Thread.sleep(sleepTime);
                        continue;
                    }
                    else {
                        throw new Exception (p);
                    }
                }

                Thread.sleep(sleepTime);
            }
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            try {
                SendEmail.sendMessage(DIFYProperty.ADMIN_RECIPIENTS,
                        DIFYProperty.EMAIL_SENDER,
                        DIFYProperty.EMAIL_HOST,
                        DIFYProperty.SERVER +
                                " : Ack Polling Error at " + df.format(new Date()),
                        e.getMessage());
            }
            catch (Exception f) {
                System.out.println(f.getMessage());
            }
        }
        finally {

        }
    }

}
