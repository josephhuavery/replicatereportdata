package com.avery;

import picocli.CommandLine;

/**
 * Created by jhu on 7/11/2017.
 */
@CommandLine.Command (name="replicate",
        requiredOptionMarker = '*',
        description = "@|bold,underline,bg(red) replicate|@ orders and credit memos from one database to another",
        header = "********************************************************************************",
        footer = "********************************************************************************"
)
public class Config {
    @CommandLine.Option(names = {"-o","-order"}, split = ",", description = "Order number(s) to replicate/export")
    public String[] orders;
    @CommandLine.Option(names = { "-d", "-days" }, description = "How many days (default=1) to replicate?")
    public int offset = 0;
    @CommandLine.Option(names = { "-h", "-hour" }, description = "How many hours to replicate?")
    public int hours = 1;
    @CommandLine.Option(names = { "-e", "-env" } , description = "Which environment (default=1) to use? 0:window, 1:dev, 2:prod")
    public int environment = 1;
    @CommandLine.Option(names = { "-r", "-runtype" } , description = "Which process (default=0) to run? 0:import/export, 1:import only, 2:export only")
    public int runtype = 0;
    @CommandLine.Option(names = { "-s", "-send" } , description = "Do file send (default=1) to ILS/Glovia? 1:do send files, 0:do not send files")
    public int sending = 0;
    @CommandLine.Option(names = { "-help" } , help = true, description = "display this help and exit")
    public boolean help;
}
